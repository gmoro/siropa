# Title: _Siropa_
#   Algebraic and robotic functions for SIROPA

$define Manipulator record ('Equations','Constraints','PoseVariables','ArticularVariables','PassiveVariables','GeometricParameters','GenericEquations','GenericConstraints','PoseValues','ArticularValues','GeometricValues','Points','Loops','Chains','Actuators','Model')

# Class: AlgebraicTools Class
#   Provides the algebraic functions based on fgbrs library described in <AlgebraicTools>.
AlgebraicTools := module()

local SaturationTest,
      expandTrigonometric,
      clean:

export Elimination,
       Projection,
       TrigonometricSubs,
       TrigonometricAlgebraic,
       TrigonometricVariables,
       AlgebraicTrigonometric,
       TrigonometricTanHalf,
       Intersection,
       Division,
       NormalForm,
       InRadical,
       PolynomialSaturation,
       IteratedJacobian,
       PropernessDefect,
       FactorSystem,
       Hypersurfaces,
       Split,
       IsHullPrime,
       IsHullRadical,
       SaturationIdeal,
       RealSolve,
       RandomSection,
       EasyConstraintFormula,
       EasyRealFormula,
       SizeReduction:

option package:

$include<AlgebraicTools/algebraic_tools.mpl>

end module;


# Group: Siropa
#   Provides equation modeling, analysing and plotting functions for
#   different manipulators. Its functions are described in:
#   - <Modeling>   - functions to model manipulators
#   - <Analysing>  - functions to analyze singularities and more
#   - <Plotting>   - functions to plot
#   - <Mechanisms> - Manipulators database
Siropa := module()
local plotActuator,
      filterCurve;

export # modelling.mpl
       CreateManipulator,
       SubsParameters,
       UnassignParameters,

       # mechanisms.mpl
       Parallel_3RPR,
       Parallel_2PRR_PRP,
       Parallel_RPRRP,
       Parallel_2RR,
       Parallel_PRRP,
       Orthoglide,
       
       # analysing.mpl
       ConstraintEquations,
       Type1SingularityEquations,
       Type2SingularityEquations,
       CuspidalEquations,
       InfiniteEquations,
       Configurations,
       Projection,

       # plotting.mpl
       Plot2D,
       Plot3D,
       PlotWorkspace,
       PlotRobot2D;

option package;

$include<Siropa/modeling.mpl>
$include<Siropa/mechanisms.mpl>
$include<Siropa/analysing.mpl>
$include<Siropa/plotting.mpl>

end module;

savelib ('AlgebraicTools', 'Siropa', "siropa.mla");

