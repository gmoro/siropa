# Title: Home
#   Algebraic and robotic functions for SIROPA

#TODO: installation instruction; names Type1/Type2

# Group: Siropa
#   Provides modeling, analysing and plotting functions for
#   different manipulators. Its functions are described in:
#   - <Modeling>   - functions to model manipulators
#   - <Analysing>  - functions to analyze singularities and more
#   - <Plotting>   - functions to plot
#   - <Mechanisms> - Manipulators database
$include<Siropa/Siropa.mpl>


# Group: AlgebraicTools
#   Provides algebraic functions based on the fgbrs library.
#   The functions are in:
#   - <Base Algebra>   - functions to manipulate polynomial ideals
#   - <Extra Algebra>  - specialized functions to manipulate polynomial ideals
#   - <Trigonometric>  - functions to handle trigonometric expressions
$include<AlgebraicTools/AlgebraicTools.mpl>


savelib ('AlgebraicTools', 'Siropa', "siropa.mla");

