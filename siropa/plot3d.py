from numpy import *
from enthought.mayavi.mlab import *

phi, alpha3, rho1 = ogrid[-3.1:3.1:64j, -3.1:3.1:64j, 0:6:64j]
scalars = 27*cos(phi)**2 + 45/4 - 9*sin(phi) - 9*rho1**2\
          - 108*cos(alpha3)*cos(phi) + 81*cos(alpha3)**2
obj = contour3d(scalars, contours=[0], transparent=True)
