Format: Development Release 12-07-2007 (1.35 base)


Title: Siropa
SubTitle: A library for manipulator singularities analysis

# You can add a footer to your documentation like this:
# Footer: [text]
# If you want to add a copyright notice, this would be the place to do it.

# You can add a timestamp to your documentation like one of these:
# Timestamp: Generated on month day, year
# Timestamp: Updated mm/dd/yyyy
# Timestamp: Last updated mon day
#
#   m     - One or two digit month.  January is "1"
#   mm    - Always two digit month.  January is "01"
#   mon   - Short month word.  January is "Jan"
#   month - Long month word.  January is "January"
#   d     - One or two digit day.  1 is "1"
#   dd    - Always two digit day.  1 is "01"
#   day   - Day with letter extension.  1 is "1st"
#   yy    - Two digit year.  2006 is "06"
#   yyyy  - Four digit year.  2006 is "2006"
#   year  - Four digit year.  2006 is "2006"

# These are indexes you deleted, so Natural Docs will not add them again
# unless you remove them from this line.

Don't Index: Files, Classes


# --------------------------------------------------------------------------
# 
# Cut and paste the lines below to change the order in which your files
# appear on the menu.  Don't worry about adding or removing files, Natural
# Docs will take care of that.
# 
# You can further organize the menu by grouping the entries.  Add a
# "Group: [name] {" line to start a group, and add a "}" to end it.
# 
# You can add text and web links to the menu by adding "Text: [text]" and
# "Link: [name] ([URL])" lines, respectively.
# 
# The formatting and comments are auto-generated, so don't worry about
# neatness when editing the file.  Natural Docs will clean it up the next
# time it is run.  When working with groups, just deal with the braces and
# forget about the indentation and comments.
# 
# --------------------------------------------------------------------------


File: Home  (no auto-title, siropa.mpl)

Group: Siropa  {

   File: Analysing  (Siropa/analysing.mpl)
   File: Mechanisms  (Siropa/mechanisms.mpl)
   File: Modeling  (Siropa/modeling.mpl)
   File: Plotting  (Siropa/plotting.mpl)
   }  # Group: Siropa

Group: AlgebraicTools  {

   File: Base Algebra  (AlgebraicTools/base.mpl)
   File: Extra Algebra  (AlgebraicTools/extra.mpl)
   File: Trigonometric  (AlgebraicTools/trigonometric.mpl)
   }  # Group: AlgebraicTools

Group: Index  {

   Index: Everything
   Function Index: Functions
   }  # Group: Index

