# Title: Base Algebra

# Function: Projection
#   Project on variables or expressions in a polynomial system.
#
# Parameters:
#   sys    - list of polynomials with rational coefficients
#            and trigonometric functions: the system
#   vars1  - list of names or equation of the shape name=algebraic:
#            variables to project on.
#   vars2      (optional)  - list of names: variables to eliminate
#   char       (optional)  - integer: characteristic of the base field
#                            default value: 0
#   output=fmt (optional)  - *fmt* is the keyword *list* or *listlist*;
#                            if *list*, the output is a list of polynomials,
#                            otherwise, it is a list of list of polynomials.
#   incremental=b (optional) - *b* is a boolean: when true, the variables are
#                              eliminated one by one;
#                            default value: false
#   verbose    (optional)  - "verb"=true or "verb"=false: verbose support 
#                            default value: "verb"=false
#
# Returns:
#   With *output=list*, a list of polynomials: the generators of 
#   the elimination ideal. With *output=listlist*, a list of components of 
#   the elimination ideal (multiplicities may have been lost);
#   each component is represented by a list of polynomials.
#
# Example:
#   > > Projection ( [x+y^2,y-x], [z=x+y] );
#   > < [z^2+2*z]
Projection := proc ( sys   ::list(algebraic),
                     vars1 ::list({name,name=algebraic}),
                     vars2 ::list(name)  := [op(indets(sys,name)
                                                minus {op(vars1)}
                                                minus indets(sys,constant))],
                     char  ::integer     := 0,
                     { output ::identical(list,listlist) := 'list',
                       allvariables ::truefalse := false,
                       keeporder::truefalse := false,
                       nofactor::truefalse  := false } )
    option cache;
    local vars, newsys, Lsys, Id, J, upvars;
    
    vars := map( v->if type(v,name) then v else lhs(v) end if, vars1);
    upvars := vars2;
    newsys  := [ op(sys), seq (lhs(v)-rhs(v), v in select (type, vars1,
                                                           name=algebraic)) ];


#    if not keeporder then
#        vars := select (v->member(v,vars),
#                        [ Groebner:-SuggestVariableOrder (newsys) ]);
#        upvars:= [ Groebner:-SuggestVariableOrder (newsys,vars2) ];
#    end if;

    if indets(newsys,name) subset ({op(vars)} union indets(newsys,constant))
        then
        if output=listlist then
            return FactorSystem(newsys, ordering=[op(upvars),op(vars)]); 
        else 
            return newsys
        end if;
    end if;

    if allvariables and output='list' then
        return Elimination ( newsys, upvars, vars, char,
                            ':-allvariables'=true,':-keeporder'=keeporder,
                             _rest );
    end if;

    if nofactor then
        Lsys := [newsys];
    else    
        Lsys := FactorSystem (newsys, ordering=[op(upvars),op(vars)]); 
    end if;

    Lsys := map ( Elimination, Lsys, upvars, vars, char,
                 ':-allvariables'=allvariables,':-keeporder'=keeporder, _rest );
    Lsys := remove (x->x=[1], Lsys);
    if   member ([0], Lsys) then Lsys := [[0]]; 
    elif member (sys, Lsys) then Lsys := [sys]; end if;
    
    if output=listlist then return Lsys end if;

    Id := [1];
    for J in Lsys do Id := Intersection ( Id, J, vars, char, _rest ); end do;
    return Id;

end proc;

# Function: TrigonometricBasis
#   Computes a Gröbner basis of a trigonometric system.
#
# Parameters:
#   sys    - list of polynomials with rational coefficients
#            and trigonometric functions: the system
#   vars1  - list of names: variables to eliminate
#   vars2   (optional) - list of names: remaining variables
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
# Returns:
#   A list of polynomials: the reduced Gröbner basis where the 
#   trigonometric expressions are considered as algebraic variables.
#
# Example:
#   > > Elimination ( [x+y^2,y], [y], [x] );
#   > < [x]
TrigonometricBasis := proc( sys   ::list(algebraic),
                            vars1 ::list(name),
                            vars2 ::list(name)  := [op(indets(sys,name)
                                                       minus {op(vars1)})],
                            char  ::integer     := 0 )
    local algsys,  algvars, algvars1, algvars2, gb, opts;

    if not type(infolevel[Groebner], integer) then
        infolevel[Groebner] := 0;
    end if;

    opts := (`if`(infolevel[Groebner]>=2, "verb"=true, NULL),
              "matrix" = Matrix(1, 4, [[4,0,5000000,0]], datatype=integer[4]),
              'method'='direct');

    algsys, algvars := TrigonometricAlgebraic (sys, 'constants');
    algsys   := numer (normal (algsys));

    algvars1 := TrigonometricVariables (vars1, algvars); 
    algvars2 := TrigonometricVariables (vars2, algvars);
    algvars2 := [op(algvars2),
                 seq (algvars[v],
                      v in select(type,[indices(algvars,'nolist')],
                      constant))];
 
    gb := fgbrs:-fgb_gbasis (map(expand@primpart,algsys),
                                  algvars1, algvars2, char, opts, _rest)[..,3];

    return AlgebraicTrigonometric (gb, algvars, nosimplify, _rest);

end proc;

# Function: Elimination
#   Eliminates variables in a polynomial system.
#
# Parameters:
#   sys    - list of polynomials with rational coefficients
#            and trigonometric functions: the system
#   vars1  - list of names: variables to eliminate
#   vars2   (optional) - list of names: remaining variables
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   allvariables=b (optional) - *b* is a boolean: when true, the output
#                               is the full Groebner bases with vars2>>vars1
#                               default value: false
#   incremental=b (optional)  - *b* is a boolean: when true, the variables are
#                               eliminated one by one;
#                               default value: false
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the generators of the elimination ideal.
#
# Example:
#   > > Elimination ( [x+y^2,y], [y], [x] );
#   > < [x]
Elimination := proc ( sys   ::list(algebraic),
                      vars1 ::list(name),
                      vars2 ::list(name)  := [op(indets(sys,name)
                                                 minus {op(vars1)})],
                      char  ::integer     := 0,
                      { allvariables ::truefalse := false,
                        keeporder ::truefalse := false,
                        direct    ::truefalse := false,
                        incremental::truefalse := false,
                        generic   ::truefalse := false })
    option cache;
    local algsys, algrelations, algvars, algvars1, algvars2, elimsys, opts,
          minisys, rel, algconst, cvars, v1, v2, v;

    if not type(infolevel[Groebner], integer) then
        infolevel[Groebner] := 0;
    end if;
    algsys, algvars := TrigonometricAlgebraic (sys, 'const');
    algsys   := numer (normal (algsys));

    algvars1 := map (v->if member(v,[indices(algvars,'nolist')]) then 
                           algvars[v] else v end if, vars1);
    algvars2 := map (v->if member(v,[indices(algvars,'nolist')]) then 
                           algvars[v] else v end if, vars2);
    algconst := [seq (algvars[v],
                      v in select(type,[indices(algvars,'nolist')],
                      constant))];
 
    if not keeporder then
        algvars1 := [ Groebner:-SuggestVariableOrder (algsys,algvars1) ];
        algvars2 := select (v->member(v,algvars2),
                            [ Groebner:-SuggestVariableOrder (algsys) ]);
    end if;
    algvars2 :=  [op(algvars2), op(algconst)];

    opts := (`if`(infolevel[Groebner]>=2, "verb"=true, NULL),
             "matrix" = Matrix(1, 4, [[4,0,5000000,0]], datatype=integer[4]));
    if allvariables then
        elimsys := fgbrs:-fgb_gbasis (map(expand@primpart,algsys),
                                  algvars1, algvars2, char, opts, _rest)[..,3];
    else
        algsys, algvars1 := simplifyElim (algsys, algvars1,':-generic'=generic);
        if infolevel[Groebner]>=1 and nops(algsys)<nops(sys) then 
            printf ("-> linear simplifications detected\n",v):
        end if:
#experimental simplification
        rel, minisys := selectremove (p->indets(p) subset {op(algvars2)},
                                      algsys);
        if not direct and minisys<>algsys 
                      and (IsProjectionProper (minisys, algvars2)
                           or 
                           (nops(algconst) >= 1
                            and PropernessDefect (minisys, algvars2)=[])) then
            algsys := minisys:
        end if;
#end experimental simplification        
        
        if incremental then
            v1 := []:
            v2 := [op(algvars1), op(algvars2)]:
            elimsys := algsys:
            for v in algvars1 do
                if infolevel[Groebner]>=1 then 
                    printf ("-> eliminating %a\n",v):
                end if:
                v1 := [op(v1),v]:
                v2 := subs(v=NULL,v2):
                elimsys := select (p->indets(p) intersect {op(v1)} = {},
                                 fgbrs:-fgb_gbasis (map(expand@primpart,algsys),
                                     v1, v2, char,
                                     "elim force" = true, opts, _rest)[..,3] );
            end do
        else
            elimsys := select (p->indets(p) intersect {op(vars1)} = {},
                               fgbrs:-fgb_gbasis (map(expand@primpart,algsys),
                                 algvars1, algvars2, char,
                                 "elim force" = true, opts, _rest)[..,3] );
        end if;
    end if;

# testing interreduction
#    if nops(algconst)>=1 then
#        elimsys := remove (p->indets(p) subset {op(algconst)}, elimsys);
#        elimsys := Groebner:-InterReduce (elimsys, 
#                                          lexdeg(algvars1,
#                                                 subs(map(x->x=NULL,algconst),
#                                                      algvars2)));
#    end if;
# bug with lexdeg and algebraic numbers

# Size reduction
    if nops(algconst)>=1 and  not allvariables then
        cvars := table ([ seq (c=algvars[c],
                               c in select(type,[indices(algvars,nolist)],
                                                constant))]);
        elimsys := AlgebraicTrigonometric (elimsys, cvars, _rest);

        elimsys := Groebner:-InterReduce (elimsys, 
                                          tdeg(op(subs(map(x->x=NULL,algconst),
                                                       algvars2))));
    end if;
# possibly buggy

    elimsys := [op(rel),op(elimsys)];
    if elimsys = [] then elimsys := [0]; end if;
    elimsys := AlgebraicTrigonometric (elimsys, algvars, _rest);

    return elimsys;
end proc;

# local function to pre simplify a system before eliminating variables
simplifyElim := proc ( sys::list(polynom),
                       vars::list(name),
                       {generic::truefalse := false} )
    local x, pos, rel, nsys, nvars, fvars, d, p, q, mon, c, r;

    nvars := vars;
    nsys  := map(expand,sys);
    for x in vars do
        d := 0;
        for p in nsys while d<>1 do
            coeffs(p,x,'mon');
            d := foldl (gcd, d, seq (degree (m,x), m in mon));
        end do;
        if d>1 then nsys := map (expand,algsubs (x^d=x, nsys)) end if;
    end do;
    c := `if`(generic,polynom(constant,indets(sys) minus {op(vars)}),constant);
    q := 1;
    for x in ListTools:-Reverse (vars) do
        if member ([1,true], map (p-> [degree(p,x),
                                       `if`(degree(p,x)<>1,
                                            false,type(lcoeff(p,x),c))],
                                  nsys), 'pos') then
            ## simple substitution
            # rel := solve (nsys[pos],{x})[1];
            # nvars := subs (lhs(rel)=NULL,nvars);
            # nsys  := map(expand,remove (x->x=0, subs (rel, nsys)));
            ## substituting with resultants
            q := q*lcoeff(nsys[pos],x);
            nsys := remove (x->x=0, map (resultant, nsys, nsys[pos], x));
        end if;
    end do;

    if generic then nsys := map (polsat, nsys, q) end if;
    fvars := indets(nsys);
    return nsys, select (v->member(v,fvars),nvars);
end proc;

polsat := proc(p::polynom, q::polynom)
    local r,s;
    s := p;
    r := gcd(p,q);
    while r<>1 do
        divide(s,r,'s');
        r := gcd(s,q);
    end do;
    return s;
end proc;

# Function: Intersection
#   Intersects two ideals.
#
# Parameters:
#   sys1 - list of polynomials: generating the first ideal
#   sys2 - list of polynomials: generating the second ideal
#   vars    (optional) - list of names: the variables in *sys1* and *sys2*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the generators of the intersection ideal.
#
# Example:
#   > > Intersection ( [x,z], [y] );
#   > < [x*y,z*y]
Intersection := proc (sys1 ::list(algebraic),
                      sys2 ::list(algebraic),
                      vars ::list(name)     := [op (indets ([sys1,sys2],name))],
                      char ::integer        := 0)
    local T,p;
    if ormap (x->type(x,constant) and x<>0, sys1 ) then return sys2; end if;
    if ormap (x->type(x,constant) and x<>0, sys2 ) then return sys1; end if;
    if nops(sys1)=nops(sys2) and nops(sys1)=1 and
       type(sys1,list(polynom)) and type(sys2,list(polynom)) then
        p := primpart (lcm (sys1[1],sys2[1]));
        p := sign (Groebner:-LeadingCoefficient (p,tdeg(op(vars))))*p;
        return [p];
    end if;
    Elimination ( [op(map(s->T*s,sys1)), op(map(s->(1-T)*s,sys2))],
                  [T], vars, char, 'direct', _rest );
end proc;

# Function: Division
#   Division of an ideal by another.
#
# Parameters:
#   sys1 - list of polynomials: generating the first ideal
#   sys2 - list of polynomials: generating the second ideal
#   vars    (optional) - list of names: the variables in *sys1* and *sys2*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the generators of the division ideal.
#
# Example:
#   > > Division ( [x^3,z], [x] );
#   > < [x^2,z]
Division := proc (sys1 ::list(algebraic),
                  sys2 ::list(algebraic),
                  vars ::list(name)     := [op (indets ([sys1,sys2],name))],
                  char ::integer        := 0)
    local Id, p, s, f;

    Id := [1];
    
    for p in sys2 do
        if p<> 0 then
            s := Intersection (sys1,[p],vars,char,_rest);
	    if char=0 or not andmap(type,sys1,polynom) or
	                 not andmap(type,sys2,polynom) then
		f := (q,p)->simplify(q/p,'trig');
            else
                f :=proc(q) local r; Divide(q,p,r) mod char; return r; end proc;
            end if;
	    s := map ( q->f(q,p), s);
            Id := Intersection (Id, s, vars, char, _rest);
        end if;
    end do;

    return Id;
end proc;





# Function: NormalForm
#   Reduces polynomials w.r.t a Groebner basis.
#
# Parameters:
#   polys - list of polynomials: the polynomials to reduce
#   sys   - list of polynomials: a Groebner basis with
#                                lexdeg(*vars1*, *vars2*) ordering
#   vars1 - list of names: the first block of the grevlex ordering
#   vars2   (optional) - list of names: the second block of the grevlex ordering
#                        default value: [] 
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#
# Returns:
#   A list of polynomials: the normal forms of the polynomials in *polys*
#                          up to a scalar factor.
#
# Example:
#   > > NormalForm ( [x^2+1,x+y], [x^2,y], [x,y] );
#   > < [1,x]
NormalForm := proc (polys ::list(algebraic),
                    sys   ::list(algebraic),
                    vars1 ::list(name)     := NULL,
                    vars2 ::list(name)     := [],
                    char  ::integer        := 0)
    local algsys, algvars, trigvars1, trigvars2, algred, algpolys, gsys;

    
    algsys, algvars := TrigonometricAlgebraic ([op(sys),op(polys)]);
    algpolys := algsys[-nops(polys)..-1];
    algsys := algsys[1..-nops(polys)-1];

    if vars1=NULL then
       trigvars1 := [ Groebner:-SuggestVariableOrder (algsys) ];
       trigvars2 := [];
    else
        trigvars1 := map( v->if member (v,[indices(algvars,'nolist')]) then 
                             algvars[v] else v end if, vars1 );
        trigvars2 := map( v->if member (v,[indices(algvars,'nolist')]) then 
                             algvars[v] else v end if, vars2 );
    end if;

    gsys := Groebner:-Basis (algsys, lexdeg(trigvars1,trigvars2),
                             'characteristic'=char, 'method'='direct'):

    algred := Groebner:-Reduce (algpolys, gsys, lexdeg(trigvars1,trigvars2),
                               'characteristic'=char);
    return AlgebraicTrigonometric (algred, algvars, nosimplify);
end proc;

# Function: InRadical
#   Test the membership to the radical of an ideal.
#
# Parameters:
#   p   - polynomial with rational coefficients
#   sys - list of polynomials: the generators of the ideal
#   vars    (optional) - list of names: the variables in *sys* and *p*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A boolean: true if *p* is in the radical of *sys*, false otherwise.
#
# Example:
#   > > InRadical ( x, [x^2,y] );
#   > < true
InRadical := proc (p    ::algebraic,
                   sys  ::list(algebraic),
                   vars ::list(name)     := [op (indets ([sys,p],name))],
                   char ::integer        := 0)
    evalb (PolynomialSaturation (sys, p, vars, char, _rest)=[1]);
end proc;

# Function: Dimension
#   Computes the dimension of a system.
#
# Parameters:
#   sys - list of polynomials or trigonometric expressions: the system
#   vars    (optional) - list of names: the variables in *sys*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#
# Returns:
#   An integer: the dimension of the system.
#
# Example:
#   > > Dimension ( [x^2,cos(y)-0.5] );
#   > < 0
Dimension := proc (sys  ::list(algebraic),
                   vars ::list(name)     := [op (indets (sys,name))],
                   char ::integer        := 0)
    local algsys, algvars, trigvars, dim, trigconsts;
    algsys, algvars := TrigonometricAlgebraic (sys);
    trigvars := map( v->if member (v,[indices(algvars,'nolist')]) then 
                        algvars[v] else v end if, vars );
    trigconsts := TrigonometricConstants (algvars);
    dim := Groebner:-HilbertDimension (algsys, {op(trigvars),op(trigconsts)},
                                      'characteristic' = char );
    return dim;
end proc;

# Function: SaturationIdeal
#   Saturate an ideal by another ideal.
#
# Parameters:
#   sys1 - list of polynomials: the generators of the saturated ideal
#   sys2 - list of polynomials: the generators of the saturating ideal
#   vars    (optional) - list of names: the variables in *sys1* and *sys2*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose         (optional) - "verb"=true or "verb"=false: verbose support 
#                                default value: "verb"=false
#   alternative = b (optional) - *b* is a boolean; if true a different algorithm
#                                is used for the saturation
#                                default value: false
#
# Returns:
#   A list of polynomials: the generators of *sys1* saturated by *sys2*.
#
# Example:
#   > > SaturationIdeal ( [x*y*z], [z*x,z*y] )
#   > < [x*y]
SaturationIdeal := proc (sys1 ::list(algebraic),
                         sys2 ::list(algebraic),
                         vars ::list(name) := [op (indets ([sys1,sys2],name))],
                         char ::integer    := 0,
                         {alternative::truefalse :=false})
    if alternative then return Saturation2 (args)
    else                return SaturationTest (args) end if;
end proc;

# Function: SaturationTest
#   Saturate an ideal by another ideal (new method).
#
# Parameters:
#   sys1 - list of polynomials: the generators of the saturated ideal
#   sys2 - list of polynomials: the generators of the saturating ideal
#   vars    (optional) - list of names: the variables in *sys1* and *sys2*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the generators of *sys1* saturated by *sys2*.
#
# Example:
#   > > SaturationTest ( [x*y*z], [z*x,z*y] )
#   > < [x*y]
#
# Algorithm:
#   1 - Initialize *sat*, *satsys2* and *cover*
#   2 - While the *satsys2* is not contained in the radical of *sys1* do
#   3 - - Take *p* in *satsys2*
#   4 - - Compute *partsat*, the saturation of *sys1* by *p*
#   5 - - If *p* is not in the radical of *sys1* then
#   6 - - - Update *sat* with the intersection *sat* with *partsat*
#   7 - - - Update *satsys2* with the intersection *satsys2* with *partsat*
#   8 - - Else
#   9 - - - Take a new polynomial *p* in *satsys2* and go back to 4.
#  10 - Returns *sat*
SaturationTest := proc (sys1 ::list(algebraic),
                        sys2 ::list(algebraic),
                        vars ::list(name) := [op (indets ([sys1,sys2],name))],
                        char ::integer    := 0)
    local sat, satsys2, partsat, cover, p;
#   1 - Initialize *sat*, *satsys2* and *cover*
    sat := [1];
    satsys2 := sort(sys2, (p,q)->degree(p)<=degree(q));
    cover := false;

#   2 - While the *satsys2* is not contained in the radical of *sys1* do
    while not cover do
        cover := true;
#   3 - - Take *p* in *satsys2*
        for p in satsys2 while cover do
#   4 - - Compute *partsat*, the saturation of *sys1* by *p*
            partsat := PolynomialSaturation (sys1, p, vars, char, nosimplify,
                                             _rest);
#   5 - - If *p* is not in the radical of *sys1* then
            if partsat <> [1] then
#   6 - - - Update *sat* with the intersection *sat* with *partsat*
                sat := Intersection (sat, partsat, vars, char, nosimplify,
                                     _rest);
#   7 - - - Update *satsys2* with the intersection *satsys2* with *partsat*
                satsys2 := Intersection (satsys2, partsat, vars, char,
                                         nosimplify, _rest);
                satsys2 := select (`<>`,NormalForm (satsys2,sys1,vars,char),0);
                cover := false;
            end if;
#   8 - - Else
#   9 - - - Take a new polynomial *p* in *satsys2* and go back to 4.
        end do;
#  10 - Returns *sat*
    end do;

    return sat;

end proc;

# Function: Saturation5
#   Saturate an ideal by another ideal (new method).
#
# Parameters:
#   sys1 - list of polynomials: the generators of the saturated ideal
#   sys2 - list of polynomials: the generators of the saturating ideal
#   vars    (optional) - list of names: the variables in *sys1* and *sys2*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the generators of *sys1* saturated by *sys2*.
#
# Example:
#   > > SaturationTest2 ( [x*y*z], [z*x,z*y] )
#   > < [x*y]
Saturation5 := proc (sys1   ::list(algebraic),
                         sys2   ::list(algebraic),
                         vars   ::list(name) := [op (indets ([sys1,sys2],name))],
                         char   ::integer    := 0,
                         outpol ::algebraic  := 1  )
    local sat, sortsys2, partsat, cover, p;

    sortsys2 := sort(sys2, (p,q)->degree(p)<=degree(q));
    sortsys2 := NormalForm (map(p->p*outpol,sortsys2),sys1,vars,char);
    sortsys2 := remove (`=`, sortsys2, 0);

    if sortsys2=[] then return [1]; end if;
    p := sortsys2[1];

    sat := PolynomialSaturation (sys1, p, vars, char, nosimplify, _rest);

    for p in sortsys2[2..] do
        partsat := Saturation5 ( sys1, sat, vars, char, p);
        if partsat <> [1] then
            sat := Intersection (sat, partsat, vars, char, nosimplify, _rest);
        end if;
    end do;

    return sat;
end proc;


# Function: SaturationDirect
#   Saturate an ideal by another ideal (direct method).
#
# Parameters:
#   sys1 - list of polynomials: the generators of the saturated ideal
#   sys2 - list of polynomials: the generators of the saturating ideal
#   vars    (optional) - list of names: the variables in *sys1* and *sys2*
#   char    (optional) - integer: characteristic of the base field
#                        default value: 0
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the generators of *sys1* saturated by *sys2*.
#
# Example:
#   > > SaturationDirect ( [x*y*z], [z*x,z*y] )
#   > < [x*y]
SaturationDirect := proc (sys1 ::list(algebraic),
                          sys2 ::list(algebraic),
                          vars ::list(name) := [op (indets ([sys1,sys2],name))],
                          char ::integer    := 0)
    local a;
#TODO

end proc;

# Group: Singularity Manipulation

# Function: IteratedJacobian
#   Computes the nth iterated jacobian.
#
# Parameters:
#   sys - list of polynomials: the initial system
#   vars (optional)           - list of names: the variables;
#                               default values: the variables appearing in *sys*
#   n    (optional)           - integer: the number of iterations;
#                               default value: 1
#   rank = r (optional)       - r is an integer: the codimension of the system;
#                               default value: minimum of number of polynomials
#                               in *sys* and number of variables
#   maxminors = m  (optional) - m is an integer: a bound on the number of minors
#                               computed at each iteration;
#                               default value: infinity
#
# Returns:
#   A list of polynomials generating the nth element of the sequence (J_n) s.t.:
#   - J_0     = *sys*
#   - J_{n+1} = *r* x *r* minors of the Jacobian matrix
#               of *J_n* w.r.t the variables *vars*
#
# Example:
# > > IteratedJacobian ([x^3,y^3], [x,y], 2);
# >                        3   3     2  2      4         4
# >                      [x , y , 9 x  y , 54 x  y, -54 y  x]
IteratedJacobian := proc (sys  ::list(algebraic),
                          vars ::list(name)    := [op(indets(sys,name))],
                          n    ::integer       := 1,
                          { rank::integer      := min (nops(sys),nops(vars)),
                            maxminors::integer := infinity})
    local M, equ_minors, var_minors, m, v, pre, sing, L, k, mm, em;
    option cache;

    if n=-1 then return [] end if;
    if n=0 then return sys end if;
    
    pre := IteratedJacobian (sys, vars, n-2, maxminors);
    sing := IteratedJacobian (sys, vars, n-1, maxminors);
 
    M := VectorCalculus:-Jacobian (sing, vars);
    equ_minors := combinat:-choose (nops(sing), rank);
    equ_minors := subs (map(a->a=NULL, combinat:-choose (nops(pre),
                                                         rank       )), 
                        equ_minors);
    var_minors := combinat:-choose (nops(vars), rank);

    mm := iquo(maxminors,nops(var_minors));
    L := [ 0 $ mm ];
    k := 0;
    while L = [ 0 $ mm ] and k < nops(equ_minors) do
        em := equ_minors[k+1 .. min(nops(equ_minors),k+mm)];
        L := [ seq( seq( LinearAlgebra:-Determinant 
                             ( LinearAlgebra:-SubMatrix ( M, m, v ) ),
                         m in em ),
                    v in var_minors ) ];
        k := k+mm;
    end do;
    L := remove (`=`,L,0);

    #In the recursion process, the code assumes that sing is at the beginning
    #of the list.
    return [ op(sing), op(L) ];
end proc;

# local function to save results of previous isolations
isole := proc ()
    option cache;
    RootFinding:-Isolate (args);
end proc;

# Function: RealSolve
#   Solve polynomial and trigonometric systems.
#
# Parameters:
#   sys - list of algebraic expressions: the system to solve
#   vars (optional) - list of variables: the variables of the system
#   spec (optional) - sequence of element of the form *name=constant*:
#                     allow to specify variables in *sys*; in this case,
#                     some intermediate values are remembered to optimize
#                     other resolutions with different values.
#
# Returns:
#   A sequence of lists of the form *v=n* where *v* is a name and *n* is
#   a numerical constant.
RealSolve := proc ( sys  ::{algebraic,list({algebraic,algebraic<algebraic})},
                    vars ::list(name)         := [op(indets([sys,spec],name))],
                    spec ::seq(name=algebraic) := NULL,
                    { noerror::truefalse := false,
                      precision::integer := Digits })
    option cache;
    local eq, ineq, algsys, algvars, ltvars, sols;

    if type(sys,algebraic) then eq, ineq := [sys], [];
                           else eq, ineq := selectremove (type, sys, algebraic);
    end if;

    eq   := Projection (eq, [op(indets(ineq,name) union {op(vars)}
                                                  union indets({spec},name) )]);
    eq   := TrigonometricSubs (spec, eq, precision);
    eq   := simplify(eq);
    ineq := TrigonometricSubs (spec, ineq, precision);

#    algsys, algvars := TrigonometricTanHalf (eq);
#    algsys := map( op@Split, algsys);
    algsys, algvars := TrigonometricAlgebraic (eq, 'const');
    algsys := AlgebraicTools:-FactorSystem (algsys);
    ltvars := map(`[]`@op@indets, algsys);

    sols := zip (proc(x,y) try op(isole(x,y,'digits'=precision))
                    catch : 
                      if noerror then print('Warning',op([lastexception][2..]));
                                 else error(lastexception[2..]) end if;
                        end; end proc, algsys, ltvars);
#    sols := map2 ( map, lhs-rhs, sols);
    sols := map (AlgebraicTrigonometric, sols, algvars);
    sols := map (miniSolve, sols, precision);
    sols := map (expandTrigonometric, sols);
    sols := map (`union`, sols, {spec});
    sols := select ( x->andmap(y->y,evalf(subs(x,ineq))), sols );
    sols := map (s->zip(`=`, vars, subs(s,vars)), sols);

    return op (map2 (select, s->member(lhs(s),{op(vars)}), sols));
end proc;


miniSolve := proc ( spec ::list(anything=constant),
                    precision ::integer )
    local vt, tspec, cosivt, const, ignore;

    vt := select (v->evalb(nops({cos(v),sin(v)} intersect 
                                                map(lhs,{op(spec)}))=2),
                  indets (spec,name) );
    cosivt := map (v->(cos(v),sin(v)), vt);
    const  := map (x->if type(lhs(x),constant) then lhs(x) else NULL end if,
                   spec);
    ignore := [op(cosivt),op(const)];
    if ormap (x->type(lhs(x),`^`) and rhs(x)<0, spec) then return NULL end if;
    tspec := { seq ( v = (evalf[precision]@subs) (spec, arctan (sin(v), cos(v) )),
                     v in vt ) };

    tspec := remove (s->lhs(s) in ignore, {op(spec)}) union tspec;

    return solve ([op(tspec)], AllSolutions=true);
end proc;



