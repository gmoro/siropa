# Title:
# Class: AlgebraicTools
#   Provides the algebraic functions based on fgbrs library described in <AlgebraicTools>.
AlgebraicTools := module()

$define BIGPRIME 65521
$define BIGINTEGER 100


local SaturationTest,
      SaturationDirect,
      simplifyElim,
      normalAngle,
      expandTrigonometric,
      miniSolve,
      splitDouble,
      isole,
      prime,
      size,
      polyCompare,
      polsat,
      clean:

export # base.mpl
       TrigonometricBasis,
       Elimination,
       Projection,
       Intersection,
       Division,
       NormalForm,
       PolynomialSaturation,
       IteratedJacobian,
       SaturationIdeal,
       Saturation5,
       RealSolve,

       # extra.mpl
       InRadical,
       Dimension,
       IsProjectionProper,
       PropernessDefect,
       FactorSystem,
       Hypersurfaces,
       Split,
       IsHullPrime,
       IsHullRadical,
       RandomSection,
       EasyConstraintFormula,
       EasyRealFormula,
       EquidimRadicalDiscriminantVariety,
       SizeReduction,
       SortPolynomials,
       Saturation1,
       Saturation2,
       Saturation3,
       Saturation4,
       ToLaTeX,
       ToLatex,

       # trigonometric.mpl
       TrigonometricGcd,
       TrigonometricSubs,
       TrigonometricAlgebraic,
       TrigonometricVariables,
       TrigonometricConstants,
       AlgebraicVariables,
       AlgebraicTrigonometric,
       TrigonometricAlgebraicCosSin,
       TrigonometricTanHalf:

option package:

$include<AlgebraicTools/base.mpl>
$include<AlgebraicTools/extra.mpl>
$include<AlgebraicTools/trigonometric.mpl>

end module;

