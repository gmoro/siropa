# File: Trigonometric

# Function: TrigonometricSubs
#   Substitute trigonometric variables with numerical values
#
# Parameters:
#   spec - sequence of equalities *var=value* where *var* is a name and
#          *value* a numerical value.
#   formula - equation, inequation, polynomial or list of those,
#             with trigonometric and algebraic expressions.
#   precision (optional) - integer: the number of digits that must be kept
#                          when modifying the values in *spec*.
#
# Returns:
#   The input *formula* where the name in *spec* have been replaced by
#   the corresponding values; the algebraic relations satissfied by cos and sin
#   are still valid with the chosen numerical values.
#
# Exemple
# begin(code)
# > TrigonometricSubs (a=3, cos(a)^2+sin(a)^2);
#                                        1
# end(code)
TrigonometricSubs := proc (spec ::seq(name=algebraic),
                           sys  ::{algebraic, algebraic<algebraic,
                                   list({algebraic,algebraic=algebraic,
                                         algebraic<algebraic})},
                           precision ::integer := 4,
                           { noradical::truefalse := false } )
    local trigvars, trigspec, varspec, fspec, tfspec, rspec, equ, equspec, u,
          lsys, sysspec, tanspec, formula, trad, vrad;
    
    if not type(sys,list) then lsys := [sys] else lsys := sys; end if;
    
    #For rounding bug
    map (convert, [spec], float, 2*precision);
    
    #To fill the number of digits
    u := SFloat (10^precision, -precision);

    trigvars := indets(indets(sys,'trig'),'name');
    trigspec, varspec := selectremove (s->member(lhs(s), trigvars)
                                          and type(rhs(s),constant), [spec]);
    varspec, formula := selectremove (s->type(rhs(s),constant), varspec);

    if not noradical then
        trad, trigspec := selectremove (x->type(rhs(x)/Pi,integer) 
                                           or type(convert (tan(rhs(x)/2),
                                                           ':-radical'),
                                                radnum), trigspec );
        vrad, varspec  := selectremove (x->type(convert (rhs(x),':-radical'),
                                                radnum), varspec );

        formula := [op(formula),op(vrad),op(trad)];
    end if;

    trigspec := map (s->if cos(rhs(s))=-1 then lhs(s)=infinity
                                          else lhs(s)=tan (rhs(s)/2) end if,
                     trigspec);
    tanspec, trigspec := selectremove (s->has(lsys,tan(lhs(s)/2)), trigspec);
    
    #double precision first for rounding bug
    tfspec := [ op( map (s->lhs(s)=convert(rhs(s)*u, float, 2*precision),
                         [op(varspec),op(tanspec)]) ),
                op( map (convert, trigspec, float, precision)) ];

    tfspec := [ op( map (s->lhs(s)=convert(rhs(s)*u, float, precision),
                         [op(varspec),op(tanspec)]) ),
                op( map (convert, trigspec, float, iquo (precision, 2))) ];
    rspec := map (convert, tfspec, rational, exact);

    rspec := zip ( (v,t)->if member (v,map(lhs,[op(trigspec),op(tanspec)])) then
                               (cos(v)=`if`(t=infinity,-1,(1-t^2)/(1+t^2)),
                                sin(v)=`if`(t=infinity, 0, 2*t/(1+t^2)),
                                tan(v)=`if`(t=infinity, 0, 
                                       `if`(t=-1 or t=1,infinity, 2*t/(1-t^2))),
                                tan(v/2)=t                                  )
                          else v=t end if, 
                   map(lhs,rspec), map(rhs, rspec) );

    sysspec := # simplify (
               expand (subs (op(formula), op(rspec), expand(lsys)))
               # , 'trig')
               ;
    sysspec := convert (sysspec, ':-radical');

    if type (sys, list) then return sysspec else return op(sysspec) end if;

end proc;

# Function: TrigonometricAlgebraic
#   Convert trigonometric and radical expressions to algebraic ones
#
# Parameters:
#   sys - list of polynomials: the polynomial to convert
#   norelation = b (optional) - *b* is a boolean: if false then trigonometric 
#                               relations are added to the output system;
#                               default value: false
#   cossin = l     (optional) - *l* is a list of name: the variables to change
#                               into cosine and sine of the angle;
#                               default value: list of all the variables
#   tanhalf = l    (optional) - *l* is a list of name: the variables to change
#                               into tan of half of the angle;
#                               default value: []
#
# Returns:
#   A sequence of 2 elements :
#   - a list of polynomials: the new system with algebraic variables and
#     new relations added at the beginning of the system
#   - a table that has for indices the variables appearing in cos, sin and tan,
#     and for entries the sequences of the corresponding algebraic variables.
#
# Example:
# (begin code)
# > sys, vars := TrigonometricAlgebraic ( [cos(x)-cos(y),cos(y)-z^3] );
#                                                    3
#                 sys, vars := [cosx - cosy, cosy - z ], algvars
# > vars[y];
#                                      cosy
# (end code)
TrigonometricAlgebraic := proc( sys ::list(algebraic),
                                 {norelation ::truefalse := false,
                                  cossin ::{set,list}(name) := indets(sys,name),
                                  tanhalf ::{set,list}(name) := [],
                                  const ::truefalse := false} )
    local vcossin, vtanhalf, algsys, vartable, cstable, thtable,
          expsys, cfun, cons, subconst, subfun;
    vcossin  := {op(cossin)} minus {op(tanhalf)};
    vcossin  := [op(vcossin)];
    vtanhalf := [op(tanhalf)];

    expsys := expand (sys);

    if not const then
        cfun  := indets (expsys,And(constant,function));
        cons := indets (cfun,And(constant,Not(function)));
    
        subconst := map (c->cat(':-c',convert(c,name))=c, cons);
        subfun   := map (f -> f=eval (subs ([cos=(c->cos(cat(':-c',
                                                             convert(c,name)))),
                                             sin=(c->sin(cat(':-c',
                                                             convert(c,name)))),
                                             tan=(c->tan(cat(':-c',
                                                             convert(c,name))))
                                            ], f)),
                         cfun); 
        
    
        expsys := TrigonometricSubs (op(subconst), subs (subfun, expsys));
    end if;

    algsys, cstable := TrigonometricAlgebraicCosSin (expsys, 
                                                     ':-relations'=relations,
                                                     variables = vcossin,
                                                     ':-const'=const);
    algsys, thtable := TrigonometricTanHalf (algsys, ':-generic',
                                                  variables = vtanhalf,
                                                  ':-const'=const);
    return algsys, table (map(op@op@op,[cstable,thtable]));
end proc;

# Function: TrigonometricAlgebraicCosSin
#   Convert trigonometric and radical expressions to algebraic ones
#
# Parameters:
#   sys - list of polynomials: the polynomial to convert
#   norelation = b (optional) - *b* is a boolean: if false then trigonometric 
#                               relations are added to the output system;
#                               default value: false
#   variables = l  (optional) - *l* is a list of name: the variables to change;
#                               default value: all the variables
#
# Returns:
#   A sequence of 2 elements :
#   - a list of polynomials: the new system with algebraic variables and
#     new relations added at the beginning of the system
#   - a table that has for indices the variables appearing in cos, sin and tan,
#     and for entries the sequences of the corresponding algebraic variables.
#
# Example:
# (begin code)
# > sys, vars := TrigonometricAlgebraic ( [cos(x)-cos(y),cos(y)-z^3] );
#                                                    3
#                 sys, vars := [cosx - cosy, cosy - z ], algvars
# > vars[y];
#                                      cosy
# (end code)
TrigonometricAlgebraicCosSin := proc ( sys ::list(algebraic),
                                 {norelation ::truefalse := false,
                                  variables ::{set,list}(name) := NULL,
                                  const ::truefalse := false} )
    local expsys, trigtype, trigfun, trigvars, trignames, trigrelations, v,
          algvars, algsys, algrelations, tanhalffun, tanhalfvars, tanhalftype,
          transname, transsin, transcos, transtan, vars, testvars, testfun,
          testtype;

    #Initialization
    expsys := expand (sys);
    if variables=NULL then vars := indets(expsys,name)
                      else vars := {op(variables)} end if;

    # Check expressions are of type tan(x/2)
    transname := v->cat('tanHalf_',convert(v,name));
    transcos := x-> (1-x^2)/(1+x^2);
    transsin := x-> 2*x/(1+x^2);
    transtan := x-> 2*x/(1-x^2);

    tanhalftype := satisfies (x->nops(indets(x,name))=1 and
                                 subs(indets(x,name)[1]=2*indets(x,name)[1], x)
                                 =tan(indets(x,name)[1]) and
                                 member(indets(x,name)[1],vars));
    tanhalffun  := select (type, indets (expsys), tanhalftype);
    tanhalfvars := indets (tanhalffun, name);

    expsys := subs (seq (op([tan(v/2)=transname(v),
                             cos(v)=(transcos@transname)(v),
                             sin(v)=(transsin@transname)(v),
                             tan(v)=(transtan@transname)(v)]), v=tanhalfvars),
                    expsys);
    expsys := normal(expsys);

    if tanhalfvars intersect indets(expsys,name) <> {} then
        error "These variables are not trigonometric only: %1",
              tanhalfvars intersect indets(expsys,name);
    end if;


    # Check expressions are of type cos(x),sin(x),tan(x) or a power
    trigtype := And ( Or(function(name),`if`(const,constant,NULL),`^`),
                         'satisfies'(x->((member (op(0,x),{cos,sin,tan,sqrt})
                                          or
                                          (op(0,x)=`^` and op(2,x)=1/2)) and
                                     ((type(x,constant) and const) or 
                                      type(x,sqrt) or
                                      (not type(x,constant) and
                                       member (indets(x,name)[1],vars))))));

    if not andmap (type, select(has,indets(expsys),vars),
                         Or (name,trigtype)) then
        error "These expressions are not trigonometric: %1",
              remove (type, indets(expsys), Or(name,trigtype));
    end if;

    trigfun  := indets (expsys,trigtype);
#    trigvars := indets (trigfun, name);
    trigvars := map2 (op, 1, trigfun);

    testtype := And ( Or(function(name),`if`(const,constant,NULL),'sqrt'),
                     'satisfies'(x->(member (op(0,x),{cos,sin,tan,`^`,sqrt}) and
                                     ((type(x,constant) and const) or 
                                      type(x,sqrt) or
                                      (not type(x,constant) and
                                       member (indets(x,name)[1],vars))))));
    testfun  := indets (expsys,testtype);
#    trigvars := indets (trigfun, name);
    testvars := map2 (op, 1, testfun);

    if testvars intersect indets(eval (expsys, {cos=curry(cat,cos),
                                                sin=curry(cat,sin),
                                                tan=curry(cat,tan)}),
                                 name) <> {} then
        error "These variables are not trigonometric only: %1",
              trigvars intersect indets(expsys,name);
    end if;

    trignames := table (map (e->if op(0,e)=`^` then 
                                    e=cat ('sqrt_', convert(op(1,e),name))
                                else
                                    e=cat (op(0,e),'_', convert(op(1,e),name))
                                end if,
                             trigfun));

    # Construction of the output table
    algvars := table();
    for v in trigvars do
        algvars[v] := seq ( trignames[e], e=select (has,trigfun,v) );
        if StringTools:-Split(convert([algvars[v]][1],string), '_')[1]="sqrt"
            and type(v,name) then
            expsys := subs ( v^(1/2)=cat('sqrt_',convert(v,name)), expsys);
            expsys := subs ( v=cat('sqrt_',convert(v,name))^2, expsys);
        end if;
    end do;

    for v in tanhalfvars do
        algvars[v] := cat('tanHalf_',convert(v,name));
    end do;
    
    # Construction of the output system
    if not norelation then
        trigrelations := table([{}=(x->[]),
                                {cos}=(x->[]), {sin}=(x->[]), {tan}=(x->[]),
                                {cos,sin}=(x->[cos(x)^2+sin(x)^2-1]),
                                {cos,tan}=(x->[cos(x)^2*(tan(x)^2+1)-1]),
                                {sin,tan}=(x->[sin(x)^2*(tan(x)^2+1)-tan(x)^2]),
                                {cos,sin,tan}=(x->[cos(x)^2+sin(x)^2-1,
                                                   cos(x)*tan(x)-sin(x)]),
                                {sqrt}=(x->[]),  #[sqrt(x)^2-x]),
                                {`^`}=(x->if type(x,constant) then
                                            [cat('sqrt_',convert(x,name))^2-x]
                                          else [] end if) ] );
        algrelations := map (v->op( trigrelations[map2(op,0,
                                                   select(has,trigfun,v))](v) ),
                             [op(trigvars)]);
    else algrelations := [] end if;

    algsys := map(expand,subs (zip (`=`, [indices(trignames,':-nolist')],
                              [entries(trignames,':-nolist')] ),
                         [op(algrelations),op(expsys)]));
    
    return convert(algsys,rational,exact), algvars;

end proc;

# Function: TrigonometricVariables
#   Updates trigonometric variables to algebraic ones.
#
# Parameters:
#   vars    - list of name: the variables to convert.
#   algvars - table returned by the *TrigonometricAlgebraic* or
#             *TrigonometricTanHalf* function.
#
# Returns:
#   A list of names: the variables updated with new algebraic variables
#   corresponding to the trigonometric functions.
TrigonometricVariables := proc ( vars    ::list(name),
                                 algvars ::table      )
    return map( v->if member (v,[indices(algvars,'nolist')]) then 
                              algvars[v] else v end if, vars );
end proc;

# Function: TrigonometricConstants
#   Returns the algebraic variables of trigonometric constants.
#
# Parameters:
#   algvars - table returned by the *TrigonometricAlgebraic* or
#             *TrigonometricTanHalf* function.
#
# Returns:
#   A list of names: the algebraic variables
#   corresponding to the trigonometric constants.
TrigonometricConstants := proc ( algvars ::table      )
    local ind;
    ind := remove (type, [indices(algvars,'nolist')], name);
    return map( v-> algvars[v], ind );
end proc;

# Function: AlgebraicVariables
#    Convert algebraic variables to trigonometric ones.
#
# Parameters:
#   vars    - list of name: the variables to convert.
#   algvars - table returned by the *TrigonometricAlgebraic* or
#             *TrigonometricTanHalf* function.
#
# Returns:
#   A list of names: the original trigonometric variables
#   corresponding to the algebraic variables.
AlgebraicVariables := proc ( vars    ::list(name),
                                 algvars ::table      )
    local l,p;
    l := [indices(algvars,'nolist')];
    return map( v->if membertype ('satisfies'(l->member(v,l)),
                                  [entries(algvars)],
                                  'p') then 
                              l[p] else v end if, vars );
end proc;

# Function: AlgebraicTrigonometric
#   Convert algebraic systems back to trigonometric ones.
#
# Parameters:
#   algsys  - list of polynomials: the polynomial to convert
#   algvars - table returned by *TrigonometricAlgebraic*
#                            or *TrigonometricTanHalf*.
#   nosimplify=b (optional) - *b* is a boolean: if true, the trigonometric
#                             expression are not simplified;
#                             default value: false.
#
# Returns:
#   A list of algebraic expressions: the system back with
#   trigonometric functions.
#
# Example:
# (begin code)
# > sys,vars:=TrigonometricAlgebraic ( [cos(x)-cos(y),cos(y)-z^3] );
#                                                    3
#                 sys, vars := [cosx - cosy, cosy - z ], algvars
# 
# > AlgebraicTrigonometric (sys,vars);
#                                                     3
#                         [cos(x) - cos(y), cos(y) - z ]
# 
# (end code)
AlgebraicTrigonometric := proc ( algsys  ::list(anything),
                                 algvars ::table,
                                 {nosimplify ::truefalse     := false} )
    local trigvars, trigsys, trigfun;

    trigfun := table (["cos"='cos', "sin"='sin', "tan"='tan',
                       "tanHalf"=(x->tan(x/2)), "sqrt"='sqrt']);

    trigvars := map (v->seq (s=trigfun[StringTools:-Split(convert(s,string),
                                                                  '_')[1]] (v),
                             s in [algvars[v]]),
                     [indices (algvars,'nolist')]);
    trigsys := subs (trigvars, algsys);

    if not nosimplify then
        return remove (x->x=0, simplify (trigsys,'trig'));
    else
        return trigsys;
    end if;
end proc;


# Function: TrigonometricTanHalf
#   Parametrize trigonometric and radical expressions with tangent expression
#
# Parameters:
#   sys - list of polynomials: the polynomial to convert
#   generic = b (optional) - *b* is a boolean: if true, the angles are supposed
#                            different from Pi;
#                            default value: false.
#   variables = l  (optional) - *l* is a list of name: the variables to change;
#                               default value: all the variables
#
# Returns:
#   a sequence of 2 elements:
#   - a list of list of polynomial fractions: the union of several systems
#     such that the union of there zeroes are the zeroes of the input system;
#     if generic is true, then only a list of fractions is returned.
#   - a table that has for indices the variables appearing in cos, sin and tan,
#     and for entries the sequences of the corresponding algebraic variable.
#   - new relations for the radical computations are added at the beginning
#     of the system.
TrigonometricTanHalf := proc( sys ::list(algebraic),
                              {generic::truefalse := false,
                               variables::{list,set}(name) := NULL,
                               const::truefalse := false})
    local expsys, trigtype, trigfun, trigvars, algvars, v, algsys, transname,
          transcos, transsin, transtan, L, T, subsmap, extra, transnamecos,
          transnamesin, tanhalftype, tanhalfvars, tanhalffun, vars,
          sqrttype, sqrtvars, sqrtfun, transnamesqrt, s;

    expsys := expand (sys);
    
    if variables=NULL then vars := indets(expsys,name)
                      else vars := {op(variables)} end if;

    # Check expressions are of type tan(x/2)
    transname := v->cat('tanHalf_',convert(v,name));
    transcos := x-> (1-x^2)/(1+x^2);
    transsin := x-> 2*x/(1+x^2);
    transtan := x-> 2*x/(1-x^2);

    tanhalftype := satisfies (x->nops(indets(x,name))=1 and
                                 subs(indets(x,name)[1]=2*indets(x,name)[1], x)
                                 =tan(indets(x,name)[1]) and
                                 member(indets(x,name)[1],vars));
    tanhalffun  := select (type, indets (expsys), tanhalftype);
    tanhalfvars := indets (tanhalffun, name);

    expsys := subs (seq (op([tan(v/2)=transname(v),
                             cos(v)=(transcos@transname)(v),
                             sin(v)=(transsin@transname)(v),
                             tan(v)=(transtan@transname)(v)]), v=tanhalfvars),
                    expsys);
    expsys := normal(expsys);

    if tanhalfvars intersect indets(expsys,name) <> {} then
        error "These variables are not trigonometric only: %1",
              tanhalfvars intersect indets(expsys,name);
    end if;
    
    # Check expressions are of type cos(x),sin(x),tan(x)
    trigtype := And ( Or(function(name),`if`(const,constant,NULL)),
                     'satisfies'(x->member (op(0,x),{cos,sin,tan}) and
                                    (type(x,constant) or 
                                     member (indets(x)[1],vars))));

    sqrttype := And ( Or(function(name),`if`(const,constant,NULL),`^`),
                      'satisfies'(x->(op(0,x)=sqrt or
                                        (op(0,x)=`^` and op(2,x)=1/2))
                                      and
                                      ((type(x,constant) and const) or 
                                       (not type(x,constant) and 
                                        member (indets(x,name)[1],vars)))) );

    if not andmap (type, select(has,indets(expsys),vars),
                         Or (name,trigtype,sqrttype)) then
        error "These expressions are not trigonometric: %1",
              remove (type, indets(expsys), Or(name,trigtype));
    end if;

    trigfun  := indets (expsys,trigtype);
    trigvars := map2 (op, 1, trigfun);

    sqrtfun  := indets (expsys,sqrttype);
    sqrtvars := map2 (op, 1, sqrtfun);

    if trigvars intersect indets(eval (expsys, {cos=curry(cat,cos),
                                                sin=curry(cat,sin),
                                                tan=curry(cat,tan)}),
                                 name) <> {} then
        error "These variables are not trigonometric only: %1",
              trigvars intersect indets(expsys,name);
    end if;

    # Conversion table
    transnamecos  := v->cat('cos_',convert(v,name));
    transnamesin  := v->cat('sin_',convert(v,name));
    transnamesqrt := v->cat('sqrt_',convert(v,name));
    transname := v->cat('tanHalf_',convert(v,name));
    transcos := x-> (1-x^2)/(1+x^2);
    transsin := x-> 2*x/(1+x^2);
    transtan := x-> 2*x/(1-x^2);
    
    if not generic then
        L := [seq ([ [extra(v)=NULL, cos(v)=transcos(transname(v)),
                      sin(v)=transsin(transname(v)),
                      tan(v)=transtan(transname(v))],
                     [extra(v)=op([transnamecos(v)+1,transnamesin(v)]), 
                      cos(v)=-1, sin(v)=0, tan(v)=0] ],
                   v in trigvars),
              seq ([[ extra(v)=`if`(type(v,constant),transnamesqrt(v)^2-v,NULL),
                      `if`(type(v,name),v=transnamesqrt(v)^2,NULL),
                      v^(1/2)=transnamesqrt(v)]], v in sqrtvars) ];
        T := combinat:-cartprod (L);
        subsmap := [ seq ( map(op,T['nextvalue']()), i=1..2^nops(trigvars) ) ];
    else
        subsmap := [ [op(map (v->(extra(v)=NULL, cos(v)=transcos(transname(v)),
                                 sin(v)=transsin(transname(v)),
                                 tan(v)=transtan(transname(v))),
                             trigvars)),
                      op(map (v->( extra(v)=`if`(type(v,constant),
                                                 transnamesqrt(v)^2-v,NULL),
                                   `if`(type(v,name),v=transnamesqrt(v)^2,NULL),
                                   v^(1/2)=transnamesqrt(v) ),
                             sqrtvars))] ];
    end if;

    # Construction of the output systems
    expsys := [ seq(extra(v),v in [op(trigvars),op(sqrtvars)]), op(expsys) ];
    
    algsys := map ( s->foldl(normal@eval,expsys,op(s)), subsmap);
    #algsys := map ( normal@subs, subsmap, expsys );

    algsys := subs (seq (extra(v)=NULL, v in trigvars), algsys);
    if not generic then
        algsys := remove (s->ormap (p->type(p,constant) and p<>0, s), algsys);
    end if;

    # Construction of the output table
    algvars := table();
    for v in trigvars do
        if has (algsys, transnamecos(v)) or has (algsys, transnamesin(v)) then
            algvars[v] := transname(v), transnamecos(v), transnamesin(v);
        else
            algvars[v] := transname(v);
        end if;
    end do;

    for v in sqrtvars do
        algvars[v] := transnamesqrt(v)
    end do;

    for v in tanhalfvars do
        algvars[v] := cat('tanHalf_',convert(v,name));
    end do;
    
    if generic then return op(algsys), algvars
               else return algsys, algvars; end if;

end proc;

# Local function to shift an angle value in [-Pi;Pi[
normalAngle := a->if (evalf(a-Pi)<0 and evalf(a+Pi)>=0) then return a
                  else return evalf(a-floor((a+Pi)/(2*Pi))*2*Pi) end if;

# Local function used by RealSolve to expand boolean variables and 
# select real solutions
expandTrigonometric := proc (e ::set(name=algebraic))
    local N, B, T, v, res, intvars, trigvars;
    intvars := indets(e, And(name,'satisfies' (v->is(v,integer))));
    trigvars := map(lhs, select (s-> indets(rhs(s)) intersect intvars <> {},e));
    B, N := selectremove (v->is(v,OrProp(0,1)), intvars);
    N := map (v-> [round(2*Pi/abs(coeff(rhs(select (has,e,v)[1]),v)))-1,v],N);
    T := combinat:-cartprod ([ seq([v=0,v=1], v=B),
                               seq([seq(c[2]=i,i=0..c[1])],c=N) ]);
    res := [ seq ( (evalf@subs) (op(T['nextvalue']()),e),
                    i=1..2^nops(B)*mul(c[1]+1,c=N) ) ];
    res := select ( s->andmap (e->type(rhs(e),'realcons'),s), res );
    res := map2 (map,s->if member(lhs(s),trigvars) then
                            lhs(s)=normalAngle(rhs(s))
                        else s end if,res);

    #select the real solutions only
    return op( select ( s->andmap (e->type(rhs(e),'realcons'),s), res ) );
end proc;




# Function: TrigonometricGcd
#   Extract common factors of trigonometric expressions
#
# Parameters:
#   sys - list of trigonometric formula
#
# Returns:
#   a list of formula: the common factors of the formula in sys.
TrigonometricGcd := proc (sys ::list(algebraic),
                          {squarefree::truefalse := true,
                           ordering::list(name) := NULL  })
    local algsys, algvars, lsys, simplesys;
#    simplesys := map (simplify, sys, trig);
    simplesys := sys;

    if nops(simplesys)<=1 and not squarefree then
        lsys := FactorSystem (simplesys);
    else
        algsys, algvars := TrigonometricTanHalf(simplesys,'generic','const');
        lsys := map (op@Hypersurfaces, [numer(algsys)],
                                       ':-squarefree'=squarefree);
        lsys := map (AlgebraicTrigonometric, lsys, algvars, ':-nosimplify',
                                                            _rest);
        lsys := map (`[]`@curry(foldl,gcd)@op, lsys);
        lsys := remove (x->x=[1], lsys);
        if nops(lsys)=1 then
            if   nops(lsys[1])=0 then return [0]
            elif nops(lsys[1])>=2 then return [1] end if
        elif nops(lsys)=0 then return [1]
        end if;
        lsys := select (s->nops(s)=1,lsys);
    end if;
    return SortPolynomials (map(op,lsys), ordering);
end proc;

