cusp := [-y+R3*sin(alpha3), cos(alpha3)^2+sin(alpha3)^2-1, y*cos(alpha3)+S3*
sin(alpha3)-x*sin(alpha3), S3*cos(alpha3)-x*cos(alpha3)-y*sin(alpha3)+R3, S3-x
+R3*cos(alpha3), cos(alpha2)^2+sin(alpha2)^2-1, S2-y+R2*sin(alpha2)+b*sin(phi)
, cos(phi)^2+sin(phi)^2-1, -x+b*cos(phi)+R2*cos(alpha2), R3^2-S3^2+2*S3*x-x^2-
y^2, S3^2*sin(alpha3)-2*S3*x*sin(alpha3)+x^2*sin(alpha3)+y^2*sin(alpha3)-R3*y,
2*R2*S2*sin(alpha2)+1-L1^2+R2^2+S2^2-2*x*cos(phi)-2*y*sin(phi)+2*b, 2*x*cos(
alpha2)*R2+1+2*R2*y*sin(alpha2)-L1^2+b^2-2*x*cos(phi)-x^2+2*S2*y-2*y*sin(phi)-
y^2+2*b, S2*sin(phi)*cos(alpha2)-cos(phi)*x*cos(alpha2)-sin(phi)*y*cos(alpha2)
+S2*cos(phi)*sin(alpha2)+sin(phi)*x*sin(alpha2)-cos(phi)*y*sin(alpha2)+cos(phi
)*R2+b*cos(alpha2), sin(phi)*cos(alpha2)*R2-R2*cos(phi)*sin(alpha2)-S2*cos(phi
)-x*sin(phi)+y*cos(phi), S2*cos(phi)*cos(alpha2)+sin(phi)*x*cos(alpha2)-cos(
phi)*y*cos(alpha2)-S2*sin(phi)*sin(alpha2)+cos(phi)*x*sin(alpha2)+sin(phi)*y*
sin(alpha2)-sin(phi)*R2-b*sin(alpha2), cos(phi)*cos(alpha2)*R2+R2*sin(phi)*sin
(alpha2)+S2*sin(phi)-x*cos(phi)-y*sin(phi)+b, sin(phi)*sin(alpha2)^2*R2+R2*y*
sin(alpha2)^2+2*S2*x*cos(alpha2)-4*sin(phi)*x*cos(alpha2)+2*cos(phi)*y*cos(
alpha2)-R2^2*sin(alpha2)-S2^2*sin(alpha2)+3*S2*sin(phi)*sin(alpha2)+cos(phi)*x
*sin(alpha2)+x^2*sin(alpha2)+S2*y*sin(alpha2)-2*sin(phi)*y*sin(alpha2)-2*R2*S2
+2*sin(phi)*R2+b*sin(alpha2), 2*cos(phi)*sin(alpha2)^2*R2+2*x*sin(alpha2)^2*R2
+L1^2*cos(alpha2)+2*cos(alpha2)*R2^2-2*S2^2*cos(alpha2)-b^2*cos(alpha2)+10*cos
(phi)*x*cos(alpha2)+3*x^2*cos(alpha2)+4*sin(phi)*y*cos(alpha2)+y^2*cos(alpha2)
-4*S2*cos(phi)*sin(alpha2)-4*S2*x*sin(alpha2)+4*sin(phi)*x*sin(alpha2)-6*cos(
phi)*R2-6*x*R2-6*b*cos(alpha2)-cos(alpha2), 2*sin(alpha2)^2*R2^2-2-3*R2*sin(
phi)*sin(alpha2)-3*R2*y*sin(alpha2)+2*L1^2-2*R2^2-b^2-3*S2*sin(phi)+3*x*cos(
phi)-3*S2*y+6*y*sin(phi)+y^2-5*b, 2*R2*y*cos(alpha2)*sin(alpha2)-2*x*sin(
alpha2)^2*R2-L1^2*cos(alpha2)+b^2*cos(alpha2)-2*cos(phi)*x*cos(alpha2)-x^2*cos
(alpha2)+2*S2*y*cos(alpha2)-2*sin(phi)*y*cos(alpha2)-y^2*cos(alpha2)+2*x*R2+2*
b*cos(alpha2)+cos(alpha2), R2^2*cos(alpha2)*sin(alpha2)+R2*S2*cos(alpha2)-R2*
cos(phi)*sin(alpha2)-R2*x*sin(alpha2)-S2*cos(phi)-S2*x+x*sin(phi), 2*R2*sin(
phi)*x*sin(alpha2)-2*R2*cos(phi)*y*sin(alpha2)+cos(phi)*L1^2+2*S2*sin(phi)*x-2
*x*sin(phi)^2-x^2*cos(phi)-2*S2*cos(phi)*y+2*cos(phi)*sin(phi)*y-2*x*y*sin(phi
)+cos(phi)*y^2+cos(alpha2)*R2*b+b*x+2*R2*cos(alpha2)-cos(phi), 2*R2*cos(phi)*x
*sin(alpha2)+2*R2*sin(phi)*y*sin(alpha2)-sin(phi)*L1^2+2*S2*cos(phi)*x-2*cos(
phi)*sin(phi)*x+sin(phi)*x^2+2*S2*sin(phi)*y-2*y*sin(phi)^2-2*y*x*cos(phi)-y^2
*sin(phi)-R2*b*sin(alpha2)-S2*b+b*y-2*R2*sin(alpha2)-2*S2+sin(phi)+2*y, R2*sin
(phi)^2*sin(alpha2)+R2*sin(phi)*y*sin(alpha2)+S2*sin(phi)^2-cos(phi)*sin(phi)*
x+S2*sin(phi)*y-2*y*sin(phi)^2-y^2*sin(phi)+R2*b*sin(alpha2)+S2*b+b*y+R2*sin(
alpha2)+S2+y, R2*cos(phi)*sin(phi)*sin(alpha2)+R2*cos(phi)*y*sin(alpha2)+S2*
cos(phi)*sin(phi)+x*sin(phi)^2+S2*cos(phi)*y-2*cos(phi)*sin(phi)*y-cos(phi)*y^
2-cos(alpha2)*R2*b+b*x-R2*cos(alpha2)+2*x, 2*R2*b^2*sin(alpha2)+R2*sin(phi)*y*
sin(alpha2)+R2*y^2*sin(alpha2)+2*S2*b^2+sin(phi)*L1^2-2*cos(phi)*sin(phi)*x-
sin(phi)*x^2+b^2*y+S2*sin(phi)*y-2*y*sin(phi)^2-y*x*cos(phi)+S2*y^2-3*y^2*sin(
phi)-y^3+7*R2*b*sin(alpha2)+7*S2*b+2*b*y+6*R2*sin(alpha2)+6*S2-sin(phi), -5*y+
9*S2-2*sin(phi)+8*R2*sin(alpha2)-y*x*cos(phi)+5*y^2*sin(phi)+y^3-S2^3-L1^2*S2+
3*R2^2*S2+2*S2*b^2-2*S2*x^2+3*L1^2*y-3*R2^2*y-b^2*y+3*S2^2*y-5*S2*y^2+9*S2*b-4
*b*y+2*sin(phi)*L1^2+5*sin(phi)*x^2-3*R2^2*sin(phi)+3*S2^2*sin(phi)+2*R2^3*sin
(alpha2)-R2*sin(phi)*y*sin(alpha2)-7*S2*sin(phi)*y+5*R2*b*sin(alpha2)-3*R2*y^2
*sin(alpha2)-2*R2*x^2*sin(alpha2), 2*L1^2*R2*sin(alpha2)-2*R2*x^2*sin(alpha2)-
R2*sin(phi)*y*sin(alpha2)-3*R2*y^2*sin(alpha2)+2*L1^2*S2+sin(phi)*L1^2-2*cos(
phi)*sin(phi)*x-2*S2*x^2+3*sin(phi)*x^2-b^2*y-S2*sin(phi)*y-2*y*sin(phi)^2-3*y
*x*cos(phi)-3*S2*y^2+y^2*sin(phi)+y^3+3*R2*b*sin(alpha2)+3*S2*b+4*R2*sin(
alpha2)+4*S2-sin(phi), sin(phi)^2*y*cos(alpha2)-cos(phi)*x*y*cos(alpha2)+S2*
cos(phi)*sin(phi)*sin(alpha2)+sin(phi)^2*x*sin(alpha2)+S2*cos(phi)*y*sin(
alpha2)-2*cos(phi)*sin(phi)*y*sin(alpha2)-cos(phi)*y^2*sin(alpha2)-2*R2*b*cos(
alpha2)*sin(alpha2)+R2*cos(phi)*sin(phi)+R2*cos(phi)*y-S2*b*cos(alpha2)+b*x*
sin(alpha2)-3*R2*cos(alpha2)*sin(alpha2)-2*S2*cos(alpha2)+2*x*sin(alpha2), cos
(phi)*sin(phi)*y*cos(alpha2)+sin(phi)*x*y*cos(alpha2)-S2*sin(phi)^2*sin(alpha2
)+cos(phi)*sin(phi)*x*sin(alpha2)-S2*sin(phi)*y*sin(alpha2)+2*sin(phi)^2*y*sin
(alpha2)+sin(phi)*y^2*sin(alpha2)-2*R2*b*sin(alpha2)^2-sin(phi)^2*R2-R2*sin(
phi)*y-b*x*cos(alpha2)-S2*b*sin(alpha2)-b*y*sin(alpha2)-2*sin(alpha2)^2*R2+R2*
b-2*x*cos(alpha2)-S2*sin(alpha2)-y*sin(alpha2)+R2, -R2+6*sin(alpha2)^2*R2+R2^3
+L1^2*R2-3*R2*S2^2-2*R2*b^2-6*R2*x^2-12*R2*S2*y+2*R2*y^2-2*R2*b+6*S2*sin(
alpha2)-9*y*sin(alpha2)+4*x^3*cos(alpha2)-2*S2^3*sin(alpha2)+2*y^3*sin(alpha2)
-3*sin(phi)*sin(alpha2)-24*sin(phi)*x*y*cos(alpha2)+6*cos(phi)*sin(phi)*x*sin(
alpha2)+8*S2*sin(phi)*y*sin(alpha2)+12*S2*x*y*cos(alpha2)-2*S2*cos(phi)*x*sin(
alpha2)+2*cos(phi)*x*y*sin(alpha2)+6*sin(phi)^2*y*sin(alpha2)-4*sin(phi)*y^2*
sin(alpha2)+2*R2*b*sin(alpha2)^2+14*R2*sin(phi)*y+4*S2*b*sin(alpha2)+14*sin(
phi)*x^2*sin(alpha2)+12*cos(phi)*y^2*cos(alpha2)+3*L1^2*sin(phi)*sin(alpha2)-
R2^2*sin(phi)*sin(alpha2)+5*S2^2*sin(phi)*sin(alpha2)-6*S2*x^2*sin(alpha2)+3*
L1^2*y*sin(alpha2)-2*b^2*y*sin(alpha2)-9*R2^2*y*sin(alpha2)-3*S2^2*y*sin(
alpha2)+6*x^2*y*sin(alpha2)+4*R2*S2*sin(phi)-2*R2*cos(phi)*x+8*cos(phi)*x^2*
cos(alpha2), 6*R2*x*y-4*R2*S2*x-y^3*cos(alpha2)+2*x^3*sin(alpha2)+cos(phi)*sin
(alpha2)+y*cos(alpha2)-6*cos(phi)*x*y*cos(alpha2)+6*S2*cos(phi)*y*sin(alpha2)-\
2*cos(phi)*sin(phi)*y*sin(alpha2)-R2*b*cos(alpha2)*sin(alpha2)-6*sin(phi)*x*y*
sin(alpha2)+6*S2*x*y*sin(alpha2)+4*S2*sin(phi)*x*sin(alpha2)+2*sin(phi)^2*x*
sin(alpha2)-cos(phi)*y^2*sin(alpha2)+6*R2*cos(phi)*y+b*x*sin(alpha2)-2*R2*cos(
alpha2)*sin(alpha2)+4*sin(phi)*x*R2-2*R2^2*y*cos(alpha2)+3*cos(phi)*x^2*sin(
alpha2)+2*S2^2*y*cos(alpha2)-3*x^2*y*cos(alpha2)+b^2*y*cos(alpha2)-L1^2*cos(
phi)*sin(alpha2)+6*b*y*cos(alpha2)-L1^2*y*cos(alpha2)-4*sin(phi)*y^2*cos(
alpha2)-2*R2^2*x*sin(alpha2)-2*S2^2*x*sin(alpha2)-8*sin(phi)*x^2*cos(alpha2)+4
*S2*x^2*cos(alpha2), 7*R2-2*sin(alpha2)^2*R2+R2^3+L1^2*R2-3*R2*S2^2-2*R2*b^2-6
*R2*x^2-12*R2*S2*y+2*R2*y^2+6*R2*b-16*sin(phi)^2*R2-16*x*cos(alpha2)+6*S2*sin(
alpha2)-17*y*sin(alpha2)+4*x^3*cos(alpha2)-2*S2^3*sin(alpha2)+2*y^3*sin(alpha2
)-3*sin(phi)*sin(alpha2)-24*sin(phi)*x*y*cos(alpha2)+6*cos(phi)*sin(phi)*x*sin
(alpha2)+12*S2*x*y*cos(alpha2)+6*S2*cos(phi)*x*sin(alpha2)-6*cos(phi)*x*y*sin(
alpha2)-16*S2*sin(phi)^2*sin(alpha2)+22*sin(phi)^2*y*sin(alpha2)-10*R2*b*sin(
alpha2)^2+6*R2*sin(phi)*y-4*b*y*sin(alpha2)+18*sin(phi)*x^2*sin(alpha2)+12*cos
(phi)*y^2*cos(alpha2)+3*L1^2*sin(phi)*sin(alpha2)+3*R2^2*sin(phi)*sin(alpha2)+
9*S2^2*sin(phi)*sin(alpha2)-6*S2*x^2*sin(alpha2)+3*L1^2*y*sin(alpha2)-2*b^2*y*
sin(alpha2)-9*R2^2*y*sin(alpha2)-3*S2^2*y*sin(alpha2)+6*x^2*y*sin(alpha2)+12*
R2*S2*sin(phi)+6*R2*cos(phi)*x+16*sin(phi)^2*x*cos(alpha2), 4*cos(phi)*sin(phi
)*x*cos(alpha2)+2*sin(phi)*x^2*cos(alpha2)+R2^2*cos(phi)*sin(alpha2)+S2^2*cos(
phi)*sin(alpha2)-4*S2*cos(phi)*sin(phi)*sin(alpha2)-2*S2*sin(phi)*x*sin(alpha2
)+cos(phi)*x^2*sin(alpha2)-2*S2*cos(phi)*y*sin(alpha2)+4*cos(phi)*sin(phi)*y*
sin(alpha2)+2*sin(phi)*x*y*sin(alpha2)+cos(phi)*y^2*sin(alpha2)+3*R2*b*cos(
alpha2)*sin(alpha2)+2*R2*S2*cos(phi)-4*R2*cos(phi)*sin(phi)-2*sin(phi)*x*R2-2*
R2*cos(phi)*y+2*S2*b*cos(alpha2)-3*b*x*sin(alpha2)+6*R2*cos(alpha2)*sin(alpha2
)+4*S2*cos(alpha2)-2*y*cos(alpha2)-4*x*sin(alpha2), -12*sin(alpha2)^2*R2-2*R2*
b^2-6*R2*S2*y+2*R2*y^2-2*R2*b-12*S2*sin(alpha2)+y^3*sin(alpha2)+2*sin(phi)*sin
(alpha2)-10*sin(phi)*x*y*cos(alpha2)+4*cos(phi)*sin(phi)*x*sin(alpha2)+8*S2*
sin(phi)*y*sin(alpha2)+6*S2*x*y*cos(alpha2)+4*cos(phi)*x*y*sin(alpha2)+4*sin(
phi)^2*y*sin(alpha2)-sin(phi)*y^2*sin(alpha2)-9*R2*b*sin(alpha2)^2+8*R2*sin(
phi)*y+4*b*x*cos(alpha2)-11*S2*b*sin(alpha2)-b*y*sin(alpha2)+sin(phi)*x^2*sin(
alpha2)+4*cos(phi)*y^2*cos(alpha2)-2*L1^2*sin(phi)*sin(alpha2)-R2^2*sin(phi)*
sin(alpha2)-S2^2*sin(phi)*sin(alpha2)-b^2*y*sin(alpha2)-3*R2^2*y*sin(alpha2)-3
*S2^2*y*sin(alpha2)+3*x^2*y*sin(alpha2)-2*R2*S2*sin(phi)-2*x*y^2*cos(alpha2)-2
*S2*b^2*sin(alpha2)+2*S2*y^2*sin(alpha2)+2*b^2*x*cos(alpha2), 4*sin(alpha2)^2*
R2+R2^3-3*R2*S2^2-R2*b^2-3*R2*x^2-6*R2*S2*y+R2*y^2+4*S2*sin(alpha2)-6*y*sin(
alpha2)+2*x^3*cos(alpha2)-2*S2^3*sin(alpha2)+y^3*sin(alpha2)-2*sin(phi)*sin(
alpha2)-18*sin(phi)*x*y*cos(alpha2)+4*cos(phi)*sin(phi)*x*sin(alpha2)+4*S2*sin
(phi)*y*sin(alpha2)+6*S2*x*y*cos(alpha2)+4*S2*cos(phi)*x*sin(alpha2)-4*cos(phi
)*x*y*sin(alpha2)+4*sin(phi)^2*y*sin(alpha2)-3*sin(phi)*y^2*sin(alpha2)+R2*b*
sin(alpha2)^2+8*R2*sin(phi)*y+4*b*x*cos(alpha2)+3*S2*b*sin(alpha2)+b*y*sin(
alpha2)+11*sin(phi)*x^2*sin(alpha2)+8*cos(phi)*y^2*cos(alpha2)+2*L1^2*sin(phi)
*sin(alpha2)-R2^2*sin(phi)*sin(alpha2)+5*S2^2*sin(phi)*sin(alpha2)-2*S2*x^2*
sin(alpha2)+2*L1^2*y*sin(alpha2)-b^2*y*sin(alpha2)-5*R2^2*y*sin(alpha2)-S2^2*y
*sin(alpha2)+3*x^2*y*sin(alpha2)+4*R2*S2*sin(phi)+2*R2*cos(phi)*x+2*S2^2*x*cos
(alpha2), 3*R2+2*sin(alpha2)^2*R2+R2^3-3*L1^2*R2-3*R2*S2^2+2*R2*b^2-2*R2*x^2+4
*R2*S2*y-2*R2*y^2+6*R2*b-4*x*cos(alpha2)+6*S2*sin(alpha2)-7*y*sin(alpha2)-2*S2
^3*sin(alpha2)-sin(phi)*sin(alpha2)-4*sin(phi)*x*y*cos(alpha2)+2*cos(phi)*sin(
phi)*x*sin(alpha2)-8*S2*sin(phi)*y*sin(alpha2)+2*S2*cos(phi)*x*sin(alpha2)-2*
cos(phi)*x*y*sin(alpha2)+2*sin(phi)^2*y*sin(alpha2)+6*sin(phi)*y^2*sin(alpha2)
-2*R2*sin(phi)*y+10*S2*b*sin(alpha2)-6*b*y*sin(alpha2)+8*sin(phi)*x^2*sin(
alpha2)+4*cos(phi)*y^2*cos(alpha2)+L1^2*sin(phi)*sin(alpha2)-R2^2*sin(phi)*sin
(alpha2)+5*S2^2*sin(phi)*sin(alpha2)-2*S2*x^2*sin(alpha2)+5*L1^2*y*sin(alpha2)
-R2^2*y*sin(alpha2)+5*S2^2*y*sin(alpha2)+4*R2*S2*sin(phi)+2*R2*cos(phi)*x+4*S2
*b^2*sin(alpha2)-4*S2*y^2*sin(alpha2)-4*L1^2*S2*sin(alpha2)+4*L1^2*x*cos(
alpha2), 2*sin(phi)*L1^2*cos(alpha2)-2*sin(phi)*y^2*cos(alpha2)-2*L1^2*cos(phi
)*sin(alpha2)-R2^2*cos(phi)*sin(alpha2)-S2^2*cos(phi)*sin(alpha2)-2*S2*sin(phi
)*x*sin(alpha2)+cos(phi)*x^2*sin(alpha2)+2*S2*cos(phi)*y*sin(alpha2)+2*sin(phi
)*x*y*sin(alpha2)+cos(phi)*y^2*sin(alpha2)+5*R2*b*cos(alpha2)*sin(alpha2)-2*R2
*S2*cos(phi)-2*sin(phi)*x*R2+2*R2*cos(phi)*y+4*S2*b*cos(alpha2)+2*b*y*cos(
alpha2)-3*b*x*sin(alpha2)+6*R2*cos(alpha2)*sin(alpha2)+8*S2*cos(alpha2)-2*sin(
phi)*cos(alpha2)-2*y*cos(alpha2)+2*cos(phi)*sin(alpha2)-4*x*sin(alpha2), 2*cos
(phi)*L1^2*cos(alpha2)-2*cos(phi)*y^2*cos(alpha2)+2*L1^2*sin(phi)*sin(alpha2)+
R2^2*sin(phi)*sin(alpha2)+S2^2*sin(phi)*sin(alpha2)-2*S2*cos(phi)*x*sin(alpha2
)-sin(phi)*x^2*sin(alpha2)-2*S2*sin(phi)*y*sin(alpha2)+2*cos(phi)*x*y*sin(
alpha2)-sin(phi)*y^2*sin(alpha2)+5*R2*b*sin(alpha2)^2+2*R2*S2*sin(phi)-2*R2*
cos(phi)*x-2*R2*sin(phi)*y+2*b*x*cos(alpha2)+5*S2*b*sin(alpha2)+b*y*sin(alpha2
)+6*sin(alpha2)^2*R2-2*cos(phi)*cos(alpha2)+4*x*cos(alpha2)+8*S2*sin(alpha2)-2
*sin(phi)*sin(alpha2)-2*y*sin(alpha2)+2*R2, 6*R2*x*y+8*x*sin(alpha2)-y^3*cos(
alpha2)-2*cos(phi)*sin(alpha2)-14*cos(phi)*x*y*cos(alpha2)+8*S2*cos(phi)*y*sin
(alpha2)-4*cos(phi)*sin(phi)*y*sin(alpha2)-R2*b*cos(alpha2)*sin(alpha2)-4*sin(
phi)*x*y*sin(alpha2)+6*S2*x*y*sin(alpha2)+4*sin(phi)^2*x*sin(alpha2)-7*cos(phi
)*y^2*sin(alpha2)+8*R2*cos(phi)*y+4*S2*b*cos(alpha2)+9*b*x*sin(alpha2)-3*R2^2*
y*cos(alpha2)-cos(phi)*x^2*sin(alpha2)+3*S2^2*y*cos(alpha2)-3*x^2*y*cos(alpha2
)+b^2*y*cos(alpha2)+2*L1^2*cos(phi)*sin(alpha2)+6*b*y*cos(alpha2)-4*sin(phi)*y
^2*cos(alpha2)+R2^2*cos(phi)*sin(alpha2)+S2^2*cos(phi)*sin(alpha2)+2*R2*S2*cos
(phi)-2*x*y^2*sin(alpha2)-2*S2*y^2*cos(alpha2)+2*b^2*x*sin(alpha2)+2*S2*b^2*
cos(alpha2), 2*cos(alpha2)*R2*b^2-2*R2*y^2*cos(alpha2)-2*R2*cos(phi)*y*sin(
alpha2)-cos(phi)*L1^2-2*x*b^2-2*x*sin(phi)^2+x^2*cos(phi)-2*S2*cos(phi)*y+2*
cos(phi)*sin(phi)*y-2*x*y*sin(phi)+5*cos(phi)*y^2+2*x*y^2+3*cos(alpha2)*R2*b-7
*b*x+cos(phi)-4*x, -6*R2*x*y-4*R2*S2*x+y^3*cos(alpha2)-2*x^3*sin(alpha2)-3*cos
(phi)*sin(alpha2)-3*y*cos(alpha2)-4*S2^3*cos(alpha2)+30*cos(phi)*x*y*cos(
alpha2)-10*S2*cos(phi)*y*sin(alpha2)+6*cos(phi)*sin(phi)*y*sin(alpha2)+R2*b*
cos(alpha2)*sin(alpha2)-14*sin(phi)*x*y*sin(alpha2)-6*S2*x*y*sin(alpha2)+20*S2
*sin(phi)*x*sin(alpha2)-6*sin(phi)^2*x*sin(alpha2)+5*cos(phi)*y^2*sin(alpha2)-\
10*R2*cos(phi)*y-8*S2*b*cos(alpha2)+15*b*x*sin(alpha2)+6*R2*cos(alpha2)*sin(
alpha2)+12*sin(phi)*x*R2-19*cos(phi)*x^2*sin(alpha2)+3*x^2*y*cos(alpha2)-b^2*y
*cos(alpha2)+3*L1^2*cos(phi)*sin(alpha2)-14*b*y*cos(alpha2)+3*L1^2*y*cos(
alpha2)+12*sin(phi)*y^2*cos(alpha2)+2*R2^2*x*sin(alpha2)-6*S2^2*x*sin(alpha2)-\
8*sin(phi)*x^2*cos(alpha2)-2*R2^2*cos(phi)*sin(alpha2)-10*S2^2*cos(phi)*sin(
alpha2)-12*R2*S2*cos(phi)+4*R2^2*S2*cos(alpha2), 6*R2*x*y-4*R2*S2*x-4*S2*cos(
alpha2)+12*x*sin(alpha2)-y^3*cos(alpha2)-2*x^3*sin(alpha2)-5*cos(phi)*sin(
alpha2)+3*y*cos(alpha2)-14*cos(phi)*x*y*cos(alpha2)+2*S2*cos(phi)*y*sin(alpha2
)-6*cos(phi)*sin(phi)*y*sin(alpha2)-R2*b*cos(alpha2)*sin(alpha2)-10*sin(phi)*x
*y*sin(alpha2)+6*S2*x*y*sin(alpha2)+4*S2*sin(phi)*x*sin(alpha2)+6*sin(phi)^2*x
*sin(alpha2)-5*cos(phi)*y^2*sin(alpha2)+2*R2*cos(phi)*y+17*b*x*sin(alpha2)+2*
R2*cos(alpha2)*sin(alpha2)+4*sin(phi)*x*R2-5*cos(phi)*x^2*sin(alpha2)-3*x^2*y*
cos(alpha2)+b^2*y*cos(alpha2)+5*L1^2*cos(phi)*sin(alpha2)+6*b*y*cos(alpha2)-3*
L1^2*y*cos(alpha2)-4*sin(phi)*y^2*cos(alpha2)-2*R2^2*x*sin(alpha2)-2*S2^2*x*
sin(alpha2)+2*R2^2*cos(phi)*sin(alpha2)+2*S2^2*cos(phi)*sin(alpha2)+4*R2*S2*
cos(phi)-4*x*y^2*sin(alpha2)+4*b^2*x*sin(alpha2)+4*L1^2*x*sin(alpha2)+4*L1^2*
S2*cos(alpha2), 2*cos(alpha2)*R2^3-2*R2*S2^2*cos(alpha2)-2*cos(phi)*L1^2-2*R2^
2*cos(phi)+2*S2^2*cos(phi)-x*L1^2-2*x*R2^2+2*S2^2*x-x*b^2-4*S2*sin(phi)*x+4*x^
2*cos(phi)+x^3+2*x*y*sin(phi)+2*cos(phi)*y^2+x*y^2-6*b*x-2*R2*cos(alpha2)+2*
cos(phi)-3*x, 2*L1^2*cos(alpha2)*R2-2*cos(phi)*L1^2-3*x*L1^2-x*b^2+2*x^2*cos(
phi)+x^3+2*x*y*sin(phi)+x*y^2-2*cos(alpha2)*R2*b-4*b*x-4*R2*cos(alpha2)+2*cos(
phi)-x, 4*sin(phi)^3*x-2*cos(phi)*sin(phi)*x^2-L1^2*cos(phi)*y-4*cos(phi)*sin(
phi)^2*y-2*sin(phi)^2*x*y+cos(phi)*x^2*y+cos(phi)*y^3+R2*b*y*cos(alpha2)+2*R2*
b*x*sin(alpha2)+2*S2*b*x-b*x*y-2*R2*y*cos(alpha2)+2*R2*x*sin(alpha2)+2*S2*x+3*
y*cos(phi)-2*x*y, 16*S2*b*y+7*b-L1^2+1+5*x^2-16*y^2-14*y*sin(phi)+x*cos(phi)+
10*b^2+4*b^3+2*x^2*b-2*L1^2*b-2*b*y^2+29*S2*y+S2*sin(phi)+4*sin(phi)^2*x^2-2*
sin(phi)*y^3+8*sin(phi)^3*y-4*cos(phi)*sin(phi)*x*y+16*R2*b*y*sin(alpha2)+29*
R2*y*sin(alpha2)+R2*sin(phi)*sin(alpha2)+2*L1^2*sin(phi)*y-2*sin(phi)*x^2*y+8*
cos(phi)*sin(phi)^2*x, 2*R2^2*b-2*S2^2*b+6*S2*b*y+3*b+2*x^2-3*y^2+2*y*sin(phi)
-x*cos(phi)+5*b^2+2*b^3+2*R2^2+2*x^2*b-4*L1^2*b-2*S2^2+5*S2*y-3*S2*sin(phi)+2*
sin(phi)^2*x^2-2*sin(phi)*y^3-6*sin(phi)^2*y^2+2*sin(phi)^2*R2^2-2*S2^2*sin(
phi)^2-8*cos(phi)*sin(phi)*x*y+10*R2*b*y*sin(alpha2)+4*S2*cos(phi)*sin(phi)*x+
9*R2*y*sin(alpha2)-3*R2*sin(phi)*sin(alpha2)-2*sin(phi)*x^2*y+2*R2^2*sin(phi)*
y-2*S2^2*sin(phi)*y+8*S2*sin(phi)^2*y+4*S2*sin(phi)*y^2, 4*L1^2*sin(phi)^2-1+2
*L1^2*sin(phi)*y-4*cos(phi)*sin(phi)*x*y-2*sin(phi)*x^2*y-4*sin(phi)^2*y^2-2*
sin(phi)*y^3+4*R2*b*y*sin(alpha2)-2*L1^2*b+2*x^2*b+4*S2*b*y+2*b*y^2+R2*sin(phi
)*sin(alpha2)-3*R2*y*sin(alpha2)+L1^2-4*b^2+S2*sin(phi)-4*sin(phi)^2-3*x*cos(
phi)+3*x^2-3*S2*y+6*y*sin(phi)+6*y^2-5*b, x*sin(phi)-4*S2*b*x+3*x*y-8*S2*x+b*x
*y-S2*cos(phi)-cos(phi)*y^3-R2*b*y*cos(alpha2)-2*R2*b*x*sin(alpha2)+4*S2*cos(
phi)*sin(phi)*y+2*R2*S2*b*cos(alpha2)-R2*cos(phi)*sin(alpha2)+2*R2*S2*cos(
alpha2)-4*R2*x*sin(alpha2)+cos(phi)*sin(phi)*x^2+4*sin(phi)^2*x*y-cos(phi)*x^2
*y-R2*y*cos(alpha2)-2*S2*sin(phi)^2*x+R2^2*cos(phi)*y-S2^2*cos(phi)*y+2*S2*cos
(phi)*y^2-3*cos(phi)*sin(phi)*y^2+R2^2*cos(phi)*sin(phi)-S2^2*cos(phi)*sin(phi
), 2*L1^2*cos(phi)*sin(phi)+L1^2*cos(phi)*y+2*sin(phi)^2*x*y-cos(phi)*x^2*y-2*
cos(phi)*sin(phi)*y^2-cos(phi)*y^3-R2*b*y*cos(alpha2)-2*R2*b*x*sin(alpha2)-2*
S2*b*x+b*x*y+2*R2*cos(phi)*sin(alpha2)-6*R2*x*sin(alpha2)+2*S2*cos(phi)-2*cos(
phi)*sin(phi)-6*S2*x+6*x*sin(phi)-3*y*cos(phi)+4*x*y, -R2^2*b+S2^2*b-2*S2*b*y+
b-L1^2+1-x^2+3*y^2+b^2-2*R2^2+L1^2*b+2*S2^2-4*S2*y-2*S2*sin(phi)+4*sin(phi)^2*
x^2-4*sin(phi)^2*y^2-8*cos(phi)*sin(phi)*x*y+4*S2*cos(phi)*sin(phi)*x+4*S2*cos
(phi)*x*y-2*L1^2*sin(phi)*y+2*R2^2*sin(phi)*y-2*S2^2*sin(phi)*y+4*S2*sin(phi)^
2*y+2*S2*sin(phi)*y^2-2*L1^2*cos(phi)*x+2*R2^2*cos(phi)*x-2*S2^2*cos(phi)*x-2*
S2*sin(phi)*x^2+2*L1^2*S2*sin(phi), -x*sin(phi)+y*cos(phi)+S2*b*x-2*x*y+2*S2*x
-S2*cos(phi)+2*S2*cos(phi)*sin(phi)*y+R2*S2*b*cos(alpha2)-2*S2*sin(phi)*x*y+2*
R2*S2*cos(alpha2)+2*R2*x*sin(alpha2)+2*cos(phi)*sin(phi)*x^2-L1^2*cos(phi)*y+4
*sin(phi)^2*x*y-2*R2*y*cos(alpha2)-2*S2*sin(phi)^2*x+R2^2*cos(phi)*y-S2^2*cos(
phi)*y+S2*cos(phi)*y^2-2*cos(phi)*sin(phi)*y^2+L1^2*S2*cos(phi)+L1^2*sin(phi)*
x-R2^2*sin(phi)*x+S2^2*sin(phi)*x-S2*cos(phi)*x^2, -2*S2^2*b^2-2*b^2*x^2-2*S2*
b^2*y+2*x^2*y^2-3*L1^2*y^2+R2^2*y^2-4*b^2*y^2-S2^2*y^2+2*S2*y^3+6*R2^2*b-6*S2^
2*b+16*b-4*L1^2+4-4*x^2-16*y^2+3*y*sin(phi)-5*x*cos(phi)+23*b^2+13*b^3+4*R2^2-\
5*x^2*b-9*L1^2*b+2*b^4-4*S2^2+2*y^4-17*b*y^2-S2*y+2*R2^2*b^2-3*S2*sin(phi)+4*
sin(phi)*y^3-2*sin(phi)^2*y^2+2*cos(phi)*x^3-2*cos(phi)*sin(phi)*x*y+6*R2*b*y*
sin(alpha2)+8*S2*cos(phi)*sin(phi)*x+6*S2*cos(phi)*x*y+3*R2*y*sin(alpha2)-3*R2
*sin(phi)*sin(alpha2)-9*L1^2*sin(phi)*y+2*sin(phi)*x^2*y+3*R2^2*sin(phi)*y-3*
S2^2*sin(phi)*y+8*S2*sin(phi)^2*y+8*S2*sin(phi)*y^2-4*L1^2*cos(phi)*x+2*R2^2*
cos(phi)*x-2*S2^2*cos(phi)*x+4*cos(phi)*x*y^2, -2*b^2*x^2+2*x^2*y^2-2*L1^2*y^2
-4*b^2*y^2+6*S2*b*y+8*b-2*L1^2+2+2*x^2-17*y^2+4*y*sin(phi)-3*x*cos(phi)+15*b^2
+11*b^3-3*x^2*b-5*L1^2*b+2*b^4+2*y^4-17*b*y^2+5*S2*y+2*L1^2*b^2-3*S2*sin(phi)-\
4*sin(phi)^2*x^2+6*sin(phi)*y^3+4*sin(phi)^2*y^2+2*cos(phi)*x^3+8*cos(phi)*sin
(phi)*x*y+6*R2*b*y*sin(alpha2)+5*R2*y*sin(alpha2)-3*R2*sin(phi)*sin(alpha2)-6*
L1^2*sin(phi)*y+2*sin(phi)*x^2*y-2*L1^2*cos(phi)*x+6*cos(phi)*x*y^2, -2*L1^2*
S2^2+4*R2^2*S2^2+4*S2^2*b^2-2*L1^2*x^2-2*S2^2*x^2+6*L1^2*S2*y-6*R2^2*S2*y+6*S2
^3*y-2*S2*b^2*y-3*L1^2*y^2+3*R2^2*y^2-7*S2^2*y^2+2*S2*y^3-7*R2^2*b+11*S2^2*b-4
*S2*b*y-13*b+5*L1^2-5+9*x^2+4*y^2+7*y*sin(phi)+6*x*cos(phi)-7*b^2-6*R2^2+4*x^2
*b+3*L1^2*b-2*R2^4+2*L1^2*R2^2+2*x^2*R2^2+6*S2^2-2*S2^4+6*b*y^2-4*S2*y-8*sin(
phi)^2*x^2-6*sin(phi)*y^3+6*sin(phi)^2*y^2-4*cos(phi)*x^3+6*S2^3*sin(phi)+14*
cos(phi)*sin(phi)*x*y-8*S2*cos(phi)*sin(phi)*x-10*S2*cos(phi)*x*y-2*R2*y*sin(
alpha2)+3*L1^2*sin(phi)*y-4*sin(phi)*x^2*y+R2^2*sin(phi)*y-9*S2^2*sin(phi)*y-8
*S2*sin(phi)^2*y+6*S2*sin(phi)*y^2+4*L1^2*cos(phi)*x+4*S2^2*cos(phi)*x+14*S2*
sin(phi)*x^2-6*cos(phi)*x*y^2-6*R2^2*S2*sin(phi), 4*R2^2*S2^2+4*S2^2*b^2-4*L1^
2*x^2-4*S2^2*x^2+6*L1^2*S2*y-6*R2^2*S2*y+6*S2^3*y-4*S2*b^2*y-6*L1^2*y^2+6*R2^2
*y^2-10*S2^2*y^2+4*S2*y^3-9*R2^2*b+13*S2^2*b-2*S2*b*y-19*b+5*L1^2-7+15*x^2+9*y
^2+10*y*sin(phi)+8*x*cos(phi)-11*b^2-8*R2^2+8*x^2*b+L1^2*b-2*R2^4+2*L1^4+4*x^2
*R2^2+8*S2^2-2*S2^4+12*b*y^2-2*S2*y-12*sin(phi)^2*x^2-12*sin(phi)*y^3+8*sin(
phi)^2*y^2-8*cos(phi)*x^3+6*S2^3*sin(phi)+20*cos(phi)*sin(phi)*x*y-16*S2*cos(
phi)*sin(phi)*x-20*S2*cos(phi)*x*y-4*R2*y*sin(alpha2)+8*L1^2*sin(phi)*y-8*sin(
phi)*x^2*y-8*S2^2*sin(phi)*y-16*S2*sin(phi)^2*y+6*S2*sin(phi)*y^2+10*L1^2*cos(
phi)*x-2*R2^2*cos(phi)*x+6*S2^2*cos(phi)*x+22*S2*sin(phi)*x^2-12*cos(phi)*x*y^
2-6*R2^2*S2*sin(phi), 2*R2*S3*cos(alpha2)*sin(alpha2)*sin(alpha3)+sin(alpha2)*
sin(alpha3)-cos(alpha2)*cos(alpha3)-2*sin(phi)*y*sin(alpha2)*sin(alpha3)+2*sin
(phi)*x*cos(alpha2)*sin(alpha3)+x*y*cos(alpha2)*sin(alpha3)+2*cos(phi)*x*cos(
alpha2)*cos(alpha3)+2*S2*S3*cos(alpha2)*sin(alpha3)-2*S3*sin(phi)*cos(alpha2)*
sin(alpha3)-2*S2*x*cos(alpha2)*sin(alpha3)-2*cos(phi)*x*sin(alpha2)*sin(alpha3
)+2*R2*x*sin(alpha2)^2*cos(alpha3)+2*R2*y*sin(alpha2)^2*sin(alpha3)-S3*y*cos(
alpha2)*sin(alpha3)+2*S2*y*sin(alpha2)*sin(alpha3)+L1^2*cos(alpha2)*cos(alpha3
)-b^2*cos(alpha2)*cos(alpha3)+x^2*cos(alpha2)*cos(alpha3)-L1^2*sin(alpha2)*sin
(alpha3)+b^2*sin(alpha2)*sin(alpha3)-x^2*sin(alpha2)*sin(alpha3)-y^2*sin(
alpha2)*sin(alpha3)-2*R2*x*cos(alpha3)-2*b*cos(alpha2)*cos(alpha3)+2*b*sin(
alpha2)*sin(alpha3), -4*cos(phi)*sin(phi)*x*sin(alpha2)*cos(alpha3)+4*S3*cos(
phi)*sin(phi)*cos(alpha2)*sin(alpha3)+4*S3*sin(phi)*x*cos(alpha2)*sin(alpha3)-\
2*sin(phi)*x*y*sin(alpha2)*sin(alpha3)-4*S2*S3*sin(phi)*sin(alpha2)*sin(alpha3
)-4*S2*cos(phi)*sin(phi)*sin(alpha2)*sin(alpha3)+2*S2*sin(phi)*x*sin(alpha2)*
sin(alpha3)-2*S2*cos(phi)*y*sin(alpha2)*sin(alpha3)+4*S3*sin(phi)*y*sin(alpha2
)*sin(alpha3)+4*cos(phi)*sin(phi)*y*sin(alpha2)*sin(alpha3)+3*R2*b*cos(alpha2)
*sin(alpha2)*sin(alpha3)-4*R2*cos(alpha3)-8*sin(phi)^2*x*sin(alpha2)*sin(
alpha3)-2*sin(phi)*x^2*cos(alpha2)*sin(alpha3)+4*S2*sin(phi)^2*sin(alpha2)*cos
(alpha3)+8*R2*b*sin(alpha2)^2*cos(alpha3)+R2^2*cos(phi)*sin(alpha2)*sin(alpha3
)+S2^2*cos(phi)*sin(alpha2)*sin(alpha3)+8*S3*sin(phi)^2*sin(alpha2)*sin(alpha3
)+cos(phi)*x^2*sin(alpha2)*sin(alpha3)+cos(phi)*y^2*sin(alpha2)*sin(alpha3)+4*
b*x*cos(alpha2)*cos(alpha3)+4*S2*b*sin(alpha2)*cos(alpha3)+2*R2*S2*cos(phi)*
sin(alpha3)-4*R2*S3*sin(phi)*sin(alpha3)-4*R2*cos(phi)*sin(phi)*sin(alpha3)+2*
R2*sin(phi)*x*sin(alpha3)-2*R2*cos(phi)*y*sin(alpha3)+2*S2*b*cos(alpha2)*sin(
alpha3)-4*S3*b*sin(alpha2)*sin(alpha3)+b*x*sin(alpha2)*sin(alpha3)+6*R2*cos(
alpha2)*sin(alpha2)*sin(alpha3)+8*x*cos(alpha2)*cos(alpha3)+4*R2*sin(phi)^2*
cos(alpha3)+8*R2*sin(alpha2)^2*cos(alpha3)-2*y*cos(alpha2)*sin(alpha3)-4*R2*b*
cos(alpha3)+4*S2*sin(alpha2)*cos(alpha3)+4*S2*cos(alpha2)*sin(alpha3)-4*S3*sin
(alpha2)*sin(alpha3), -24*sin(phi)*x*y*cos(alpha2)*sin(alpha3)+16*S2*cos(phi)*
sin(phi)*sin(alpha2)*cos(alpha3)-32*R2*b*cos(alpha2)*sin(alpha2)*cos(alpha3)+
16*S3*cos(phi)*x*cos(alpha2)*sin(alpha3)+12*S2*x*y*cos(alpha2)*sin(alpha3)-16*
S2*S3*cos(phi)*sin(alpha2)*sin(alpha3)-26*cos(phi)*sin(phi)*x*sin(alpha2)*sin(
alpha3)+32*S3*cos(phi)*sin(phi)*sin(alpha2)*sin(alpha3)+6*S2*cos(phi)*x*sin(
alpha2)*sin(alpha3)-6*cos(phi)*x*y*sin(alpha2)*sin(alpha3)+16*S3*cos(phi)*y*
sin(alpha2)*sin(alpha3)+16*S2*sin(phi)*y*sin(alpha2)*sin(alpha3)+R2^3*sin(
alpha3)-9*R2*sin(alpha3)+10*sin(phi)*x^2*sin(alpha2)*sin(alpha3)-10*sin(phi)^2
*y*sin(alpha2)*sin(alpha3)+16*sin(phi)^2*x*sin(alpha2)*cos(alpha3)-8*sin(phi)*
y^2*sin(alpha2)*sin(alpha3)-16*S3*sin(phi)^2*cos(alpha2)*sin(alpha3)+12*cos(
phi)*y^2*cos(alpha2)*sin(alpha3)+6*x^2*y*sin(alpha2)*sin(alpha3)+3*L1^2*sin(
phi)*sin(alpha2)*sin(alpha3)-5*R2^2*sin(phi)*sin(alpha2)*sin(alpha3)+S2^2*sin(
phi)*sin(alpha2)*sin(alpha3)+16*S2*sin(phi)^2*sin(alpha2)*sin(alpha3)-6*S2*x^2
*sin(alpha2)*sin(alpha3)+3*L1^2*y*sin(alpha2)*sin(alpha3)-9*R2^2*y*sin(alpha2)
*sin(alpha3)-3*S2^2*y*sin(alpha2)*sin(alpha3)-2*b^2*y*sin(alpha2)*sin(alpha3)+
14*R2*b*sin(alpha2)^2*sin(alpha3)+16*R2*cos(phi)*sin(phi)*cos(alpha3)-16*S2*b*
cos(alpha2)*cos(alpha3)+16*b*x*sin(alpha2)*cos(alpha3)-48*R2*cos(alpha2)*sin(
alpha2)*cos(alpha3)-16*R2*S3*cos(phi)*sin(alpha3)-4*R2*S2*sin(phi)*sin(alpha3)
+6*R2*cos(phi)*x*sin(alpha3)-12*R2*S2*y*sin(alpha3)+22*R2*sin(phi)*y*sin(
alpha3)+8*S2*b*sin(alpha2)*sin(alpha3)+4*b*y*sin(alpha2)*sin(alpha3)+2*y^3*sin
(alpha2)*sin(alpha3)+4*x^3*cos(alpha2)*sin(alpha3)-2*S2^3*sin(alpha2)*sin(
alpha3)+16*x*cos(alpha2)*sin(alpha3)+L1^2*R2*sin(alpha3)-3*R2*S2^2*sin(alpha3)
-2*R2*b^2*sin(alpha3)+16*R2*sin(phi)^2*sin(alpha3)-6*R2*x^2*sin(alpha3)+2*R2*y
^2*sin(alpha3)-y*sin(alpha2)*sin(alpha3)+14*R2*sin(alpha2)^2*sin(alpha3)-3*sin
(phi)*sin(alpha2)*sin(alpha3)-32*S2*cos(alpha2)*cos(alpha3)+32*x*sin(alpha2)*
cos(alpha3)-10*R2*b*sin(alpha3)+6*S2*sin(alpha2)*sin(alpha3), 2*R2*b*y*sin(
alpha2)^2+4*S2*b*x*cos(alpha2)-2*R2^2*b*sin(alpha2)-2*S2^2*b*sin(alpha2)+2*b*x
^2*sin(alpha2)+2*S2*b*y*sin(alpha2)+4*R2*y*sin(alpha2)^2-4*R2*S2*b+14*S2*x*cos
(alpha2)-12*sin(phi)*x*cos(alpha2)+6*cos(phi)*y*cos(alpha2)-4*x*y*cos(alpha2)+
L1^2*sin(alpha2)-5*R2^2*sin(alpha2)-5*S2^2*sin(alpha2)-2*b^2*sin(alpha2)+6*S2*
sin(phi)*sin(alpha2)+4*cos(phi)*x*sin(alpha2)+8*x^2*sin(alpha2)+4*S2*y*sin(
alpha2)-2*sin(phi)*y*sin(alpha2)-10*R2*S2+6*sin(phi)*R2-sin(alpha2), -R2-16*
sin(alpha2)^2*R2-R2^3+L1^2*R2+3*R2*S2^2-2*R2*b^2-2*R2*S2*y+2*R2*y^2-4*R2*b-18*
S2*sin(alpha2)+10*y*sin(alpha2)+2*S2^3*sin(alpha2)-2*y^3*sin(alpha2)+4*sin(phi
)*sin(alpha2)+8*sin(phi)*x*y*cos(alpha2)+8*S2*sin(phi)*y*sin(alpha2)-4*S2*x*y*
cos(alpha2)-6*sin(phi)*y^2*sin(alpha2)-10*R2*b*sin(alpha2)^2+2*R2*sin(phi)*y-\
18*S2*b*sin(alpha2)+6*b*y*sin(alpha2)-10*sin(phi)*x^2*sin(alpha2)-4*cos(phi)*y
^2*cos(alpha2)-4*L1^2*sin(phi)*sin(alpha2)-6*S2^2*sin(phi)*sin(alpha2)+4*S2*x^
2*sin(alpha2)-6*L1^2*y*sin(alpha2)+2*b^2*y*sin(alpha2)+2*R2^2*y*sin(alpha2)-4*
S2^2*y*sin(alpha2)-2*x^2*y*sin(alpha2)-6*R2*S2*sin(phi)-4*S2*b^2*sin(alpha2)+8
*S2*y^2*sin(alpha2)+2*L1^2*S2*sin(alpha2)+4*R2*x^2*sin(alpha2)^2+4*R2*y^2*sin(
alpha2)^2, -2*cos(alpha2)-18*x*R2-6*R2*b*x-6*cos(phi)*R2-9*b*cos(alpha2)+2*L1^
2*cos(alpha2)+5*cos(alpha2)*R2^2-5*S2^2*cos(alpha2)-5*b^2*cos(alpha2)+11*x^2*
cos(alpha2)+3*y^2*cos(alpha2)-b^3*cos(alpha2)-4*S2*b*x*sin(alpha2)+2*R2*b*x*
sin(alpha2)^2+12*cos(phi)*x*cos(alpha2)+6*sin(phi)*y*cos(alpha2)-6*S2*cos(phi)
*sin(alpha2)+6*sin(phi)*x*sin(alpha2)+4*x*sin(alpha2)^2*R2-14*S2*x*sin(alpha2)
+L1^2*b*cos(alpha2)+2*R2^2*b*cos(alpha2)-2*S2^2*b*cos(alpha2)+3*b*x^2*cos(
alpha2)+b*y^2*cos(alpha2)+4*x*y*sin(alpha2), -19*cos(phi)+42*R2*cos(alpha2)-12
*x-18*x*y*sin(phi)-9*x^2*cos(phi)-46*x*sin(phi)^2+4*x*L1^2-4*x*R2^2-2*x*b^2-6*
S2*x*y+2*x*y^2+b*x-8*S2^2*x-60*cos(phi)*x*y*cos(alpha2)*sin(alpha2)+19*cos(phi
)*L1^2+19*cos(phi)*y^2-4*R2^2*cos(phi)-16*S2^2*cos(phi)+4*x^3*sin(alpha2)^2+6*
cos(phi)*sin(alpha2)^2-10*R2*cos(phi)*y*sin(alpha2)+28*sin(phi)*x*y*sin(alpha2
)^2-24*sin(phi)*y^2*cos(alpha2)*sin(alpha2)+16*sin(phi)*x^2*cos(alpha2)*sin(
alpha2)+6*R2*x*y*sin(alpha2)-6*x^2*y*cos(alpha2)*sin(alpha2)-6*L1^2*y*cos(
alpha2)*sin(alpha2)+2*b^2*y*cos(alpha2)*sin(alpha2)-40*S2*sin(phi)*x*sin(
alpha2)^2+20*S2*cos(phi)*y*sin(alpha2)^2-12*cos(phi)*sin(phi)*y*sin(alpha2)^2+
12*S2*x*y*sin(alpha2)^2-2*R2*b*cos(alpha2)*sin(alpha2)^2+16*S2*b*cos(alpha2)*
sin(alpha2)+28*b*y*cos(alpha2)*sin(alpha2)+32*S2*sin(phi)*x-30*S2*cos(phi)*y+
46*cos(phi)*sin(phi)*y+19*cos(alpha2)*R2*b+8*R2*S2^2*cos(alpha2)-2*y^3*cos(
alpha2)*sin(alpha2)+8*S2^3*cos(alpha2)*sin(alpha2)-6*L1^2*cos(phi)*sin(alpha2)
^2+12*sin(phi)^2*x*sin(alpha2)^2+20*S2^2*cos(phi)*sin(alpha2)^2+12*S2^2*x*sin(
alpha2)^2+38*cos(phi)*x^2*sin(alpha2)^2-10*cos(phi)*y^2*sin(alpha2)^2+6*y*cos(
alpha2)*sin(alpha2)-30*b*x*sin(alpha2)^2-12*R2*cos(alpha2)*sin(alpha2)^2, 10*b
^2*y^2+6*S2*y^3-7*b+L1^2-1-9*x^2+24*y^2-6*y*sin(phi)+3*x*cos(phi)-18*b^2-16*b^
3-6*x^2*b+6*L1^2*b-4*b^4-6*y^4+32*b*y^2+3*S2*y+3*S2*sin(phi)-18*sin(phi)*y^3-\
12*sin(phi)^2*y^2-12*cos(phi)*sin(phi)*x*y+6*R2*sin(phi)*y^2*sin(alpha2)+3*R2*
y*sin(alpha2)+3*R2*sin(phi)*sin(alpha2)+6*L1^2*sin(phi)*y-6*sin(phi)*x^2*y+6*
S2*sin(phi)*y^2-6*cos(phi)*x*y^2+6*R2*y^3*sin(alpha2), 12*R2^2*S2^2+12*S2^2*b^
2-12*S2^2*x^2+12*b^2*x^2+18*L1^2*S2*y-18*R2^2*S2*y+18*S2^3*y-12*S2*b^2*y-12*x^
2*y^2+12*S2*x^2*y-18*L1^2*y^2+18*R2^2*y^2+8*b^2*y^2-30*S2^2*y^2+24*S2*y^3-27*
R2^2*b+39*S2^2*b-54*S2*b*y-92*b+32*L1^2-32+72*x^2+72*y^2+42*y*sin(phi)+30*x*
cos(phi)-78*b^2-23*b^3-24*R2^2+69*x^2*b+30*L1^2*b-6*R2^4+12*x^2*R2^2-2*b^4-6*x
^4+24*S2^2-6*S2^4-6*y^4+61*b*y^2-72*S2*y+6*S2*sin(phi)-24*sin(phi)^2*x^2-42*
sin(phi)*y^3+36*sin(phi)^2*y^2-42*cos(phi)*x^3+18*S2^3*sin(phi)+60*cos(phi)*
sin(phi)*x*y-48*R2*b*y*sin(alpha2)-48*S2*cos(phi)*sin(phi)*x-60*S2*cos(phi)*x*
y+12*R2*x^2*y*sin(alpha2)-78*R2*y*sin(alpha2)+6*R2*sin(phi)*sin(alpha2)+18*L1^
2*sin(phi)*y-54*sin(phi)*x^2*y-24*S2^2*sin(phi)*y-48*S2*sin(phi)^2*y+18*S2*sin
(phi)*y^2+36*L1^2*cos(phi)*x-6*R2^2*cos(phi)*x+18*S2^2*cos(phi)*x+66*S2*sin(
phi)*x^2-30*cos(phi)*x*y^2-18*R2^2*S2*sin(phi)+12*R2*y^3*sin(alpha2), -5*sin(
alpha2)-18*R2*S2*b+2*R2*b*y-42*R2*S2+2*R2*y-4*S2*cos(phi)*sin(phi)*x*sin(
alpha2)+8*cos(phi)*sin(phi)*x*y*sin(alpha2)+24*sin(phi)*R2-6*b*sin(alpha2)-22*
R2^2*sin(alpha2)-20*S2^2*sin(alpha2)+35*x^2*sin(alpha2)+5*L1^2*sin(alpha2)-15*
b^2*sin(alpha2)+3*y^2*sin(alpha2)-2*b^3*sin(alpha2)+20*S2*b*x*cos(alpha2)+4*S2
*b*y*sin(alpha2)+2*sin(phi)*x^2*y*sin(alpha2)+2*S2^2*sin(phi)*y*sin(alpha2)-8*
S2*sin(phi)^2*y*sin(alpha2)-4*S2*sin(phi)*y^2*sin(alpha2)-2*R2*cos(phi)*sin(
phi)*x+2*R2*S2*sin(phi)*y-48*sin(phi)*x*cos(alpha2)+24*cos(phi)*y*cos(alpha2)+
24*S2*sin(phi)*sin(alpha2)+18*cos(phi)*x*sin(alpha2)-6*sin(phi)*y*sin(alpha2)+
8*R2*y*sin(alpha2)^2+64*S2*x*cos(alpha2)+12*S2*y*sin(alpha2)-10*R2^2*b*sin(
alpha2)-8*S2^2*b*sin(alpha2)+8*b*x^2*sin(alpha2)-20*x*y*cos(alpha2)-2*sin(phi)
^2*x^2*sin(alpha2)+6*sin(phi)^2*y^2*sin(alpha2)+2*sin(phi)*y^3*sin(alpha2)-4*
R2*sin(phi)^2*y-2*R2*sin(phi)*y^2+4*L1^2*b*sin(alpha2)+2*S2^2*sin(phi)^2*sin(
alpha2)+2*R2*S2*sin(phi)^2, 7*cos(alpha2)+62*x*R2+18*R2*b*x-8*S2*cos(phi)*sin(
phi)*y*sin(alpha2)+24*cos(phi)*R2+34*b*cos(alpha2)-7*L1^2*cos(alpha2)-17*cos(
alpha2)*R2^2+19*S2^2*cos(alpha2)+16*b^2*cos(alpha2)-36*x^2*cos(alpha2)-10*y^2*
cos(alpha2)+2*b^3*cos(alpha2)+20*S2*b*x*sin(alpha2)+6*cos(phi)*sin(phi)*y^2*
sin(alpha2)-8*sin(phi)^2*x*y*sin(alpha2)+2*S2^2*cos(phi)*y*sin(alpha2)-4*S2*
cos(phi)*y^2*sin(alpha2)+2*R2*S2*cos(phi)*y-2*S2*b*y*cos(alpha2)-2*b*x*y*sin(
alpha2)+4*S2*sin(phi)^2*x*sin(alpha2)-2*cos(phi)*sin(phi)*x^2*sin(alpha2)+2*
cos(phi)*x^2*y*sin(alpha2)-4*R2*cos(phi)*sin(phi)*y+2*S2^2*cos(phi)*sin(phi)*
sin(alpha2)+2*R2*S2*cos(phi)*sin(phi)-46*cos(phi)*x*cos(alpha2)-22*sin(phi)*y*
cos(alpha2)+24*S2*cos(phi)*sin(alpha2)-24*sin(phi)*x*sin(alpha2)-4*x*sin(
alpha2)^2*R2+62*S2*x*sin(alpha2)-2*S2*y*cos(alpha2)-4*L1^2*b*cos(alpha2)-6*R2^
2*b*cos(alpha2)+8*S2^2*b*cos(alpha2)-8*b*x^2*cos(alpha2)-2*b*y^2*cos(alpha2)-\
18*x*y*sin(alpha2)+2*cos(phi)*y^3*sin(alpha2)-2*R2*cos(phi)*y^2+2*R2*sin(phi)^
2*x, -16*sin(alpha2)-45*R2*S2*b+5*R2*b*y-104*R2*S2+4*R2*y-16*S2*cos(phi)*sin(
phi)*x*sin(alpha2)+32*cos(phi)*sin(phi)*x*y*sin(alpha2)-4*S2*cos(phi)*x*y*sin(
alpha2)+60*sin(phi)*R2-22*b*sin(alpha2)-56*R2^2*sin(alpha2)-48*S2^2*sin(alpha2
)+92*x^2*sin(alpha2)+16*L1^2*sin(alpha2)-40*b^2*sin(alpha2)+8*y^2*sin(alpha2)-\
5*b^3*sin(alpha2)-R2^3*sin(phi)+52*S2*b*x*cos(alpha2)+10*S2*b*y*sin(alpha2)+6*
sin(phi)*x^2*y*sin(alpha2)+2*S2^2*sin(phi)*y*sin(alpha2)-16*S2*sin(phi)^2*y*
sin(alpha2)-10*S2*sin(phi)*y^2*sin(alpha2)-8*R2*cos(phi)*sin(phi)*x+2*R2*S2*
sin(phi)*y+2*cos(phi)*x*y^2*sin(alpha2)-2*R2*cos(phi)*x*y+2*S2^2*cos(phi)*x*
sin(alpha2)+2*S2*sin(phi)*x^2*sin(alpha2)+2*R2*S2*cos(phi)*x-120*sin(phi)*x*
cos(alpha2)+60*cos(phi)*y*cos(alpha2)+60*S2*sin(phi)*sin(alpha2)+52*cos(phi)*x
*sin(alpha2)-8*sin(phi)*y*sin(alpha2)+16*R2*y*sin(alpha2)^2+164*S2*x*cos(
alpha2)+24*S2*y*sin(alpha2)-26*R2^2*b*sin(alpha2)-19*S2^2*b*sin(alpha2)+19*b*x
^2*sin(alpha2)-52*x*y*cos(alpha2)-16*sin(phi)^2*x^2*sin(alpha2)+16*sin(phi)^2*
y^2*sin(alpha2)+6*sin(phi)*y^3*sin(alpha2)-8*R2*sin(phi)^2*y-5*R2*sin(phi)*y^2
+10*L1^2*b*sin(alpha2)-b*y^2*sin(alpha2)+2*cos(phi)*x^3*sin(alpha2)+R2*sin(phi
)*x^2+3*R2*S2^2*sin(phi)+2*S2^3*sin(phi)*sin(alpha2), 16*cos(alpha2)+156*x*R2+
45*R2*b*x-16*S2*cos(phi)*sin(phi)*y*sin(alpha2)+4*S2*sin(phi)*x*y*sin(alpha2)+
60*cos(phi)*R2+82*b*cos(alpha2)-16*L1^2*cos(alpha2)-44*cos(alpha2)*R2^2+48*S2^
2*cos(alpha2)+40*b^2*cos(alpha2)-92*x^2*cos(alpha2)-24*y^2*cos(alpha2)+5*b^3*
cos(alpha2)-R2^3*cos(phi)+48*S2*b*x*sin(alpha2)+16*cos(phi)*sin(phi)*y^2*sin(
alpha2)-32*sin(phi)^2*x*y*sin(alpha2)+2*S2^2*cos(phi)*y*sin(alpha2)-10*S2*cos(
phi)*y^2*sin(alpha2)+2*R2*S2*cos(phi)*y-6*S2*b*y*cos(alpha2)-4*b*x*y*sin(
alpha2)+16*S2*sin(phi)^2*x*sin(alpha2)-16*cos(phi)*sin(phi)*x^2*sin(alpha2)+6*
cos(phi)*x^2*y*sin(alpha2)-8*R2*cos(phi)*sin(phi)*y-2*sin(phi)*x*y^2*sin(
alpha2)+2*R2*sin(phi)*x*y-2*S2^2*sin(phi)*x*sin(alpha2)+2*S2*cos(phi)*x^2*sin(
alpha2)-2*R2*S2*sin(phi)*x-112*cos(phi)*x*cos(alpha2)-52*sin(phi)*y*cos(alpha2
)+60*S2*cos(phi)*sin(alpha2)-60*sin(phi)*x*sin(alpha2)-16*x*sin(alpha2)^2*R2+
148*S2*x*sin(alpha2)-8*S2*y*cos(alpha2)-10*L1^2*b*cos(alpha2)-16*R2^2*b*cos(
alpha2)+21*S2^2*b*cos(alpha2)-21*b*x^2*cos(alpha2)-5*b*y^2*cos(alpha2)-36*x*y*
sin(alpha2)+6*cos(phi)*y^3*sin(alpha2)-5*R2*cos(phi)*y^2+8*R2*sin(phi)^2*x-2*
sin(phi)*x^3*sin(alpha2)+2*S2^3*cos(phi)*sin(alpha2)+3*R2*S2^2*cos(phi)+R2*cos
(phi)*x^2, -7*sin(alpha2)+R2*b^2*y-R2*y^3-6*R2*S2*b+3*R2*b*y-20*R2*S2+2*R2*y+
R2*S2*y^2+2*R2*S2*b^2-8*S2*cos(phi)*sin(phi)*x*sin(alpha2)+2*cos(phi)*sin(phi)
*x*y*sin(alpha2)-6*S2*cos(phi)*x*y*sin(alpha2)+12*sin(phi)*R2-19*b*sin(alpha2)
-12*R2^2*sin(alpha2)-8*S2^2*sin(alpha2)+25*x^2*sin(alpha2)+7*L1^2*sin(alpha2)-\
29*b^2*sin(alpha2)+16*y^2*sin(alpha2)-13*b^3*sin(alpha2)-2*y^4*sin(alpha2)-2*b
^4*sin(alpha2)+12*S2*b*x*cos(alpha2)+6*S2*b*y*sin(alpha2)-2*sin(phi)*x^2*y*sin
(alpha2)+3*S2^2*sin(phi)*y*sin(alpha2)-8*S2*sin(phi)^2*y*sin(alpha2)-8*S2*sin(
phi)*y^2*sin(alpha2)-4*R2*cos(phi)*sin(phi)*x+3*R2*S2*sin(phi)*y-4*cos(phi)*x*
y^2*sin(alpha2)-3*R2*cos(phi)*x*y+2*S2^2*cos(phi)*x*sin(alpha2)+2*R2*S2*cos(
phi)*x+9*L1^2*sin(phi)*y*sin(alpha2)+4*L1^2*cos(phi)*x*sin(alpha2)+2*S2*b^2*y*
sin(alpha2)-24*sin(phi)*x*cos(alpha2)+12*cos(phi)*y*cos(alpha2)+12*S2*sin(phi)
*sin(alpha2)+14*cos(phi)*x*sin(alpha2)-3*sin(phi)*y*sin(alpha2)+6*R2*y*sin(
alpha2)^2+36*S2*x*cos(alpha2)+10*S2*y*sin(alpha2)-6*R2^2*b*sin(alpha2)+11*b*x^
2*sin(alpha2)-12*x*y*cos(alpha2)+2*sin(phi)^2*y^2*sin(alpha2)-4*sin(phi)*y^3*
sin(alpha2)-4*R2*sin(phi)^2*y-4*R2*sin(phi)*y^2+9*L1^2*b*sin(alpha2)+17*b*y^2*
sin(alpha2)-2*cos(phi)*x^3*sin(alpha2)+3*L1^2*y^2*sin(alpha2)+4*b^2*y^2*sin(
alpha2)+S2^2*y^2*sin(alpha2)-2*x^2*y^2*sin(alpha2)+2*b^2*x^2*sin(alpha2)-2*S2*
y^3*sin(alpha2)+2*S2^2*b^2*sin(alpha2), -48*sin(alpha2)+L1^2*R2*y-R2^3*y-3*R2*
S2^2*y+R2*b^2*y-R2*y^3-139*R2*S2*b+14*R2*b*y-327*R2*S2+9*R2*y+R2*S2*y^2-L1^2*
R2*S2+2*R2*S2*x^2-R2^3*S2+3*R2*S2^3+2*R2*S2*b^2-48*S2*cos(phi)*sin(phi)*x*sin(
alpha2)+84*cos(phi)*sin(phi)*x*y*sin(alpha2)-4*S2*cos(phi)*x*y*sin(alpha2)+190
*sin(phi)*R2-68*b*sin(alpha2)-172*R2^2*sin(alpha2)-156*S2^2*sin(alpha2)+284*x^
2*sin(alpha2)+48*L1^2*sin(alpha2)-140*b^2*sin(alpha2)+34*y^2*sin(alpha2)-28*b^
3*sin(alpha2)-3*R2^3*sin(phi)-2*y^4*sin(alpha2)-2*b^4*sin(alpha2)+2*x^4*sin(
alpha2)+2*S2^4*sin(alpha2)+168*S2*b*x*cos(alpha2)+36*S2*b*y*sin(alpha2)+24*sin
(phi)*x^2*y*sin(alpha2)+14*S2^2*sin(phi)*y*sin(alpha2)-48*S2*sin(phi)^2*y*sin(
alpha2)-40*S2*sin(phi)*y^2*sin(alpha2)-24*R2*cos(phi)*sin(phi)*x+7*R2*S2*sin(
phi)*y+12*cos(phi)*x*y^2*sin(alpha2)-3*R2*cos(phi)*x*y+4*S2^2*cos(phi)*x*sin(
alpha2)-16*S2*sin(phi)*x^2*sin(alpha2)+4*R2*S2*cos(phi)*x+6*L1^2*sin(phi)*y*
sin(alpha2)-4*L1^2*cos(phi)*x*sin(alpha2)+4*S2*b^2*y*sin(alpha2)-R2^2*sin(phi)
*y*sin(alpha2)-4*L1^2*S2*y*sin(alpha2)-384*sin(phi)*x*cos(alpha2)+192*cos(phi)
*y*cos(alpha2)+192*S2*sin(phi)*sin(alpha2)+164*cos(phi)*x*sin(alpha2)-38*sin(
phi)*y*sin(alpha2)+60*R2*y*sin(alpha2)^2+528*S2*x*cos(alpha2)+88*S2*y*sin(
alpha2)-79*R2^2*b*sin(alpha2)-64*S2^2*b*sin(alpha2)+52*b*x^2*sin(alpha2)-168*x
*y*cos(alpha2)-40*sin(phi)^2*x^2*sin(alpha2)+44*sin(phi)^2*y^2*sin(alpha2)+20*
sin(phi)*y^3*sin(alpha2)-24*R2*sin(phi)^2*y-15*R2*sin(phi)*y^2+36*L1^2*b*sin(
alpha2)+8*b*y^2*sin(alpha2)+16*cos(phi)*x^3*sin(alpha2)-3*R2*sin(phi)*x^2+3*R2
*S2^2*sin(phi)+6*L1^2*y^2*sin(alpha2)+4*b^2*y^2*sin(alpha2)+6*S2^2*y^2*sin(
alpha2)-4*S2*y^3*sin(alpha2)+2*sin(phi)*L1^2*R2-4*S2^3*y*sin(alpha2)-3*R2^2*y^
2*sin(alpha2)+4*S2^2*x^2*sin(alpha2)-2*R2^2*x^2*sin(alpha2), -9*sin(alpha2)-L1
^2*R2*y+R2^3*y-3*R2*S2^2*y+R2*b^2*y-R2*y^3-10*R2*S2*b+5*R2*b*y-22*R2*S2+3*R2*y
+3*R2*S2*y^2+2*L1^2*R2*S2-2*R2*S2*x^2-8*S2*cos(phi)*sin(phi)*x*sin(alpha2)+2*
cos(phi)*sin(phi)*x*y*sin(alpha2)-10*S2*cos(phi)*x*y*sin(alpha2)+12*sin(phi)*
R2-23*b*sin(alpha2)-12*R2^2*sin(alpha2)-10*S2^2*sin(alpha2)+33*x^2*sin(alpha2)
+9*L1^2*sin(alpha2)-31*b^2*sin(alpha2)+18*y^2*sin(alpha2)-13*b^3*sin(alpha2)-2
*y^4*sin(alpha2)-2*b^4*sin(alpha2)-2*x^4*sin(alpha2)+12*S2*b*x*cos(alpha2)+10*
S2*b*y*sin(alpha2)-6*sin(phi)*x^2*y*sin(alpha2)+7*S2^2*sin(phi)*y*sin(alpha2)-\
8*S2*sin(phi)^2*y*sin(alpha2)-12*S2*sin(phi)*y^2*sin(alpha2)-4*R2*cos(phi)*sin
(phi)*x+7*R2*S2*sin(phi)*y-8*cos(phi)*x*y^2*sin(alpha2)-5*R2*cos(phi)*x*y+2*S2
^2*cos(phi)*x*sin(alpha2)+8*S2*sin(phi)*x^2*sin(alpha2)+2*R2*S2*cos(phi)*x+9*
L1^2*sin(phi)*y*sin(alpha2)+8*L1^2*cos(phi)*x*sin(alpha2)+2*S2*b^2*y*sin(
alpha2)-2*L1^2*S2*y*sin(alpha2)-24*sin(phi)*x*cos(alpha2)+12*cos(phi)*y*cos(
alpha2)+12*S2*sin(phi)*sin(alpha2)+14*cos(phi)*x*sin(alpha2)+sin(phi)*y*sin(
alpha2)+2*R2*y*sin(alpha2)^2+36*S2*x*cos(alpha2)+8*S2*y*sin(alpha2)-6*R2^2*b*
sin(alpha2)-4*S2^2*b*sin(alpha2)+23*b*x^2*sin(alpha2)-12*x*y*cos(alpha2)+2*sin
(phi)^2*y^2*sin(alpha2)-4*sin(phi)*y^3*sin(alpha2)-4*R2*sin(phi)^2*y-6*R2*sin(
phi)*y^2+9*L1^2*b*sin(alpha2)+17*b*y^2*sin(alpha2)-10*cos(phi)*x^3*sin(alpha2)
+4*R2*sin(phi)*x^2+3*L1^2*y^2*sin(alpha2)+4*b^2*y^2*sin(alpha2)+3*S2^2*y^2*sin
(alpha2)-4*x^2*y^2*sin(alpha2)+4*b^2*x^2*sin(alpha2)-2*S2*y^3*sin(alpha2)-2*S2
^3*y*sin(alpha2)-2*S2^2*x^2*sin(alpha2)+2*L1^2*S2^2*sin(alpha2)+2*L1^2*x^2*sin
(alpha2), sin(alpha2)-6*R2*S2-12*R2*S2*y^2+12*cos(phi)*sin(phi)*x*y*sin(alpha2
)+6*sin(phi)*R2+10*b*sin(alpha2)-3*R2^2*sin(alpha2)-3*S2^2*sin(alpha2)+12*x^2*
sin(alpha2)-L1^2*sin(alpha2)+18*b^2*sin(alpha2)-24*y^2*sin(alpha2)+16*b^3*sin(
alpha2)+6*y^4*sin(alpha2)+4*b^4*sin(alpha2)+6*sin(phi)*x^2*y*sin(alpha2)+12*S2
*sin(phi)*y^2*sin(alpha2)+12*cos(phi)*x*y^2*sin(alpha2)-6*L1^2*sin(phi)*y*sin(
alpha2)-24*sin(phi)*x*y^2*cos(alpha2)+12*S2*x*y^2*cos(alpha2)-12*sin(phi)*x*
cos(alpha2)+6*cos(phi)*y*cos(alpha2)+6*S2*sin(phi)*sin(alpha2)+6*S2*x*cos(
alpha2)+6*b*x^2*sin(alpha2)+12*sin(phi)^2*y^2*sin(alpha2)+6*sin(phi)*y^3*sin(
alpha2)+12*R2*sin(phi)*y^2-6*L1^2*b*sin(alpha2)-26*b*y^2*sin(alpha2)-10*b^2*y^
2*sin(alpha2)-6*S2^2*y^2*sin(alpha2)+6*x^2*y^2*sin(alpha2)-6*R2^2*y^2*sin(
alpha2)+12*cos(phi)*y^3*cos(alpha2), 30*sin(alpha2)+L1^2*R2*y+R2^3*y-3*R2*S2^2
*y-2*R2*b^2*y-6*R2*x^2*y+2*R2*y^3+92*R2*S2*b-10*R2*b*y+236*R2*S2-9*R2*y+16*S2*
cos(phi)*sin(phi)*x*sin(alpha2)-38*cos(phi)*sin(phi)*x*y*sin(alpha2)+6*S2*cos(
phi)*x*y*sin(alpha2)-144*sin(phi)*R2+22*b*sin(alpha2)+118*R2^2*sin(alpha2)+118
*S2^2*sin(alpha2)-204*x^2*sin(alpha2)-30*L1^2*sin(alpha2)+68*b^2*sin(alpha2)-\
17*y^2*sin(alpha2)-4*y^4*sin(alpha2)-4*b^4*sin(alpha2)-108*S2*b*x*cos(alpha2)-\
6*S2*b*y*sin(alpha2)+4*sin(phi)*x^2*y*sin(alpha2)+S2^2*sin(phi)*y*sin(alpha2)+
16*S2*sin(phi)^2*y*sin(alpha2)+4*S2*sin(phi)*y^2*sin(alpha2)+16*R2*cos(phi)*
sin(phi)*x-4*R2*S2*sin(phi)*y-18*cos(phi)*x*y^2*sin(alpha2)+6*R2*cos(phi)*x*y+
9*L1^2*sin(phi)*y*sin(alpha2)-5*R2^2*sin(phi)*y*sin(alpha2)-6*S2*x^2*y*sin(
alpha2)+288*sin(phi)*x*cos(alpha2)-144*cos(phi)*y*cos(alpha2)-144*S2*sin(phi)*
sin(alpha2)-108*cos(phi)*x*sin(alpha2)+27*sin(phi)*y*sin(alpha2)-30*R2*y*sin(
alpha2)^2-360*S2*x*cos(alpha2)-38*S2*y*sin(alpha2)+46*R2^2*b*sin(alpha2)+46*S2
^2*b*sin(alpha2)-52*b*x^2*sin(alpha2)+108*x*y*cos(alpha2)+16*sin(phi)^2*x^2*
sin(alpha2)-22*sin(phi)^2*y^2*sin(alpha2)-14*sin(phi)*y^3*sin(alpha2)+16*R2*
sin(phi)^2*y+10*R2*sin(phi)*y^2-10*L1^2*b*sin(alpha2)+14*b*y^2*sin(alpha2)+3*
L1^2*y^2*sin(alpha2)+8*b^2*y^2*sin(alpha2)+3*S2^2*y^2*sin(alpha2)-2*S2^3*y*sin
(alpha2)-3*R2^2*y^2*sin(alpha2)+4*x^3*y*cos(alpha2), -19*cos(alpha2)-178*x*R2-\
52*R2*b*x+8*S2*cos(phi)*sin(phi)*y*sin(alpha2)-4*S2*sin(phi)*x*y*sin(alpha2)-\
66*cos(phi)*R2-90*b*cos(alpha2)+19*L1^2*cos(alpha2)+55*cos(alpha2)*R2^2-55*S2^
2*cos(alpha2)-46*b^2*cos(alpha2)+96*x^2*cos(alpha2)+28*y^2*cos(alpha2)-8*b^3*
cos(alpha2)-52*S2*b*x*sin(alpha2)-8*cos(phi)*sin(phi)*y^2*sin(alpha2)+16*sin(
phi)^2*x*y*sin(alpha2)-2*S2^2*cos(phi)*y*sin(alpha2)+4*S2*cos(phi)*y^2*sin(
alpha2)-4*R2*S2*cos(phi)*y+2*S2*b*y*cos(alpha2)-2*b*x*y*sin(alpha2)-8*S2*sin(
phi)^2*x*sin(alpha2)+8*cos(phi)*sin(phi)*x^2*sin(alpha2)-2*cos(phi)*x^2*y*sin(
alpha2)+8*R2*cos(phi)*sin(phi)*y+4*sin(phi)*x*y^2*sin(alpha2)-4*R2*sin(phi)*x*
y-2*R2^2*cos(phi)*y*sin(alpha2)+4*sin(phi)*x^2*y*cos(alpha2)+126*cos(phi)*x*
cos(alpha2)+60*sin(phi)*y*cos(alpha2)-66*S2*cos(phi)*sin(alpha2)+66*sin(phi)*x
*sin(alpha2)+16*x*sin(alpha2)^2*R2-162*S2*x*sin(alpha2)+4*S2*y*cos(alpha2)+8*
L1^2*b*cos(alpha2)+22*R2^2*b*cos(alpha2)-22*S2^2*b*cos(alpha2)+22*b*x^2*cos(
alpha2)+8*b*y^2*cos(alpha2)+44*x*y*sin(alpha2)-2*cos(phi)*y^3*sin(alpha2)+4*R2
*cos(phi)*y^2-8*R2*sin(phi)^2*x, -4*R2-78*sin(alpha2)^2*R2-2*R2^3+4*L1^2*R2+2*
L1^2*R2*b+2*R2^3*b-6*R2*S2^2*b-4*R2*b^3-12*R2*b*x^2-24*R2*S2*b*y+4*R2*b*y^2+6*
R2*S2^2-14*R2*b^2-18*R2*x^2-40*R2*S2*y+14*R2*y^2-18*R2*b+24*S2*b*x*y*cos(
alpha2)-84*S2*sin(alpha2)+39*y*sin(alpha2)+16*x^3*cos(alpha2)+4*S2^3*sin(
alpha2)+10*y^3*sin(alpha2)+23*sin(phi)*sin(alpha2)+16*sin(phi)*x*y*cos(alpha2)
-14*cos(phi)*sin(phi)*x*sin(alpha2)+40*S2*sin(phi)*y*sin(alpha2)+40*S2*x*y*cos
(alpha2)-6*S2*cos(phi)*x*sin(alpha2)+22*cos(phi)*x*y*sin(alpha2)-12*S2*b*x^2*
sin(alpha2)+6*L1^2*b*y*sin(alpha2)-18*R2^2*b*y*sin(alpha2)-6*S2^2*b*y*sin(
alpha2)+12*b*x^2*y*sin(alpha2)-14*sin(phi)^2*y*sin(alpha2)+2*sin(phi)*y^2*sin(
alpha2)-44*R2*b*sin(alpha2)^2+16*R2*sin(phi)*y-74*S2*b*sin(alpha2)-8*b*y*sin(
alpha2)-28*sin(phi)*x^2*sin(alpha2)-8*cos(phi)*y^2*cos(alpha2)-23*L1^2*sin(phi
)*sin(alpha2)+3*R2^2*sin(phi)*sin(alpha2)-27*S2^2*sin(phi)*sin(alpha2)-18*S2*x
^2*sin(alpha2)-9*L1^2*y*sin(alpha2)-18*b^2*y*sin(alpha2)-17*R2^2*y*sin(alpha2)
-23*S2^2*y*sin(alpha2)+50*x^2*y*sin(alpha2)-24*R2*S2*sin(phi)-24*x*y^2*cos(
alpha2)-16*S2*b^2*sin(alpha2)+12*S2*y^2*sin(alpha2)+6*L1^2*S2*sin(alpha2)+8*b*
x^3*cos(alpha2)-4*S2^3*b*sin(alpha2)-4*b^3*y*sin(alpha2)+4*b*y^3*sin(alpha2),
14*cos(alpha2)+114*x*R2-6*R2*x*y^2+30*R2*b*x+42*cos(phi)*R2+59*b*cos(alpha2)-\
14*L1^2*cos(alpha2)-36*cos(alpha2)*R2^2+36*S2^2*cos(alpha2)+30*b^2*cos(alpha2)
-60*x^2*cos(alpha2)-18*y^2*cos(alpha2)+8*b^3*cos(alpha2)+2*b^4*cos(alpha2)+3*y
^4*cos(alpha2)+30*S2*b*x*sin(alpha2)+6*cos(phi)*sin(phi)*y^2*sin(alpha2)-6*sin
(phi)^2*x*y*sin(alpha2)-3*S2^2*cos(phi)*y*sin(alpha2)-6*S2*cos(phi)*y^2*sin(
alpha2)-6*R2*S2*cos(phi)*y-6*S2*b*y*cos(alpha2)-6*b*x*y*sin(alpha2)+6*sin(phi)
*x*y^2*sin(alpha2)-3*R2^2*cos(phi)*y*sin(alpha2)+18*cos(phi)*x*y^2*cos(alpha2)
-3*L1^2*cos(phi)*y*sin(alpha2)-6*S2*x*y^2*sin(alpha2)-84*cos(phi)*x*cos(alpha2
)-42*sin(phi)*y*cos(alpha2)+42*S2*cos(phi)*sin(alpha2)-42*sin(phi)*x*sin(
alpha2)+3*cos(phi)*y*sin(alpha2)-12*x*sin(alpha2)^2*R2+102*S2*x*sin(alpha2)-3*
L1^2*b*cos(alpha2)-15*R2^2*b*cos(alpha2)+15*S2^2*b*cos(alpha2)-12*b*x^2*cos(
alpha2)-16*b*y^2*cos(alpha2)-42*x*y*sin(alpha2)+6*cos(phi)*y^3*sin(alpha2)-6*
R2*cos(phi)*y^2+6*sin(phi)*y^3*cos(alpha2)+3*R2^2*y^2*cos(alpha2)-3*S2^2*y^2*
cos(alpha2)+3*x^2*y^2*cos(alpha2)-5*b^2*y^2*cos(alpha2), -7*y+48*S2-14*sin(phi
)+52*R2*sin(alpha2)-45*y*x*cos(phi)-20*y^2*sin(phi)+4*y*sin(phi)^2-80*y^3+16*
S2^3+32*L1^2*S2*b-6*b*y^3-16*R2^2*S2*b+16*S2^3*b-24*S2*b^3-32*S2*b*x^2-68*L1^2
*b*y+16*b*x^2*y+52*R2^2*b*y-36*S2^2*b*y+62*S2*b*y^2+4*L1^2*S2-16*R2^2*S2-48*S2
*b^2-36*S2*x^2-9*L1^2*y+54*R2^2*y+90*b^2*y-38*S2^2*y-45*x^2*y+107*S2*y^2-4*S2*
b+95*b*y+18*b^3*y+128*S2*cos(phi)*sin(phi)*x*y+14*sin(phi)*L1^2+16*sin(phi)*x^
2-18*R2^2*sin(phi)+18*S2^2*sin(phi)+4*S2^4*sin(phi)-32*sin(phi)^2*y^3-12*sin(
phi)*y^4+4*sin(phi)*x^4+4*R2^4*sin(phi)-9*R2*sin(phi)*y*sin(alpha2)-32*S2^2*
cos(phi)*sin(phi)*x+16*R2^2*cos(phi)*x*y-16*S2^2*cos(phi)*x*y+32*S2*cos(phi)*x
*y^2-96*cos(phi)*sin(phi)*x*y^2-16*R2*b*x^2*sin(alpha2)+70*R2*b*y^2*sin(alpha2
)+20*S2*cos(phi)*x+4*cos(phi)*sin(phi)*x-25*S2*sin(phi)*y+24*R2*b*sin(alpha2)+
83*R2*y^2*sin(alpha2)-16*R2*x^2*sin(alpha2)-8*sin(phi)*x^2*y^2+96*sin(phi)^2*x
^2*y-8*R2^2*S2^2*sin(phi)-8*R2^2*sin(phi)*x^2+32*cos(phi)*sin(phi)*x^3+8*S2^2*
sin(phi)*x^2-64*S2*sin(phi)^2*x^2-32*S2^2*sin(phi)^2*y-16*cos(phi)*x^3*y+8*R2^
2*sin(phi)*y^2-24*S2^2*sin(phi)*y^2+64*S2*sin(phi)^2*y^2-16*cos(phi)*x*y^3+32*
S2*sin(phi)*y^3, 13*cos(phi)-18*R2*cos(alpha2)-20*x+38*x*y*sin(phi)+43*x^2*cos
(phi)+10*x*sin(phi)^2+4*x^3+4*x*L1^2-24*x*R2^2-34*x*b^2+32*L1^2*b*x-8*b^3*x-16
*b*x^3+4*b*x*y^2-98*S2*x*y+34*x*y^2-63*b*x-16*R2^2*b*x+32*S2^2*b*x-72*S2*b*x*y
+56*S2^2*x+32*R2*S2*b*y*cos(alpha2)-72*R2*b*x*y*sin(alpha2)-13*cos(phi)*L1^2-5
*cos(phi)*y^2-8*R2^2*cos(phi)+8*S2^2*cos(phi)-32*sin(phi)^2*x^3-12*cos(phi)*y^
4+4*R2^4*cos(phi)+4*S2^4*cos(phi)+4*cos(phi)*x^4+10*R2*cos(phi)*y*sin(alpha2)-\
82*R2*x*y*sin(alpha2)+96*cos(phi)*sin(phi)*x^2*y-12*R2*b*y^2*cos(alpha2)-64*S2
*cos(phi)*sin(phi)*x^2-32*S2^2*cos(phi)*sin(phi)*y-16*R2^2*sin(phi)*x*y+16*S2^
2*sin(phi)*x*y-128*S2*sin(phi)^2*x*y+64*S2*cos(phi)*sin(phi)*y^2-32*S2*sin(phi
)*x*y^2-16*R2*S2^2*b*cos(alpha2)+32*R2*S2*y*cos(alpha2)-16*S2*sin(phi)*x+10*S2
*cos(phi)*y-10*cos(phi)*sin(phi)*y-9*cos(alpha2)*R2*b-12*R2*y^2*cos(alpha2)-16
*R2*S2^2*cos(alpha2)+16*sin(phi)*x^3*y+96*sin(phi)^2*x*y^2-8*cos(phi)*x^2*y^2+
32*S2*cos(phi)*y^3-32*cos(phi)*sin(phi)*y^3+16*sin(phi)*x*y^3-8*R2^2*S2^2*cos(
phi)+32*S2^2*sin(phi)^2*x-8*R2^2*cos(phi)*x^2+8*S2^2*cos(phi)*x^2+8*R2^2*cos(
phi)*y^2-24*S2^2*cos(phi)*y^2, 15*y-4*S2+3*sin(phi)-6*R2*sin(alpha2)-6*y*x*cos
(phi)-6*y*sin(phi)^2+36*y^3-20*S2*b^2*y^2-6*L1^2*y^3+6*R2^2*y^3-6*S2^2*y^3+12*
S2*y^4-12*L1^2*S2*b+12*b*y^3+32*S2*b^3+12*S2*b*x^2+6*L1^2*b*y-6*R2^2*b*y+6*S2^
2*b*y-76*S2*b*y^2-2*L1^2*S2+36*S2*b^2+18*S2*x^2-9*L1^2*y-9*R2^2*y+6*b^2*y+9*S2
^2*y-6*x^2*y-84*S2*y^2+14*S2*b+12*b*y+8*S2*b^4+48*S2*cos(phi)*sin(phi)*x*y-3*
sin(phi)*L1^2+3*R2^2*sin(phi)-3*S2^2*sin(phi)-36*sin(phi)^2*y^3-12*sin(phi)*y^
4+12*R2^2*cos(phi)*x*y-12*S2^2*cos(phi)*x*y+36*S2*cos(phi)*x*y^2-60*cos(phi)*
sin(phi)*x*y^2-12*L1^2*cos(phi)*x*y-6*S2*cos(phi)*x-6*cos(phi)*sin(phi)*x-12*
R2*y^2*sin(alpha2)+24*sin(phi)^2*x^2*y+18*R2^2*sin(phi)*y^2-18*S2^2*sin(phi)*y
^2+48*S2*sin(phi)^2*y^2-12*cos(phi)*x*y^3+48*S2*sin(phi)*y^3-18*L1^2*sin(phi)*
y^2, 67*y+12*S2+3*sin(phi)-6*R2*sin(alpha2)-54*y*x*cos(phi)-104*y^2*sin(phi)-6
*y*sin(phi)^2-8*y^3-32*S2^3+8*S2^5+8*R2^4*S2-16*R2^2*S2^3-16*S2^3*b^2-16*R2^2*
S2*x^2+16*S2^3*x^2-16*S2*b^2*x^2+8*S2*x^4-24*L1^2*S2^2*y+24*R2^2*S2^2*y-24*S2^
4*y+16*S2^2*b^2*y-8*L1^2*x^2*y+8*R2^2*x^2*y-8*S2^2*x^2*y+24*L1^2*S2*y^2-24*R2^
2*S2*y^2+40*S2^3*y^2-4*S2*b^2*y^2+16*S2*x^2*y^2-6*L1^2*y^3+6*R2^2*y^3-22*S2^2*
y^3+4*S2*y^4-12*L1^2*S2*b+12*b*y^3+36*R2^2*S2*b-52*S2^3*b-4*S2*b^3-24*S2*b*x^2
+18*L1^2*b*y+16*b*x^2*y-18*R2^2*b*y+26*S2^2*b*y-56*S2*b*y^2-18*L1^2*S2+32*R2^2
*S2+20*S2*b^2+66*S2*x^2-61*L1^2*y-25*R2^2*y+50*b^2*y+17*S2^2*y-138*x^2*y+52*S2
*y^2+46*S2*b+104*b*y+48*S2*cos(phi)*sin(phi)*x*y-3*sin(phi)*L1^2-48*sin(phi)*x
^2+3*R2^2*sin(phi)-3*S2^2*sin(phi)-24*S2^4*sin(phi)-36*sin(phi)^2*y^3-12*sin(
phi)*y^4+64*S2^2*cos(phi)*sin(phi)*x+68*R2^2*cos(phi)*x*y+12*S2^2*cos(phi)*x*y
+124*S2*cos(phi)*x*y^2-172*cos(phi)*sin(phi)*x*y^2-68*L1^2*cos(phi)*x*y+8*R2^2
*S2*cos(phi)*x-48*S2*sin(phi)*x^2*y-38*S2*cos(phi)*x-6*cos(phi)*sin(phi)*x-32*
S2*sin(phi)*y+100*R2*y^2*sin(alpha2)+96*R2*x^2*sin(alpha2)-16*sin(phi)*x^2*y^2
+232*sin(phi)^2*x^2*y+24*R2^2*S2^2*sin(phi)-48*R2^2*sin(phi)*x^2+96*cos(phi)*
sin(phi)*x^3-40*S2^2*sin(phi)*x^2-64*S2*sin(phi)^2*x^2+64*S2^2*sin(phi)^2*y-16
*cos(phi)*x^3*y+18*R2^2*sin(phi)*y^2-42*S2^2*sin(phi)*y^2-16*S2*sin(phi)^2*y^2
-12*cos(phi)*x*y^3+64*S2*sin(phi)*y^3-18*L1^2*sin(phi)*y^2-24*S2^3*cos(phi)*x+
48*L1^2*sin(phi)*x^2+8*S2*cos(phi)*x^3+32*S2^3*sin(phi)*y, -2*cos(alpha2)*sin(
alpha3)-6*S2*b*x*sin(alpha2)*sin(alpha3)+2*R2*S3*b*sin(alpha2)^2*sin(alpha3)-4
*S2*b*x*cos(alpha2)*cos(alpha3)+2*S2*S3*b*sin(alpha2)*sin(alpha3)+sin(alpha2)*
cos(alpha3)-2*b*x^2*sin(alpha2)*cos(alpha3)+4*x*y*sin(alpha2)*sin(alpha3)+2*R2
^2*b*cos(alpha2)*sin(alpha3)-2*S2^2*b*cos(alpha2)*sin(alpha3)+3*b*x^2*cos(
alpha2)*sin(alpha3)+b*y^2*cos(alpha2)*sin(alpha3)-14*S2*x*cos(alpha2)*cos(
alpha3)+12*sin(phi)*x*cos(alpha2)*cos(alpha3)-6*S2*sin(phi)*sin(alpha2)*cos(
alpha3)-6*R2*b*x*sin(alpha3)+6*cos(phi)*x*cos(alpha2)*sin(alpha3)+6*S3*cos(phi
)*cos(alpha2)*sin(alpha3)+6*sin(phi)*y*cos(alpha2)*sin(alpha3)-6*S2*cos(phi)*
sin(alpha2)*sin(alpha3)-18*S2*x*sin(alpha2)*sin(alpha3)+8*sin(phi)*x*sin(
alpha2)*sin(alpha3)+2*R2^2*b*sin(alpha2)*cos(alpha3)+2*S2^2*b*sin(alpha2)*cos(
alpha3)+L1^2*b*cos(alpha2)*sin(alpha3)+4*R2*S3*sin(alpha2)^2*sin(alpha3)+4*R2*
S2*b*cos(alpha3)-4*cos(phi)*x*sin(alpha2)*cos(alpha3)-4*S3*x*cos(alpha2)*sin(
alpha3)+4*S2*S3*sin(alpha2)*sin(alpha3)-2*S3*sin(phi)*sin(alpha2)*sin(alpha3)-
L1^2*sin(alpha2)*cos(alpha3)+2*b^2*sin(alpha2)*cos(alpha3)+5*R2^2*sin(alpha2)*
cos(alpha3)+5*S2^2*sin(alpha2)*cos(alpha3)-8*x^2*sin(alpha2)*cos(alpha3)+2*L1^
2*cos(alpha2)*sin(alpha3)-5*b^2*cos(alpha2)*sin(alpha3)+5*R2^2*cos(alpha2)*sin
(alpha3)-5*S2^2*cos(alpha2)*sin(alpha3)+15*x^2*cos(alpha2)*sin(alpha3)+3*y^2*
cos(alpha2)*sin(alpha3)+10*R2*S2*cos(alpha3)-6*R2*sin(phi)*cos(alpha3)-6*R2*
cos(phi)*sin(alpha3)-18*R2*x*sin(alpha3)-9*b*cos(alpha2)*sin(alpha3)-b^3*cos(
alpha2)*sin(alpha3), 2*R2*S3*b*cos(alpha2)*sin(alpha2)*sin(alpha3)+4*R2*S3*cos
(alpha2)*sin(alpha2)*sin(alpha3)+4*S2*b*x*sin(alpha2)*cos(alpha3)+2*S2*S3*b*
cos(alpha2)*sin(alpha3)-6*S2*b*x*cos(alpha2)*sin(alpha3)+2*sin(alpha2)*sin(
alpha3)+cos(alpha2)*cos(alpha3)+8*sin(phi)*x*cos(alpha2)*sin(alpha3)+4*x*y*cos
(alpha2)*sin(alpha3)-10*cos(phi)*x*cos(alpha2)*cos(alpha3)+4*S2*S3*cos(alpha2)
*sin(alpha3)+4*S3*sin(phi)*cos(alpha2)*sin(alpha3)-18*S2*x*cos(alpha2)*sin(
alpha3)-6*cos(phi)*x*sin(alpha2)*sin(alpha3)-2*R2^2*b*cos(alpha2)*cos(alpha3)+
2*S2^2*b*cos(alpha2)*cos(alpha3)-2*b*x^2*cos(alpha2)*cos(alpha3)-L1^2*b*sin(
alpha2)*sin(alpha3)+2*R2^2*b*sin(alpha2)*sin(alpha3)+2*S2^2*b*sin(alpha2)*sin(
alpha3)-3*b*x^2*sin(alpha2)*sin(alpha3)-b*y^2*sin(alpha2)*sin(alpha3)+4*R2*b*x
*cos(alpha3)+6*S2*cos(phi)*sin(alpha2)*cos(alpha3)+14*S2*x*sin(alpha2)*cos(
alpha3)-6*sin(phi)*x*sin(alpha2)*cos(alpha3)+4*R2*S2*b*sin(alpha3)-6*cos(phi)*
y*cos(alpha2)*sin(alpha3)-6*S2*sin(phi)*sin(alpha2)*sin(alpha3)+4*S3*x*sin(
alpha2)*sin(alpha3)-L1^2*cos(alpha2)*cos(alpha3)+2*b^2*cos(alpha2)*cos(alpha3)
-8*x^2*cos(alpha2)*cos(alpha3)-2*L1^2*sin(alpha2)*sin(alpha3)+5*b^2*sin(alpha2
)*sin(alpha3)-15*x^2*sin(alpha2)*sin(alpha3)-3*y^2*sin(alpha2)*sin(alpha3)+14*
R2*x*cos(alpha3)+6*b*cos(alpha2)*cos(alpha3)+3*b*sin(alpha2)*sin(alpha3)+b^3*
sin(alpha2)*sin(alpha3)-5*R2^2*cos(alpha2)*cos(alpha3)+5*S2^2*cos(alpha2)*cos(
alpha3)+5*R2^2*sin(alpha2)*sin(alpha3)+5*S2^2*sin(alpha2)*sin(alpha3)+6*R2*cos
(phi)*cos(alpha3)+10*R2*S2*sin(alpha3)-6*R2*sin(phi)*sin(alpha3), cos(alpha3)+
6*R2*S3*sin(phi)*y*sin(alpha2)*sin(alpha3)+6*R2*S3*y^2*sin(alpha2)*sin(alpha3)
-6*R2*x*y^2*sin(alpha2)*sin(alpha3)-12*S3*cos(phi)*sin(phi)*x*sin(alpha3)-6*S3
*cos(phi)*x*y*sin(alpha3)+6*S2*S3*sin(phi)*y*sin(alpha3)-6*R2*cos(phi)*y^2*sin
(alpha2)*sin(alpha3)+3*R2*b*y*cos(alpha2)*sin(alpha3)+4*b^4*cos(alpha3)+16*b^3
*cos(alpha3)-L1^2*cos(alpha3)+18*b^2*cos(alpha3)+9*x^2*cos(alpha3)+7*b*cos(
alpha3)+6*L1^2*S3*sin(phi)*sin(alpha3)-6*L1^2*sin(phi)*x*sin(alpha3)+12*cos(
phi)*sin(phi)*x^2*sin(alpha3)-6*S3*sin(phi)*x^2*sin(alpha3)+10*S3*b^2*y*sin(
alpha3)-12*S3*sin(phi)^2*y*sin(alpha3)-10*b^2*x*y*sin(alpha3)+6*sin(phi)^2*x*y
*sin(alpha3)+3*cos(phi)*x^2*y*sin(alpha3)+6*S2*S3*y^2*sin(alpha3)-18*S3*sin(
phi)*y^2*sin(alpha3)-6*S2*x*y^2*sin(alpha3)+12*sin(phi)*x*y^2*sin(alpha3)-3*R2
*sin(phi)*sin(alpha2)*cos(alpha3)+32*S3*b*y*sin(alpha3)-29*b*x*y*sin(alpha3)+3
*R2*S3*sin(alpha2)*sin(alpha3)-3*R2*x*sin(alpha2)*sin(alpha3)+3*L1^2*cos(phi)*
y*sin(alpha3)+6*cos(phi)*sin(phi)*y^2*sin(alpha3)-6*S2*cos(phi)*y^2*sin(alpha3
)+6*R2*y*cos(alpha2)*sin(alpha3)+6*x*y^3*sin(alpha3)+6*sin(phi)*x^3*sin(alpha3
)-6*S3*y^3*sin(alpha3)-6*L1^2*b*cos(alpha3)+6*b*x^2*cos(alpha3)-3*cos(phi)*x*
cos(alpha3)-3*S2*sin(phi)*cos(alpha3)+3*S2*S3*sin(alpha3)-6*S3*sin(phi)*sin(
alpha3)-3*S2*x*sin(alpha3)-24*x*y*sin(alpha3)+6*sin(phi)*x*sin(alpha3)+24*S3*y
*sin(alpha3)+3*cos(phi)*y^3*sin(alpha3)-3*cos(phi)*y*sin(alpha3), 32*cos(
alpha3)+12*R2*S3*y^2*sin(alpha2)*sin(alpha3)-12*R2*x*y^2*sin(alpha2)*sin(
alpha3)+60*S3*cos(phi)*sin(phi)*x*sin(alpha3)-30*S3*cos(phi)*x*y*sin(alpha3)+
18*S2*S3*sin(phi)*y*sin(alpha3)+12*R2*S3*x^2*sin(alpha2)*sin(alpha3)+48*S2*cos
(phi)*sin(phi)*x*cos(alpha3)-60*S2*S3*cos(phi)*x*sin(alpha3)-18*S2*sin(phi)*x*
y*sin(alpha3)-48*R2*S3*b*sin(alpha2)*sin(alpha3)+48*R2*b*x*sin(alpha2)*sin(
alpha3)+2*b^4*cos(alpha3)+23*b^3*cos(alpha3)-32*L1^2*cos(alpha3)+78*b^2*cos(
alpha3)-72*x^2*cos(alpha3)+92*b*cos(alpha3)+6*R2^4*cos(alpha3)+6*S2^4*cos(
alpha3)+6*x^4*cos(alpha3)+24*R2^2*cos(alpha3)-24*S2^2*cos(alpha3)+18*L1^2*S3*
sin(phi)*sin(alpha3)-18*L1^2*sin(phi)*x*sin(alpha3)-60*cos(phi)*sin(phi)*x^2*
sin(alpha3)-54*S3*sin(phi)*x^2*sin(alpha3)+8*S3*b^2*y*sin(alpha3)+36*S3*sin(
phi)^2*y*sin(alpha3)-8*b^2*x*y*sin(alpha3)-36*sin(phi)^2*x*y*sin(alpha3)+30*
cos(phi)*x^2*y*sin(alpha3)+24*S2*S3*y^2*sin(alpha3)-42*S3*sin(phi)*y^2*sin(
alpha3)-24*S2*x*y^2*sin(alpha3)+42*sin(phi)*x*y^2*sin(alpha3)-6*R2*sin(phi)*
sin(alpha2)*cos(alpha3)+61*S3*b*y*sin(alpha3)-61*b*x*y*sin(alpha3)-78*R2*S3*
sin(alpha2)*sin(alpha3)+78*R2*x*sin(alpha2)*sin(alpha3)-36*L1^2*cos(phi)*x*cos
(alpha3)-18*L1^2*S3*y*sin(alpha3)+18*L1^2*x*y*sin(alpha3)-12*S3*x^2*y*sin(
alpha3)-12*R2*x^3*sin(alpha2)*sin(alpha3)+18*R2^2*S2*sin(phi)*cos(alpha3)+6*R2
^2*cos(phi)*x*cos(alpha3)-18*S2^2*cos(phi)*x*cos(alpha3)-66*S2*sin(phi)*x^2*
cos(alpha3)+18*L1^2*S2*S3*sin(alpha3)-18*R2^2*S2*S3*sin(alpha3)-12*S2*S3*b^2*
sin(alpha3)-24*S2^2*S3*sin(phi)*sin(alpha3)-48*S2*S3*sin(phi)^2*sin(alpha3)-18
*L1^2*S2*x*sin(alpha3)+18*R2^2*S2*x*sin(alpha3)+12*S2*b^2*x*sin(alpha3)+24*S2^
2*sin(phi)*x*sin(alpha3)+48*S2*sin(phi)^2*x*sin(alpha3)+12*S2*S3*x^2*sin(
alpha3)+60*S2*cos(phi)*x^2*sin(alpha3)+18*R2^2*S3*y*sin(alpha3)-30*S2^2*S3*y*
sin(alpha3)-18*R2^2*x*y*sin(alpha3)+30*S2^2*x*y*sin(alpha3)-54*S2*S3*b*sin(
alpha3)+54*S2*b*x*sin(alpha3)+6*x*y^3*sin(alpha3)+54*sin(phi)*x^3*sin(alpha3)-\
6*S3*y^3*sin(alpha3)-30*L1^2*b*cos(alpha3)-69*b*x^2*cos(alpha3)-30*cos(phi)*x*
cos(alpha3)-6*S2*sin(phi)*cos(alpha3)-72*S2*S3*sin(alpha3)+42*S3*sin(phi)*sin(
alpha3)+72*S2*x*sin(alpha3)-72*x*y*sin(alpha3)-42*sin(phi)*x*sin(alpha3)+72*S3
*y*sin(alpha3)+12*x^3*y*sin(alpha3)+42*cos(phi)*x^3*cos(alpha3)+24*sin(phi)^2*
x^2*cos(alpha3)-12*b^2*x^2*cos(alpha3)-12*R2^2*S2^2*cos(alpha3)-12*S2^2*b^2*
cos(alpha3)-18*S2^3*sin(phi)*cos(alpha3)-12*R2^2*x^2*cos(alpha3)+12*S2^2*x^2*
cos(alpha3)+18*S2^3*S3*sin(alpha3)-18*S2^3*x*sin(alpha3)-12*S2*x^3*sin(alpha3)
+27*R2^2*b*cos(alpha3)-39*S2^2*b*cos(alpha3), -24*S3*cos(phi)*sin(phi)*x*sin(
alpha2)*sin(alpha3)-24*S2*S3*sin(phi)*y*sin(alpha2)*sin(alpha3)-24*S3*cos(phi)
*x*y*sin(alpha2)*sin(alpha3)+3*cos(alpha2)*sin(alpha3)+48*S3*sin(phi)*x*y*cos(
alpha2)*sin(alpha3)-24*S2*S3*x*y*cos(alpha2)*sin(alpha3)+12*S2*b*x*sin(alpha2)
*sin(alpha3)+60*cos(phi)*x*y^2*cos(alpha2)*sin(alpha3)-24*S3*cos(phi)*y^2*cos(
alpha2)*sin(alpha3)+12*L1^2*S3*sin(phi)*sin(alpha2)*sin(alpha3)-12*L1^2*sin(
phi)*x*sin(alpha2)*sin(alpha3)-12*S3*sin(phi)*x^2*sin(alpha2)*sin(alpha3)+24*
cos(phi)*sin(phi)*x^2*sin(alpha2)*sin(alpha3)+12*R2^2*S3*y*sin(alpha2)*sin(
alpha3)+12*S2^2*S3*y*sin(alpha2)*sin(alpha3)+20*S3*b^2*y*sin(alpha2)*sin(
alpha3)+6*L1^2*cos(phi)*y*sin(alpha2)*sin(alpha3)+12*sin(phi)^2*x*y*sin(alpha2
)*sin(alpha3)-24*S3*sin(phi)^2*y*sin(alpha2)*sin(alpha3)-20*b^2*x*y*sin(alpha2
)*sin(alpha3)-12*S3*x^2*y*sin(alpha2)*sin(alpha3)+6*cos(phi)*x^2*y*sin(alpha2)
*sin(alpha3)-36*S2*cos(phi)*y^2*sin(alpha2)*sin(alpha3)-12*S3*sin(phi)*y^2*sin
(alpha2)*sin(alpha3)+12*cos(phi)*sin(phi)*y^2*sin(alpha2)*sin(alpha3)-36*S2*x*
y^2*sin(alpha2)*sin(alpha3)+48*sin(phi)*x*y^2*sin(alpha2)*sin(alpha3)+24*R2*S2
*S3*y*sin(alpha3)-24*R2*S3*sin(phi)*y*sin(alpha3)-6*S2*b*y*cos(alpha2)*sin(
alpha3)+52*S3*b*y*sin(alpha2)*sin(alpha3)-58*b*x*y*sin(alpha2)*sin(alpha3)+2*
sin(alpha2)*cos(alpha3)+12*b*x^2*sin(alpha2)*cos(alpha3)-60*x*y*sin(alpha2)*
sin(alpha3)-6*R2^2*b*cos(alpha2)*sin(alpha3)+6*S2^2*b*cos(alpha2)*sin(alpha3)-\
6*b*x^2*cos(alpha2)*sin(alpha3)-36*b*y^2*cos(alpha2)*sin(alpha3)+12*S2*x*cos(
alpha2)*cos(alpha3)-24*sin(phi)*x*cos(alpha2)*cos(alpha3)+12*S2*sin(phi)*sin(
alpha2)*cos(alpha3)+12*R2*b*x*sin(alpha3)-18*cos(phi)*x*cos(alpha2)*sin(alpha3
)-12*S3*cos(phi)*cos(alpha2)*sin(alpha3)-12*sin(phi)*y*cos(alpha2)*sin(alpha3)
+18*S2*cos(phi)*sin(alpha2)*sin(alpha3)+42*S2*x*sin(alpha2)*sin(alpha3)-18*sin
(phi)*x*sin(alpha2)*sin(alpha3)+12*x*y^3*sin(alpha2)*sin(alpha3)+18*x^2*y^2*
cos(alpha2)*sin(alpha3)+6*L1^2*y^2*cos(alpha2)*sin(alpha3)+12*R2^2*y^2*cos(
alpha2)*sin(alpha3)+24*sin(phi)*y^3*cos(alpha2)*sin(alpha3)-12*S2^2*y^2*cos(
alpha2)*sin(alpha3)-6*b^2*y^2*cos(alpha2)*sin(alpha3)+12*sin(phi)*x^3*sin(
alpha2)*sin(alpha3)-12*S3*y^3*sin(alpha2)*sin(alpha3)+6*cos(phi)*y^3*sin(
alpha2)*sin(alpha3)-12*L1^2*b*sin(alpha2)*cos(alpha3)-36*R2*cos(phi)*y^2*sin(
alpha3)-36*R2*x*y^2*sin(alpha3)-12*S2*y*cos(alpha2)*sin(alpha3)+48*S3*y*sin(
alpha2)*sin(alpha3)-6*cos(phi)*y*sin(alpha2)*sin(alpha3)-2*L1^2*sin(alpha2)*
cos(alpha3)+36*b^2*sin(alpha2)*cos(alpha3)-6*R2^2*sin(alpha2)*cos(alpha3)-6*S2
^2*sin(alpha2)*cos(alpha3)+24*x^2*sin(alpha2)*cos(alpha3)-3*L1^2*cos(alpha2)*
sin(alpha3)+6*b^2*cos(alpha2)*sin(alpha3)-15*R2^2*cos(alpha2)*sin(alpha3)+15*
S2^2*cos(alpha2)*sin(alpha3)-24*x^2*cos(alpha2)*sin(alpha3)-6*y^2*cos(alpha2)*
sin(alpha3)-12*R2*S2*cos(alpha3)+12*R2*sin(phi)*cos(alpha3)+18*R2*cos(phi)*sin
(alpha3)+42*R2*x*sin(alpha3)+18*b*cos(alpha2)*sin(alpha3)+6*y^4*cos(alpha2)*
sin(alpha3)+8*b^4*sin(alpha2)*cos(alpha3)+32*b^3*sin(alpha2)*cos(alpha3)+20*b*
sin(alpha2)*cos(alpha3), 3*cos(phi)-6*R2*cos(alpha2)-2*x-6*x*y*sin(phi)+9*x^2*
cos(phi)+6*x*sin(phi)^2-18*x^3+2*x*L1^2-36*x*b^2-12*x*y^4-8*b^4*x+20*b^2*x*y^2
+12*S2*x*y^3+12*L1^2*b*x-32*b^3*x-12*b*x^3+58*b*x*y^2+6*S2*x*y+48*x*y^2-17*b*x
-3*cos(phi)*L1^2+3*cos(phi)*y^2-6*cos(phi)*y^4+6*R2*cos(phi)*y*sin(alpha2)+6*
R2*x*y*sin(alpha2)-24*cos(phi)*sin(phi)*x^2*y-6*R2*b*y^2*cos(alpha2)+12*R2*cos
(phi)*y^3*sin(alpha2)+12*R2*x*y^3*sin(alpha2)+12*L1^2*sin(phi)*x*y+6*S2*cos(
phi)*y-6*cos(phi)*sin(phi)*y-3*cos(alpha2)*R2*b-12*R2*y^2*cos(alpha2)-12*sin(
phi)*x^3*y-12*sin(phi)^2*x*y^2-6*cos(phi)*x^2*y^2+12*S2*cos(phi)*y^3-12*cos(
phi)*sin(phi)*y^3-24*sin(phi)*x*y^3-6*L1^2*cos(phi)*y^2, 20*b^3*y^2+12*S2*b*y^
3-12*b*y^4-30*x^2*y^2+30*L1^2*y^2+110*b^2*y^2+24*S2*y^3-75*b+13*L1^2-13-45*x^2
+156*y^2-66*y*sin(phi)+21*x*cos(phi)-176*b^2-166*b^3-24*x^2*b+56*L1^2*b-8*b^5-\
60*b^4-54*y^4+248*b*y^2+45*S2*y+21*S2*sin(phi)+24*sin(phi)^2*x^2-90*sin(phi)*y
^3-60*sin(phi)^2*y^2-12*cos(phi)*x^3-84*cos(phi)*sin(phi)*x*y+12*R2*b*y^3*sin(
alpha2)+45*R2*y*sin(alpha2)+21*R2*sin(phi)*sin(alpha2)+78*L1^2*sin(phi)*y-30*
sin(phi)*x^2*y+12*L1^2*cos(phi)*x-72*cos(phi)*x*y^2+24*R2*y^3*sin(alpha2), -36
*R2*x*y^3+12*R2*b*x*y+24*R2*x*y-12*L1^2*sin(phi)*x*y*sin(alpha2)+24*cos(phi)*
sin(phi)*x^2*y*sin(alpha2)+12*S2*b*x*y*sin(alpha2)+2*x*sin(alpha2)-3*y^3*cos(
alpha2)+18*x^3*sin(alpha2)-3*cos(phi)*sin(alpha2)+6*y^5*cos(alpha2)+6*cos(phi)
*sin(phi)*y*sin(alpha2)+3*R2*b*cos(alpha2)*sin(alpha2)+24*S2*x*y*sin(alpha2)+
60*cos(phi)*x*y^3*cos(alpha2)+12*sin(phi)*x^3*y*sin(alpha2)+6*L1^2*cos(phi)*y^
2*sin(alpha2)+6*cos(phi)*x^2*y^2*sin(alpha2)-20*b^2*x*y^2*sin(alpha2)+12*sin(
phi)^2*x*y^2*sin(alpha2)-36*S2*cos(phi)*y^3*sin(alpha2)-6*sin(phi)^2*x*sin(
alpha2)-3*cos(phi)*y^2*sin(alpha2)+17*b*x*sin(alpha2)+6*R2*cos(alpha2)*sin(
alpha2)-9*R2^2*y*cos(alpha2)-9*cos(phi)*x^2*sin(alpha2)+9*S2^2*y*cos(alpha2)-\
15*x^2*y*cos(alpha2)+3*b^2*y*cos(alpha2)+3*L1^2*cos(phi)*sin(alpha2)-60*x*y^2*
sin(alpha2)-12*S2*y^2*cos(alpha2)+36*b^2*x*sin(alpha2)-2*L1^2*x*sin(alpha2)+12
*x*y^4*sin(alpha2)+6*L1^2*y^3*cos(alpha2)+12*R2^2*y^3*cos(alpha2)-12*S2^2*y^3*
cos(alpha2)+18*x^2*y^3*cos(alpha2)-6*b^2*y^3*cos(alpha2)+24*sin(phi)*y^4*cos(
alpha2)+8*b^4*x*sin(alpha2)+6*cos(phi)*y^4*sin(alpha2)-36*R2*cos(phi)*y^3-36*b
*y^3*cos(alpha2)+32*b^3*x*sin(alpha2)+12*b*x^3*sin(alpha2)+12*cos(phi)*sin(phi
)*y^3*sin(alpha2)-36*S2*x*y^3*sin(alpha2)+48*sin(phi)*x*y^3*sin(alpha2)-6*R2^2
*b*y*cos(alpha2)+6*S2^2*b*y*cos(alpha2)-6*b*x^2*y*cos(alpha2)-6*S2*b*y^2*cos(
alpha2)-12*L1^2*b*x*sin(alpha2)-58*b*x*y^2*sin(alpha2), 24*b*x^4+42*L1^2*b*y^2
-54*R2^2*b*y^2+78*S2^2*b*y^2+212*b^3*y^2+52*b*x^2*y^2-12*S2*b*y^3-108*b*y^4+12
*R2^2*S2^2+12*S2^2*b^2-12*x^4*y^2-4*L1^2*x^2-12*S2^2*x^2+84*b^2*x^2+18*L1^2*S2
*y-18*R2^2*S2*y+18*S2^3*y-12*S2*b^2*y-210*x^2*y^2+348*L1^2*y^2-30*R2^2*y^2+986
*b^2*y^2+18*S2^2*y^2+48*S2*y^3-27*R2^2*b+39*S2^2*b-66*S2*b*y-837*b+163*L1^2-\
163-347*x^2+1539*y^2-594*y*sin(phi)+234*x*cos(phi)-1811*b^2-1654*b^3-24*R2^2-\
122*x^2*b+581*L1^2*b-6*R2^4+12*x^2*R2^2-80*b^5-594*b^4+30*x^4+24*S2^2-6*S2^4-\
444*y^4+12*b^4*y^2+24*R2^2*S2^2*y^2-12*S2^4*y^2+24*S2^2*b^2*y^2+12*y^6-24*S2^2
*x^2*y^2+36*L1^2*S2*y^3-36*R2^2*S2*y^3+36*S2^3*y^3-24*S2*b^2*y^3-36*L1^2*y^4+
36*R2^2*y^4-60*S2^2*y^4-24*b^2*y^4+24*S2*y^5-24*L1^2*b*x^2+64*b^3*x^2+2312*b*y
^2+348*S2*y+16*b^4*x^2-12*R2^4*y^2+24*R2^2*x^2*y^2-16*b^2*x^2*y^2-96*S2*cos(
phi)*sin(phi)*x*y^2+210*S2*sin(phi)+204*sin(phi)^2*x^2-780*sin(phi)*y^3-552*
sin(phi)^2*y^2-180*cos(phi)*x^3+18*S2^3*sin(phi)+96*sin(phi)^2*y^4-24*sin(phi)
*y^5-756*cos(phi)*sin(phi)*x*y-60*R2*b*y*sin(alpha2)-48*S2*cos(phi)*sin(phi)*x
-60*S2*cos(phi)*x*y+342*R2*y*sin(alpha2)+210*R2*sin(phi)*sin(alpha2)+780*L1^2*
sin(phi)*y-324*sin(phi)*x^2*y-24*S2^2*sin(phi)*y-48*S2*sin(phi)^2*y+18*S2*sin(
phi)*y^2+162*L1^2*cos(phi)*x-6*R2^2*cos(phi)*x+18*S2^2*cos(phi)*x+66*S2*sin(
phi)*x^2-696*cos(phi)*x*y^2-18*R2^2*S2*sin(phi)+24*R2*y^3*sin(alpha2)+24*sin(
phi)*x^4*y+36*S2^3*sin(phi)*y^2-72*cos(phi)*x^3*y^2-24*sin(phi)^2*x^2*y^2-24*
sin(phi)*x^2*y^3-48*cos(phi)*x*y^4-48*S2^2*sin(phi)*y^3-96*S2*sin(phi)^2*y^3+
36*S2*sin(phi)*y^4-24*L1^2*sin(phi)*x^2*y+48*cos(phi)*sin(phi)*x^3*y-36*R2^2*
S2*sin(phi)*y^2+84*L1^2*cos(phi)*x*y^2-12*R2^2*cos(phi)*x*y^2+36*S2^2*cos(phi)
*x*y^2+132*S2*sin(phi)*x^2*y^2+168*cos(phi)*sin(phi)*x*y^3-120*S2*cos(phi)*x*y
^3, -36*L1^2*b*y^2-124*b^3*y^2+36*b*x^2*y^2+52*b*y^4-60*R2^2*S2^2-60*S2^2*b^2+
60*S2^2*x^2-80*b^2*x^2-90*L1^2*S2*y+90*R2^2*S2*y-90*S2^3*y+60*S2*b^2*y+224*x^2
*y^2+8*b^6-126*L1^2*y^2-90*R2^2*y^2-532*b^2*y^2+150*S2^2*y^2-60*S2*y^3+135*R2^
2*b-195*S2^2*b+246*S2*b*y+841*b-231*L1^2+231-181*x^2-1051*y^2+106*y*sin(phi)-\
252*x*cos(phi)+1257*b^2+944*b^3+120*R2^2-288*x^2*b-423*L1^2*b+30*R2^4-60*x^2*
R2^2+72*b^5+346*b^4+30*x^4-120*S2^2+30*S2^4+194*y^4-16*b^4*y^2+8*b^2*y^4-1392*
b*y^2+122*S2*y-132*S2*sin(phi)-4*sin(phi)^2*x^2+540*sin(phi)*y^3+112*sin(phi)^
2*y^2+272*cos(phi)*x^3-90*S2^3*sin(phi)+116*cos(phi)*sin(phi)*x*y+216*R2*b*y*
sin(alpha2)+240*S2*cos(phi)*sin(phi)*x+300*S2*cos(phi)*x*y+152*R2*y*sin(alpha2
)-132*R2*sin(phi)*sin(alpha2)-480*L1^2*sin(phi)*y+296*sin(phi)*x^2*y+120*S2^2*
sin(phi)*y+240*S2*sin(phi)^2*y-90*S2*sin(phi)*y^2-254*L1^2*cos(phi)*x+30*R2^2*
cos(phi)*x-90*S2^2*cos(phi)*x-330*S2*sin(phi)*x^2+516*cos(phi)*x*y^2+90*R2^2*
S2*sin(phi), -1272*b*x^4+8976*L1^2*S2*b*y-7116*R2^2*S2*b*y+6684*S2^3*b*y-4788*
S2*b^3*y+2124*S2*b*x^2*y-8466*L1^2*b*y^2+7794*R2^2*b*y^2-10770*S2^2*b*y^2-6896
*b^3*y^2-2016*b*x^2*y^2+3912*S2*b*y^3+3816*b*y^4-1644*L1^2*S2^2+2424*R2^2*S2^2
+4512*S2^2*b^2-1344*L1^2*x^2-2172*S2^2*x^2+5760*b^2*x^2+5850*L1^2*S2*y-7794*R2
^2*S2*y+6882*S2^3*y-18552*S2*b^2*y-2358*x^2*y^2+8040*S2*x^2*y-17682*L1^2*y^2+
13296*R2^2*y^2-33154*b^2*y^2-18936*S2^2*y^2+16212*S2*y^3+9597*R2^2*b-6801*S2^2
*b-14106*S2*b*y+57995*b-13965*L1^2+13965+1857*x^2-67449*y^2+15588*y*sin(phi)-\
18684*x*cos(phi)+92999*b^2+70860*b^3+10512*R2^2+9660*x^2*b-33641*L1^2*b-540*R2
^4+3912*x^2*R2^2+3272*b^5+23852*b^4-492*x^4-8868*S2^2-1884*S2^4+10950*y^4+48*
R2^6+96*S2^6-144*R2^2*S2^4-288*S2^4*b^2-144*R2^4*x^2+144*S2^4*x^2-48*x^6-288*
S2^2*b^2*x^2+144*R2^2*x^4-432*L1^2*S2^3*y+432*R2^2*S2^3*y-432*S2^5*y+480*S2^3*
b^2*y-144*L1^2*S2*x^2*y+144*R2^2*S2*x^2*y-144*S2^3*x^2*y+720*L1^2*S2^2*y^2-48*
b^4*y^2-720*R2^2*S2^2*y^2+1008*S2^4*y^2-360*S2^2*b^2*y^2-48*y^6+288*S2^2*x^2*y
^2-540*L1^2*S2*y^3+540*R2^2*S2*y^3-1020*S2^3*y^3+144*S2*b^2*y^3+216*L1^2*y^4-\
216*R2^2*y^4+576*S2^2*y^4+96*b^2*y^4-144*S2*y^5-888*R2^4*b-1848*L1^2*S2^2*b+
3192*R2^2*S2^2*b-2592*S2^4*b+3432*S2^2*b^3+432*L1^2*b*x^2+1344*R2^2*b*x^2-912*
S2^2*b*x^2+1920*b^3*x^2-80334*b*y^2-9666*S2*y+8640*S2*cos(phi)*sin(phi)*x*y^2+
1128*R2^2*S2*cos(phi)*x*y-6048*S2^2*cos(phi)*sin(phi)*x*y-10728*S2*sin(phi)+
684*sin(phi)^2*x^2+21600*sin(phi)*y^3+5316*sin(phi)^2*y^2+8904*cos(phi)*x^3-\
3510*S2^3*sin(phi)-1752*sin(phi)^2*y^4-192*sin(phi)*y^5-432*S2^5*sin(phi)-3264
*sin(phi)^2*x^4+4632*cos(phi)*sin(phi)*x*y+20208*R2*b*y*sin(alpha2)+21024*S2*
cos(phi)*sin(phi)*x+26880*S2*cos(phi)*x*y+6696*R2*y*sin(alpha2)-10728*R2*sin(
phi)*sin(alpha2)-36366*L1^2*sin(phi)*y+19536*sin(phi)*x^2*y+5142*R2^2*sin(phi)
*y-3750*S2^2*sin(phi)*y+21024*S2*sin(phi)^2*y+24954*S2*sin(phi)*y^2-12822*L1^2
*cos(phi)*x+4446*R2^2*cos(phi)*x-10074*S2^2*cos(phi)*x-3318*S2*sin(phi)*x^2+
11328*cos(phi)*x*y^2+3510*R2^2*S2*sin(phi)-3792*R2*y^3*sin(alpha2)+480*sin(phi
)*x^4*y-132*S2^3*sin(phi)*y^2-528*cos(phi)*x^3*y^2+11592*sin(phi)^2*x^2*y^2+
432*sin(phi)*x^2*y^3-672*cos(phi)*x*y^4-540*S2^2*sin(phi)*y^3+4032*S2*sin(phi)
^2*y^3+732*S2*sin(phi)*y^4+432*R2^2*S2^3*sin(phi)-432*S2^4*cos(phi)*x-144*S2^3
*sin(phi)*x^2+1152*S2^2*sin(phi)^2*x^2+864*L1^2*cos(phi)*x^3-864*R2^2*cos(phi)
*x^3+1008*S2^2*cos(phi)*x^3+1440*S2*sin(phi)*x^4+672*S2^4*sin(phi)*y+2688*S2^3
*sin(phi)^2*y-4896*S2^2*sin(phi)^2*y^2-108*L1^2*sin(phi)*y^3+444*R2^2*sin(phi)
*y^3+1512*L1^2*sin(phi)*x^2*y+9744*cos(phi)*sin(phi)*x^3*y+84*R2^2*S2*sin(phi)
*y^2-1116*L1^2*cos(phi)*x*y^2+1644*R2^2*cos(phi)*x*y^2-1620*S2^2*cos(phi)*x*y^
2-4068*S2*sin(phi)*x^2*y^2-6864*cos(phi)*sin(phi)*x*y^3+3240*S2*cos(phi)*x*y^3
+144*R2^2*S2^2*cos(phi)*x+2688*S2^3*cos(phi)*sin(phi)*x-1440*R2^2*S2*sin(phi)*
x^2-2304*S2*cos(phi)*sin(phi)*x^3-96*R2^2*S2^2*sin(phi)*y+888*S2^3*cos(phi)*x*
y-1992*R2^2*sin(phi)*x^2*y+1224*S2^2*sin(phi)*x^2*y-6912*S2*sin(phi)^2*x^2*y-\
3144*S2*cos(phi)*x^3*y, 12*R2*S3*cos(phi)*y^2*sin(alpha2)*sin(alpha3)+12*R2*S3
*x*y^2*sin(alpha2)*sin(alpha3)-48*S2*cos(phi)*sin(phi)*x*y*sin(alpha3)-6*R2*S3
*b*y*cos(alpha2)*sin(alpha3)+12*L1^2*S3*sin(phi)*x*sin(alpha3)-24*S3*cos(phi)*
sin(phi)*x^2*sin(alpha3)-6*L1^2*S3*cos(phi)*y*sin(alpha3)-18*R2^2*S2*sin(phi)*
y*sin(alpha3)+20*S3*b^2*x*y*sin(alpha3)+42*L1^2*cos(phi)*x*y*sin(alpha3)-6*R2^
2*cos(phi)*x*y*sin(alpha3)+18*S2^2*cos(phi)*x*y*sin(alpha3)-12*S3*sin(phi)^2*x
*y*sin(alpha3)-6*S3*cos(phi)*x^2*y*sin(alpha3)+66*S2*sin(phi)*x^2*y*sin(alpha3
)+84*cos(phi)*sin(phi)*x*y^2*sin(alpha3)+12*S2*S3*cos(phi)*y^2*sin(alpha3)-12*
S3*cos(phi)*sin(phi)*y^2*sin(alpha3)+12*S2*S3*x*y^2*sin(alpha3)-60*S2*cos(phi)
*x*y^2*sin(alpha3)-24*S3*sin(phi)*x*y^2*sin(alpha3)-60*R2*b*y^2*sin(alpha2)*
sin(alpha3)+58*S3*b*x*y*sin(alpha3)-12*R2*S3*y*cos(alpha2)*sin(alpha3)+6*R2*S3
*cos(phi)*sin(alpha2)*sin(alpha3)+6*R2*S3*x*sin(alpha2)*sin(alpha3)+6*R2*sin(
phi)*y*sin(alpha2)*sin(alpha3)+2*x*cos(alpha3)-27*y*sin(alpha3)+6*y^5*sin(
alpha3)+18*x^3*cos(alpha3)+45*y^3*sin(alpha3)-3*cos(phi)*cos(alpha3)-6*S2*sin(
alpha3)+3*sin(phi)*sin(alpha3)+8*b^4*x*cos(alpha3)+12*sin(phi)*x^4*sin(alpha3)
-6*R2^4*y*sin(alpha3)-6*S2^4*y*sin(alpha3)+6*b^4*y*sin(alpha3)-6*x^4*y*sin(
alpha3)+18*S2^3*y^2*sin(alpha3)-18*L1^2*y^3*sin(alpha3)+18*R2^2*y^3*sin(alpha3
)-30*S2^2*y^3*sin(alpha3)-12*b^2*y^3*sin(alpha3)+48*sin(phi)^2*y^3*sin(alpha3)
+12*S2*y^4*sin(alpha3)-12*sin(phi)*y^4*sin(alpha3)+32*b^3*x*cos(alpha3)+12*b*x
^3*cos(alpha3)-9*cos(phi)*x^2*cos(alpha3)-6*sin(phi)^2*x*cos(alpha3)+6*b^3*y*
sin(alpha3)+6*b*y^3*sin(alpha3)+3*L1^2*cos(phi)*cos(alpha3)-2*L1^2*x*cos(
alpha3)+36*b^2*x*cos(alpha3)-3*L1^2*sin(phi)*sin(alpha3)-6*S2*x^2*sin(alpha3)+
9*sin(phi)*x^2*sin(alpha3)+33*L1^2*y*sin(alpha3)-51*b^2*y*sin(alpha3)-24*R2^2*
y*sin(alpha3)+24*S2^2*y*sin(alpha3)+51*x^2*y*sin(alpha3)-6*sin(phi)^2*y*sin(
alpha3)-102*S2*y^2*sin(alpha3)+63*sin(phi)*y^2*sin(alpha3)+17*b*x*cos(alpha3)+
6*R2*cos(alpha2)*cos(alpha3)-3*S2*b*sin(alpha3)-84*b*y*sin(alpha3)-6*R2*sin(
alpha2)*sin(alpha3)-12*L1^2*sin(phi)*x^2*sin(alpha3)+24*cos(phi)*sin(phi)*x^3*
sin(alpha3)-12*S3*sin(phi)*x^3*sin(alpha3)-36*cos(phi)*x^3*y*sin(alpha3)+12*R2
^2*S2^2*y*sin(alpha3)+12*S2^2*b^2*y*sin(alpha3)+18*S2^3*sin(phi)*y*sin(alpha3)
+12*R2^2*x^2*y*sin(alpha3)-12*sin(phi)^2*x^2*y*sin(alpha3)-12*S2^2*x^2*y*sin(
alpha3)-8*b^2*x^2*y*sin(alpha3)+18*L1^2*S2*y^2*sin(alpha3)-18*R2^2*S2*y^2*sin(
alpha3)-12*sin(phi)*x^2*y^2*sin(alpha3)-12*S2*b^2*y^2*sin(alpha3)-24*S2^2*sin(
phi)*y^2*sin(alpha3)-48*S2*sin(phi)^2*y^2*sin(alpha3)-6*S3*cos(phi)*y^3*sin(
alpha3)+18*S2*sin(phi)*y^3*sin(alpha3)-12*S3*x*y^3*sin(alpha3)-24*cos(phi)*x*y
^3*sin(alpha3)-12*L1^2*b*x*cos(alpha3)+21*L1^2*b*y*sin(alpha3)-27*R2^2*b*y*sin
(alpha3)+39*S2^2*b*y*sin(alpha3)+26*b*x^2*y*sin(alpha3)-66*S2*b*y^2*sin(alpha3
)-6*R2*x^2*sin(alpha2)*sin(alpha3)-108*R2*y^2*sin(alpha2)*sin(alpha3)+3*R2*b*
cos(alpha2)*cos(alpha3)+6*S2*S3*cos(phi)*sin(alpha3)-6*S3*cos(phi)*sin(phi)*
sin(alpha3)+6*S2*S3*x*sin(alpha3)-6*S3*sin(phi)*x*sin(alpha3)+21*cos(phi)*x*y*
sin(alpha3)+3*S3*cos(phi)*y*sin(alpha3)+6*S2*sin(phi)*y*sin(alpha3)+48*S3*x*y*
sin(alpha3)-3*R2*b*sin(alpha2)*sin(alpha3), 13*cos(alpha3)+12*R2*S3*b*y^2*sin(
alpha2)*sin(alpha3)-12*R2*b*x*y^2*sin(alpha2)*sin(alpha3)+24*R2*S3*y^2*sin(
alpha2)*sin(alpha3)-24*R2*x*y^2*sin(alpha2)*sin(alpha3)-84*S3*cos(phi)*sin(phi
)*x*sin(alpha3)-72*S3*cos(phi)*x*y*sin(alpha3)+12*S2*S3*b*y^2*sin(alpha3)-12*
S2*b*x*y^2*sin(alpha3)+60*b^4*cos(alpha3)+166*b^3*cos(alpha3)-13*L1^2*cos(
alpha3)+176*b^2*cos(alpha3)+45*x^2*cos(alpha3)+75*b*cos(alpha3)+8*b^5*cos(
alpha3)+78*L1^2*S3*sin(phi)*sin(alpha3)-78*L1^2*sin(phi)*x*sin(alpha3)+84*cos(
phi)*sin(phi)*x^2*sin(alpha3)-30*S3*sin(phi)*x^2*sin(alpha3)+110*S3*b^2*y*sin(
alpha3)-60*S3*sin(phi)^2*y*sin(alpha3)-110*b^2*x*y*sin(alpha3)+60*sin(phi)^2*x
*y*sin(alpha3)+72*cos(phi)*x^2*y*sin(alpha3)+24*S2*S3*y^2*sin(alpha3)-90*S3*
sin(phi)*y^2*sin(alpha3)-24*S2*x*y^2*sin(alpha3)+90*sin(phi)*x*y^2*sin(alpha3)
-21*R2*sin(phi)*sin(alpha2)*cos(alpha3)+248*S3*b*y*sin(alpha3)-248*b*x*y*sin(
alpha3)+45*R2*S3*sin(alpha2)*sin(alpha3)-45*R2*x*sin(alpha2)*sin(alpha3)-12*L1
^2*cos(phi)*x*cos(alpha3)+30*L1^2*S3*y*sin(alpha3)-30*L1^2*x*y*sin(alpha3)-30*
S3*x^2*y*sin(alpha3)+54*x*y^3*sin(alpha3)+30*sin(phi)*x^3*sin(alpha3)-54*S3*y^
3*sin(alpha3)-56*L1^2*b*cos(alpha3)+24*b*x^2*cos(alpha3)-21*cos(phi)*x*cos(
alpha3)-21*S2*sin(phi)*cos(alpha3)+45*S2*S3*sin(alpha3)-66*S3*sin(phi)*sin(
alpha3)-45*S2*x*sin(alpha3)-156*x*y*sin(alpha3)+66*sin(phi)*x*sin(alpha3)+156*
S3*y*sin(alpha3)+30*x^3*y*sin(alpha3)+12*cos(phi)*x^3*cos(alpha3)-24*sin(phi)^
2*x^2*cos(alpha3)+20*S3*b^3*y*sin(alpha3)-20*b^3*x*y*sin(alpha3)-12*S3*b*y^3*
sin(alpha3)+12*b*x*y^3*sin(alpha3)];
