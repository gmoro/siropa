with(Siropa);with(GraphTheory);
cells_art:= CellDecompositionPlus([rho2+3*cos(alpha2)-x, 3*sin(alpha2)-1/2,
x^2-2*x*cos(phi)+cos(phi)^2+1/4-sin(phi)+sin(phi)^2-rho1^2, 3*cos(alpha3)-cos(phi)-x,
rho3+3*sin(alpha3)-sin(phi)-1/2], [rho1, rho1, tan((1/2)*alpha3), -1+tan((1/2)*alpha2),
1+tan((1/2)*alpha2)], [alpha2,  rho2, rho3, x, phi], [rho1, alpha3]):;
cells_graph_art:=CellGraph(cells_art);
cells_graph_plus_art:=ConnectedComponents(cells_graph_art);
PlotCell2D (cells_art,cells_graph_plus_art);

