Equations := [x^2-420*x*cos(gamma1)+44100*cos(gamma1)^2+y^2-420*y*sin(gamma1)+
44100*sin(gamma1)^2-44100, 445900+73500*sin(alpha)*3^(1/2)*cos(gamma2)-73500*
cos(alpha)*3^(1/2)*sin(gamma2)-700*x-490000*cos(alpha)+147000*cos(gamma2)+
122500*cos(alpha)^2+122500*sin(alpha)^2+350*y*cos(alpha)*3^(1/2)-350*x*sin(
alpha)*3^(1/2)-73500*sin(alpha)*sin(gamma2)-420*x*cos(gamma2)-420*y*sin(gamma2
)+147000*3^(1/2)*sin(gamma2)-73500*cos(alpha)*cos(gamma2)+x^2+350*x*cos(alpha)
+y^2+350*y*sin(alpha)-700*y*3^(1/2)+44100*cos(gamma2)^2+44100*sin(gamma2)^2, x
^2+700*x*cos(alpha)-1400*x-420*x*cos(gamma3)+122500*cos(alpha)^2-490000*cos(
alpha)-147000*cos(alpha)*cos(gamma3)+445900+294000*cos(gamma3)+44100*cos(
gamma3)^2+y^2+700*y*sin(alpha)-420*y*sin(gamma3)+122500*sin(alpha)^2-147000*
sin(alpha)*sin(gamma3)+44100*sin(gamma3)^2];
