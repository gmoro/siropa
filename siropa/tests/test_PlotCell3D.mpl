p := 27*cos(phi)^2+45/4-9*sin(phi)-9*rho1^2-108*cos(alpha3)*cos(phi)+81*cos(alpha3)^2;
cells := CellDecompositionPlus ([T], [p^2], [T]);
PlotCell3D (cells, nowall);
Plot3D ([p], phi=-5..5);

