# 
with(Siropa);
infolevel[Groebner] := 10;
robot:= CreateManipulator([r1*cos(theta1)-x, r1*sin(theta1)-y, x^2+y^2-r1^2, (x+d1*cos(alpha)-A2x)^2+(y+d1*sin(alpha))^2-r2^2, (x+d3*(cos(alpha)*cos(beta)-sin(alpha)*sin(beta))-A3x)^2+(y+d3*(cos(alpha)*sin(beta)+sin(alpha)*cos(beta))-A3y)^2-r3^2, r1>0, r2>0, r3>0], [x, y, alpha],[r1, r2, r3], [theta1], [d1 = 17.04, d3 = 20.84, beta = .8826031096, A2x = 15.91, A3x = 0, A3y = 10],[A1 = [0, 0], A2 = [A2x, 0], A3 = [A3x, A3y], B1 = [x, y], B2 = [x+d1*cos(alpha), y+d1*sin(alpha)], B3 = [x+d3*cos(alpha+beta), y+d3*sin(alpha+beta)]],[[B1, B2, B3]],[[A1, A2, A3, A1]], [[A1, B1], [A2, B2], [A3, B3]], "3-RPR");
s2:= ParallelSingularities(robot):
s2_art:= Projection(s2, [r1, r2, r3]):
#stopat(AlgebraicTools:-Projection);
kernelopts (opaquemodules=false);
stopat(AlgebraicTools:-simplifyElim, 11);
s2_art_cart:= Projection([op(s2_art), op(robot:-Equations)], [r1, alpha, theta1]):
# 
