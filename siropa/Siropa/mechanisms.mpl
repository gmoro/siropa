# Title: Mechanisms


# Function: Parallel_3RPR
#   Constructs the *Manipulator* object of a planar 3-R _P_ R manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot (see Figure1.png),
#    
#       - where *name* is one of *d1, d2, d3, beta, A2x, A3x, A3y*,
#       - and *constant* is a numerical value.
#       - All the variables *d1, d3, A2x, A3x, A3y* must be assigned.
#       - One of *d2, beta* must be assigned (if both are assigned,
#         *d3* is ignored).
#       - By default, the values are *d1* = 17.04, *d2* = 16.54, *d3* = 20.84,
#         *A2x* = 15.91, *A3x* = 0, *A3y* = 10.
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the planar 3-R _P_ R manipulator
#   whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel_3RPR (d1=3, d2=4, d3=5, A2x=5, A3x=0, A3y=5):
# > robot[Equations];
#          2           2         2      2          2
# [cosAlpha  + sinAlpha  - 1, B1x  + B1y  - R1, B1x  + 6 B1x cosAlpha - 10 B1x
# 
#                  2                         2                              2
#      + 9 cosAlpha  - 30 cosAlpha + 25 + B1y  + 6 B1y sinAlpha + 9 sinAlpha
# 
#               2                                                2
#      - R2, B1x  + 6 B1x cosAlpha - 8 B1x sinAlpha + 25 cosAlpha
# 
#                   2      2
#      + 25 sinAlpha  + B1y  + 8 B1y cosAlpha + 6 B1y sinAlpha - 10 B1y
# 
#      - 40 cosAlpha - 30 sinAlpha + 25 - R3]
# (end code)
Parallel_3RPR := proc( {d1::algebraic:= 17.04,
                       d2::algebraic:= 16.54, 
                       d3::algebraic:= 20.84,
                       beta::algebraic:= arccos ((d2^2-d3^2-d1^2)/(-2*d3*d1)),
                       A2x::algebraic:= 15.91,
                       A3x::algebraic:= 0,
                       A3y::algebraic:= 10,
                       precision::integer := 4},
                       morespec   ::seq(name=algebraic),
                       moreranges ::seq(name=range))
    local t, cosBeta, sinBeta, d, shift, E, P, V, M, R, C, spec, Po, Ch, Lo, Ac;

    spec := [ ':-d1'=d1, ':-d3'=d3, ':-beta'=beta,
              ':-A2x'=A2x, ':-A3x'=A3x, ':-A3y'=A3y, morespec ];

# Equations: Gosselin derivative
#    E := [ cosAlpha^2+sinAlpha^2-1,
#           B1x^2 + B1y^2 - r1^2,
#           (B1x + ':-d1'*cosAlpha - ':-A2x')^2 + (B1y+':-d1'*sinAlpha)^2 - r2^2,
#           (B1x +':-d3'*(cosAlpha*':-cosBeta'-sinAlpha*':-sinBeta') -':-A3x')^2
#           + (B1y +':-d3'*(cosAlpha*':-sinBeta'+sinAlpha*':-cosBeta')-':-A3y')^2
#           - r3^2 ];
#    E := (numer @ normal) (subs ( ':-cosBeta'=(1-':-tanHalfBeta'^2)
#                                              /(1+':-tanHalfBeta'^2),
#                                  ':-sinBeta'=2*':-tanHalfBeta'
#                                              /(1+':-tanHalfBeta'^2),
#                                  E ) );
    E := [ B1x^2 + B1y^2 - r1^2,
           (B1x +':-d1'*cos(alpha) -':-A2x')^2 + (B1y +':-d1'*sin(alpha))^2
           - r2^2,
           (B1x +':-d3'*(cos(alpha)*cos(':-beta')-sin(alpha)*sin(':-beta'))
           -':-A3x')^2
           + (B1y +':-d3'*(cos(alpha)*sin(':-beta')+sin(alpha)*cos(':-beta'))
           -':-A3y')^2 - r3^2 ];
    C := [r1>0, r2>0, r3>0];
    R := [r1=0..50, r2=0..50, r3=0..50];
    R := map( x->if has([moreranges],lhs(x)) then
                    select(y->lhs(y)=lhs(x),[moreranges])[1]
                 else x end if,
              R );
    P := [r1, r2, r3];
    V := [B1x, B1y, alpha];
    M := "3RPR Gosselin";
    Po := [A1=[0,0], A2=[':-A2x',0], A3=[':-A3x',':-A3y'],
           B1=[B1x,B1y], B2=[B1x+':-d1'*cos(alpha), B1y+':-d1'*sin(alpha)],
           B3=[B1x+':-d3'*cos(alpha+':-beta'), B1y+':-d3'*sin(alpha+':-beta')]];
    Lo := [ [B1,B2,B3] ];
    Ch := [ [A1,A2,A3,A1] ];
    Ac  := [ [A1,B1], [A2,B2], [A3,B3] ];
                                
    return CreateManipulator ( [op(E),op(C)], V, P, spec, R, Po, Lo, Ch, Ac, M, precision );
end proc;

# Function: Parallel_3RPR_full
#   Constructs the *Manipulator* object of a planar 3-R _P_ R manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot (see Figure1.png),
#    
#       - where *name* is one of *d1, d2, d3, beta, A2x, A3x, A3y*,
#       - and *constant* is a numerical value.
#       - All the variables *d1, d3, A2x, A3x, A3y* must be assigned.
#       - One of *d2, beta* must be assigned (if both are assigned,
#         *d3* is ignored).
#       - By default, the values are *d1* = 17.04, *d2* = 16.54, *d3* = 20.84,
#         *A2x* = 15.91, *A3x* = 0, *A3y* = 10.
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the planar 3-R _P_ R manipulator
#   whose dimensions are given in input.
Parallel_3RPR_full := proc( morespec ::seq(name=algebraic),
                            precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := { d1 = 17.04, d2= 16.54, d3 = 20.84,
               A2x = 15.91, A3x = 0, A3y = 10 };

#    E := [ r1*cos(theta1) + d1*cos(alpha)      = A2x + r2*cos(theta2),
#           r1*sin(theta1) + d1*sin(alpha)      = r2*sin(theta2),
#           r1*cos(theta1) + d3*cos(alpha+beta) = A3x + r3*cos(theta3),
#           r1*sin(theta1) + d3*sin(alpha+beta) = A3y + r3*sin(theta3),
#           r1>0, r2>0, r3>0 ];
#    Po := [theta1, theta2, alpha];
#    Ar := [r1, r2, r3];
#    Pa := [theta3];
    E := [ r1*cos(theta1) + d1*cos(alpha)      = A2x + r2*cos(theta2),
           r1*sin(theta1) + d1*sin(alpha)      = r2*sin(theta2),
           r1*cos(theta1) + d3*cos(alpha+beta) = A3x + r3*cos(theta3),
           r1*sin(theta1) + d3*sin(alpha+beta) = A3y + r3*sin(theta3),
           x = r1*cos(theta1),
           y = r1*sin(theta1),
           r1>0, r2>0, r3>0 ];
    Po := [x, y, alpha];
    Ar := [r1, r2, r3];
    Pa := [theta1, theta2, theta3];
 
    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    if not member (beta, map(lhs, spec)) then
        spec := spec union
                     {subs (spec, beta=arccos ((d2^2-d3^2-d1^2)/(-2*d3*d1)))};
    end if;

    R := [alpha=-Pi..Pi, r1=0..50, r2=0..50, r3=0..50,
          theta1 = -Pi..Pi, theta2 = -Pi..Pi, theta3 = -Pi..Pi];
    M := "3RPR Gosselin";
    Pts := [A1=[0,0], A2=[A2x,0], A3=[A3x,A3y],
           B1=[r1*cos(theta1),r1*sin(theta1)],
           B2=[A2x+r2*cos(theta2), r2*sin(theta2)],
           B3=[A3x+r3*cos(theta3), A3y+r3*sin(theta3)]];
    Lo := [ [B1,B2,B3] ];
    Ch := [ [A1,A2,A3,A1] ];
    Ac  := [ [A1,B1], [A2,B2], [A3,B3] ];
 
    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;


# Function: Parallel_3PRR
#   Constructs the *Manipulator* object of a 3- _P_ R R manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *l1, l2*,
#       - and *constant* is a numerical value.
#       - By default, the values are *l1* = 1, *l2* = 1.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the planar _P_ R R _P_
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel_3PRR (l1=1,l2=1):
# > robot[Equations];
# (end code)
#TODO
Parallel_3PRR := proc( morespec ::seq(name=algebraic),
                       precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := { A2x=3, A3x=3/2, A3y= 3*sqrt(3)/2,
               theta1=0, theta2=2*Pi/3, theta3=-2*Pi/3,
               d1=1, d3=1, beta=Pi/3 };
    
    E := [ (B1x-r1*cos(theta1))^2+(B1y-r1*sin(theta1))^2=1,
           ( B1x + d1*cos(alpha) - (A2x+r2*cos(theta2)) )^2+
           ( B1y + d1*sin(alpha) - r2*sin(theta2) )^2 = 1,
           ( B1x + d3*cos(alpha+beta) - A3x - r3*cos(theta3) )^2+
           ( B1y + d3*sin(alpha+beta) - A3y - r3*sin(theta3) )^2 = 1,
           r1>0, r2>0, r3>0 ];
    Po := [B1x,B1y,alpha];
    Ar := [r1, r2, r3];
    Pa := [];
    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    R := [alpha=-Pi..Pi, r1=0..2.5, r2=0..2.5, r3=0..2.5,
          B1x=-2.5..2.5, B1y=-2.5..2.5];
    Pts := [A1=[0,0], A2=[A2x,0], A3=[A3x,A3y],
            B1=[B1x, B1y], B2=[B1x+d1*cos(alpha),B1y+d1*sin(alpha)], 
            B3=[B1x+d3*cos(alpha+beta),B1y+d3*sin(alpha+beta)],
            M1=[r1*cos(theta1),r1*sin(theta1)],
            M2=[A2x+r2*cos(theta2),r2*sin(theta2)],
            M3=[A3x+r3*cos(theta3),A3y+r3*sin(theta3)] ];
    Lo := [ [B1,B2,B3] ];
    Ch := [ [M1,B1], [M2,B2], [M3,B3] ];
    Ac := [ [A1,M1], [A2,M2], [A3,M3] ];
    M := "Parallel 3PRR";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;


# Function: ParallelPRP2PRR
#   Constructs the *Manipulator* object of a P R P - 2 P R R manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *l1, l2, a, b*,
#       - and *constant* is a numerical value.
#       - By default, the values are *l1* = 1, *l2* = 1.5, *a* = 1, *b* = 1.2
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the planar 2 P R R - P R P
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := ParallelPRP2PRR (l1=1,l2=1):
# > robot[Equations];
#       2                                     2                         2    2
# [1 - x  + 2 x cos(phi) + 2 x rho1 - cos(phi)  - 2 cos(phi) rho1 - rho1  - y
# 
#                               2       2
#      + 2 y sin(phi) - sin(phi) , 1 - x  - 12/5 x cos(phi) + 2 x rho2
# 
#        36         2                            2    2
#      - -- cos(phi)  + 12/5 cos(phi) rho2 - rho2  - y  - 12/5 y sin(phi)
#        25
# 
#        36         2
#      - -- sin(phi) , -4 x cos(phi) + 4 cos(phi) rho3 - 4 y sin(phi)]
#        25
# (end code)
ParallelPRP2PRR := proc( morespec ::seq(name=algebraic),
                 precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := { l1=1, l2=1, a=1, b=1 };
    
    E := [ l1^2 = (x-a*cos(phi)-rho1)^2 + (y-a*sin(phi))^2,
           l2^2 = (x+b*cos(phi)-rho2)^2 + (y+b*sin(phi))^2,
           (x-cos(phi)-rho3 )^2 + (y-sin(phi))^2 =
           (x+cos(phi)-rho3)^2 + (y+sin(phi))^2 ];
    
    Po := [x,y,phi];
    Ar := [rho1, rho2, rho3];
    Pa := [];
    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    R := [theta1=-Pi..Pi, theta2=-Pi..Pi, theta3=-Pi..Pi, theta4=-Pi..Pi,
          x=-1.5..2.5, y=-2..2];
    Pts := [ A1=[rho1, 0], A2=[rho2, 0], A3=[rho3, 0],
             B1=[x-a*cos(phi), y-a*sin(phi)], B2=[x+b*cos(phi), y+b*sin(phi)],
             B3=[x, y], O=[0,0] ];
    Lo := [];
    Ch := [ [A1,B1,B2,A2] ];
    Ac := [ [O,A1], [O,A2], [O,A3], [A3,B3] ];
    M := "Parallel 2PRR-PRR";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;


# Function: Parallel_RPRRP
#   Constructs the *Manipulator* object of a R _P_ R R _P_ manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *e, L4, S2min, S2max, S4min, S4max*,
#       - and *constant* is a numerical value.
#       - By default, the values are *e* = 0.7, *L4* = 1, *S2min* = 0.5,
#         *S2max* = 1.5, *S4min* = 0.1, *S4max* = 1.1.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the planar R _P_ R R _P_
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel_RPRRP (e=1,L4=1):
# > robot[Equations];
# (end code)
Parallel_RPRRP := proc( {e ::constant    := 0.7,
                       L4::constant    := 1,
                       S2min::constant := 0.5,
                       S2max::constant := 1.5,
                       S4min::constant := 0.1,
                       S4max::constant := 1.1,
                       precision := 4},
                       morespec ::seq(name=algebraic) )
    local E, Po, Ar, Pa, M, C, R, spec, Pts, Lo, Ch, Ac;
    
# Equations
#    E := [ cosTheta2^2+sinTheta2^2-1,
#           cosTheta4^2+sinTheta4^2-1,
#           S2*cosTheta2 = S4 + cosTheta4*':-L4',
#           S2*sinTheta2 = ':-e' + sinTheta4*':-L4' ];
    E := [ x = S2*cos(theta2), y = S2*sin(theta2),
           S2*cos(theta2) = S4 + cos(theta4)*':-L4',
           S2*sin(theta2) = ':-e' + sin(theta4)*':-L4' ];
    C := [ ':-S2min'<S2, S2<':-S2max', ':-S4min'<S4, S4<':-S4max' ];
    Po := [x,y];
    Ar := [S2, S4];
    Pa := [theta2,theta4];
    R := [theta2=-Pi..Pi, theta4=-Pi..Pi,
          S2=-0.5..2, S4=-0.5..2, x=-2..2, y=-2..2];
    spec := [ ':-e'=e, ':-L4'=L4, ':-S2min'=S2min, ':-S2max'=S2max,
              ':-S4min'=S4min, ':-S4max'=S4max, morespec ];
    Pts := [A=[0,0], M=[S2*cos(theta2), S2*sin(theta2)], N=[S4,e], B=[0,e]];
    Lo := [];
    Ch := [ [M,N] ];
    Ac := [ [A,M], [B,N] ];

    M := "Parallel RPRRP";

    return CreateManipulator ([op(E),op(C)], Po, Ar, Pa, spec, R, 
                              Pts, Lo, Ch, Ac, M, precision);
end proc;

# Function: Parallel_RR_RRR
#   Constructs the *Manipulator* object of a 2-R R manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *e, l1, l2, l3, l4*,
#       - and *constant* is a numerical value.
#       - By default, the values are *e* = 1, *l1* = 1, *l2* = 1,
#         *l3* = 1, *l4* = 1.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the planar 2-R R
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel_RR_RRR (e=1,L4=1):
# > robot[Equations];
# (end code)
Parallel_RR_RRR := proc( morespec ::seq(name=algebraic),
                      precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := { e=1, l1=1, l2=1, l3=1, l4=1 };
    
    E := [ x = l1*cos(theta1) + l2*cos(theta2),
           y = l1*sin(theta1) + l2*sin(theta2),
           x = e + l3*cos(theta3) + l4*cos(theta4),
           y = l3*sin(theta3) + l4*sin(theta4) ];
    Po := [x,y];
    Ar := [theta1, theta3];
    Pa := [theta2,theta4];
    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    R := [theta1=-Pi..Pi, theta2=-Pi..Pi, theta3=-Pi..Pi, theta4=-Pi..Pi,
          x=-1.5..2.5, y=-2..2];
    Pts := [A0=[0,0], A1=[l1*cos(theta1), l1*sin(theta1)],
            A2=[l1*cos(theta1)+l2*cos(theta2), l1*sin(theta1)+l2*sin(theta2)],
            A4=[e+l3*cos(theta3), l3*sin(theta3)], A3=[e,0]];
    Lo := [];
    Ch := [ [A0,A1,A2,A4,A3] ];
    Ac := [ A0, A3 ];
    M := "Parallel 2RR";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

# Function: Parallel_PRRP
#   Constructs the *Manipulator* object of a _P_ R R _P_ manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *l1, l2*,
#       - and *constant* is a numerical value.
#       - By default, the values are *l1* = 1, *l2* = 1.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the planar _P_ R R _P_
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel_PRRP (l1=1,l2=1):
# > robot[Equations];
# (end code)
Parallel_PRRP := proc( morespec ::seq(name=algebraic),
                      precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := { l1=1, l2=1 };
    
    E := [ x = r1 + l1*cos(theta1),
           y = l1*sin(theta1),
           x = l2*cos(theta2),
           y = r2+l2*sin(theta2),
           x>0,y>0,
           r1>0,
           r2>0];
    Po := [x,y];
    Ar := [r1, r2];
    Pa := [theta1,theta2];
    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    R := [theta1=-Pi..Pi, theta2=-Pi..Pi, r1=0..2.5, r2=0..2.5,
          x=-1.5..2.5, y=-2..2];
    Pts := [A=[0,0], B1=[0, r1], B2=[r2,0], M=[l1*sin(theta1),r1+cos(theta1)]];
    Lo := [];
    Ch := [ [B1,M,B2] ];
    Ac := [ [A,B1], [A,B2] ];
    M := "Parallel PRRP";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

# Function: Orthoglide
#   Constructs the *Manipulator* object of the Orthoglide manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *l1, l2*,
#       - and *constant* is a numerical value.
#       - By default, the values are *l1* = 1, *l2* = 1.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the Orthoglide
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Orthoglide (l1=1,l2=1,l3=1):
# > robot[Equations];
# (end code)
Orthoglide := proc( morespec ::seq(name=algebraic),
                    precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := { l1=1, l2=1, l3=1 };
    
#    E := [ x = r1 + l1*sin(phi1)*cos(theta1),
#           y = l1*sin(phi1)*sin(theta1),
#           z = l1*cos(phi1),
#
#           x = l2*sin(phi2)*cos(theta2),
#           y = r2 + l2*sin(phi2)*sin(theta2),
#           z = l2*cos(phi2),
#           
#           x = l3*sin(phi3)*cos(theta3),
#           y = l3*sin(phi3)*sin(theta3),
#           z = r3 + l3*cos(phi3),
#           r1>0, r2>0, r3>0 ];
#    Pa := [theta1,theta2,theta3,phi1,phi2,phi3];
    E := [ (x-r1)^2+y^2+z^2=l1^2, 
           x^2+(y-r2)^2+z^2=l2^2, 
           x^2+y^2+(z-r3)^2=l3^2,
           r1>0, r2>0, r3>0      ];
    Pa := [];
    Po := [x,y,z];
    Ar := [r1, r2, r3];
    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    R := [theta1=-Pi..Pi, theta2=-Pi..Pi, r1=0..2.5, r2=0..2.5, r3=0..2.5,
          x=-1.5..2.5, y=-1.5..2.5, z=-1.5..2.5];

    Pts := [ A1= [2*l1,0,0], A2= [0,2*l2,0], A3=[0,0,2*l3], M= [x,y,z],
             B1= [r1,0,0], B2= [0,r2,0], B3=[0,0,r3] ];
    Lo := [ ];
    Ch := [ [B1, M], [B2, M], [B3, M] ];
    Ac := [ [A1, B1], [A2, B2], [A3, B3] ];

    M := "Orthoglide";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

# Function: ParallelRPR2PRR
#   Constructs the *Manipulator* object of the R P R 2-P R R manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *a, b, L2, L3*,
#       - and *constant* is a numerical value.
#       - By default, the values are *a* = 1, *b* = 4, *L2* = 2, *L3* = 2.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the Orthoglide
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := ParallelRPR2PRR ():
# > robot[Equations];
# (end code)
ParallelRPR2PRR := proc( morespec ::seq(name=algebraic),
                         precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := { a=1, b=4, L2=2, L3=2 };

    E:= [ x^2+y^2-S1^2,
          x - (S2 + L2 * cos(alpha2)-(a+b) * cos(phi)),
          y - (L2 * sin(alpha2) - (a+b) * sin(phi)),
          x - (L3 * cos(alpha3) - a * cos(phi)),
          y - (S3 + L3 * sin(alpha3) - a*sin(phi)),
          S1>0 ];
    Po := [x,y,phi];
    Ar := [S1, alpha2, alpha3];
    Pa := [S2, S3];
    spec := [morespec, op(remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec))];
    R := [ S1=0..10, alpha2=-3.2..3.2, alpha3=-3.2...3.2,
           x=-10..10, y=-10..10, phi=-3.2..3.2            ];

    Pts := [ A1= [0,0], A2= [S2,0], A3=[0,S3], B1= [x,y],
             B2= [x+(a+b)*cos(phi), y+(a+b)*sin(phi)],
             B3= [x+a*cos(phi), y+a*sin(phi)]             ];
    Lo := [ [B1, B2, B3] ];
    Ch := [ [A2, B2], [A3, B3] ];
    Ac := [ [A1, B1], [A1, A2], [A1, A3], A2, A3 ];
    M := "Parallel RPR-2PRR";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

# Function: Parallel3PPPS
#   Constructs the *Manipulator* object of the 3- _P_ _P_ P S manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is either one of *Ax, Ay, Az, Bx, By, Bz, Cx, Cy, Cz*,
#                             or one of *L1, L2, L3*.
#       - and *constant* is a numerical value.
#       - By default, the values are *Ax* = 1, *Ay* = 0, *Az* = 0,
#                                    *Bx* = 0, *By* = 1, *Bz* = 0,
#                                    *Cx* = 0, *Cy* = 0, *Cz* = 1, and
#                                    *L1* = (Bx-Ax)^2 + (By-Ay)^2 + (Bz-Az)^2,
#                                    *L2* = (Cx-Bx)^2 + (Cy-By)^2 + (Cz-Bz)^2,
#                                    *L3* = (Ax-Cx)^2 + (Ay-Cy)^2 + (Az-Cz)^2.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the Orthoglide
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel3PPPS ():
# > robot[Equations];
# (end code)
Parallel3PPPS := proc( morespec ::seq(name=algebraic),
                       precision    := 4,
                       { quaternions::truefalse    := false,
                         tiltandtorsion::truefalse := false,
                         normalize::truefalse      := false,
                         ortho::truefalse          := false } )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac, c,
          zz1, xx2, yy3, C1, C2, C3, Q, retrospec, psi, RY, RZ,
          u, v, S;
    dspec := { Ax=1, Ay=0, Az=0, Bx=0, By=1, Bz=0, Cx=0, Cy=0, Cz=1,
               L1= (Bx-Ax)^2 + (By-Ay)^2 + (Bz-Az)^2,
               L2= (Cx-Bx)^2 + (Cy-By)^2 + (Cz-Bz)^2,
               L3= (Ax-Cx)^2 + (Ay-Cy)^2 + (Az-Cz)^2 };

    retrospec := { l1 = Ax, a = Ay,  g = Az,
                   b = Bx,  l2 = By, h = Bz,
                   e = Cx,  f = Cy,  l3 = Cz };
    retrospec := subs (retrospec, {morespec});
    spec := retrospec union remove (s->member(lhs(s),map(lhs,retrospec)),
                                          dspec);

    u := subs (spec, < Bx-Ax, By-Ay, Bz-Az >);
    v := subs (spec, < Cx-Ax, Cy-Ay, Cz-Az >);

    if ortho or normalize then 
        S := [u,v,LinearAlgebra:-CrossProduct (u,v)];
        S := LinearAlgebra:-GramSchmidt ( S, ':-conjugate' =false,
                                             ':-normalized'=normalize);
        S := Matrix (S);
    else
        S := 1;
    end if;


    if quaternions then
        Q  := < < q1^2+q2^2   | q2*q3-q1*q4 | q2*q4+q1*q3 >, 
                < q2*q3+q1*q4 | q1^2+q3^2   | q3*q4-q1*q2 >,
                < q2*q4-q1*q3 | q3*q4+q1*q2 | q1^2+q4^2   > >;
        R  := 2*Q - 1;
        R  := S.R.S^(-1);
        C1 := <cx, cy, cz> + R.< Ax, Ay, Az > - <x1, y1, zz1>;
        C2 := <cx, cy, cz> + R.< Bx, By, Bz > - <xx2, y2, z2>;
        C3 := <cx, cy, cz> + R.< Cx, Cy, Cz > - <x3, yy3, z3>;
        E  := [ C1[1], C1[2], C2[2], C2[3], C3[1], C3[3],
                q1^2 + q2^2 + q3^2 + q4^2 - 1 ];
        Po := [cx, cy, cz, q2, q3, q4];
        Ar := [x1, y1, y2, z2, x3, z3];
        Pa := [q1];
        assign (solve (C1[3],{zz1}));
        assign (solve (C2[1],{xx2}));
        assign (solve (C3[2],{yy3}));
    Pts := [ A1 = [x1,y1,-c], A2 = [-c,y2,z2], A3 =[x3,-c,z3],
             B1 = [x1,y1,zz1], B2 = [xx2,y2,z2], B3 = [x3,yy3,z3],
             F1 = [-c, -c, -c], F2 = [-c, -c, c], F3 = [-c, c, -c],
             F4 = [-c, c, c], F5 = [c, -c, -c], F6 = [c, -c, c],
             F7 = [c, c, -c] ];
    elif tiltandtorsion then
        psi := sigma - phi;
        RZ := a -> <<cos(a) | -sin(a) | 0>,
                    <sin(a) | cos(a)  | 0>,
                    <0      | 0       | 1>>;
        RY := a -> <<cos(a)  | 0 | sin(a)>,
                    <0       | 1 | 0     >,
                    <-sin(a) | 0 | cos(a)>>;
        R := RZ(phi).RY(theta).RZ(psi);
        R := S.R.S^(-1);

        C1 := <cx, cy, cz> + R.< Ax, Ay, Az > - <x1, y1, zz1>;
        C2 := <cx, cy, cz> + R.< Bx, By, Bz > - <xx2, y2, z2>;
        C3 := <cx, cy, cz> + R.< Cx, Cy, Cz > - <x3, yy3, z3>;
        E  := simplify ([ C1[1], C1[2], C2[2], C2[3], C3[1], C3[3] ],'trig');
        Po := [cx, cy, cz, theta, phi, sigma];
        Ar := [x1, y1, y2, z2, x3, z3];
        Pa := [];
        assign (solve (C1[3],{zz1}));
        assign (solve (C2[1],{xx2}));
        assign (solve (C3[2],{yy3}));
    Pts := [ A1 = [x1,y1,-c], A2 = [-c,y2,z2], A3 =[x3,-c,z3],
             B1 = [x1,y1,zz1], B2 = [xx2,y2,z2], B3 = [x3,yy3,z3],
             F1 = [-c, -c, -c], F2 = [-c, -c, c], F3 = [-c, c, -c],
             F4 = [-c, c, c], F5 = [c, -c, -c], F6 = [c, -c, c],
             F7 = [c, c, -c] ];

    else
        E := [(x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2 = L1,
              (x3-x2)^2 + (y3-y2)^2 + (z3-z2)^2 = L2,
              (x1-x3)^2 + (y1-y3)^2 + (z1-z3)^2 = L3];
        Po := [z1, x2, y3];
        Ar := [x1, y1, y2, z2, x3, z3];
        Pa := [];
        Pts := [ A1 = [x1,y1,-c], A2 = [-c,y2,z2], A3 =[x3,-c,z3],
                 B1 = [x1,y1,z1], B2 = [x2,y2,z2], B3 = [x3,y3,z3],
                 F1 = [-c, -c, -c], F2 = [-c, -c, c], F3 = [-c, c, -c],
                 F4 = [-c, c, c], F5 = [c, -c, -c], F6 = [c, -c, c],
                 F7 = [c, c, -c] ];
    end if;

    c := 2;
    R := [ x1 = -c..c, y1 = -c..c, z1 = -c..c,
           x2 = -c..c, y2 = -c..c, z2 = -c..c,
           x3 = -c..c, y3 = -c..c, z3 = -c..c ];

    Lo := [ [B1, B2, B3] ];
    Ch := [ [A1, B1], [A2, B2], [A3, B3],
            [F1,F3,F4,F2,F1], [F1,F3,F7,F5,F1], [F1,F5,F6,F2,F1]  ];
    Ac := [ ];
    M := "Parallel 3PPPS";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

# Function: Serial3R
#   Constructs the *Manipulator* object of the serial 3R manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *theta1, d2, d3, d4, r2, r3*,
#       - and *constant* is a numerical value.
#       - By default, the values are *theta1* = 0, *d2* = 1, *d3* = 0.5,
#                                    *d4* = 0.8, *r2* = 0.15, *r3* = 0.1.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the Orthoglide
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Serial3R ():
# > robot[Equations];
# (end code)
Serial3R := proc( morespec ::seq(name=algebraic),
                  precision    := 4              )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac;

    dspec := {theta1=0, d2=1, d3=2, d4=1.5, r2=1, r3=0};

    E:= [ r3*cos(theta2)+(d3+d4*cos(theta3))*sin(theta2)-z,
          ((d2+r3*sin(theta2)+(d3+d4*cos(theta3))*cos(theta2))*cos(theta1)-
           (r2+d4*sin(theta3))*sin(theta1))^2+
          ((d2+r3*sin(theta2)+(d3+d4*cos(theta3))*cos(theta2))*sin(theta1)+
           (r2+d4*sin(theta3))*cos(theta1))^2-rho^2 ];
    Po := [rho, z];
    Ar := [theta1, theta2, theta3];
    Pa := [];
    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    R := [ theta1=-3.2..3.2, theta2=-3.2..3.2, theta3=-3.2...3.2,
           rho=0..5, z=-5..5 ];

    Pts := [O = [0, 0, 0],
            B1=[d2*cos(theta1),d2*sin(theta1),0],
            B2=[d2*cos(theta1)-r2*sin(theta1),d2*sin(theta1)+r2*cos(theta1),0],
            B3=[(d2+d3*cos(theta2))*cos(theta1)-r2*sin(theta1),
                (d2+d3*cos(theta2))*sin(theta1)+r2*cos(theta1),d3*sin(theta2)],
            B4=[(d2+d3*cos(theta2)+r3*sin(theta2))*cos(theta1)-r2*sin(theta1),
                (d2+d3*cos(theta2)+r3*sin(theta2))*sin(theta1)+r2*cos(theta1),
                d3*sin(theta2)+r3*cos(theta2)],
            P =[(d2+(d3+d4*cos(theta3))*cos(theta2)+r3*sin(theta2))*cos(theta1)-
                 (r2+d4*sin(theta3))*sin(theta1),
                (d2+(d3+d4*cos(theta3))*cos(theta2)+r3*sin(theta2))*sin(theta1)+
                 (r2+d4*sin(theta3))*cos(theta1),
                (d3+d4*cos(theta3))*sin(theta2)+r3*cos(theta2)] ];
    Lo := [ ];
    Ch := [ [O, B1, B2, B3, B4, P] ];
    Ac := [ ];
    M := "Serial 3R";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

# Function: Parallel3PRSd
#   Constructs the *Manipulator* object of the 3- _P_ R S manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *r, l*.
#       - and *constant* is a numerical value.
#       - By default, the values are *r*  = 1.3 and *l*=1.5.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the 3- _P R S
#   manipulator with parallel prismatics, whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel3PRSd ():
# > robot[Equations];
# (end code)
Parallel3PRSd := proc( morespec ::seq(name=algebraic),
                       precision    := 4,
                       { quaternions::truefalse := false,
                         tiltandtorsion::truefalse := false,
                         ortho::truefalse       := false,
                         normalize::truefalse   := false } )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac, u,
          u1, u2, u3, a1, a2, a3, v, b1, b2, b3, C1, C2, C3, C4, C5, C6,
          Q, V, P, r2, r3, S, nq, Vz, Vx, RZ, RY, psi;

    dspec := { Ax=0, Ay=0, Az=0, Bx=-1, By=0, Bz=1, Cx=0, Cy=-1, Cz=1,
               r=1.3, l=1 };

    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);

    u  := subs (spec, < Bx-Ax, By-Ay, Bz-Az >);
    v  := subs (spec, < Cx-Ax, Cy-Ay, Cz-Az >);
    u1 := subs (spec, < Ax, Ay, Az >);
    u2 := subs (spec, < Bx, By, Bz >);
    u3 := subs (spec, < Cx, Cy, Cz >);

    if ortho or normalize or tiltandtorsion then 
        S := [u,v,LinearAlgebra:-CrossProduct (u,v)];
        S := LinearAlgebra:-GramSchmidt ( S, ':-conjugate' =false,
                                             ':-normalized'=normalize);
        S := Matrix (S);
    else
        S := 1;
    end if;
#    r2 := sqrt(2)/2;
#    r3 := sqrt(3)/3;
#    S  := << r2    | -r2   | 0       >,
#           < r2*r3 | r2*r3 | 2*r2*r3 >,
#           < -r3   | -r3   | r3      >>;
#    S  := <<  1 | -1 | 0 >,
#           <  1 |  1 | 2 >,
#           < -1 | -1 | 1 >>;
#    S  := << (r3+1)/2 | (r3-1)/2 | r3 >,
#           < (r3-1)/2 | (r3+1)/2 | r3 >,
#           < -r3      | -r3      | r3 >>;

#    u1 := < 0, 0, 0 >;
#    u2 := < 1, 0, 1 >;
#    u3 := < 0, 1, 1 >;
    
#    S := < u2-u3 | u2+u3-2*u1 | LinearAlgebra:-CrossProduct (u2-u1, u3-u1) >;

    #V  := LinearAlgebra:-NullSpace (<u2-u3>^+);
    Vz := LinearAlgebra:-CrossProduct (u2-u1, u3-u1);
    Vx := u2+u3-2*u1;

    a1 := (r*u1 + rho1*Vz);
    a2 := (r*u2 + rho2*Vz);
    a3 := (r*u3 + rho3*Vz);

    if quaternions then
        Q  := < < q1^2+q2^2   | q2*q3-q1*q4 | q2*q4+q1*q3 >, 
                < q2*q3+q1*q4 | q1^2+q3^2   | q3*q4-q1*q2 >,
                < q2*q4-q1*q3 | q3*q4+q1*q2 | q1^2+q4^2   > >;
        R  := 2*Q - 1;
        nq := S.<Q2,Q3,Q4>;
        R  := map(expand,subs ({q1=Q1, q2=nq[1], q3=nq[2], q4=nq[3]}, R));
        b1 := a1 + s*Vz + t*Vx;
        P  := b1 - R.u1;
        b2 := P  + R.u2;
        b3 := P  + R.u3;
        C1 := (b1-a1)^+ . (b1-a1) - l^2;
        C2 := (b2-a2)^+ . (b2-a2) - l^2;
        C3 := (b3-a3)^+ . (b3-a3) - l^2;
        C5 := (b2-a2)^+ . (u3-u1);
        C6 := (b3-a3)^+ . (u1-u2);
        E  := [ z-(b1+b2+b3)^+.Vz/3, C1, C2, C3, C5, C6,
                q1^2 + q2^2 + q3^2 + q4^2 - 1 ];
        E  := expand(subs ({q1=Q1, q2=nq[1], q3=nq[2], q4=nq[3]}, E));
        Po := [s, t, Q2, Q3, Q4];
        Ar := [rho1, rho2, rho3];
        Pa := [Q1,z];
        Pts := [ F1 = convert(r*u1,list), F2 = convert(r*u2,list),
                 F3 = convert(r*u3,list),
                 A1 = convert(a1,list), A2 = convert(a2,list),
                 A3 = convert(a3,list),
                 B1 = convert(eval(b1),list), B2 = convert(eval(b2),list),
                 B3 = convert(eval(b3),list) ];
    elif tiltandtorsion then
        psi := sigma - phi;
        RZ := a -> <<cos(a) | -sin(a) | 0>,
                    <sin(a) | cos(a)  | 0>,
                    <0      | 0       | 1>>;
        RY := a -> <<cos(a)  | 0 | sin(a)>,
                    <0       | 1 | 0     >,
                    <-sin(a) | 0 | cos(a)>>;
        R := RZ(phi).RY(theta).RZ(psi);
        R := S.R.S^(-1);

        b1 := a1 + s*Vz + t*Vx;
        P  := b1 - R.u1;
        b2 := P  + R.u2;
        b3 := P  + R.u3;
        C1 := (b1-a1)^+ . (b1-a1) - l^2;
        C2 := (b2-a2)^+ . (b2-a2) - l^2;
        C3 := (b3-a3)^+ . (b3-a3) - l^2;
        C5 := (b2-a2)^+ . (u3-u1);
        C6 := (b3-a3)^+ . (u2-u1);
        E  := [ C1, C2, C3, C5 ];
        Po := [s, t, sigma, phi, theta];
        Ar := [rho1, rho2, rho3];
        Pa := [];
        Pts := [ F1 = convert(r*u1,list), F2 = convert(r*u2,list),
                 F3 = convert(r*u3,list),
                 A1 = convert(a1,list), A2 = convert(a2,list),
                 A3 = convert(a3,list),
                 B1 = convert(eval(b1),list), B2 = convert(eval(b2),list),
                 B3 = convert(eval(b3),list) ];
    else
        b1 := a1 + l*c1*u1 + l*s1*(u2+u3);
        b2 := a2 + l*c2*u2 + l*s2*(u1+u3);
        b3 := a3 + l*c3*u3 + l*s3*(u1+u2);
        C1 := (b2-b1)^+ . (b2-b1) - 2;
        C2 := (b3-b2)^+ . (b3-b2) - 2;
        C3 := (b1-b3)^+ . (b1-b3) - 2;
        C4 := c1^2+2*s1^2-2;
        C5 := c2^2+2*s2^2-2;
        C6 := c3^2+2*s3^2-2;
        E  := [ C1, C2, C3, C4, C5, C6 ];
        Po := [c1,s1,c2,s2,c3,s3];
        Ar := [rho1, rho2, rho3];
        Pts := [ F1 = convert(r*u1,list), F2 = convert(r*u2,list),
                 F3 = convert(r*u3,list),
                 A1 = convert(a1,list), A2 = convert(a2,list),
                 A3 = convert(a3,list),
                 B1 = convert(eval(b1),list), B2 = convert(eval(b2),list),
                 B3 = convert(eval(b3),list) ];
    end if;

#    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
#                                          dspec);
    R := [ rho1 = 0..5, rho2 = 0..5, rho3 = 0..5,
           q1 = -1..1, q2 = -1..1, q3 = -1..1, q4 = -1..1 ];

    Lo := [ [B1, B2, B3] ];
    Ch := [ [A1, B1], [A2, B2], [A3, B3] ];
    Ac := [ [F1,A1], [F2,A2], [F3,A3] ];
    M := "Parallel 3PRSd";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

# Function: Parallel3PRSc
#   Constructs the *Manipulator* object of the 3- _P_ R S manipulator.
#
# Parameters:
#   name = constant - the geometric parameters of the robot,
#    
#       - where *name* is one of *r, l*.
#       - and *constant* is a numerical value.
#       - By default, the values are *r*  = 1.3 and *l*=1.5.
#
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
# Returns:
#   A *Manipulator* data structure representing the Orthoglide
#   manipulator whose dimensions are given in input.
#
# Example:
# (start code)
# > robot := Parallel3PRSc ():
# > robot[Equations];
# (end code)
Parallel3PRSc:= proc( morespec ::seq(name=algebraic),
                       precision    := 4,
                       { quaternions::truefalse := false } )
    local E, Po, Ar, Pa, M, C, R, spec, dspec, Pts, Lo, Ch, Ac,
          u1, u2, u3, a1, a2, a3, v, b1, b2, b3, C1, C2, C3, C4, C5, C6,
          Q, V, P, r2, r3, S, nq, Vx, Vz;
    dspec := { r=1, l=1.5 };

#    u1 := < 0, 0, 0 >;
#    u2 := < 1, 0, 1 >;
#    u3 := < 0, 1, 1 >;

    u1 := < 1, 0, 0 >;
    u2 := < 0, 1, 0 >;
    u3 := < 0, 0, 1 >;

    # Change of variable for torsion free quaternions
    S := < LinearAlgebra:-Normalize (u2-u3,2) | 
           LinearAlgebra:-Normalize (u2+u3-2*u1) |
           LinearAlgebra:-Normalize (LinearAlgebra:-CrossProduct(u2-u1, u3-u1),
                                     2) >;

    #V  := LinearAlgebra:-NullSpace (<u2-u3>^+);
    Vz := LinearAlgebra:-CrossProduct (u2-u1, u3-u1);
    Vx := u2+u3-2*u1;

    a1 := (r*u1 + rho1*(u2+u3-2*u1));
    a2 := (r*u2 + rho2*(u1+u3-2*u2));
    a3 := (r*u3 + rho3*(u1+u2-2*u3));

    if quaternions then
        Q  := < < q1^2+q2^2   | q2*q3-q1*q4 | q2*q4+q1*q3 >, 
                < q2*q3+q1*q4 | q1^2+q3^2   | q3*q4-q1*q2 >,
                < q2*q4-q1*q3 | q3*q4+q1*q2 | q1^2+q4^2   > >;
        R  := 2*Q - 1;
        nq := S.<Q2,Q3,Q4>;
        R  := expand(subs ({q1=Q1, q2=nq[1], q3=nq[2], q4=nq[3]}, R));
        b1 := a1 + s*Vz + t*Vx;
        P  := b1 - R.u1;
        b2 := P  + R.u2;
        b3 := P  + R.u3;
        C1 := (b1-a1)^+ . (b1-a1) - l^2;
        C2 := (b2-a2)^+ . (b2-a2) - l^2;
        C3 := (b3-a3)^+ . (b3-a3) - l^2;
        C5 := (b2-a2)^+ . (u3-u1);
        C6 := (b3-a3)^+ . (u1-u2);
        E  := [ z-(b1+b2+b3)^+.Vz/3, C1, C2, C3, C5, C6,
                q1^2 + q2^2 + q3^2 + q4^2 - 1 ];
        E  := expand(subs ({q1=Q1, q2=nq[1], q3=nq[2], q4=nq[3]}, E));
        Po := [s, t, Q2, Q3, Q4];
        Ar := [rho1, rho2, rho3];
        Pa := [Q1,z];
        Pts := [ F1 = convert(r*u1,list), F2 = convert(r*u2,list),
                 F3 = convert(r*u3,list),
                 A1 = convert(a1,list), A2 = convert(a2,list),
                 A3 = convert(a3,list),
                 B1 = convert(eval(b1),list), B2 = convert(eval(b2),list),
                 B3 = convert(eval(b3),list) ];
    else
        b1 := a1 + l*c1*u1 + l*s1*(u2+u3);
        b2 := a2 + l*c2*u2 + l*s2*(u1+u3);
        b3 := a3 + l*c3*u3 + l*s3*(u1+u2);
        C1 := (b2-b1)^+ . (b2-b1) - 2;
        C2 := (b3-b2)^+ . (b3-b2) - 2;
        C3 := (b1-b3)^+ . (b1-b3) - 2;
        C4 := c1^2+2*s1^2-2;
        C5 := c2^2+2*s2^2-2;
        C6 := c3^2+2*s3^2-2;
        E  := [ C1, C2, C3, C4, C5, C6 ];
        Po := [c1,s1,c2,s2,c3,s3];
        Ar := [rho1, rho2, rho3];
        Pts := [ F1 = convert(r*u1,list), F2 = convert(r*u2,list),
                 F3 = convert(r*u3,list),
                 A1 = convert(a1,list), A2 = convert(a2,list),
                 A3 = convert(a3,list),
                 B1 = convert(eval(b1),list), B2 = convert(eval(b2),list),
                 B3 = convert(eval(b3),list) ];
    end if;

    spec := {morespec} union remove (s->member(lhs(s),map(lhs,{morespec})),
                                          dspec);
    R := [ rho1 = 0..5, rho2 = 0..5, rho3 = 0..5,
           q1 = -1..1, q2 = -1..1, q3 = -1..1, q4 = -1..1 ];

    Lo := [ [B1, B2, B3] ];
    Ch := [ [A1, B1], [A2, B2], [A3, B3] ];
    Ac := [ [F1,A1], [F2,A2], [F3,A3] ];
    M := "Parallel 3PRSc";

    CreateManipulator ( E, Po, Ar, Pa, spec, R, Pts, Lo, Ch, Ac, M, precision );
end proc;

