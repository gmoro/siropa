# Title:
# Class: Siropa
#   Provides equation modeling, analysing and plotting functions for
#   different manipulators. Its functions are described in:
#   - <Modeling>   - functions to model manipulators
#   - <Analysing>  - functions to analyze singularities and more
#   - <Plotting>   - functions to plot
#   - <Mechanisms> - Manipulators database

#$define Manipulator record ('Equations','Constraints','PoseVariables','ArticularVariables','PassiveVariables','GeometricParameters','GenericEquations','GenericConstraints','Precision','PoseValues','ArticularValues','GeometricValues','Points','Loops','Chains','Actuators','Model')



Siropa := module()
local ModuleLoad,
      plotActuator,
      plotActuator3D,
      filterCurve,
      surfexPrefix,
      surfexPostfix,
      surfexEquation,
      surfexInequation,
      isole,
      plotHorizontalLines,
      plotVerticalPlanes,
      plotVerticalFaces,
      plotHorizontalLeaves,
      listPoints,
      getBound,
      emptyDisplay2d,
      emptyDisplay3d,
      intervalOverlap,
      intervalValue,
      intervalMix,
      cacheCellDescription,
      linkColumns,
      sortCells,
      setAdd,
      setValues,
      auxDVNumberOfSolutionsPlus,
      CellDescriptionPlus;

export #type
       Manipulator,

       # modelling.mpl
       CreateManipulator,
       SubsPlus,
       SubsParameters,
       UnassignParameters,

       # mechanisms.mpl
       Parallel_3RPR,
       Parallel_3RPR_full,
       Parallel_3PRR,
       ParallelPRP2PRR,
       Parallel3PPPS,
       Parallel3PRSc,
       Parallel3PRSd,
       Parallel_RPRRP,
       Parallel_RR_RRR,
       Parallel_PRRP,
       Orthoglide,
       ParallelRPR2PRR,
       Serial3R,
       
       # analysing.mpl
       ConstraintEquations,
       Type1SingularityEquations,
       SerialSingularities,
       Type2SingularityEquations,
       ParallelSingularities,
       ParallelCuspidal,
       SerialCuspidal,
       CuspidalEquations,
       InfiniteEquations,
       Configurations,
       Projection,
       CellDecompositionPlus,
       NumberOfSolutionsPlus,
       DVNumberOfSolutionsPlus,
       IsIntervalEmpty,
       CellGraph,
       PseudoSingularityDecomposition,
       DirectInverseKinematics,
       CellArea2D,
       CellLocationPlus,

       # plotting.mpl
       Plot2D,
       PlotCurve3D,
       Plot3D,
       Plot3Dglsurf,
       Plot3Dsurfex,
       PlotWorkspace,
       PlotRobot2D,
       PlotRobot3D,
       PlotCell3D,
       PlotCell2D,
       Trajectory,
       ImageTrajectory,
       Path,
       SetCellColors;

option package;

ModuleLoad := proc()
    TypeTools:-AddType (Manipulator, 
                        record ('Equations','Constraints','PoseVariables',
                                'ArticularVariables','PassiveVariables',
                                'GeometricParameters','GenericEquations',
                                'GenericConstraints','Precision','PoseValues',
                                'ArticularValues','GeometricValues','Points',
                                'Loops','Chains','Actuators','Model'));
#    unprotect(fgbrs:-fgb_gbasis);
#    fgbrs:-fgb_gbasis := proc(polys0::list(polynom),
#                              vars1::list(name), vars2::list(name) := [],
#                              char::nonnegint := 0)
#        local vars, b, p, i, k, opts, gb, next_m, fgbtime, polys, nb, nf, c, zeroes, n;
#        opts := table(["record" = 0, "step" = `if`(char = 0, 8, 1),
#            "block" = 0, "verb" = 0, "elim" = false,
#            "method" = "FGB_COMPUTE_GBASIS", "nb" = 0, "elim force" = false,
#            "matrix" = Matrix([5, 0, 300000, 0, 0, NULL],
#                              'datatype' = 'integer'[4], 'order' = 'C_order'), 
#            op(select(type, [args], 'equation'))]);
#        vars := [op(vars1), op(vars2)];
#        b := 32;
#        nb := opts["nb"];
#        nf := evalb(opts["method"] = "FGB_COMPUTE_GBASIS_NF");
#        if char < 2^(1/2*b) then
#            if char = 0 then
#                polys := {op(remove(`=`, polys0[nb + 1 .. -1], 0))};
#                c := polys0[1 .. nb]
#            else
#                polys :=
#                    {op(remove(`=`, modp(polys0[nb + 1 .. -1], char), 0))};
#                c := modp(polys0[1 .. nb], char)
#            end if;
#            if polys = {} then
#                if not nf then return []
#                else return map(z -> (
#                    [Groebner:-LeadingTerm(z, 'lexdeg'(vars1, vars2)), z],
#                    [1, 1, 1]), ListTools:-Reverse(c))
#                end if
#            elif not has(polys, {op(vars1), op(vars2)}) then
#                if not nf then return [[1, 1, 1]]
#                else return [seq(op([[0, 1, 0], [1, 1, 1]]), i = 1 .. nb)]
#                end if
#            elif nops(polys) = 1 and nb = 0 then return [[
#                Groebner:-LeadingTerm(op(polys), 'lexdeg'(vars1, vars2)),
#                op(polys)]]
#            end if;
#            zeroes := [ListTools:-SearchAll(0, c)];
#            c := remove(`=`, c, 0);
#            opts["nb"] := nops(c);
#            nb := nops(c);
#            if nf and nb = 0 then return
#                [seq(op([[0, 1, 0], [1, 1, 1]]), i = 1 .. nops(zeroes))]
#            end if;
#            kernelopts(opaquemodules=false);
#            if char = 0 then fgbrs:-fgb_gbasis_INT_init(nops(vars1), nops(vars2),
#                nops(polys) + nb, opts["record"])
#            else fgbrs:-fgb_gbasis_init(nops(vars1), nops(vars2), nops(polys) + nb,
#                char, opts["record"])
#            end if;
#            try
#                for i to nb do fgbrs:-poly2fgb(c[i], vars, char) end do;
#                for p in polys do fgbrs:-poly2fgb(p, vars, char) end do
#            catch "FGb interface error: power products should be < 127":
#                error
#                "exponents must be less than 128 in positive characteristic"
#            catch "FGb interface error: power products should be < 32767":
#                error "exponents must be less than 32768"
#            catch: error "first argument must be a list of polynomials in t\
#                he variables %1 with integer coefficients", vars
#            end try
#        else error "characteristic too large, %1", char
#        end if;
#        if char = 0 then fgbrs:-fgb_gbasis_INT_compute('fgbtime', 'next_m', 'k', 0,
#            seq(opts[i], i =
#            ["verb", "step", "block", "elim", "method", "nb", "elim force", "matrix"]))
#        else fgbrs:-fgb_gbasis_compute('fgbtime', 'next_m', 'k', 0, seq(opts[i], i =
#            ["verb", "step", "block", "elim", "method", "nb", "elim force", "matrix"]))
#        end if;
#        gb := fgbrs:-fgb2polys(k, vars, next_m, char);
#        if nf and 0 < nops(zeroes) then for i to nops(zeroes) do
#                n := nops(gb) + 3 - 2*zeroes[i];
#                gb := [op(1 .. n - 1, gb), [0, 1, 0], [1, 1, 1],
#                    op(n .. nops(gb), gb)]
#            end do
#        end if;
#        kernelopts(opaquemodules=true);
#        return gb
#    end proc;
#    protect(fgbrs:-fgb_gbasis);
end proc;

$include<Siropa/modeling.mpl>
$include<Siropa/mechanisms.mpl>
$include<Siropa/analysing.mpl>
$include<Siropa/plotting.mpl>

end module;

