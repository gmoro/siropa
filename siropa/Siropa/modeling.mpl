# Title: Modeling


# Function: CreateManipulator
#   Constructs a data structure of type *Manipulator*.
#
# Parameters:
#   sys  - list of polynomials, polynomial equalities and 
#          polynomial inequalities with rational coefficients: 
#          the implicit equations and constraints of the considered manipulator
#   cart - list of names: the pose variables
#   arti      (optional) - list of names: the control parameters;
#                          default value: the names of *sys* not in *vars*
#   passive   (optional) - list of names: the passive variables;
#                          default value: []
#   geompars  (optional) - list of names: the geometric parameters;
#                          default value: the names of *sys* not in *cart*,
#                                         nor in *arti*
#   spec (optional)      - a list of equations of the form *name=formula* 
#                          where *name* is a parameter name
#                          and *formula* a polynom with trigonometric function;
#                          the new variables in a formula are handled in 
#                          the same way as the replaced variable.
#                          default value: []
#   points    (optional) - list of *name=list*: the points of the robot with
#                                               their coordinates;
#                          default value: []
#   loops     (optional) - list of list of names: the frame loops of the robot
#                          default value: []
#   chains    (optional) - list of list of names: the frame chains of the robot
#                          default value: []
#   actuators (optional) - list of names or list of names : the robot actuators;
#                          a list of name is for a leg actuator, a name is for
#                          an angle actuator;
#                          default value: []
#   model (optional)     - string: the name of the model;
#                          default value: "No name"
#   precision (optional) - an integer: the number of significative digits;
#                          default value: 4
#
# Returns:
#   A data structure of type *Manipulator* containing the fields:
#
#   Equations           - a list of polynomials *[p1, ..., pk]*:
#                         the modelling equations
#   Constraints         - a list of strict inequalities:
#                         the constraint inequalities
#   PoseVariables       - a list of names: the variables defining the pose
#   ArticularVariables  - a list of names: the control parameters
#   PassiveVariables    - a list of names: the remaining variables
#   GeometricParameters - a list of names: the geometric parameters
#   GenericEquations    - a list of polynomials: the modelling equations
#                         with symbolic geometric parameters
#   GenericConstraints  - a list of strict inequalities:
#                         the constraint inequalities appearing in *sys*
#   Precision           - an integer: the number of correct digits
#   PoseValues          - the pose values substituted in 
#                         the *GenericEquations* to get the *Equations*
#   ArticularValues     - the articular values substituted in 
#                         the *GenericEquations* to get the *Equations*
#   PassiveValues       - the passive values substituted in 
#                         the *GenericEquations* to get the *Equations*
#   GeometricValues     - the geometric values substituted in 
#                         the *GenericEquations* to get the *Equations*
#   DefaultPlotRanges   - ranges used by default for plotting if provided
#   Points              - the points coordinate of the robot
#   Loops               - the frame loops of the robot
#   Chains              - the frame chain of the robot
#   Actuators           - the actuators of the robot
#   Model               - a string: the name of the modelling
#
# Remark:
#   - Polynomials *p* appearing in *sys*, *Equations*, *GenericEquations*
#     are considered implicitly as *p=0*.
#   - When a control parameter value is specified in *spec*,
#     the parameter name is removed from the *ControlParemeters* field.
#     This is not the case for the geometric parameters that appears in the
#     field *GeometricParameters* even if they are specified.
#
# Example:
# (begin code)
# > CreateManipulator ( [x=t,y=t, -1<t, t<1], [x,y], [t], "Dummy" );
# Record(Equations = [x - t, y - t], Constraints = [-1 < t, t < 1],
# 
#     Variables = [x, y], ControlParameters = [t], GeometricParameters = [],
# 
#     GenericEquations = [x - t, y - t], GenericConstraints = [-1 < t, t < 1],
# 
#     ParameterValues = [], Model = "Dummy")
# (end code)
CreateManipulator := proc ( sys       ::list({algebraic, algebraic=algebraic,
                                                         algebraic<algebraic}),
                            cart      ::list(name),
                            arti      ::list(name)      := [op(indets(sys,name) 
                                                   minus {op(vars)}
                                                   minus map(lhs,{op(spec)}))],
                            passive   ::list(name)      := [],
                            geompars  ::list(name)      := [op(indets(sys,name)
                                                          minus {op(cart)}
                                                          minus {op(arti)}
                                                          minus {op(passive)})],
                            spec      ::{list,set}(name=algebraic) := [],
                            plotrange ::list(name=range)    := [],
                            points    ::list(name=list(algebraic)) := [],
                            loops     ::list(list(name))    := [],
                            chains    ::list(list(name))    := [],
                            actuators ::list({list(name),name}) := [],
                            model     ::string              := "No name",
                            precision ::integer             := 4,
                            { noradical ::truefalse         := false })
    local pol, nonpol, equ, ineq, fspec, rspec, trigvars, trigspec, varspec,
          equspec, ineqspec, tfspec, u, nspec, ncart, narti, npassive,
          ngeompars;

    pol, nonpol := selectremove (type, convert(sys,rational,exact), algebraic);
    equ, ineq   := selectremove (type, nonpol, `=`);
    equ := [ op(pol), op(map (lhs-rhs, equ)) ];
    
    equspec  := AlgebraicTools:-TrigonometricSubs (op(spec), equ, precision,
                                                   ':-noradical' = noradical);
    ineqspec := AlgebraicTools:-TrigonometricSubs (op(spec), ineq, precision,
                                                   ':-noradical' = noradical);

    nspec := remove( s->member(lhs(s),indets(rhs(s))), spec);
    ncart := `union`(seq (indets(rhs(s),name) minus indets(rhs(s),constant),
                          s in select(v->member(lhs(v),cart), spec)))
             minus indets(sys,name) union {op(cart)};
    narti := `union`(seq (indets(rhs(s),name) minus indets(rhs(s),constant),
                          s in select(v->member(lhs(v),arti), spec)))
             minus indets(sys,name) union {op(arti)};
    npassive := `union`(seq (indets(rhs(s),name) minus indets(rhs(s),constant),
                             s in select(v->member(lhs(v),passive), spec)))
                minus indets(sys,name) union {op(passive)};
    ngeompars := `union`(seq (indets(rhs(s),name) minus indets(rhs(s),constant),
                              s in select(v->member(lhs(v),geompars), spec)))
                minus indets(sys,name) union {op(geompars)};
    
    
    return Record ( 'Equations'           = equspec,
                    'Constraints'         = ineqspec,
                    'PoseVariables'       = remove (p->member(p,map(lhs,nspec)),
                                                    [op(ncart)]),
                    'ArticularVariables'  = remove (p->member(p,map(lhs,nspec)),
                                                    [op(narti)]),
                    'PassiveVariables'    = remove (p->member(p,map(lhs,nspec)),
                                                    [op(npassive)]),
                    'GeometricParameters' = remove (p->member(p,map(lhs,nspec)),
                                                    [op(ngeompars)]),
                    'GenericEquations'    = equ,
                    'GenericConstraints'  = ineq,
                    'Precision'           = precision,
                    'PoseValues'          = select (s->member(lhs(s),cart),
                                                    [op(spec)]),
                    'ArticularValues'     = select (s->member(lhs(s),arti),
                                                    [op(spec)]),
                    'PassiveValues'     = select (s->member(lhs(s),passive),
                                                    [op(spec)]),
                    'GeometricValues'     = select (s->member(lhs(s),geompars),
                                                    [op(spec)]),
                    'DefaultPlotRanges'   = plotrange,
                    'Points'              = points,
                    'Loops'               = loops,
                    'Chains'              = chains,
                    'Actuators'           = actuators,
                    'Model' = model );
end proc;


# Function: SubsPlus
#   Substitute coherently angles in a system.
#
# Parameters:
#   spec - sequence of equalities *var=value* where *var* is a name and
#          *value* a numerical value.
#   formula - equation, inequation, polynomial or list of those,
#             with trigonometric and algebraic expressions.
#   precision (optional) - integer: the number of digits that must be kept
#                          when modifying the values in *spec*.
#
# Returns:
#   The input *formula* where the name in *spec* have been replaced by
#   the corresponding values; the algebraic relations satisfied by cos and sin
#   are still valid with the chosen numerical values.
#
# Example:
# (begin code)
# > SubsPlus(a=3, cos(a)^2+sin(a)^2);
#                                        1
# (end code)
SubsPlus := proc (spec      ::seq (name=algebraic),
                  formula   ::{algebraic, algebraic<algebraic,
                               list({algebraic,algebraic=algebraic,
                                     algebraic<algebraic})},
                  precision ::integer          := 4)
    AlgebraicTools:-TrigonometricSubs (spec, formula, precision);
end proc;

# Function: SubsParameters
#   Specify parameter values in a *Manipulator*
#
# Parameters:
#   spec  - a sequence of equations of the form *name=formula* 
#           where *name* is a parameter name
#           and *formula* a polynom with trigonometric functions
#   robot - a *Manipulator* object
#   precision = integer (optional) - the precision, where *integer* is 
#                                    the number of significative digits;
#                                    default value: 4.
#
#
# Returns
#   A *Manipulator* object, equivalent to the input *Manipulator*
#   where some parameter have been specified by a numerical value
#
# Example:
# (begin code)
# > robot := CreateManipulator ( [x=t,y=t, -1<t, t<1], [x,y], [t], "Dummy" );
# > SubsParameters (t=0.7, robot);
# Record(Equations = [x - 7/10, y - 7/10], Constraints = [-1 < 7/10, 7/10 < 1],
# 
#     Variables = [x, y], ControlParameters = [], GeometricParameters = [],
# 
#     GenericEquations = [x - t, y - t], GenericConstraints = [-1 < t, t < 1],
# 
#     ParameterValues = [t = 0.7], Model = "Dummy")
# 
# 
# 
# 
# (end code)
SubsParameters := proc (spec  ::seq (name=algebraic),
                        robot ::Manipulator,
                        precision ::integer          := 4)
    local pars, lspec, fspec, rspec, x, maxprecision;

    lspec := select (s->member (lhs(s), [op(robot:-ArticularVariables),
                                         op(map(lhs,robot:-ArticularValues)),
                                         op(robot:-PoseVariables),
                                         op(map(lhs,robot:-PoseValues)),
                                         op(robot:-GeometricParameters),
                                         op(map(lhs,robot:-GeometricValues)),
                                         op(robot:-PassiveVariables),
                                         op(map(lhs,robot:-PassiveValues))]),
                     [spec]);
    
    if nops(lspec)=0 then 
        return copy (robot);
    end if;



    pars  := map (lhs, lspec);
    
    #For rounding bug
    #map (convert, lspec, float, 2*precision);

    lspec := [ op(lspec),
               op(remove(s->member(lhs(s),pars),robot:-ArticularValues)),
               op(remove(s->member(lhs(s),pars),robot:-PoseValues)),
               op(remove(s->member(lhs(s),pars),robot:-GeometricValues)),
               op(remove(s->member(lhs(s),pars),robot:-PassiveValues)) ];
#               op(map (convert, lspec, float, precision)) ];

    maxprecision := max (precision, robot:-Precision);
#                         map (s->ilog10(SFloatMantissa(rhs(s)))+1, fspec));

    CreateManipulator ( [op(robot:-GenericEquations),
                         op(robot:-GenericConstraints)],
                         [op(robot:-PoseVariables),
                          op(map(lhs,robot:-PoseValues))],
                         [op(robot:-ArticularVariables),
                          op(map(lhs,robot:-ArticularValues))],
                         [op(robot:-PassiveVariables),
                          op(map(lhs,robot:-PassiveValues))],
                         [op(robot:-GeometricParameters),
                          op(map(lhs,robot:-GeometricValues))],
                         lspec,
                         robot:-DefaultPlotRanges,
                         robot:-Points,
                         robot:-Loops,
                         robot:-Chains,
                         robot:-Actuators,
                         robot:-Model,
                         maxprecision  );

end proc;

# Function: UnassignParameters
#   Release parameters in a *Manipulator*
#
# Parameters:
#   pars  - a sequence of equations of the form *name=value* 
#           where *name* is a parameter name
#           and *value* its numerical value
#   robot - a *Manipulator* object
#
# Returns
#   A *Manipulator* object, equivalent to the input *Manipulator*
#   where some parameter have been unassigned.
#
# Example:
# (begin code)
# > robot := CreateManipulator ( [x=t,y=t,-1<t,t<1],[x,y],[t],[t=0.7],"Dummy" );
# robot := Record(Equations = [x - 7/10, y - 7/10],
# 
#     Constraints = [-1 < 7/10, 7/10 < 1], Variables = [x, y],
# 
#     ControlParameters = [], GeometricParameters = [],
# 
#     GenericEquations = [x - t, y - t], GenericConstraints = [-1 < t, t < 1],
# 
#     ParameterValues = [t = 0.7], Model = "Dummy")
# 
# > UnassignParameters(t,%); 
# Record(Equations = [x - t, y - t], Constraints = [-1 < t, t < 1],
# 
#     Variables = [x, y], ControlParameters = [t], GeometricParameters = [],
# 
#     GenericEquations = [x - t, y - t], GenericConstraints = [-1 < t, t < 1],
# 
#     ParameterValues = [], Model = "No name")
# (end code)
UnassignParameters := proc (pars  ::seq (And(name,Not(Manipulator))),
                            robot ::Manipulator)
    local posevars, artivars, geompars, maxprecision;

#   posevars    := select (p->member(p,map(lhs,robot:-PoseValues)), [pars]);
#   artivars    := select (p->member(p,map(lhs,robot:-ArticularValues)),[pars]);
#   geompars    := select (p->member(p,map(lhs,robot:-GeometricValues)),[pars]);

    posevars := subs(map(v->v=NULL,robot:-PoseVariables),
                     map(lhs,robot:-PoseValues)                );
    artivars := subs(map(v->v=NULL,robot:-ArticularVariables),
                     map(lhs,robot:-ArticularValues)           );
    geompars := subs(map(v->v=NULL,robot:-GeometricParameters),
                     map(lhs,robot:-GeometricValues)           );


#    maxprecision := max (map (s->ilog10(SFloatMantissa(rhs(s)))+1,
#                              [op(robot:-PoseValues),
#                               op(robot:-ArticularValues),
#                               op(robot:-GeometricValues)]));
    maxprecision := robot:-Precision;

    CreateManipulator ( [op(robot:-GenericEquations),
                         op(robot:-GenericConstraints)],
                         [op(robot:-PoseVariables),op(posevars)],
                         [op(robot:-ArticularVariables),op(artivars)],
                         robot:-PassiveVariables,
                         [op(robot:-GeometricParameters),op(geompars)],
                         remove (s->member(lhs(s),[pars]),
                                 [op(robot:-PoseValues),
                                  op(robot:-ArticularValues),
                                  op(robot:-GeometricValues)]),
                         robot:-DefaultPlotRanges,
                         robot:-Points,
                         robot:-Loops,
                         robot:-Chains,
                         robot:-Actuators,
                         robot:-Model,
                         maxprecision  );


end proc;

# SetArticular := proc ( vars  ::seq(name),
#                        robot ::Manipulator )
#     local valvars, namvars, newrobot;
# 
#     valvars := select ( x->member (lhs(x),[vars]), [op(robot:-GeometricValues),
#                                                     op(robot:-PoseValues)] );
#     namvars := select ( x->member (x,[vars]), [op(robot:-GeometricVariables),
#                                                op(robot:-PoseVariables)] );
# 
#     newrobot := copy (robot);
# 
#     newrobot:-ArticularVariables := [namvars];
#     newrobot:-ArticularValues := [valvars];
# 
#     newrobot
# 
# 
# 
# end proc;

#Quaternion := proc ( q1::name := q1,
#                     q2::name := q2,
#                     q3::name := q3,
#                     q4::name := q4 )
#        Q  := < < q1^2+q2^2   | q2*q3-q1*q4 | q2*q4+q1*q3 >, 
#                < q2*q3+q1*q4 | q1^2+q3^2   | q3*q4-q1*q2 >,
#                < q2*q4-q1*q3 | q3*q4+q1*q2 | q1^2+q4^2   > >;
#        R  := 2*Q - 1;
#        return R;

