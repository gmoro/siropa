# Title: Analysing

# Function: ConstraintEquations
#   Computes the implicit equations induced by the constraints.
#
# Parameters:
#   robot - a data structure returned by a function mechanisms.
#
# Returns:
#   A list of list of polynomials: each list represents a component of
#   the equations satisfied by the constraints.
ConstraintEquations := proc (robot::Manipulator,
                             {constraints ::truefalse := false})
    if robot:-Constraints = [] then return [[1]] end if;
    if constraints then
        map (c-> [op(robot:-Equations), lhs(c)-rhs(c),
                  op(remove(`=`,robot:-Constraints,c))], robot:-Constraints);
    else
        map (c-> [op(robot:-Equations), lhs(c)-rhs(c)], robot:-Constraints);
    end if;
end proc;


# Function: Type1SingularityEquations / SerialSingularities
#   Computes the implicit equations satisfied by the singularities.
#
# Parameters:
#   robot - a data structure returned by a function mechanisms.
#   noinf=b (optional) - *b* is a boolean; if true, the output will not contain
#                       pose values reached by infinitely many articular values;
#                       default value: false.
#
# Returns:
#   A list of polynomials: the equations satisfied by the 
#   singular poses of the 1st kind.
Type1SingularityEquations := proc (robot::Manipulator,
                                   {noinf::truefalse := false})
    local s1, inf, i, vars;

    vars := subs (map (x->x=NULL, robot:-PoseVariables),
                  [op(robot:-ArticularVariables), op(robot:-PassiveVariables)]);

    s1 := AlgebraicTools:-IteratedJacobian (robot:-Equations,
                                            vars,
                                            1);
                                            
    if noinf then
        inf := AlgebraicTools:-PropernessDefect (robot:-Equations,
                                            [op(robot:-PoseVariables),
                                             op(robot:-GeometricParameters)]);
        for i in {op(inf[..,1])} do
            s1 := AlgebraicTools:-PolynomialSaturation (s1, i);
        end do;
    end if;

    return s1;
end proc;

SerialSingularities := Type1SingularityEquations;


# Function: Type2SingularityEquations / ParallelSingularities
#   Computes the implicit equations satisfied by the singularities.
#
# Parameters:
#   robot - a data structure returned by a function mechanisms.
#   noinf=b (optional) - *b* is a boolean; if true, the output will not contain
#                       articular values leading to infinitely many pose values;
#                       default value: false.
#
# Returns:
#   A list of polynomials: the equations satisfied by the 
#   singular poses of the 2nd kind.
Type2SingularityEquations := proc (robot::Manipulator,
                                   {noinf::truefalse := false})
    local s2, inf, i, vars;
    vars := subs ( map (x->x=NULL, robot:-ArticularVariables),
                   [op(robot:-PoseVariables), op(robot:-PassiveVariables)] );
    

    s2 := AlgebraicTools:-IteratedJacobian (robot:-Equations,
                                            vars,
                                            1);
    if noinf then
        inf := AlgebraicTools:-PropernessDefect (robot:-Equations,
                                            [op(robot:-ArticularVariables),
                                             op(robot:-GeometricParameters)]);
        for i in {op(inf[..,1])} do
            s2 := AlgebraicTools:-PolynomialSaturation (s2, i);
        end do;
    end if;

    return s2;
end proc;

ParallelSingularities := Type2SingularityEquations;

# Function: InfiniteEquations
#   Computes the equations where the manipulator has infinitely many solutions.
#
# Parameters:
#   robot - a data structure returned by a function mechanisms.
#   vars  - a list of names: the space above which the number of solutions is
#                            analyzed;
#           default value: the articular and geometric variables.
#
# Returns:
#   A list of polynomials: the equations satisfied by the 
#   variables above which the manipulator has infinitely many solutions.
InfiniteEquations := proc (robot::Manipulator,
                           vars ::list(name) :=[op(robot:-ArticularVariables),
                                                op(robot:-GeometricParameters)])
    local Lsys, i, previous;
    
    previous := [];
    Lsys := [[]];
    for i from 1 to nops(vars)+1 while {op(Lsys)} <> {op(previous)} do
        previous := Lsys;
        Lsys := map(s->(op@AlgebraicTools:-PropernessDefect)
                         ( [op(robot:-Equations),op(s)], vars ), Lsys);
    end do;
    if Lsys=[] then return [] end if;
    return AlgebraicTools:-FactorSystem (Lsys);
#    return map (op@AlgebraicTools:-Split, Lsys);
#    Lsys := [ seq (AlgebraicTools:-RandomSection (robot:-Equations,
#                                                  robot:-PoseVariables),
#              i=1..nops(robot:-ArticularVariables)+1) ];
#
#    Lsys := map (op@AlgebraicTools:-Projection, Lsys,
#                                                robot:-ArticularVariables);
#    return AlgebraicTools:-Split (Lsys);

end proc;


CuspidalEquations := ParallelCuspidal;

# Function: ParallelCuspidal
#   Computes the implicit equations satisfied by the cuspidal points
#
# Parameters:
#   robot - a data structure returned by a function mechanisms.
#   notest=b (optional) - *b* is a boolean; if true, the output will contain
#                       all points of multiplicity 3 and higher.
#   parameters=l (optional) - *l* is a list of name; the variables considered
#                             to describe the curve;
#                             default value:[].
#   direct=b (optional) - *b* is a boolean; if true, the function tries to
#                         remove the quadratic roots even if none exists;
#                         default value: false.
#   alternative = b (optional) - *b* is a boolean; if true a different algorithm
#                                is used for the saturation
#                                default value: false
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the equations satisfied by the cuspidal poses.
ParallelCuspidal := proc (robot::Manipulator,
                           { [fast,notest]::truefalse := false,
                             direct::truefalse := false,
                             parameters::list(name) := []})
    local vars, pars, cusp, cusp_back, quad, inf, i;
    
    vars := subs (map (x->x=NULL, robot:-ArticularVariables),
                  [op(robot:-PoseVariables),op(robot:-PassiveVariables)]);
    pars := [op(robot:-ArticularVariables), op(robot:-GeometricParameters)];

    cusp := AlgebraicTools:-IteratedJacobian (robot:-Equations, vars, 2);

    if notest then return cusp; end if;
    if direct then 
        quad := AlgebraicTools:-IteratedJacobian (robot:-Equations, vars, 3);
        return AlgebraicTools:-SaturationIdeal (cusp, quad, _rest);
    end if;

    cusp_back := cusp;

#    inf := InfiniteEquations (robot);
    inf := AlgebraicTools:-PropernessDefect (robot:-Equations,
                                             [op(robot:-ArticularVariables),
                                              op(robot:-GeometricParameters)]);
    for i in {op(inf[..,1])} do
        cusp := AlgebraicTools:-PolynomialSaturation (cusp, i);
    end do;

# Test to avoid quadratic roots computation if possible
    if ( AlgebraicTools:-Dimension (cusp) =
         AlgebraicTools:-Dimension (cusp_back) and
         AlgebraicTools:-IsHullRadical (cusp_back) ) or
         AlgebraicTools:-IsHullRadical (cusp,parameters) then
        return cusp;
    end if;

    if nops(robot:-Equations)>nops(vars) then
        error ("The number of equations in the modelling must be the same as the number of variables");
    end if;

# Quadratic roots through Jacobian: heavy computation
    quad := AlgebraicTools:-IteratedJacobian (robot:-Equations, vars, 3);

    return AlgebraicTools:-SaturationIdeal (cusp, quad, _rest);

end proc;

# Function: SerialCuspidal
#   Computes the implicit equations satisfied by the cuspidal points
#
# Parameters:
#   robot - a data structure returned by a function mechanisms.
#   notest=b (optional) - *b* is a boolean; if true, the output will contain
#                       all points of multiplicity 3 and higher.
#   parameters=l (optional) - *l* is a list of name; the variables considered
#                             to describe the curve;
#                             default value:[].
#   direct=b (optional) - *b* is a boolean; if true, the function tries to
#                         remove the quadratic roots even if none exists;
#                         default value: false.
#   verbose (optional) - "verb"=true or "verb"=false: verbose support 
#                        default value: "verb"=false
#
# Returns:
#   A list of polynomials: the equations satisfied by the cuspidal poses.
SerialCuspidal := proc (robot::Manipulator,
                           { [fast,notest]::truefalse := false,
                             direct::truefalse := false,
                             parameters::list(name) := []})
    local vars, pars, cusp, cusp_back, quad, inf, i;
    
    vars := subs (map(x->x=NULL, robot:-PoseVariables),
                  [op(robot:-ArticularVariables),op(robot:-PassiveVariables)]);
    pars := [op(robot:-PoseVariables), op(robot:-GeometricParameters)];

    cusp := AlgebraicTools:-IteratedJacobian (robot:-Equations, vars, 2);

    if notest then return cusp; end if;
    if direct then 
        quad := AlgebraicTools:-IteratedJacobian (robot:-Equations, vars, 3);
        return AlgebraicTools:-SaturationIdeal (cusp, quad, _rest);
    end if;

    cusp_back := cusp;

#    inf := InfiniteEquations (robot);
#    inf := AlgebraicTools:-PropernessDefect (robot:-Equations,
#                                             [op(robot:-PoseVariables),
#                                              op(robot:-GeometricParameters)]);
#    for i in {op(inf[..,1])} do
#        cusp := AlgebraicTools:-PolynomialSaturation (cusp, i);
#    end do;

# Test to avoid quadratic roots computation if possible
    if AlgebraicTools:-IsHullRadical (cusp) then
        return cusp;
    end if;

    if nops(robot:-Equations)>nops(vars) then
        error ("The number of equations in the modelling must be the same as the number of variables");
    end if;

# Quadratic roots through Jacobian: heavy computation
    quad := AlgebraicTools:-IteratedJacobian (robot:-Equations, vars, 3);

    return AlgebraicTools:-SaturationIdeal (cusp, quad, _rest);

end proc;


# Function: Projection
#   Project on variables or expressions in a polynomial system.
#
# Parameters:
#   sys    - list of polynomials with rational coefficients
#            and trigonometric functions: the system
#   vars1  - list of names or equation of the shape name=algebraic:
#            variables to project on.
#   vars2      (optional) - list of names: variables to eliminate
#   char       (optional) - integer: characteristic of the base field
#                        default value: 0
#   output=fmt (optional) - *fmt* is the keyword *list* or *listlist*;
#                           if *list*, the output is a list of polynomials,
#                           otherwise, it is a list of list of polynomials.
#   verbose    (optional) - "verb"=true or "verb"=false: verbose support 
#                           default value: "verb"=false
#
# Returns:
#   With *output=list*, a list of polynomials: the generators of 
#   the elimination ideal. With *output=listlist*, a list of components of 
#   the elimination ideal (multiplicities may have been lost);
#   each component is represented by a list of polynomials.
#
# Example:
#   > > Projection ( [x+y^2,y-x], [z=x+y] );
#   > < [z^2+2*z]
Projection := proc ( sys   ::list(algebraic),
                     vars1 ::list({name,name=algebraic}),
                     vars2 ::list(name)  :=NULL,#[op(indets(sys,name)
                                                #    minus {op(vars1)})],
                     char  ::integer     := 0 )
    AlgebraicTools:-Projection (sys, vars1, vars2, char, _rest);
end proc;

# Function: CellDecompositionPlus
#   Describes the parameter space according to the number of real roots.
#
# Parameters:
#   equ  - a list of polynomials and trigonometric expressions: the equations.
#   ineq - a list of polynomials and trigonometric expressions: the inequalities
#                   where each expression *p* stands for *p>0*.
#   vars - a list of names: the variables of the system
#   pars (optional) - a list of names: the parameters of the system;
#                     default value: the remaining variables of *equ* and *ineq*
#
# Returns:
#   A maple object: the same as the one returned by the maple function
#   RootFinding[Parametric][CellDecomposition]. The main difference is that it
#   handles trigonometric expressions.
CellDecompositionPlus := proc (equ  ::list(algebraic),
                               ineq ::list(algebraic),
                               vars ::list(name),
                               pars ::list(name) := 
                                   [ op(indets([equ,ineq],name)
                                     minus {op(vars)}) ],
                               {nofactor ::truefalse :=false,
                                gbfactor ::truefalse :=false,
                                norealrootstest ::truefalse :=false})
local T, dv, algsys, vartable, algequ, algineq, cells, trigineq,
      partable, algpars, algvars, transform, dvl, invtransform, den,
      alginequser, v1, lv, minitest, angles, loop;

    algsys, vartable := AlgebraicTools:-TrigonometricAlgebraic ([op(equ),
                                                                 op(ineq)],
                                                                 tanhalf=pars,
                                                                 'const'=false,
                                                                 ':-generic');
    algequ := numer (normal (algsys[1..-nops(ineq)-1]));
    algineq := algsys[-nops(ineq)..-1];
    algpars := AlgebraicTools:-TrigonometricVariables (pars, vartable);
    algvars := [ op(AlgebraicTools:-TrigonometricVariables (vars, vartable)),
                 op(AlgebraicTools:-TrigonometricConstants (vartable)) ];

    den := map(`[]`,denom(normal(algsys)));
    den := map(op,AlgebraicTools:-FactorSystem(den));

    trigineq := select (v->nops(v)=1 and not StringTools:-IsPrefix('tan',v[1]),
                        [entries(vartable)] );
    trigineq := map(i-> (1-i[1],1+i[1]), trigineq);
    alginequser := algineq;
    algineq  := [op(algineq), op(trigineq), op(den)];
    
    printf ("Discriminant Variety computation ...\n");
    dv := AlgebraicTools:-EquidimRadicalDiscriminantVariety (algequ, algineq, algvars, algpars, ':-nofactor'=nofactor, ':-gbfactor'=gbfactor);
#    dvl := map (`[]`@AlgebraicTools:-TrigonometricTanHalf, dv, ':-generic');
    dv := map (numer@normal,dv);
    if not norealrootstest then
        if member (':-HasRealRoots', [exports (RootFinding)])then
            printf ("Real roots testing ...\n");
            dv := select (RootFinding:-HasRealRoots, dv);
#        else
#            v1, lv := algpars[1], algpars[2..];
#            minitest := p -> RootFinding:-Isolate(subs(map(v->v=1,lv),p[1]),
#                                                  v1)<>[]
#                             or
#                             RootFinding:-Isolate(Projection 
#                               (AlgebraicTools:-PolynomialSaturation
#                                    ([p[1],op(map2(diff,p[1],lv))],
#                                      diff(p[1],v1)),
#                                [v1])[1], v1)<>[];
#            dv := select (minitest, dv);
        end if;
    end if;
#    partable := table([seq(op(zip(`=`,[indices(t,nolist)],[entries(t,nolist)])),
#                           t in dvl[..,2])]);
#    algpars := AlgebraicTools:-TrigonometricVariables (pars, partable);
    transform := map (x->if not assigned(vartable[x]) then x else 2*arctan(x) end if, pars);
    transform := eval (unapply (transform, pars));
    invtransform := map (x->if not assigned(vartable[x]) then (y,e)->y 
                            else (y,e)->evalf[-ilog(e)+1](tan(min (evalf(Pi-e),
                                                 max(evalf(-Pi+e),
                                                     evalf(y)))/2)) end if,
                         pars);
    algpars := ListTools:-Reverse (algpars);
    printf ("Cylindrical Algebraic Decomposition computation ...\n");
    cells := RootFinding:-Parametric:-CellDecomposition (
                [T],map(x->if member(x,alginequser) then x else x^2 end if,dv[..,1]),
                [T], algpars );
    
#    cells:-Inequalities := [op(algineq),op(trigineq),op(den)];
    cells:-Inequalities := alginequser;

    printf ("Saturation by the inequalities ...\n");
    cells:-Equations := foldl (AlgebraicTools:-PolynomialSaturation,
                               numer(normal(algequ)),
                               op(numer(cells:-Inequalities)));
#    cells:-Variables := AlgebraicTools:-TrigonometricVariables (vars,vartable);
    cells:-Variables  := algvars;
    cells:-Parameters := algpars;

    angles := map (x->assigned(vartable[x]), pars);
    loop   := zip ((x,y)->x and ( algvars=[] or 
                                  (AlgebraicTools:-Dimension
                                   (
                                AlgebraicTools:-IteratedJacobian(eval(equ,y=Pi),
                                                                 vars)
                                    )
                                  )
                                <= nops(pars)-1), angles, pars);
    
    return Record (AngleTransform=transform,
                   AngleInvTransform=invtransform,
                   TableVariables=eval(vartable),
		   Angles = angles,
                   Equations = cells:-Equations,
                   Filter = (0<>1),
                   Inequalities = cells:-Inequalities,
                   Variables = cells:-Variables,
                   Parameters = cells:-Parameters,
                   DiscriminantVariety = cells:-DiscriminantVariety,
                   ProjectionPolynomials = cells:-ProjectionPolynomials,
                   Boxes,
                   SamplePoints = cells:-SamplePoints,
                   LoopAngles = loop,
                   _rest);
end proc;


# Function: NumberOfSolutionsPlus
#   Returns the number of real solutions of all cells obtained by CellDecompositionPlus.
#
# Parameters:
#   celldec                  - output by *CellDecompositionPlus*
#
# Returns:
#   A list whose elements are of the form [CellId, NumSolsInCell, NumSolsLowBorder, NumSolsUpBorder]
#
NumberOfSolutionsPlus := proc (celldec) 
 local numsols, cellssols, i, celli, polinf, polsup, polleft, polright, l, r, m, dn, up, sysinf, syssup, solinf, solsup, sdown, ssup, d0, dff, u0, uf, u, v, pts; 
 numsols := RootFinding[Parametric][NumberOfSolutions](celldec); 
 cellssols := []; 
 for i to nops(numsols) do
    celli := RootFinding[Parametric][CellDescription](celldec, i); 
#
#    polleft := [celli[1][1], celli[1][2]]; 
#    l := subs([AlgebraicTools[RealSolve]([polleft[1]])][polleft[2]], celli[1][3]); 
#    if evalb(celli[1][4] = infinity) then 
#        r := l+10 
#    else 
#        polright := [celli[1][4], celli[1][5]]; 
#        r := subs([AlgebraicTools[RealSolve]([polright[1]])][polright[2]], celli[1][3]);
#    end if; 
#    m := (1/2)*l+(1/2)*r; 
#
    m:=subs(celldec[SamplePoints][i], celli[1][3]); 
    polinf := [celli[2][1], celli[2][2]]; 
    sdown := [AlgebraicTools[RealSolve]([subs(celli[1][3] = m, polinf[1])])]; 
    dn := subs(sdown[polinf[2]], celli[2][3]); 
    if polinf[2] = 1 then 
        d0 := dn-1;
    else 
        d0 := (1/2)*dn+(1/2)*subs(sdown[polinf[2]-1], celli[2][3]);
    end if; 
    if nops(sdown) = polinf[2] then 
        dff := dn+1 
    else 
        dff := (1/2)*dn+(1/2)*subs(sdown[polinf[2]+1], celli[2][3]);
    end if; 
    if evalb(celli[2][4] = infinity) then 
        polsup := [celli[2][3]-dn-10, 1]; 
        up := dn+10; 
        u0 := up-.5; 
        uf := up+.5; 
    else 
        polsup := [celli[2][4], celli[2][5]]; 
        ssup := [AlgebraicTools[RealSolve]([subs(celli[1][3] = m, polsup[1])])]; 
        up := subs(ssup[polsup[2]], celli[2][3]); 
        if polsup[2] = 1 then 
            u0 := up-1 
        else 
            u0 := (1/2)*up+(1/2)*subs(ssup[polsup[2]-1], celli[2][3]);
        end if; 
        if nops(ssup) = polsup[2] then 
            uf := up+1; 
        else 
            uf := (1/2)*up+(1/2)*subs(ssup[polsup[2]+1], celli[2][3]);
        end if;
    end if; 
    sysinf := [op(celldec[Equations]), polinf[1], celli[1][3]-m]; 
    syssup := [op(celldec[Equations]), polsup[1], celli[1][3]-m]; 
    solinf := [AlgebraicTools[RealSolve](sysinf)]; 
    solinf := select(u-> evalb(subs(u,celli[2][3])> d0) and evalb(subs(u,celli[2][3])< dff), solinf);
    solsup := [AlgebraicTools[RealSolve](syssup)]; 
    solsup := select(u-> evalb(subs(u,celli[2][3])> u0) and evalb(subs(u,celli[2][3])< uf), solsup);
    for v in celldec[Inequalities] do 
        solinf := select(u->evalb(0 <= subs(u, v)), solinf); 
        solsup := select(u->evalb(0 <= subs(u, v)), solsup);
    end do; 
    cellssols := [op(cellssols), [i, numsols[i][2], nops(solinf), nops(solsup)]];
 end do; 
  
 return cellssols;
end proc;


# Function: DVNumberOfSolutionsPlus
#   Returns the number of real solutions on the intersection points of the Discriminant Variety.
#
# Parameters:
#   celldec   - output by *CellDecompositionPlus*
#
# Returns:
#   A list of lists. Each element is of the form [point, numsols]
#
DVNumberOfSolutionsPlus := proc (celldec) 
 local celli, cpts; 

  celli := RootFinding[Parametric][CellDescription](celldec,1); 
  cpts:= auxDVNumberOfSolutionsPlus(celldec[Equations], celldec[DiscriminantVariety], celldec[Inequalities], celli[1][3], celli[2][3]);
  return cpts;
end proc;


# Function: auxDVNumberOfSolutionsPlus
#   Auxiliary function
#   Returns the number of real solutions on the intersection points of the Discriminant Variety.
#
# Parameters:
#   polysys - list of polynomials
#   dv      - list of lists (of polynomials)
#   ineq    - list of unknowns (to be assumed positive)
#   var     - unknown w.r.t. which dv depend but not used to differentiate (corresponds to the one plotted on the x-axis)
#   diffvar - unknown w.r.t. which we differentiate (normally it corresponds to the one plotted on the y-axis)
#
# Returns:
#   A list of lists. Each element is of the form [point, numsols]
#
auxDVNumberOfSolutionsPlus := proc (polysys, dv, ineq, var, diffvar) 
 local tablesols, i, autosys, sysint, dimm, solint, solii,v, u, found, k, j,solij, auxlist, cuspslist,xcoord, ycoord, cpoints, listptsol;

 tablesols := table(); 
 for i to nops(dv)-1 do 
   autosys := [op(dv[i]), diff(op(dv[i]), diffvar)];
   sysint := [op(polysys), op(autosys)]; 
   dimm := AlgebraicTools:-Dimension(sysint); 
   if dimm = 0 then 
     solint := AlgebraicTools[RealSolve](sysint); 
     solii := [solint]; 
     for v in ineq do 
       solii := select( u-> evalb(0<=subs(u,v)), solii);
     end do; 
     for u in solii do 
       if assigned (tablesols[u]) then 
          tablesols[u] := [op(tablesols[u]), [i,i]];
       else 
          tablesols[u] := [[i, i]];
       end if;  
     end do;
   end if;
   for j from i+1 to nops(dv) do 
     sysint := [op(polysys), op(dv[j]), op(dv[i])]; 
     solint := AlgebraicTools[RealSolve](sysint); 
     solij := [solint]; 
     for v in ineq do 
       solij := select(u->evalb(0<=subs(u,v)), solij);
     end do; 
     for u in solij do 
       if assigned (tablesols[u]) then 
          tablesols[u] := [op(tablesols[u]), [i,j]];
       else 
          tablesols[u] := [[i, j]];
       end if; 
     end do; 
   end do; 
 end do; 
 
#there is still one case missing (the autointersections of the last dv)
 autosys := [op(dv[-1]), diff(op(dv[-1]), diffvar)];
 sysint := [op(polysys), op(autosys)]; 
 dimm := AlgebraicTools:-Dimension(sysint); 
 if dimm = 0 then 
    solint := AlgebraicTools[RealSolve](sysint); 
    solii := [solint]; 
    for v in ineq do 
       solii := select( u-> evalb(0<=subs(u,v)), solii);
    end do; 
    for u in solii do 
       if assigned (tablesols[u]) then 
          tablesols[u] := [op(tablesols[u]), [nops(dv), nops(dv)]];
       else 
          tablesols[u] := [[nops(dv), nops(dv)]];
       end if;      
    end do 
 end if;
  
 # Up to now, "tablesols" contains all different complete solutions    
 auxlist := [indices(tablesols, 'nolist')]; 
 listptsol := []; 
 while nops(auxlist) > 0 do 
   xcoord:= subs(auxlist[1], var);
   ycoord:= subs(auxlist[1], diffvar);
   cpoints := select(u-> evalb(subs(u, var) = xcoord) and evalb(subs(u, diffvar) = ycoord), auxlist); 
   for i to nops(cpoints) do 
      if tablesols[cpoints[i]] <> tablesols[cpoints[1]] or tablesols[cpoints[i]] <> tablesols[cpoints[1]] then 
         #this situation should not reach !!
         print("Warning: something is wrong with", cpoints[i]);
      end if 
   end do;
   listptsol:=[op(listptsol), [[xcoord, ycoord], nops(cpoints)]];
   auxlist := convert(`minus`({op(auxlist)}, {op(cpoints)}), list);  
 end do; 

  return listptsol; 
end proc;

# local function, testing if 2 intervals overlap
intervalOverlap := proc (I1, I2)
    return evalb (I2[2] >= I1[1] and I2[1] <= I1[2]);
end proc;

# local function, compute a value in the intersection of 2 overlapping intervals
intervalValue := proc (I1, I2)
    local a,b,x,y,m,M;
    m := min (remove(x->x=-infinity, {I1[1],I1[2],I2[1],I2[2]}))-2;
    M := max (remove(x->x= infinity, {I1[1],I1[2],I2[1],I2[2]}))+2;
    a := `if`(I1[1]=-infinity, m, I1[1]);
    b := `if`(I1[2]=infinity,  M, I1[2]);
    x := `if`(I2[1]=-infinity, m, I2[1]);
    y := `if`(I2[2]=infinity,  M, I2[2]);
    if x<=a then
        if y<=b then return (a+y)/2, 2 else return (a+b)/2, 1 end if;
    else
        if y>=b then return (x+b)/2, 1 else return (x+y)/2, 2 end if;
    end if;
end proc;

# local function, mixing 2 lists of elements of the form:
#   [n, [lb,ub]]
# where n is the number of  cell, and [lb,ub] is an interval,
# and returns a list of elements od the form:
#   [v, c1, c2]
# where c1 and c2 are integer representing cells
# and v is a value potentially connecting c1 to c2
intervalMix := proc ( L1, L2 )
    local i, iA, ind, A, val;
    if L1=[] or L2=[] then return []; end if;
    i[1] := 1;
    i[2] := 1;
    iA := 1;
    A := table (); #nops(L1)+nops(L2));
    while (i[1]<=nops(L1) and i[2]<=nops(L2)) do
        if intervalOverlap (L1[i[1],2], L2[i[2],2]) then 
            val, ind := intervalValue (L1[i[1],2], L2[i[2],2]);
            A[iA] := [val, L1[i[1],1], L2[i[2],1]];
            i[ind] := i[ind] + 1;
            iA := iA + 1;
        else
            ind := (ind mod 2) + 1;
            i[ind] := i[ind]+1;
        end if;
    end do;
    return [ entries (A,nolist) ];
end proc;

cacheCellDescription := proc (cells::record,
                              n    ::integer)
    option cache(10000);
    return RootFinding:-Parametric:-CellDescription (cells, n);
end proc;

# local function, links the cells of 2 adjacent columns of 
#   a CAD of dimension 2.
#
# Parameters:
#   cells - the data structure returned by CellDecomposition
#   G     - a graph whose nodes are the integer representing the cells
#   C1    - list of integers: the cells of the first column
#   C2    - list of integers: the cells of the second column
#   
# Returns:
#   Nothing: the graph G is updated.
linkColumns := proc ( cells, G, C1, C2 )
    local L1, L2, v1, v2, L, i, e, LI1, LI2, v, c1, c2, n;
    n := 4;
    if C1=[] or C2=[] then return NULL end if;
    L1 := map2 (cacheCellDescription, cells, C1);
    L2 := map2 (cacheCellDescription, cells, C2);
    if member ([L1[1,1,4]],cells:-DiscriminantVariety) or
       member ([L2[1,1,1]],cells:-DiscriminantVariety) then
        return NULL;
    end if;
    v1 := getBound (L1[1,1], 2);
    v2 := getBound (L2[1,1], 1);

    if v1= infinity then  v1 := getBound(L1[1,1],1)+10^n end if;
    if v2=-infinity then v2 := getBound(L2[1,1],2)-10^n end if;

    LI1 := [seq ( [ C1[i],
                   [getBound (subs(L1[i,1,3]=v1, L1[i,2]),1),
                    getBound (subs(L1[i,1,3]=v1, L1[i,2]),2)] ],
                 i=1..nops(L1))];
    LI2 := [seq ( [ C2[i],
                   [getBound (subs(L2[i,1,3]=v2, L2[i,2]),1),
                    getBound (subs(L2[i,1,3]=v2, L2[i,2]),2)] ],
                 i=1..nops(L2))];
    L := intervalMix (LI1, LI2);
    
    for e in L do
        # substituing the ordinate variable
        v  := L1[1,2,3]=e[1];
        c1 := cacheCellDescription (cells,e[2])[2];
        c2 := cacheCellDescription (cells,e[3])[2];
        if IsIntervalEmpty (subs (v, c1[1]), v1, v2) and
           IsIntervalEmpty (subs (v, c1[4]), v1, v2) and
           IsIntervalEmpty (subs (v, c2[1]), v1, v2) and
           IsIntervalEmpty (subs (v, c2[4]), v1, v2) then
            GraphTheory:-AddEdge (G, {e[2],e[3]});
        end if;
    end do;
    return NULL;
end proc;



# sort list of Cells along a Hamiltonian path
# the cells must have the same projection on the (i-1) first variables
sortCells := proc (cells::record,
                   L    ::list(integer),
                   i    ::integer)
    local pack, ord, Lc, c, k, Vc, n, Vs, sp;
    pack := table();
    ord  := table();
    if L=[] then return [[]]
    elif nops(L)=1 then return [L] end if;
    Lc  := map2 (cacheCellDescription, cells, L);
    
    for k from 1 to nops(L) do
        c := Lc[k];
        ord[c[i,1],c[i,2]] := c[i,4], c[i,5];
        setAdd (pack[c[i,1],c[i,2]], L[k]);
    end do;
    c := -infinity,0;
    n := nops([indices (ord)]);
    Vc := Vector(n);
    for k from 1 to n do
        if assigned(ord[c]) then
            Vc[k] := c;
            c := ord[c];
        else
            sp := ListTools:-Reverse (cells:-SamplePoints[L[1]])[1..i-1];
            Vs := map  (c->[getBound(subs(sp,c[i]),1),c[i,1],c[i,2]], Lc);
            Vs := sort (Vs, (x,y)->x[1]<=y[1]);
            return [seq (setValues (pack[Vs[k,2],Vs[k,3]]), k=1..nops(Vs))];
        end if;
    end do;
    return [seq (setValues (pack[Vc[k]]), k=1..n)];
end proc;

setAdd := proc (set::uneval,
                e  ::anything)
    if assigned(set) then
        set[e] := true;
    else
        set := table ([e=true]);
    end if;
end proc;

setValues := proc (set::anything)
    return [indices(set,nolist)];
end proc;

# Function: IsIntervalEmpty
#   Checks that a polynomial has no real roots in an open interval.
#
# Parameters:
#   p  - a univariate polynomial
#   lb - a numerical value: the lower bound of an interval
#   ub - a numerical value: the upper bound of an interval
#
# Returns:
#   True means the polynomial has no root in the given open interval.
#   False is returned when the polynomial may have root in the open interval.
IsIntervalEmpty := proc (p ::polynom,
                         lb::constant,
                         ub::constant,
                         precision ::integer := 4)
    local b1, b2, n, s, q, L, x, i, k;
    if type(p,constant) then return evalb(p<>0) end if;
    x := indets (p)[1];
    n := max(-ilog10(convert(ub-lb,float))+1,0);
    s := 10^(n+precision);

    # interval wider if not rational
    if not type(b1,rational) then b1 := floor(lb*s)/s else b1 := lb; end if;
    if not type(b2,rational) then b2 := ceil (ub*s)/s else b2 := ub; end if;
    
    if b2=b1 then return false end if;
    if b2<b1 then return IsIntervalEmpty (p, -infinity, b2)
                         and IsIntervalEmpty (p, b1, infinity);
    end if;

    # change of variable to map the interval to the positive real
    if   b1=-infinity and b2=infinity then
        return IsIntervalEmpty (p,0,infinity)
               and IsIntervalEmpty (p,-infinity,0);
    elif b1=-infinity then
        q := subs (x=b2-x,p);
    elif b2=infinity then
        q := subs (x=b1+x,p);
    else
        q := numer (subs (x = b1*x/(x+1)+b2/(x+1),p));
    end if;

    # use Descartes rule of sign
    L := remove(c->c=0,PolynomialTools:-CoefficientList(q,x));
    L := map(signum,L);
    k := add (`if`(L[i]+L[i+1]=0,1,0), i=1..nops(L)-1);
    return evalb(k=0);
end proc;


# Function: CellGraph
#   Computes the connexity graph of the cells of a CAD.
#
# Parameters:
#   cells - an object returned by *CellDecomposition* or 
#           *CellDecompositionPlus*.
#
# Returns:
#   A graph representing the connections between the cells.
CellGraph := proc (cells::record)
    local n, L, G, i, p, pol, hasAngles;
    option cache;

    # Initialize cache tables
    forget (cacheCellDescription);
    forget (RootFinding:-Isolate);
    if op(4,eval(RootFinding:-Isolate))=NULL then
        Cache (procedure=RootFinding:-Isolate);
    end if;
    for p in cells:-SamplePoints do
        for i from 1 to nops(p) do
            for pol in cells:-ProjectionPolynomials[i] do
                RootFinding:-Isolate (eval(pol,p[-i+1 .. -1]),lhs(p[-i]),
                                      'digits'=10,'format'='list')
                :=
                RootFinding:-Isolate (eval(pol,p[-i+1 .. -1]),lhs(p[-i]),
                                      'digits'=10,'format'='list');
            end do;
        end do;
    end do;

    # Computation
    n := nops(cells:-SamplePoints);
    L := sortCells (cells, [$1..n], 1);
    L := map2 (sortCells, cells, L, 2);
    L := map2 (map, op, L);
    G := GraphTheory:-Graph(n);
    if nops(L)<=1 then return G; end if;
    printf (" -> Connecting %a cells ...\n", n);
    hasAngles := member(LoopAngles, [exports(cells)]);
    if hasAngles and cells:-LoopAngles[1] and nops(L)>=2 then
        linkColumns (cells, G, L[-1], L[1]);
    end if;

    for i from 1 to nops(L)-1 do
        if i mod 10=0 then printf (" ---> %d%%\n", iquo(100*i,n)) end if;
        linkColumns (cells, G, L[i], L[i+1]);
        if hasAngles and cells:-LoopAngles[2] and nops(L[i])>=2 then
            GraphTheory:-AddEdge (G, {L[i,1], L[i,-1]});
        end if;
    end do;
    if (n mod 10)<>0 then printf (" ---> 100%%\n") end if;
    forget (cacheCellDescription);
    forget (RootFinding:-Isolate);
    return G;
end proc;


# Function: PseudoSingularitiesDecomposition
#   Computes the decomposition with pseudo-singularities of a manipulator.
#
# Parameters:
#   robot - a data structure returned by a function mechanisms.
#
# Returns:
#   A maple object: the same as the one returned by the function
#   CellDecompositionPlus, augmented with the PseudoSingularities.
PseudoSingularityDecomposition := proc (robot::Manipulator)
    local dv, equ, ineq, cells, paravars, serivars, dvineq, algsys, algequ,
          algineq, vartable;
    equ  := robot:-Equations;
    ineq := map(rhs-lhs,robot:-Constraints);

    algsys, vartable := AlgebraicTools:-TrigonometricAlgebraic ([op(equ),
                                                                 op(ineq)],
                                                                 tanhalf=
                                                           robot:-PoseVariables,
                                                                 cossin=[],
                                                                 'const',
                                                                 ':-generic');
    algequ := numer (normal (algsys[1..-nops(ineq)-1]));
    algineq := algsys[-nops(ineq)..-1];


    paravars := [op(robot:-PoseVariables), op(robot:-PassiveVariables)];
    serivars := [op(robot:-ArticularVariables), op(robot:-PassiveVariables)];
    dv := AlgebraicTools:-EquidimRadicalDiscriminantVariety(equ,ineq,paravars);
    dvineq := map(p->p[1]^2, dv);
    cells := CellDecompositionPlus (equ, dvineq, serivars,
                                    OriginalSystem = [op(algequ),
                                                      op(map(x->x>0,algineq))],
                                 ArticularVariables=robot:-ArticularVariables);
    return cells;
end proc;


# Function: DirectInverseKinematics
#   Returns the cells having the same antecedent.
#
# Parameters:
#   cells - an object returned by *PseudoSingularitiesDecomposition*
#   i           (optional) - an integer or a list of integer:
#                            the indices of the cells to plot in *cells*.
#                            default value: all the cells
#
# Returns:
#   A list of components: each components is a list of cells containing a cell
#   *c* given in input and all the cells having the same antecedents as *c*.
DirectInverseKinematics := proc (cells::record := 
                                        PseudoSingularityDecomposition(robot),
                                 i::{list(integer),integer} := [ $ 1 .. 
                                                    nops(cells:-SamplePoints) ])
    local G, T, lind, sols, comp, p;

    G := CellGraph (cells);
    comp := GraphTheory:-ConnectedComponents (G);
    
    lind := `if`(type([i],list(list)),i,[i]);

    for p in cells:-SamplePoints[lind] do
        sols := {AlgebraicTools:-RealSolve (cells:-OriginalSystem,
                                            cells:-ArticularVariables,
                                            op(p))};
        sols := map (s->AlgebraicTools:-RealSolve (cells:-OriginalSystem,
                                                   cells:-Parameters,
                                                    op(s)),
                      sols);
        sols := map (`[]`@op, sols);              
        sols := map2 (RootFinding:-Parametric:-CellLocation, cells, sols);
        sols := map (x->op(select (y->member(x,y),comp)), sols);
        sols := map (op,sols);
        setAdd (T, sols);
    end do;
    return setValues(T);
end proc;

# Function: CellArea2D
#   Compute the area of the cells returned *CellDecomposition* or *CellDecompositionPlus*
#
# Parameters:
#   cells - an object returned by *CellDecomposition* or *CellDecompositionPlus*
#   i           (optional) - an integer, a list of integer or 
#                            a list of list of integer:
#                            the indices of the cells to plot in *cells*.
#                            default value: all the cells
#   ranges      (optional) - a sequence of the form *v=n1..n2* where
#                            *v* is a variable and *n1,n2* are numerical values;
#                            when not specified for a variable,
#                            the default range is *-5..5*.
#   precision = p (optional) - *p* is an integer used for the numeric precision
#                              default value: 3.
#   border = e    (optional) - *e* is a numeric value: defines the precision
#                              on the border;
#                              default value: 0.0000001.
#                              
# Returns:
#   The area of the cells indiced by *i* in *cells*.
#
# Example:
# > > cells := CellDecompositionPlus ([T],[25-(x^2+y^2),x+y],[T],[x,y]):
# > > CellArea2D (cells,[11,15],color=blue);
#
#
CellArea2D := proc (cells::record,
                    i::{list(list(integer)),list(integer),integer} :=
                                           [ $ 1 ..  nops(cells:-SamplePoints) ],
                    ranges::seq(name=range) := NULL,
                    {precision::integer := 3,
                     border::constant := 10^(-2*Digits)})
    local lcell, lind, k, disp, lranges, drange, r, c, l, j, opts, vars,
          nranges, angles, tablevars, trans, invtrans, left, right, duc, ldu,
          pts, colpts, disppt, d, pa, eps, x1, x2, r1, r2, filter, L0, L1, L2,
	  inv, phi, f, a, b, B;
    
    opts := axes=boxed;

    drange := -5..5;
    lind := `if`(type(i,list),i,[i]);
    
    if nops(lind)=0 then return [];
    elif nops(lind)>1 then
        printf (" -> Computing the area of %a cells ...\n", nops(lind));
        d := [seq(op([`if`(k mod 10=0, printf (" ---> %d%%\n",
                                                   iquo(100*k,nops(lind))),
                                           printf ("")),
                          CellArea2D(cells,lind[k],ranges,l[lind[k]],
                                    ':-border'=border,
                                    ':-grid'=grid,
                                    _rest)
                        ]),
                     k in [$1..nops(lind)])];
        if (nops(lind) mod 10)<>0 then printf (" ---> 100%%\n") end if;;
        return add (k, k in d);
    end if;
    
    k := lind[1];
#    if type(i,integer) then
#        printf (" -> Cell number %a ...\n", k);
#    end if;
    lcell := RootFinding:-Parametric:-CellDescription(cells,k);
    pa := ListTools:-Reverse (cells:-Parameters);
    inv := x->x;
    phi := x->1;
    if member (':-TableVariables',[exports(cells)]) then
        tablevars := cells:-TableVariables;
        invtrans :=  cells:-AngleInvTransform;
        trans :=  cells:-AngleTransform;
	if cells:-Angles[1] then phi := x->2/(1+x^2); end if;
	if cells:-Angles[2] then inv := x->evalf(2*arctan(x)); end if;
    else
        tablevars := table();
        invtrans := [seq ((x,e)->x,v in pa)] ;
        trans := unapply(pa, pa);
    end if;

    vars := AlgebraicTools:-AlgebraicVariables (lcell[..,3], tablevars);
    lranges := subs ([ranges],vars);
    lranges := zip ((x,r)->if r=x then x=drange else x=r end if,
                    vars, lranges);
    nranges := zip (`=`,lcell[..,3],map(rhs,lranges));
    nranges := zip ((f,x)->lhs(x)=f(lhs(rhs(x)),border)..f(rhs(rhs(x)),border),
                    invtrans, nranges);
    nranges := op(nranges);

    angles := map (x->assigned (tablevars[x]), vars);
    left := false; right := false;
    if   member ([lcell[1,1]], cells:-DiscriminantVariety) then left := true
    elif member ([lcell[1,5]], cells:-DiscriminantVariety) then right := true
    end if;

    eps := border;
    B   := 10^4;
    x1,x2 := op(lcell[..,3]);
    r1,r2 := op(subs (nranges, [x1,x2]));
    filter := (x,r)->evalb (x>=lhs(r) and x<=rhs(r));

    a,b := seq (getBound (lcell[1], j, eps), j=1..2);
    a := max(a,lhs(r1),-B);
    b := min(b,rhs(r1),B);
    f := x->#piecewise(type(x,numeric),
                      (min(rhs(r2),
		           inv(getBound(subs(x1=convert(x,rational,exact),
                                             lcell[2]),2,eps)))
                       - max(lhs(r2),
                             inv(getBound(subs(x1=convert(x,rational,exact),
                                               lcell[2]),1,eps))))
                      * phi(x);

    return evalf (Int (f,a..b,digits=precision));

#    # extract the abscices of the first coordinate
#    L0 := listPoints (lcell[1],r1,grid,eps,tangent=angles[1]);
#    if select(filter,L0,r1)=[] then return 0 end if;
#
#    #Computes first face
#    L1 := map (u->[u,getBound (subs(x1=u,lcell[2]),1,eps)], L0);
#    L1 := map(x-> trans (x[1],max (min (x[2],rhs(r2)), lhs(r2))), L1);
#
#    #Computes second face
#    L2 := map (u->[u,getBound (subs(x1=u,lcell[2]),2,eps)], L0);
#    L2 := map(x-> trans (x[1],max (min (x[2],rhs(r2)), lhs(r2))), L2);
#    
#    return evalf(add ((L1[k+1,1]-L1[k,1])*(L2[k,2]+L2[k+1,2]-L1[k,2]-L1[k+1,2]),
#                       k=1..nops(L1)-1)/2);
end proc;


# Function: CellLocationPlus
#   Computes the cells of a decomposition containing the given points.
#
# Parameters:
#   cells - an object returned by *CellDecomposition* or *CellDecompositionPlus*
#   L     - a point, or a list of point: each point is given by a list
#           of equalities of the shape 'name'='number'.
#                              
# Returns:
#   The cells of *cells* containing the points of L.
#
# Example:
# > > cells := CellDecompositionPlus ([T],[cos(x)-y,sin(x)-1/2],[T],[x,y]):
# > > CellLocationPlus (cells,[[x=1,y=2],[x=0.5,y=3]]);
# >                                     [4, 2]
CellLocationPlus := proc (cells::record,
                          L    ::{list(name=realcons),
                                  list(list(name=realcons))})
    local Lplus, names, e;
    Lplus := `if`(type(L,list(name=realcons)),[L],L);
    names := AlgebraicTools:-AlgebraicVariables (cells:-Parameters,
                                                 cells:-TableVariables);
    e     := 10^(-10);
    Lplus := map[3](zip, (f,x)->f(x,e),
                         ListTools:-Reverse(cells:-AngleInvTransform),
                         map(subs,Lplus,names));
    if type(L,list(list(name=realcons))) then
        return map2 (RootFinding:-Parametric:-CellLocation, cells, Lplus,_rest);
    else
        return RootFinding:-Parametric:-CellLocation (cells,Lplus[1],_rest);
    end if;
end proc;

# local function, corrects a numerical bug of the function CellDescription
CellDescriptionPlus := proc(m::`module`,
                            k::depends(integer[1 .. nops(m:-SamplePoints)]),
                            $)
local tp, point, ar, s, n, i, j, u, lp, p, l, e;
     if m:-ProjectionPolynomials = [] then
       error "cannot describe the cells of the solution module; try to execute CellDecomposition with the option output=cad first"
     end if;
     point := m:-SamplePoints[k];
     n := nops(point);
     ar := Array(1 .. n);
     for i to n do
       lp := eval(m:-ProjectionPolynomials[i],point[-i+1 .. -1]);
       tp := [seq(RootFinding:-Isolate(lp[j],lhs(point[-i]),('output')=('interval'),('digits') = 10,('format') = ('list')), j=1..nops(lp))];
       tp := remove(u -> u[2] = [],ListTools:-Enumerate(tp));
       tp := map(u -> seq([u[1], j, u[2,j]],j = 1 .. nops(u[2])),tp);
       tp := sort(tp,(u, v) -> u[3,1] < v[3,1]);
       if nops(tp)>1 and member(false, zip((x,y)->x[3,2]<=y[3,1],tp[1..-2],tp[2..-1])) then
        p := convert(lp,'`*`');
        l := RootFinding:-Isolate(p,output=interval,digits=10,format=list);
        e := ilog10(-min(map(`-`,l)))+1;
        tp := [seq(RootFinding:-Isolate(lp[j],lhs(point[-i]),('output')=('midpoint'),('digits') = e,('format') = ('list')), j=1..nops(lp))];
        tp := remove(u -> u[2] = [],ListTools:-Enumerate(tp));
        tp := map(u -> seq([u[1], j, u[2,j]],j = 1 .. nops(u[2])),tp);
        tp := sort(tp,(u, v) -> u[3] < v[3]);
       else
        tp := map(x->[x[1],x[2],x[3,1]],tp);
       end if; 
       s := ListTools:-BinaryPlace([seq(u[3],u = tp)],rhs(point[-i]));
       if nops(tp) = 0 then
         ar[i] := [-infinity, 0, lhs(point[-i]), infinity, 0]
       elif s = 0 then
         ar[i] := [-infinity, 0, lhs(point[-i]), m:-ProjectionPolynomials[i,tp[1,1]], tp[1,2]]
       elif s = nops(tp) then
         ar[i] := [m:-ProjectionPolynomials[i,tp[s,1]], tp[s,2], lhs(point[-i]), infinity, 0]
       else
         ar[i] := [m:-ProjectionPolynomials[i,tp[s,1]], tp[s,2], lhs(point[-i]), m:-ProjectionPolynomials[i,tp[s+1,1]], tp[s+1,2]]
       end if
     end do;
     convert(ar,'list')
end proc;


