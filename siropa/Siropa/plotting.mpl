# Title: Plotting

# local function used to cut array in 2d plot graphs
filterCurve := proc ( graph ::function,
                      filter::procedure )
    local L1, L2, res, x, take, minires, A;
    res := [];
    for A in indets (graph, Array) do
    
        L1 := 1;
        L2 := NULL;
        take := true;
    
        for x in $ [ArrayDims(A)][1] do
            if not (take = filter(A[x,1],A[x,2])) then
                if take then L2 := L2, x-1;
                        else L1 := L1, x;   end if;
                take := not take;
            end if;
        end do;
        if take then L2 := L2,x; end if;
    
        minires := zip ( (x,y)-> [seq ([A[i,1],A[i,2]],i=x..y)], [L1], [L2] );
        res := [ op(res), op(remove (l->l=[], minires)) ];

    end do;

    if res=[] then return plots:-pointplot([]) end if;
    return plots:-display (map (plots:-pointplot, res, connect=true, _rest ));
end proc;



# Function: Plot2D
#   Plots a system of 2 variables
#
# Parameters:
#   sys     - a list or a list of list of polynomials: the system
#   v1 = r1 - *v1* is a name of *sys* and *r1* a range of values
#   v2 = r2 - *v2* is a name of *sys* and *r2* a range of values
#   points = bool (optional) - bool is a boolean: if false, isolated points
#                                                 are ignored;
#                              default value: false;
#   notest = b    (optional) - *b* is a boolean; when *b* is true the
#                              inequality and real constraints are ignored;
#                              default value: true;
#   opts          (optional) - arguments passed to the maple function
#                              *plots:-implicitplot*
#
# Returns:
#   A graphic: the solutions of the system.
#   - when *sys* is a polynomial, the graphic is the zeroes of this polynomial
#   - when *sys* is a list of polynomials *[p1,...,pk]*,
#     the graphic is the zeroes of the system *p1=0* and ... and *pk=0*
#   - when *sys* is a list of list of polynomials *[L1,...,Lk]*,
#     the graphic is the union of the zeroes of each system *L1*, ..., *Lk*.
#
Plot2D := proc ( sys ::{algebraic, equation(algebraic), 
                        list({algebraic,equation(algebraic),
                                        algebraic<algebraic}),
                        list(list({algebraic,equation(algebraic),
                                             algebraic<algebraic}))},
                 e1  ::name=range,
                 e2  ::name=range,
                 { points ::truefalse  := false,
                   [ notest, draft ] ::truefalse := false } )
    option cache;
    local lsys, lconstr, lh, lp, Lpt, Lh, dpt, dh, opts, npts, lsysconstr,vars,
          T, lconstrformula, lind, formula, lvars;
    
    if type(sys,constant) or sys=[] 
                          or andmap(type,sys,constant) then
        return plots:-pointplot([],'connect', 'axes'='boxed',
                                   'scaling'='constrained',
                                   'view'=[rhs(e1), rhs(e2)]) end if;
    vars := [lhs(e1),lhs(e2)];
    opts := labels=[lhs(e1),lhs(e2)];
    if not ormap (e->type(e,`=`) and lhs(e)=color, [_rest]) then
        opts := opts, color=red;
    end if;
    opts := opts, op(select (o->type(o,`=`) and member(lhs(o),{color,legend}),
                             [_rest]));
    npts := select(x->lhs(x)='numpoints',[_rest]);
    npts := `if` (npts=[], 100, rhs(npts[1]));

#    lsys := `if`(type(sys,Not(list)),[[sys]],
#                                   `if`(type(sys,list(Not(list))),[sys],sys));

    if   type (sys, Not(list))       then lsys := [sys];
    elif type (sys, list(Not(list))) then lsys := sys;
    else Lh := map ( Plot2D, sys, e1, e2, ':-points'=points, ':-draft'=draft,
                     _rest );
         if Lh=[] then return plots:-pointplot ([], 'connect', 'axes'='boxed',
                  'scaling'='constrained', 'view'=[rhs(e1), rhs(e2)]) end if;
         return plots:-display (Lh);
    end if;

    lconstr, lsys := selectremove ( type, lsys, `<` );
    lsys := map (e->if type(e,equation) then lhs(e)-rhs(e) else e end if, lsys);
    lsys := Projection (lsys, vars,allvariables,output=listlist);
    lsys := map (s->[op(s),op(lconstr)], lsys);

    if nops(lsys)>=2 then
        Lh := map ( Plot2D, lsys, e1, e2, ':-points'=points,
                    ':-draft'=draft, _rest );
        if Lh=[] then return plots:-pointplot ([], 'connect', 'axes'='boxed',
                  'scaling'='constrained', 'view'=[rhs(e1), rhs(e2)]) end if;
        return plots:-display (Lh);
    elif nops(lsys)=1 then
        lsys := lsys[1]
    else return plots:-pointplot ([], 'connect', 'axes'='boxed',
                  'scaling'='constrained', 'view'=[rhs(e1), rhs(e2)])
    end if;

    lsys := map (e->if type(e,equation) then lhs(e)-rhs(e) else e end if, lsys );
    lconstr := select ( type, lsys, `<` );
    lsys := remove ( type, lsys, `<` );

    lconstr := select (p->indets(p,name) minus indets(p,constant) 
                          subset indets(lsys,name),lconstr);
    lsysconstr := [op(lsys),op(lconstr)];
    if draft then
        lconstrformula := [];
    else
        lconstrformula := map ( AlgebraicTools:-EasyConstraintFormula, lconstr,
                                lsys, vars);
        # to get simultaneaous inequations
        if nops(select(f->f[1]=2, lconstrformula))>=2 then
            lconstrformula := [ op(lconstrformula),
                                seq ( seq ( AlgebraicTools:-EasyConstraintFormula
                                            ( (rhs-lhs)(lconstr[i])
                                              *(rhs-lhs)(lconstr[j])>0, 
                                              lsys, vars ),
                                            i=j+1..nops(lconstr) ),
                                      j=1..nops(lconstr) ) ];
        end if;

        # to get formula induced by real positions
        lvars := indets(lsys,name) minus {op(vars)} minus indets(lsys,constant);
        lconstrformula := [op(lconstrformula),
                           op( map ( AlgebraicTools:-EasyRealFormula, lvars,
                                lsys, vars) )];
     end if;

#    if `or`(op(zip((s,t)->not indets(s) subset indets(t), lconstr, lsys))) then
#        error "Some variables in the inequalities don't appear in the equations"
#    end if;

    if not (indets(lsys,name) subset {lhs(e1),lhs(e2)}) then
        lsys := AlgebraicTools:-Projection(lsys,[lhs(e1),lhs(e2)]);
    end if;
    
    if lsys=[] then
        if not {lhs(e1),lhs(e2)} subset indets(sys) then
            error "One variable of %1, %2 doesn't appear in the input system",
                  lhs(e1), lhs(e2);
        else
            error "The dimension of the input system is greater than 2";
        end if;
    end if;

    lsys := map (numer@normal, AlgebraicTools:-Split (lsys) );

    lh, lp := selectremove (s->nops(s)=1,lsys);

    if points then
        Lpt := map (subs, map (AlgebraicTools:-RealSolve,lp, 'noerror'),
                          [lhs(e1),lhs(e2)]                    );
    else
        Lpt := [];
    end if;
    dpt := `if`(Lpt=[],NULL,plots:-pointplot (Lpt, opts));
    
#    Lh  := map (p->codegen[optimize] (codegen[makeproc] 
#                                        (op(p), [lhs(e1),lhs(e2)])), lh);

    if lconstrformula=[] then
        Lh  := codegen[optimize] (codegen[makeproc] (`*`(op(map(op,lh))),
                                                      [lhs(e1),lhs(e2)]));

        dh  := `if`(lh=[] ,NULL,plots:-implicitplot (Lh, rhs(e1), rhs(e2), 
                                                     gridrefine=5,
                                                     labels=[lhs(e1),lhs(e2)],
                                                     opts, _rest));
    elif not member (FAIL, lconstrformula) then
        formula := (x,y)->andmap ( apply, lconstrformula[..,2], x,y);
        Lh  := codegen[optimize] (codegen[makeproc] (`*`(op(map(op,lh))),
                                                      [lhs(e1),lhs(e2)]));
#        Lh  := unapply(`*`(op(map(op,lh))), [lhs(e1),lhs(e2)]);

        if lh=[] then
            dh := NULL;
        else
            dh := plots:-implicitplot (Lh, rhs(e1), rhs(e2), gridrefine=5,
                                       labels=[lhs(e1),lhs(e2)], opts, _rest);
            dh := filterCurve (dh, formula, opts,_rest);
        end if;
    else
        Lh := [ seq (AlgebraicTools:-RealSolve(lsysconstr,vars,
                                               lhs(e1)=k,'noerror'),
                          k=rhs(e1), evalf[2]((rhs-lhs)(rhs(e1))/npts)),
                seq (AlgebraicTools:-RealSolve(lsysconstr,vars,
                                               lhs(e2)=k,'noerror'),
                          k=rhs(e2), evalf[2]((rhs-lhs)(rhs(e2))/npts)) ];
        Lh := map (subs, Lh, [lhs(e1),lhs(e2)]);
        dh := `if`(Lh=[],NULL,plots:-pointplot (Lh, symbol=point, opts));
    end if;
    
    if dh=NULL and dpt=NULL then
        return plots:-pointplot ([],'connect', 'axes'='boxed',
                                'scaling'='constrained',
                                'view'=[rhs(e1), rhs(e2)])
    end if;

    plots:-display ([dpt,dh], 'axes'='boxed', 'scaling'='constrained',
                              'view'=[rhs(e1), rhs(e2)]);
end proc;


# Function: PlotCurve3D
#   Plots a curve given by implicit equations in 3 variables.
#
# Parameters:
#   sys     - a list or a list of list of polynomials: the system
#   v1 = r1 - *v1* is a name of *sys* and *r1* a range of values
#   v2 = r2 - *v2* is a name of *sys* and *r2* a range of values
#   v3 = r3 - *v3* is a name of *sys* and *r3* a range of values
#   grid = i    (optional) - *i* is an integer, the number of samples points;
#                            default value: 20.
#   output = keyword      (optional) - *keyword* is either *list* or *display*:
#                                      *display* (resp. *list*) returns a graph
#                                      (resp. a *list*).
#
# Returns:
#   A graphic: the solutions of the system.
#   - when *sys* is a list of polynomials *[p1,...,pk]*,
#     the graphic is the zeroes of the system *p1=0* and ... and *pk=0*
#   - when *sys* is a list of list of polynomials *[L1,...,Lk]*,
#     the graphic is the union of the zeroes of each system *L1*, ..., *Lk*.
#
# Example:
# > > PlotCurve3D ([x^2+y^2-1, z^2-x],grid=100,title="cusp");
#
PlotCurve3D := proc ( sys ::{algebraic, equation(algebraic),
                        list({algebraic,equation(algebraic),
                                        algebraic<algebraic}),
                        list(list({algebraic,equation(algebraic),
                                             algebraic<algebraic}))},
                 ineq ::list({polynom, polynom<polynom}) := [],
                 e1  ::name=range := sort ([op(indets([sys,x,y,z],name))])[1]
                                          =-5..5,
                 e2  ::name=range := sort ([op(indets([sys,x,y,z],name)
                                            minus {lhs(e1)})])[1] =-5..5,
                 e3  ::name=range := sort ([op(indets([sys,x,y,z],name)
                                            minus {lhs(e1),lhs(e2)})])[1]
                                            =-5..5,
                 { grid::integer      := 30,
                   output::identical(list,display) := ':-display'} )
    local opts, x1,x2,x3,r1,r2,r3, L1, L2, L3, L;

    opts := 'axes'='boxed',labels=[lhs(e1),lhs(e2),lhs(e3)],
                             view=[rhs(e1),rhs(e2),rhs(e3)];

    x1,x2,x3 := lhs(e1), lhs(e2), lhs(e3);
    r1,r2,r3 := rhs(e1), rhs(e2), rhs(e3);

    if type(sys,list(list)) then
        if output=':-display' then
            return plots:-display(map (PlotCurve3D, sys, _passed[2.._npassed]));
        else
            return map (PlotCurve3D, sys, _passed[2.._npassed]);
        end if;
    end if;

    L1 := map2 (AlgebraicTools:-RealSolve, sys, 
                          [ seq (x1=lhs(r1)+i*(rhs(r1)-lhs(r1))/(grid-1),
                                 i=0..grid-1) ]);
    L2 := map2 (AlgebraicTools:-RealSolve, sys, 
                          [ seq (x2=lhs(r1)+i*(rhs(r1)-lhs(r1))/(grid-1),
                                 i=0..grid-1) ]);
    L3 := map2 (AlgebraicTools:-RealSolve, sys, 
                          [ seq (x3=lhs(r1)+i*(rhs(r1)-lhs(r1))/(grid-1),
                                 i=0..grid-1) ]);
    L := map (subs,[op(L1),op(L2),op(L3)],[x1,x2,x3]);
    if output=':-display' then
        return plots:-pointplot3d (L, opts, _rest);
    else
        return L;
    end if;
end proc;


# Function: Plot3D
#   Plots a system of 3 variables using maple internal plotting functions.
#
# Parameters:
#   sys     - a list or a list of list of polynomials: the system
#   v1 = r1 - *v1* is a name of *sys* and *r1* a range of values
#   v2 = r2 - *v2* is a name of *sys* and *r2* a range of values
#   v3 = r3 - *v3* is a name of *sys* and *r3* a range of values
#   points = bool (optional) - *bool* is a boolean: if false, isolated points
#                                                   are ignored;
#                              default value: false;
#   grid = i    (optional) - *i* is an integer leading to a grid size *i* x *i*;
#                            default value: 20.
#   border = e  (optional) - *e* is a numeric value: defines the precision
#                            on the border;
#                            default value: 0.0001.
#   crossingrefine = bool (optional) - *bool* is a boolean: if true, the mesh
#                                      follows the cross of the different
#                                      surfaces;
#                                      default value: false.
#   output = keyword      (optional) - *keyword* is either *list* or *display*:
#                                      *display* (resp. *list*) returns a graph
#                                      (resp. a *list*).
#   noverbose = b         (optional) - *b* is a boolean: if true, no verbose
#                                      is displayed;
#                                      default value: false
#   verboserate = i       (optional) - *i* is an integer: if positive (resp.
#                                      negative) the number (resp. the
#                                      pourcentage) of cells
#                                      computed before displaying a new verbose
#                                      line;
#                                      default value: -10
#
# Returns:
#   A graphic: the solutions of the system.
#   - when *sys* is a polynomial, the graphic is the zeroes of this polynomial
#   - when *sys* is a list of polynomials *[p1,...,pk]*,
#     the graphic is the zeroes of the system *p1=0* and ... and *pk=0*
#   - when *sys* is a list of list of polynomials *[L1,...,Lk]*,
#     the graphic is the union of the zeroes of each system *L1*, ..., *Lk*.
Plot3D := proc ( sys ::{algebraic, equation(algebraic),
                        list({algebraic,equation(algebraic),
                                        algebraic<algebraic}),
                        list(list({algebraic,equation(algebraic),
                                             algebraic<algebraic}))},
                 ineq ::list({polynom, polynom<polynom}) := [],
                 e1  ::name=range := sort ([op(indets([sys,x,y,z],name))])[1]
                                          =-5..5,
                 e2  ::name=range := sort ([op(indets([sys,x,y,z],name)
                                            minus {lhs(e1)})])[1] =-5..5,
                 e3  ::name=range := sort ([op(indets([sys,x,y,z],name)
                                            minus {lhs(e1),lhs(e2)})])[1]
                                            =-5..5,
                 { points ::truefalse := false,
                   crossingrefine::truefalse := false,
                   grid::integer      := 10,
                   border::constant    := 10^(-30),
                   output::identical(list,display) := ':-display',
                   noverbose::truefalse := false,
                   verboserate::integer := -10} )
    local lsys, s, file, opts, ds, dc, npts, T, cells, ns, cd, lineq, p, va,
          bineq, x1, x2, x3, r1, r2, r3, pd, x, r, t, c, u, v, nsys,
          disp, tablevars, trans, invtrans, tr1, tr2, tx1, tx2, angles, q,
          lmini,oldva,f1,f2,e, y1, y2, pa, mpd, apd, l, all, verb, vr;
    
    verb := `if`(noverbose,x->(),printf);
    opts := 'axes'='boxed',labels=[lhs(e1),lhs(e2),lhs(e3)],
                             view=[rhs(e1),rhs(e2),rhs(e3)];

    x1,x2,x3 := lhs(e1), lhs(e2), lhs(e3);
    r1,r2,r3 := rhs(e1), rhs(e2), rhs(e3);

    if not crossingrefine then nsys := [] else nsys := sys end if;

    if type(sys,list(list)) then
        return plots:-display([seq(Plot3D(s,[op(ineq),
                                             op(map(q->q[1]^2,
                                                  [op({op(nsys)} minus {s})]))],
                                            e1,e2,e3,':-points'=points,
                                            ':-grid'=grid,':-border'=border,
                                            ':-output'=output,
                                            ':-crossingrefine'=crossingrefine,
                                            _rest),
                                    s in sys)]);
    end if;
    lsys := `if`(type(sys,Not(list)),[sys],sys);
    lineq := [ op(ineq), op(select (type, lsys,`<`)) ];
    lineq := map (e->if type(e,`<`) then rhs(e)-lhs(e) else e end if,
                     lineq);
    lineq := map (convert, lineq, rational, exact);
    f1 := (x->x); f2 := (x->x);
    y1 := x1; y2 := x2;
    e := border;
    if member (x1,indets (indets (sys,trig),name)) then
        f1:= y->if   evalf[-ilog(e)+1] (Pi-e)  <= y then infinity
                elif evalf[-ilog(e)+1] (-Pi+e) >= y then -infinity
                else evalf[-ilog(e)+1](tan(y/2)) end if;
        y1 := tan(x1/2);
    end if;
    if member (x2,indets (indets (sys,trig),name)) then
        f2:= y->if   evalf[-ilog(e)+1] (Pi-e)  <= y then infinity
                elif evalf[-ilog(e)+1] (-Pi+e) >= y then -infinity
                else evalf[-ilog(e)+1](tan(y/2)) end if;
        y2 := tan(x2/2);
    end if;
    bineq := map( convert, [ y1-f1(lhs(r1)), f1(rhs(r1))-y1,
                             y2-f2(lhs(rhs(e2))), f2(rhs(rhs(e2)))-y2 ],
                  rational, exact);
    bineq := remove (has, bineq, infinity);

    lineq := [ op(lineq), op(bineq) ];
    lsys  := remove (type, lsys, `<`);
    lsys  := map (e->if type(e,equation) then rhs(e)-lhs(e) else e end if,
                        lsys);
    
    if not (indets(lsys,name) subset {lhs(e1),lhs(e2)}) then
        lsys := AlgebraicTools:-Projection (lsys,
                     [lhs(e1),lhs(e2),lhs(e3)], output='listlist' );
    end if;
 
#    lsys := AlgebraicTools:-FactorSystem (lsys);
    lsys := map (numer@normal, AlgebraicTools:-Split (lsys) );
    lsys, lmini := selectremove (s->nops(s)=1,lsys);

#    lsys := numer (map ( op, select (s->nops(s)=1, lsys) ));
#    lsys := numer (map ( p->op (factors(p)[2,..,1]), lsys));
    
    if nops(lsys)=0   then 
        if output=':-list' then return []; end if;
        return emptyDisplay3d();
    elif nops(lsys)>1 then
        if not crossingrefine then nsys := [] else nsys := lsys end if;
        return plots:-display ([seq(Plot3D ([op(s),op(map(q->q>0,lineq))],
                                            map(q->q[1]^2,[op({op(nsys)}
                                                            minus {s})]),
                                            e1,e2,e3,':-points'=points,
                                            ':-grid'=grid,':-border'=border,
                                            ':-crossingrefine'=crossingrefine,
                                            ':-output'=output,
                                            ':-noverbose'=noverbose,
                                            ':-verboserate'=verboserate,
                                            _rest),
                                    s in lsys)]);
    end if;

    lsys := numer(convert( lsys, rational, exact));
    p := lsys[1];
    va := [op(indets(p,name) minus {x1,x2})];#lhs(e3);

    if AlgebraicTools:-Dimension(p,[op(va),x2,x1])<2 then
        if nops(p)=1 then #x3 does not appear in p
            x := [op({x1,x2,x3} minus indets(p,name)),op(indets(p,name))];
            r := subs (e1,e2,e3,x);
            t := table([x[1]=1,x[2]=2,x[3]=3]);
            t := [t[x1],t[x2],t[x3]];
            pd := Plot3D ([op(p),op(map(q->q>0,lineq))],
                          x[1]=r[1],x[2]=r[2],x[3]=r[3],
                         ':-points'=points, ':-grid'=grid,':-border'=border,
                         ':-output'=':-list', ':-noverbose'=noverbose,
                         ':-verboserate'=verboserate, _rest);
            pd := map[3] (map2, map, pt->pt[t], pd);

            if output=':-list' then return pd;
            elif nops(pd)=0 then return emptyDisplay3d(); end if;
            return plots:-display ([seq (plots:-surfdata (pts,opts,_rest),
                                         pts in pd)]);
        else
            print ("Warning, small dimension components are not displayed");
            return emptyDisplay3d();
        end if;
    end if;

    all, q := AlgebraicTools:-TrigonometricTanHalf ([op(p),op(lineq)],
                                                     variables=va, 'generic');
    p     := all[1..nops(p)];
    lineq := all[nops(p)+1..-1];
#    lineq, q := AlgebraicTools:-TrigonometricTanHalf (lineq, variables=va, 'generic');
    oldva := va;
    va := [ op(AlgebraicTools:-TrigonometricVariables (va, q)),
            op(AlgebraicTools:-TrigonometricConstants (q)) ];
    p     := map (numer@normal, p);
    lineq := map (numer@normal, lineq);

    verb (" -> Cell decomposition ...\n");
    cells := CellDecompositionPlus (p, lineq, va,[lhs(e1),lhs(e2)]);
    va := cells:-Variables;
    pa := ListTools:-Reverse (cells:-Parameters);
    p  := cells:-Equations;
    lineq := cells:-Inequalities;
    verb (" -> Number of solutions in each of the %a cell ...\n",
            nops(cells:-SamplePoints));


    ns := RootFinding:-Parametric:-NumberOfSolutions (cells,
                                         map(convert,[e1,e2],rational,exact));
    ns := select (x->x[2]>0, ns); 
    if ns=[] then 
        if output=':-list' then return [] else return emptyDisplay3d() end if;
    end if;

    # Mark trigonometric variables if any
    if member (':-TableVariables',[exports(cells)]) then
        tablevars := cells:-TableVariables;
        invtrans :=  cells:-AngleInvTransform;
        if oldva=va then
            trans :=  (x,y,z) -> [op(cells:-AngleTransform (x,y)),z];
        else
            trans :=  (x,y,z) -> [op(cells:-AngleTransform (x,y)),2*arctan(z)];
        end if;
    else
        tablevars := table();
        invtrans := [seq ((x,e)->x,v in pa)] ;
        trans := unapply(pa, pa);
    end if;

#    vars := AlgebraicTools:-AlgebraicVariables (lcell[..,3], tablevars);
#    lranges := subs ([ranges],vars);
#    lranges := zip ((x,r)->if r=x then x=drange else x=r end if,
#                    vars, lranges);
#    lranges := [x1=r1,x2=r2];
#    nranges := zip (`=`,cells:-Parameters,map(rhs,lranges));
#    nranges := zip ((f,x)->lhs(x)=f(lhs(rhs(x)),border)..f(rhs(rhs(x)),border),
#                    invtrans, nranges);
    #nranges := op(nranges);
    tr1 := invtrans[1](lhs(r1),border)..invtrans[1](rhs(r1),border);
    tr2 := invtrans[2](lhs(r2),border)..invtrans[2](rhs(r2),border);
    tx1, tx2 := op(pa);

    angles := map (x->assigned (tablevars[x]), [x1,x2]);

    cd := [ seq ( [CellDescriptionPlus(cells,ns[j,1]),
                   [ seq (seq (`if`(subs(s[i],op(cells:-SamplePoints[ns[j,1]]),
                                         `and`(op(map(q->q>0, lineq)))),
                                    i, NULL),
                               i=1..nops(s)),
                         s=[isole(subs(cells:-SamplePoints[ns[j,1]], p), va)])],
#                   select (i->subs (isole(subs(cells:-SamplePoints[ns[j,1]],
#                                               p),
#                                        va)[i],op(cells:-SamplePoints[ns[j,1]]),
#                                    `and`(op(map(q->q>0, lineq)))),
#                           [$1..ns[j,2]]),
                   j],
                  j=1..nops(ns) )  ];
#    printf (" ---> ");
    vr := `if`(verboserate<0,iquo(nops(ns)*(-verboserate),100)+1,verboserate);
    verb (" -> Plotting %a cells ...\n", nops(ns));
    pd := [ seq ( [ [seq ([seq ([u,v,
#                           isole(subs(x1=u, x2=v,cells:-Equations),va)],
                           isole(subs(tx1=u, tx2=v, p),va)],
                           v in listPoints(subs(tx1=u,c[1,2]),tr2,grid,
                                           border,tangent=angles[2]))],
                       u in listPoints(c[1,1],tr1,grid,
                                       border,tangent=angles[1]))], c[2],
                       `if`(c[3] mod vr=0, verb (" ---> %d%%\n",
                                                iquo(100*c[3],nops(ns))),
                                        verb ("")) ],
                  c in cd ) ];
    if (nops(ns) mod vr)<>0 then verb (" ---> 100%%\n") end if;;

    try
        pd := map (ll->seq (map (l->zip ((x,y)->[op(x),subs(y,op(va))],
                                        l[..,[1,2]], l[..,3,i]),ll[1]),
                            i in ll[2]),
                   pd);
        catch :
        mpd := map2 (max@op,2,pd);
        apd := map(ll->zip ((l,m)->map (z->if nops(z[3])<m then 1 else 0 end if,
                                        l),
                            ll[1], mpd),
                   pd);
        apd := Array(apd);
        l := map(x->[[lhs(x)][1],1,[lhs(x)][2]]=NULL, ArrayElems (apd));
        printf("Warning: internal error => lower resolution (%a line%sless).\n",
               nops(l),`if`(nops(l)>1,"s "," "));
        pd := subsop (op(l), pd);
        pd := map (ll->seq (map (l->zip ((x,y)->[op(x),subs(y,op(va))],
                                        l[..,[1,2]], l[..,3,i]),ll[1]),
                            i in ll[2]),
                   pd);
    end try;
    pd := map (c->map2 (map, trans@op, c), pd);

    if output=':-list' then return pd; end if;
    verb (" -> Sending results to display ...\n", nops(ns));
    disp := plots:-display ([seq (plots:-surfdata (pts), pts in pd)]);
    #disp := plottools:-transform ( trans )(disp);

    return plots:-display ([disp], opts, _rest);
#                                         [disp], l[k], labels=vars,
#                                         view=map(rhs,lranges),
#                                         opts);

#    return plots:-display ([seq (plots:-surfdata (pts,opts,_rest), pts in pd)]);

#    cd := [ seq (seq ( [ op(c[1]), `if`(j<nops(c[2]),
#                                        [p,c[2,j],lhs(e3),p,c[2,j+1]],
#                                        [p,c[2,j],lhs(e3),infinity,0]) ],
#                       j=1..nops(c[2]), 2 ),
#                 c in cd ) ];
#    printf (" -> Plotting %a cells ...\n", nops(cd));
#    return plots:-display ( [seq (plotHorizontalLeaves (c,e1,e2,e3,
#                                                        grid,border,opts,_rest),
#                                  c in cd)]);

#    ds := plots:-implicitplot3d (lsys, e1, e2, e3, opts, _rest);

#    lsys := numer (map ( op, select (s->nops(s)>1, lsys) ));
#    npts := select(x->lhs(x)='numpoints',[_rest]);
#    npts := `if` (npts=[], 100, rhs(npts[1]));
#    for s in lsys do
#        for i from lhs(rhs(e1)) to rhs(rhs(e1)) by ((rhs-lhs)(rhs(e1))/npts) do
#        solutions := [ seq (op(AlgebraicTools:-RealSolve(s, [lhs(e2),lhs(e3)],
#                                                            lhs(e1)=i)),
#                            i=lhs(rhs(e1))..rhs(rhs(e1)),
#                            (rhs-lhs)(rhs(e1))/npts) ];
#        end do:;
#
##TODO
#        toto:= {seq( [sol[i][1],sol[i][2], sol[i][3]], i=1..j-1 )}:;
#        dc := pointplot3d(toto, axes=boxed, labels=[alpha2, alpha3, L1]); 
#    end do;

end proc;

# Function: Plot3Dglsurf
#   Plots a system of 3 variables using glsurf.
#
# Parameters:
#   sys     - a list or a list of list of polynomials: the system
#   v1 = r1 - *v1* is a name of *sys* and *r1* a range of values
#   v2 = r2 - *v2* is a name of *sys* and *r2* a range of values
#   v3 = r3 - *v3* is a name of *sys* and *r3* a range of values
#   points = bool    (optional) - bool is a boolean: if false, isolated points
#                                                    are ignored;
#                                 default value: false;
#   scaled = keyword (optional) - *keyword* is one of 'constrained' or
#                                'unconstrained' for the scaling;
#                                 default value: scaled = unconstrained.
#
# Returns:
#   A graphic: the solutions of the system.
#   - when *sys* is a polynomial, the graphic is the zeroes of this polynomial
#   - when *sys* is a list of polynomials *[p1,...,pk]*,
#     the graphic is the zeroes of the system *p1=0* and ... and *pk=0*
#   - when *sys* is a list of list of polynomials *[L1,...,Lk]*,
#     the graphic is the union of the zeroes of each system *L1*, ..., *Lk*.
#
Plot3Dglsurf := proc ( sys ::{algebraic, equation(algebraic), 
                        list({algebraic,equation(algebraic)}),
                        list(list({algebraic,equation(algebraic)}))},
                 e1  ::name=range,
                 e2  ::name=range,
                 e3  ::name=range,
                 { points ::truefalse := false,
                   scale  ::identical(constrained, unconstrained)
                            := 'unconstrained'} )
    local lsys, s, file, size, m1, m2, m3, v1, v2, v3, v;

    size := 10;

    lsys := `if`(type(sys,Not(list)),[[sys]],
                                   `if`(type(sys,list(Not(list))),[sys],sys));

    lsys  := map2 (remove, type, lsys, `<`);
    lsys  := map2 (map, e->if type(e,equation) then rhs(e)-lhs(e) else e end if,
                        lsys);
 
    if not (indets(lsys,name) subset {lhs(e1),lhs(e2)}) then
        lsys := map( op@AlgebraicTools:-Projection, lsys,
                     [lhs(e1),lhs(e2),lhs(e3)], output='listlist' );
    end if;
    
    if ormap (s->nops(s)>=2, lsys) then
        print ("Warning, small dimension components are not displayed");
    end if;

    lsys := map ( op, select (s->nops(s)=1, lsys) );
    
    m1,v1 := (rhs+lhs)(rhs(e1))/2, (rhs-lhs)(rhs(e1))/2;
    m2,v2 := (rhs+lhs)(rhs(e2))/2, (rhs-lhs)(rhs(e2))/2;
    m3,v3 := (rhs+lhs)(rhs(e3))/2, (rhs-lhs)(rhs(e3))/2;

    if 'scale'='constrained' then
        v := max(v1,v2,v3);
        lsys := expand(numer (subs (lhs(e1)=m1 + 2*v/size*x,
                                    lhs(e2)=m2 + 2*v/size*y,
                                    lhs(e3)=m3 + 2*v/size*z, lsys)));
        v1, v2, v3 := evalf(v1/v*size/2),evalf(v2/v*size/2),evalf(v3/v*size/2);
    else
        lsys := expand(numer (subs (lhs(e1)=m1 + 2*v1/size*x,
                                    lhs(e2)=m2 + 2*v2/size*y,
                                    lhs(e3)=m3 + 2*v3/size*z, lsys)));
        v1, v2, v3 := evalf(size/2), evalf(size/2), evalf(size/2);
    end if;

    file := FileTools:-Text:-OpenTemporaryFile ();

    s := cat ("let size = ", size, ";\n");
    s := cat (s, seq (cat ("surface s", i, " = ", convert(lsys[i],string),
                           ";\n"),
                      i=1..nops(lsys)));
    s := cat (s, "object frame = vertices ", 
                 sprintf ("%f, %f, %f ", + v1, + v2, + v3),
                 sprintf ("%f, %f, %f ", + v1, - v2, - v3),
                 sprintf ("%f, %f, %f ", - v1, + v2, - v3),
                 sprintf ("%f, %f, %f ", - v1, - v2, + v3),
                 sprintf ("%f, %f, %f ", + v1, + v2, - v3),
                 sprintf ("%f, %f, %f ", - v1, + v2, + v3),
                 sprintf ("%f, %f, %f ", + v1, - v2, + v3),
                 " line 1,4,2,5,0,6,3,5 line 6,1 line 0,4;\n");
    s := cat (s, "vector line_color=(1,0,0,1);\n");
    s := cat (s, "object X = vertices ",
                 sprintf ("%f, %f, %f ", - v1, - v2, - v3),
                 sprintf ("%f, %f, %f ", + v1, - v2, - v3),
                 " line 0,1;\n");
    s := cat (s, "vector line_color=(0,1,0,1);\n");
    s := cat (s, "object Y = vertices ",
                 sprintf ("%f, %f, %f ", - v1, - v2, - v3),
                 sprintf ("%f, %f, %f ", - v1, + v2, - v3),
                 " line 0,1;\n");
    s := cat (s, "vector line_color=(0,0,1,1);\n");
    s := cat (s, "object Z = vertices ",
                 sprintf ("%f, %f, %f ", - v1, - v2, - v3),
                 sprintf ("%f, %f, %f ", - v1, - v2, + v3),
                 " line 0,1;\n");
    s := cat (s, "draw ", seq (cat ("s",convert(i,string),","),
                               i=1..nops(lsys)-1),
                 "s",nops(lsys), ", X, Y, Z, frame;");
    fprintf ( file, s);
    FileTools:-Text:-Close (file);

    system (cat ("LD_LIBRARY_PATH= glsurf ", file, " && rm ", file, " &"));

    return NULL;
end proc;

# local function for surfex plots
surfexPrefix := proc()
    local s;
    s :="this is surfex v0.90.00\n///////// TYPE: //////\ncomplete\n///////// GENERAL DATA: //////\n0.01\n1.0\n-0.005\n-0.005\n-0.005\n-1.0\n0.01\n0.01\n2.0\n0.0\n0.0\n0.0\n5.0\n0.0\n10.0\n255\n255\n255\nfalse\n7\n11.0\n////////////////// EQUATIONS: /////////////////////////\n";
    return s;
end proc;

# local function for surfex plots
surfexPostfix := proc()
    local s;
    s := "////////////////// CURVES: /////////////////////////\n0\n////////////////// PARAMETERS: /////////////////////////\n0\n////////////////// SOLITARY POINTS: /////////////////////////\n0\n";
    return s;
end proc;

# local function for surfex plots
surfexEquation := proc (p::polynom,
                        i::integer)
    local s,r,k;
    randomize (125362);
    r := rand (128..255);
    for k from 1 to 6*(i-1) do r(); end do;
    s := sprintf ("////////////////// Equation: /////////////////////////\n%a\n%a\n",
                  i, p);
    s := cat (s, r(),"\n",r(),"\n",r(),"\n",r(),"\n",r(),"\n",r(),"\n");
    s := cat (s, "0\ntrue\n0\ntrue\n10.0\n0\n");
    return s;
end proc;

# local function for surfex plots
surfexInequation := proc (p::polynom,
                          i::integer)
    local s,r,k;
    randomize (125362);
    r := rand (128..255);
    for k from 1 to 6*(i-1) do r(); end do;
    s := sprintf ("////////////////// Equation: /////////////////////////\n%a\n%a\n",
                  i, p);
    s := cat (s, r(),"\n",r(),"\n",r(),"\n",r(),"\n",r(),"\n",r(),"\n");
    s := cat (s, "0\ntrue\n2\ntrue\n10.0\n0\n");
    return s;
end proc;

# Function: Plot3Dsurfex
#   Plots a system of 3 variables using surfex (software based on surf).
#
# Parameters:
#   sys     - a list or a list of list of polynomials and/or inequalities:
#             the system
#   ineq    (optional) - list of polynomials: extra positive inequalities;
#                        default value: []
#   v1 = r1 (optional) - *v1* is a name of *sys* and *r1* a range of values;
#                        default value: v1=-1..1 where v1 is a variable of *sys*
#   v2 = r2 (optional) - *v2* is a name of *sys* and *r2* a range of values
#                        default value: v2=-1..1 where v2 is a variable of *sys*
#   v3 = r3 (optional) - *v3* is a name of *sys* and *r3* a range of values
#                        default value: v3=-1..1 where v3 is a variable of *sys*
#   points = bool    (optional) - bool is a boolean: if false, isolated points
#                                                    are ignored;
#                                 default value: false;
#   scaled = keyword (optional) - *keyword* is one of 'constrained' or
#                                'unconstrained' for the scaling;
#                                 default value: scaled = unconstrained.
#
# Returns:
#   A graphic: the solutions of the system.
#   - when *sys* is a polynomial, the graphic is the zeroes of this polynomial
#   - when *sys* is a list of polynomials *[p1,...,pk]*,
#     the graphic is the zeroes of the system *p1=0* and ... and *pk=0*
#   - when *sys* is a list of list of polynomials *[L1,...,Lk]*,
#     the graphic is the union of the zeroes of each system *L1*, ..., *Lk*.
Plot3Dsurfex := proc ( sys ::{algebraic, equation(algebraic), 
                        list({algebraic, equation(algebraic),
                                         algebraic<algebraic}),
                        list(list({algebraic,equation(algebraic)}))},
                        ineq ::list({polynom, polynom<polynom}) := [],
                 e1  ::name=range := sort ([op(indets([sys,x,y,z]))])[1] =-1..1,
                 e2  ::name=range := sort ([op(indets([sys,x,y,z]))])[2] =-1..1,
                 e3  ::name=range := sort ([op(indets([sys,x,y,z]))])[3] =-1..1,
                       { points ::truefalse := false,
                         scale  ::identical(constrained, unconstrained)
                                  := 'unconstrained' } )
    local lsys, s, file, size, m1, m2, m3, v1, v2, v3, v, lineq;

    size := 4;

    lsys := `if`(type(sys,Not(list)),[[sys]],
                                   `if`(type(sys,list(Not(list))),[sys],sys));
    lineq := [ op(ineq), op(map2(op@select, type, lsys,`<`)) ];
    lineq := map (e->if type(e,`<`) then rhs(e)-lhs(e) else e end if,
                     lineq);
    lineq := [ op(lineq), lhs(e1)-lhs(rhs(e1)), rhs(rhs(e1))-lhs(e1),
                          lhs(e2)-lhs(rhs(e2)), rhs(rhs(e2))-lhs(e2),
                          lhs(e3)-lhs(rhs(e3)), rhs(rhs(e3))-lhs(e3)  ];
    lsys  := map2 (remove, type, lsys, `<`);
    lsys  := map2 (map, e->if type(e,equation) then rhs(e)-lhs(e) else e end if,
                        lsys);
    
    if not (indets(lsys,name) subset {lhs(e1),lhs(e2)}) then
        lsys := map( op@AlgebraicTools:-Projection, lsys,
                     [lhs(e1),lhs(e2),lhs(e3)], output='listlist' );
    end if;
    
    if ormap (s->nops(s)>=2, lsys) then
        print ("Warning, small dimension components are not displayed");
    end if;

    lsys := map ( op, select (s->nops(s)=1, lsys) );
    
    m1,v1 := (rhs+lhs)(rhs(e1))/2, (rhs-lhs)(rhs(e1))/2;
    m2,v2 := (rhs+lhs)(rhs(e2))/2, (rhs-lhs)(rhs(e2))/2;
    m3,v3 := (rhs+lhs)(rhs(e3))/2, (rhs-lhs)(rhs(e3))/2;

    if 'scale'='constrained' then
        v := max(v1,v2,v3);
        lsys := expand(numer (subs (lhs(e1)=m1 + 2*v/size*x,
                                    lhs(e2)=m2 + 2*v/size*y,
                                    lhs(e3)=m3 + 2*v/size*z, lsys)));
        lineq := expand(numer (subs (lhs(e1)=m1 + 2*v/size*x,
                                     lhs(e2)=m2 + 2*v/size*y,
                                     lhs(e3)=m3 + 2*v/size*z, lineq)));
        v1, v2, v3 := evalf(v1/v*size/2),evalf(v2/v*size/2),evalf(v3/v*size/2);
    else
        lsys := expand(numer (subs (lhs(e1)=m1 + 2*v1/size*x,
                                    lhs(e2)=m2 + 2*v2/size*y,
                                    lhs(e3)=m3 + 2*v3/size*z, lsys)));
        lineq := expand(numer (subs (lhs(e1)=m1 + 2*v1/size*x,
                                     lhs(e2)=m2 + 2*v2/size*y,
                                     lhs(e3)=m3 + 2*v3/size*z, lineq)));
        v1, v2, v3 := evalf(size/2), evalf(size/2), evalf(size/2);
    end if;

    file := FileTools:-Text:-OpenTemporaryFile ();
    s := cat (surfexPrefix(), nops(lsys)+nops(lineq), "\n");
    s := cat (s, seq (surfexEquation (lsys[i],i), i=1..nops(lsys)));
    s := cat (s, seq (surfexInequation (lineq[i],i+nops(lsys)),
                      i=1..nops(lineq)));
    s := cat (s, surfexPostfix() );
    fprintf (file, s);
    FileTools:-Text:-Close (file);

    system (cat ("LD_LIBRARY_PATH= surfex ", file, " && rm ", file, " &"));
    return NULL;
#    FileTools:-Remove (file);
end proc;
 


# Function: PlotWorkspace
#   Plot the border of a manipulator workspace
#
# Parameters:
#   robot - a object of type *Manipulator*
#   spec  - sequence of *name=constant*: the specification of the known 
#           variables (the articular values or the pose values, or other)
#   disp   (optional) - a sequence of keywords among
#                       *type1*, *type2*, *constraints* and *infinite*:
#                       selects the graphics to plot;
#                       default value: all the keywords
#   vars   (optional) - the display variables, that can be referred as:
#   - one of the keyword 'articular' or 'pose'
#   - a list of 2 variable names
#   - the default value is 'articular'
#   - if *ranges* is provided, the variables superseed those of *vars*
#   ranges (optional) - a sequence of the form *v1=n1..n2, v2=m1..m2* where
#     *v1*, *v2* are 2 variables and *n1,n2,m1,m2* are numerical values;
#     when *ranges* is not specified, the default range is taken from the field
#     *robot:-DefaultPlotRange* if it exists, and is *-5..5* otherwise.
#   notest = b        (optional) - *b* is a boolean; when *b* is true the
#                                  inequality and real constraints are ignored;
#                                  default value: true;
#
# Returns:
#   A graphic containing:
#   - The singularities of type 1
#   - The singularities of type 2
#   - The configurations with infinitely many solutions in all tha variables
#     (including the passive variables)
#   - The border induced by the constraints
#
# Example:
# > > rprrp := Parallel_RPRRP ():
# > > PlotWorkspace (rprrp, articular);
# (see rprrp_articular.gif)
# > > PlotWorkspace (rprrp, pose);
# (see rprrp_pose.gif)
# > > PlotWorkspace (rprrp, [S2,x]);
# (see rprrp_S2_x.gif)
PlotWorkspace := proc ( robot ::Manipulator,
                        spec  ::seq(name=constant),
                        disp  ::seq(identical(type1,type2,
                                              constraints,infinite))
                                := ('type1','type2','constraints','infinite'),
                        vars  ::{list(name), identical(articular, pose)}
                                := 'articular',
                        ranges ::seq(name=range) := NULL,
                        { notest ::truefalse := false })
    local sing1, sing2, Lconstr, Psing1, Psing2, Pconstr, plotvars, plotranges,
          dsing1, dsing2, dconstr, dinf, Linf, r1, r2, rob;

    rob := SubsParameters( spec, robot);

    if nops ([ranges])=2 then
        plotvars := map(lhs,[ranges]);
    elif vars ='articular' and nops(rob:-ArticularVariables)=2 then
        plotvars := rob:-ArticularVariables
    elif vars ='pose' and nops(rob:-PoseVariables)=2 then
        plotvars := rob:-PoseVariables
    elif type(vars,list(name)) and nops(vars)=2 then
        plotvars := vars;
    else error "The function handle 2d graphs only";
    end if;


    sing1 := Type1SingularityEquations (rob);
    sing2 := Type2SingularityEquations (rob);
    Lconstr := ConstraintEquations (rob, 'constraints'= (not notest) );
    Linf := InfiniteEquations (rob, plotvars);

    if not notest then
        sing1 := [op(sing1), op(rob:-Constraints)];
        sing2 := [op(sing2), op(rob:-Constraints)];
        Linf := map (s->[op(s), op(rob:-Constraints)], Linf);
    end if;
    
    r1 := subs(rob:-DefaultPlotRanges, plotvars[1]);
    if type(r1,name) then r1 := -5..5 end if;
    r2 := subs(rob:-DefaultPlotRanges, plotvars[2]);
    if type(r2,name) then r2 := -5..5 end if;

    if nops([ranges]) = 2 then plotranges := ranges
                          else plotranges := plotvars[1]=r1,
                                             plotvars[2]=r2;
    end if;

    dsing1:=NULL; dsing2:=NULL; dinf:=NULL; dconstr:=NULL;
    if member('type1',{disp}) then
        dsing1  := Plot2D ( sing1, plotranges, ':-notest'=notest,
                            legend="Type 1 singularities", color=red, _rest):
    end if;
    if member('type2',{disp}) then
        dsing2  := Plot2D ( sing2, plotranges, ':-notest'=notest,
                            legend="Type 2 singularities", color=blue, _rest):
    end if;
    if member('infinite',{disp}) then
        dinf  := Plot2D ( Linf, plotranges, points=true, ':-notest'=notest,
                          legend="Infinity of solutions", color=green, _rest):
    end if;
    if member('constraints',{disp}) then
        dconstr := Plot2D ( Lconstr, plotranges, ':-notest'=notest,
                            legend="Constraint border", color=black, _rest):
    end if;

    plots:-display ([dsing1,dsing2,dinf,dconstr]);

end proc;

# Function: Configurations
#   Computes the different possible positions.
#
# Parameters:
#   robot - an object of type *Manipulator*
#   spec  - sequence of *name=constant*: the specification of the known 
#           variables (the articular values or the pose values, or other)
#   noconstraints=*b* (optional) - *b* is a boolean: when *true*, the constraint
#                                  inequalities are ignored;
#                                  default value: false.
#   ordering          (optional) - *ordering* is a name used to order
#                                  the solutions;
#                                  default value: NULL
#
# Returns:
#   A list of elements: each elements is a list of *name=list(constant)* and
#   represents a configuration of the input manipulator.
#
# Example:
# (begin code)
# > rpr := Parallel_3RPR ():                                   
# > Configurations (rpr, r1=20, r2=20, r3=20);                 
# [[A1 = [0., 0.], A2 = [15.91, 0.], A3 = [0., 10.00],
# 
#     B1 = [1.238874219, -19.96159289], B2 = [18.27856689, -19.85925202],
# 
#     B3 = [14.37838350, -3.78571901]], [A1 = [0., 0.], A2 = [15.91, 0.],
# 
#     A3 = [0., 10.00], B1 = [-15.37568723, 12.79016193],
# 
#     B2 = [1.62170358, 13.99444837], B3 = [-3.30997940, 29.78205101]], [
# 
#     A1 = [0., 0.], A2 = [15.91, 0.], A3 = [0., 10.00],
# 
#     B1 = [19.99999727, -0.01045036340], B2 = [31.39060352, 12.66297417],
# 
#     B3 = [16.87622894, 20.59410427]], [A1 = [0., 0.], A2 = [15.91, 0.],
# 
#     A3 = [0., 10.00], B1 = [-13.64842837, -14.61917929],
# 
#     B2 = [-4.083315519, -0.51704413], B3 = [-19.53983068, 5.37074264]]]
# (end code)
Configurations := proc ( robot ::Manipulator,
                         spec  ::seq(name=constant),
                         { noconstraints::truefalse := false,
                           ordering::name := NULL} )
    local sols, cpoints, svars, sys, vars, formula;
    sols := {op(robot:-GeometricValues), op(robot:-ArticularValues),
             op(robot:-PoseValues), op(robot:-PassiveValues)};
    vars := [op(robot:-GeometricParameters), op(robot:-ArticularVariables),
             op(robot:-PoseVariables), op(robot:-PassiveVariables)];
    sols := remove (s->member (lhs(s), map(lhs,[spec])), sols);
    sols := [[op(sols),spec]];
    formula := [op(robot:-Points),
                'det(J)'=[VectorCalculus:-Jacobian(
                                robot:-Equations[1..nops(robot:-PoseVariables)],
                                robot:-PoseVariables,
                                'determinant'
                                )][2]];
    cpoints := [subs(op(sols[1]),formula)];
    svars   := indets (map2(map,rhs,cpoints),name);
    if not svars subset {Pi} or
       not type(subs(sols[1],vars),list(constant)) then
        if noconstraints then sys := robot:-Equations
                         else sys := [op(robot:-Equations),
                                      op(robot:-Constraints)] end if;
        sols := [AlgebraicTools:-RealSolve (sys,op(sols[1]))];
        # op(sols) necessary to keep order of substitution
        cpoints := map( evalf@subs, sols, op(cpoints) );
    end if;

    if ordering = NULL then
        return eval (zip ((x,y)->[op(x),op(y)], cpoints, sols));
    else
        return sort (eval (zip ((x,y)->[op(x),op(y)], cpoints, sols)),
                     (x,y)->subs(x,ordering) < subs(y,ordering));
    end if;

end proc;

# local function to plot an anctuator; used by PlotRobot2D
plotActuator := proc ( actuator ::{list(constant), list(list(constant))} )
    local v,c1,c2,nv,d;
    if type( actuator, list(constant)) then
        return plots:-display
                      ( [ plots:-pointplot ( actuator, _rest, symbol=circle,
                                             symbolsize=30,color=red ),
                          plots:-pointplot ( actuator, _rest, symbol=circle,
                                             symbolsize=50,color=red ) ] );
    else
        v := zip( `-`,actuator[2],actuator[1]);
        c1 := 1/3;
        c2 := 1/18;
        nv := [-c1*v[2], c1*v[1]];
        d := plottools:-line (actuator[1],
                              [actuator[1,1]+c1*v[1],actuator[1,2]+c1*v[2]],
                              _rest),
             plottools:-polygon
         ([[actuator[1,1]+c1*v[1]+c2*nv[1],   actuator[1,2]+c1*v[2]+c2*nv[2]],
           [actuator[1,1]+2*c1*v[1]+c2*nv[1], actuator[1,2]+2*c1*v[2]+c2*nv[2]],
           [actuator[1,1]+2*c1*v[1]-c2*nv[1], actuator[1,2]+2*c1*v[2]-c2*nv[2]],
           [actuator[1,1]+c1*v[1]-c2*nv[1],   actuator[1,2]+c1*v[2]-c2*nv[2]]],
               _rest, 'color'="LightGray"),
             plottools:-line ([actuator[1,1]+2*c1*v[1],actuator[1,2]+2*c1*v[2]],
                              actuator[2], _rest);
        return plots:-display([d]);
    end if;
end proc;


# Function: PlotRobot2D
#   Plot a planar manipulator 
#
# Parameters:
#   robot - a *Manipulator*: the robot to plot
#   spec  - a sequence of *name=constant*: specification of variables of 
#           the robot to plot
#   k         (optional) - an integer: specifies one of the possible
#                                      configuration when several are available
#   color=col   (optional) - equation of the shape color=*col*, where col is
#                            a color or a list of colors; when the number of
#                            specified color is not enough, deterministic
#                            colors are chosen;
#                            default value: empty list.
#   legendvars  (optional) - list of names: the variables to display in 
#                            the legend;
#                            default value: the articular, passive and pose
#                            variables, minus the variables in *spec*
#   nolegend=b  (optional) - *b* is a boolean: when false, a legend is displayed
#                          (the graphic appears in a separate windows with a
#                           classic worksheet);
#                            default value: false.
#
# Returns:
#   A graphic: the different configurations of the manipulator satisfying the
#   input specifications. (Maple 13 and higher only)
#
# Example:
# (begin code)
# > rpr := Parallel_3RPR ():
# > PlotRobot2D (rpr, r1=20, r2=20, r3=20);
# (end code)
# (see 3rpr_plot.gif)
# (begin code)
# > rr := Parallel_2RR():                                              
# > plots:-animate (PlotRobot2D, [rr, theta1=1+t, theta3=2-t], t=0..1);
# (end code)
# (see 2rr_animation.gif)
PlotRobot2D := proc (robot ::Manipulator,
                     spec  ::seq(name=constant),
                     k     ::{integer,range,list(integer)}      := ..,
                     { color := [],
                       legendvars := subs (map(s->lhs(s)=NULL,[spec]),
                                           [op(robot:-ArticularVariables),
                                            op(robot:-PoseVariables),
                                            op(robot:-PassiveVariables),
                                            'det(J)']),
                       nolegend ::truefalse := false,
                       noconstraints ::truefalse := false} )
    local cpoints, svars, sols, p, d, dp, dl, dcl, dc, da, l, dpt, r, opts, c,
          miniopts;
    
    cpoints := Configurations (robot, spec, ':-noconstraints'=noconstraints);
    miniopts := (thickness=3, _rest);
    #to avoid displaying spurious legends
    opts := (miniopts, ':-legend'="__never_display_this_legend_entry" );

    if cpoints=[] then print ("No configuration"); return NULL end if;

    try
        if type(k,integer) then cpoints := cpoints[k..k];
                           else cpoints := cpoints[k];    end if;
    catch:
        print ("Warning: The number of configurations is only", nops(cpoints));
        return plots:-pointplot([], 'axes'='none')
    end;

#    l := table ( zip ((p,st)->p=('linestyle'=st), cpoints,
#                                            [ $ 1..7, 1 $ nops(cpoints)-7 ]) );

    randomize (125362);
    r := rand (128..255);
    if type(color,list) then c := color else c := [color]; end if;
    l := table ( zip ((p,co)->p=(':-color'=co), cpoints,
                                            [ op(c), 
                                              seq (RGB(r()/255,r()/255,r()/255),
                                                   i=1..nops(cpoints)) ]) );

    d := NULL;
    # the first elements are displayed on top of the subsequent ones
    if not nolegend then
        for p in cpoints do
            dcl := plots:-pointplot (subs(p,[lhs(robot:-Points[1])]), 
                                    'symbol'='solidcircle', 'symbolsize'=20,
                                     miniopts, l[p],
                    ':-legend'=convert(map(v->v=evalf[2](subs(p,v)),legendvars),
                                       string));
            d := d, dcl;
        end do;
    end if;
    for p in cpoints do
        dpt := plots:-textplot (map (pt->[rhs(pt)[1], rhs(pt)[2], 
                                          convert(lhs(pt),string)],
                                     select (type,p,name=list)),
                                align={below,left}, font=[TIMES,BOLDITALIC,15],
                                ':-color'='black');
        d   := d, dpt;
    end do;
    for p in cpoints do
        da := map (plotActuator, subs (p, robot:-Actuators), opts, l[p]);
        d  := d, op(da);
    end do;
    for p in cpoints do
        dl := map (plottools:-polygon, subs(p,robot:-Loops),
                                      ':-color'="LightBlue", opts, l[p]);
        d  := d, op(dl);
    end do;
    for p in cpoints do
        dp := plots:-pointplot (subs (p,map(lhs,robot:-Points)),
                               'symbol'='solidcircle', 'symbolsize'=20,
                                opts, l[p]);
        d  := d, dp;
    end do;
    for p in cpoints do
        dc := map(plots:-pointplot, subs(p,robot:-Chains), connect, opts, l[p]);
        d  := d, op(dc);
    end do;
    plots:-display ([d], 'axes'='none', 'scaling'='constrained',
                         'title'=robot:-Model);
end proc;

# local function to plot an anctuator; used by PlotRobot3D
plotActuator3D := proc ( actuator ::{list(constant), list(list(constant))} )
    local v,c1,c2,nv,d,alpha,pris,rv;
    if type( actuator, list(constant)) then
        return plots:-display
                      ( [ plots:-pointplot3d ( actuator, _rest, symbol=circle,
                                               symbolsize=30,color=red ),
                          plots:-pointplot3d ( actuator, _rest, symbol=circle,
                                               symbolsize=50,color=red ) ] );
    else
        v := zip( `-`,actuator[2],actuator[1]);
        nv := sqrt(v[1]^2+v[2]^2+v[3]^2);
        alpha := arctan (sqrt(v[1]^2+v[2]^2), v[3]);
        if v[1]=0 and v[2]=0 then rv := [1,0,0];
                             else rv := [-v[2],v[1],0]; end if;
        
        c1 := 1/3;
        c2 := 1/18;
        pris := [actuator[1,1]+c1*v[1],
                 actuator[1,2]+c1*v[2],
                 actuator[1,3]+c1*v[3]];
        d := plottools:-line (actuator[1], pris, _rest),
#             plots:-tubeplot ([actuator[1,1]+t*c1*v[1], 
#                               actuator[1,2]+t*c1*v[2],
#                               actuator[1,3]+t*c1*v[3]],
#                               radius=c2*sqrt(v[1]^2+v[2]^2+v[3]^2),
#                               _rest, 'color'="LightGray"),

             plottools:-rotate (
                plottools:-cylinder (pris, c2*nv, c1*nv,
                                     _rest, 'color'="LightGray",thickness=1
#                                     ,style=patchnogrid
                                     ),
                alpha, [pris, zip (`+`,pris,rv)]),
             
#             transform (
#                plottools:-cylinder (pris, c2*nv, 0,
#                                     _rest, 'color'="LightGray"),
#                alpha, [pris, zip (`+`,pris,[-v[2],v[1],0])]),
#
#             transform (
#                plottools:-cylinder (zip (`+`,pris,[0,0,c1*nv]), c2*nv, 0,
#                                     _rest, 'color'="LightGray"),
#                alpha, [pris, zip (`+`,pris,[-v[2],v[1],0])]),

             plottools:-line ([actuator[1,1]+2*c1*v[1],actuator[1,2]+2*c1*v[2],
                               actuator[1,3]+2*c1*v[3]],
                               actuator[2], _rest);
        return plots:-display([d]);
    end if;
end proc;


# Function: PlotRobot3D
#   Plot a 3D manipulator 
#
# Parameters:
#   robot - a *Manipulator*: the 3D robot to plot
#   spec  - a sequence of *name=constant*: specification of variables of 
#           the robot to plot
#   k         (optional) - an integer: specifies one of the possible
#                                      configuration when several are available
#   color=col   (optional) - equation of the shape color=*col*, where col is
#                            a color or a list of colors; when the number of
#                            specified color is not enough, deterministic
#                            colors are chosen;
#                            default value: empty list.
#  legendvars=l (optional) - *l* is list of names: the variables to display in 
#                            the legend;
#                            default value: the articular, passive and pose
#                            variables, minus the variables in *spec*
#   nolegend=b  (optional) - *b* is a boolean: when false, a legend is displayed
#                          (the graphic appears in a separate windows with a
#                           classic worksheet);
#                            default value: false.
#   nolabel=b   (optional) - *b* is a boolean: when true, no label appears on
#                            the articulations;
#                            default value: false.
#
# Returns:
#   A graphic: the different configurations of the manipulator satisfying the
#   input specifications. (Maple 13 and higher only)
#
# Example:
# (begin code)
# > robot := Orthoglide():                                              
# > PlotRobot3D (robot, r1=1,r2=1,r3=1, 2);
# (end code)
# (see orthoglide.gif)
PlotRobot3D := proc (robot ::Manipulator,
                     spec  ::seq(name=constant),
                     k     ::{integer,range,list(integer)}      := ..,
                     { color := [],
                       legendvars := subs (map(s->lhs(s)=NULL,[spec]),
                                           [op(robot:-ArticularVariables),
                                            op(robot:-PoseVariables),
                                            op(robot:-PassiveVariables),
                                            'det(J)']),
                       nolegend ::truefalse      := false,
                       noconstraints ::truefalse := false,
                       nolabel ::truefalse        := false} )
    local cpoints, svars, sols, p, d, dp, dl, dcl, dc, da, l, dpt, r, opts, c,
          miniopts;
    
    cpoints := Configurations (robot, spec, ':-noconstraints'=noconstraints);
    miniopts := (thickness=3, _rest);
    #to avoid displaying spurious legends
    opts := (miniopts); #, ':-legend'="__never_display_this_legend_entry" );

    if cpoints=[] then print ("No configuration"); return NULL end if;

    try
        if type(k,integer) then cpoints := cpoints[k..k];
                           else cpoints := cpoints[k];    end if;
    catch:
        print ("Warning: The number of configurations is only", nops(cpoints));
        return emptyDisplay3d(); #plots:-pointplot([], 'axes'='none')
    end;

#    l := table ( zip ((p,st)->p=('linestyle'=st), cpoints,
#                                            [ $ 1..7, 1 $ nops(cpoints)-7 ]) );

    randomize (125362);
    r := rand (128..255);
    if type(color,list) then c := color else c := [color]; end if;
    l := table ( zip ((p,co)->p=(':-color'=co), cpoints,
                                            [ op(c), 
                                              seq (RGB(r()/255,r()/255,r()/255),
                                                   i=1..nops(cpoints)) ]) );

    d := NULL;
    # the first elements are displayed on top of the subsequent ones
    if not nolegend then
        for p in cpoints do
            dcl := plots:-pointplot3d (subs(p,[lhs(robot:-Points[1])]), 
                                    'symbol'='solidcircle', 'symbolsize'=20,
                                     miniopts, l[p]
#                  ,':-legend'=convert(map(v->v=evalf[2](subs(p,v)),legendvars),
#                                       string)
                                       );
            d := d, dcl;
        end do;
    end if;
    if not nolabel then
     for p in cpoints do
         dpt := plots:-textplot3d (map (pt->[rhs(pt)[1], rhs(pt)[2], rhs(pt)[3],
                                           convert(lhs(pt),string)],
                                      select (type,p,name=list)),
                                 align={below,left}, font=[TIMES,BOLDITALIC,15],
                                 ':-color'='black');
         d   := d, dpt;
     end do;
    end if;
    for p in cpoints do
        da := map (plotActuator3D, subs (p, robot:-Actuators), opts, l[p]);
        d  := d, op(da);
    end do;
    for p in cpoints do
        dl := map (plottools:-polygon, subs(p,robot:-Loops),
                                      ':-color'="LightBlue", opts, l[p]);
        d  := d, op(dl);
    end do;
    for p in cpoints do
        dp := plots:-pointplot3d (subs (p,map(lhs,robot:-Points)),
                               'symbol'='solidcircle', 'symbolsize'=20,
                                opts, l[p]);
        d  := d, dp;
    end do;
    for p in cpoints do
        dc := map(plots:-pointplot3d, subs(p,robot:-Chains), connect, opts, l[p]);
        d  := d, op(dc);
    end do;
    plots:-display ([d], 'axes'='none', 'scaling'='constrained',
                         'title'=robot:-Model);
end proc;

#local function to return an empty display without errors
emptyDisplay2d := proc()
    return plots:-pointplot ([],'connect',_rest);
end proc;

#local function to return an empty display without errors
emptyDisplay3d := proc()
    return plots:-pointplot3d ([],'connect',_rest);
end proc;

# local function to save results of previous isolations
isole := proc ()
    option remember;

    if type([args][1],list(polynom(rational))) and nops([args][1])=1 
        or type([args][1],polynom(rational)) then
        RootFinding:-Isolate (args);
    else
        sort ([AlgebraicTools:-RealSolve (args)]);
    end if;
end proc;

getBound := proc( c::[polynom,integer,name,polynom,integer],
                  n::integer,
                  eps::constant := 0.001 )
    local reps,p,i,x,q,j,di,r1,r2,e;
    option cache;
    reps := convert(eps,rational,exact);
    di := -ilog10(eps);
    p,i,x,q,j := op(c);
    p := primpart(convert(p,rational,exact));
    q := primpart(convert(q,rational,exact));
    r1 := -infinity;
    r2 := +infinity;
    if i<>0 then
        tmp:=subs([isole (p,x,output=interval,digits=di,constraints=[q])][1,i], x);
        if nops(tmp) = 2 then
            r1 := tmp[2];
        else:
            r1 := tmp[1,2];
        end if;
    end if;
    if j<>0 then
        tmp:=subs([isole (q,x,output=interval,digits=di,constraints=[p])][1,j], x);
        if nops(tmp) = 2 then
            r2 := tmp[1];
        else
            r2 := tmp[1,1];
        end if;
    end if;
    e := min (reps, (r2-r1)/2, reps*(r2-r1));
    if n=1 and i<>0   then return r1+e;
    elif n=1 and i=0  then return -infinity;
    elif n=2 and j<>0 then return r2-e;
    elif n=2 and j=0 then return +infinity;
    end if;
    error ("Invalid input %1",c);
end proc;

# returns a list of points between to algebraic numbers
# with a higher density toward the extremities
listPoints := proc( c::[polynom,integer,name,polynom,integer],
                    r::range,
                    n::integer    := 10,
                    eps::constant := 0.001,
                    { tangent::truefalse := false } )
    local L,r1,r2,x,d,e,p,i,q,j,b1,b2,f,reps,lr,rr,di,g;
    #option cache;
    option remember;
    reps := convert(eps,rational,exact);
    di := -ilog10(eps);
    lr   := convert(lhs(r),rational,exact);
    rr   := convert(rhs(r),rational,exact);
    p,i,x,q,j := op(c);
    p := primpart(p);
    q := primpart(q);
    if nops(indets([p,q]))>1 then
        error("The input polynomials must be univariate.");
    end if;
    d := min(1,convert(rhs(r)-lhs(r),rational,exact));
    r1, r2 := convert(lhs(r),rational,exact), convert(rhs(r),rational,exact);
    # risks: r2 might be smaller than r1 if the 2 polynomial are too close
    if i<>0 then
        r1:=subs([isole (p,x,output=interval,digits=di)][1,i],
                 x)[2];
    end if;
    if j<>0 then
        r2:=subs([isole (q,x,output=interval,digits=di)][1,j],
                 x)[1];
    end if;
    if r2<=r1 then
        if i=0   then r1 := r2 - 2*reps*d;
        elif j=0 then r2 := r1 + 2*reps*d;
        elif r2<r1 then 
            r1:=subs([isole (p,x,output=interval,digits=di,
                             constraints=[q])][1,i], x)[2];
            r2:=subs([isole (q,x,output=interval,digits=di,
                             constraints=[p])][1,j], x)[1];
        end if;
    end if;
    e := min (reps*d, (r2-r1)/2);
    if r2>lr then b1 := max(r1+e,lr); else b1 := r1 + e end if;
    if r1<rr then b2 := min(r2-e,rr); else b2 := r2 - e end if;
#    b1 := r1+e;#convert(e,rational,exact);
#    b2 := r2-e;#convert(e,rational,exact);
# equidistant points    
#    return [ seq (b1+k*(b2-b1)/(n-1), k=0..n-1) ];

# higher density on the extremities (near singularities and asymptotes)
    f := x->x^2*(3-2*x);
    if tangent and b1<b2 then
        g := x->convert (evalf[-ilog(e)+1](tan(arctan(b1)+
                                           f(x)*(arctan(b2)-arctan(b1)))),
                         rational, exact);
        ASSERT (r1+e<=g(1/(n-1)));
        ASSERT (g((n-2)/(n-1))<=r2-e);
        for i from 1 to n-2 do
            ASSERT (g(i/(n-1))<g((i+1)/(n-1)));
        end do;
##        if i=0 then f := x->1-(1-x)^(ilog[2](b2-b1)); end if;
#        if i=0 then
#            f := x->piecewise(x>1/4,1-16/(b2-b1)*(1-x)^3*16/9,4*x-48*x/(b2-b1));
#        end if;
##        if j=0 then f := x->x^(ilog[2](b2-b1)); end if;
#        if j=0 then 
#            f := x->piecewise(x<3/4,16/(b2-b1)*x^3*16/9,4*x-3+48*(1-x)/(b2-b1));
#        end if;
#        return [ r1+e, seq (b1+f(k/(n-1))*(b2-b1),
#                            k=1..n-2), r2-e ];
        return [ r1+e, seq (g(k/(n-1)), k=1..n-2), r2-e ];
     else
        return [ r1+e, seq (b1+f(k/(n-1))*(b2-b1), k=1..n-2), r2-e ];
    end if;
end proc;


plotHorizontalLines := proc(cell::listlist,
                            ranges::seq(name=range),
                            n::integer    := 10,
                            eps::constant := 0.001,
                            angles::list := [false,false],
                            { nowall::truefalse := false,
                              left::truefalse :=false,
                              right::truefalse :=false,
                              downupcolors::list := ['black', 'black'] } )
    local x1,x2,r1,r2,L0,L1,L2,C1,C2,S,u,filter,W1,W2,downcolor,upcolor,T;

    x1,x2 := op(cell[..,3]);
    r1,r2 := op(subs (ranges, [x1,x2]));
    if select (type, [r1,r2], name) <> [] then
        error ("Range missing");
    end if;
    filter := (x,r)->evalb (x>=lhs(r) and x<=rhs(r));
    downcolor:=downupcolors[1];
    upcolor:=downupcolors[2]; 

    # extract the abscices of the first coordinate
    L0 := listPoints (cell[1],r1,n,eps,tangent=angles[1]);
    if select(filter,L0,r1)=[] then return emptyDisplay2d(); end if;

    #Computes first face
    L1 := map (u->[u,getBound (subs(x1=u,cell[2]),1,eps)], L0);
    if not has(L1,infinity) then
        C1 := plots:-pointplot(L1,connect,color=downcolor,_rest); 
    else 
        L1 := map(x->if x[2]=infinity then [x[1],rhs(r2)+1] 
                                      else [x[1],lhs(r2)-1] end if, L1);
        C1 := emptyDisplay2d();
    end if;

    #Computes second face
    L2 := map (u->[u,getBound (subs(x1=u,cell[2]),2,eps)], L0);
    if not has(L2,infinity) then
        C2 := plots:-pointplot(L2,connect,color=upcolor,_rest);
    else
        L2 := map(x->if x[2]=infinity then [x[1],rhs(r2)+1] 
                                      else [x[1],lhs(r2)-1] end if, L2);
        C2 := emptyDisplay2d();
    end if;
    
    S := plots:-polygonplot ([seq([L1[i],L1[i+1],L2[i+1],L2[i]],
                                  i=1..nops(L1)-1)],
                             style=patchnogrid,_rest);
    T := plots:-polygonplot ([seq([L1[i],L1[i+1],L2[i+1],L2[i]],
                                  i=2..nops(L1)-2)],
                             style=line,_rest);

    W1 := NULL; W2 := NULL;
    if not nowall or left then
        W1 := plottools:-line(L1[1],L2[1],color=black,_rest);
    elif not nowall or right then
        W2 := plottools:-line(L1[-1],L2[-1],color=black,_rest);
    end if;
    return plots:-display([C1,C2,W1,W2,S,T]);
end proc;


plotVerticalPlanes := proc(cell::listlist,
                           ranges::seq(name=range),
                           n::integer    := 10,
                           eps::constant := 0.001)
    local x1,x2,x3,r1,r2,r3,L0,L1,L2,F1,F2,u;

    x1,x2,x3 := op(cell[..,3]);
    r1,r2,r3 := op(subs (ranges, [x1,x2,x3]));
    if select (type, [r1,r2,r3], name) <> [] then
        error ("Range missing");
    end if;

    # extract the abscices of the 2 faces
    L0 := listPoints (cell[1],r1,2,eps);

    #Computes first face
    if cell[1,2]=0 or L0[1]<lhs(r1) or L0[1]>rhs(r1) then
        F1 := emptyDisplay3d();
    else
        u := L0[1];
        L1 := listPoints (subs(x1=u,cell[2]),r2,n,eps);
        L1 := map ( v->map (w->[u,v,w],
                                listPoints (subs(x1=u,x2=v,cell[3]),r3,2,eps)),
                    L1 );
        F1 := plots:-surfdata (L1,_rest);
    end if;
    
    #Computes second face
    if cell[1,5]=0 or L0[2]<lhs(r1) or L0[2]>rhs(r1) then
        F2 := emptyDisplay3d();
    else
        u := L0[2];
        L2 := listPoints (subs(x1=u,cell[2]),r2,n,eps);
        L2 := map ( v->map (w->[u,v,w],
                                listPoints (subs(x1=u,x2=v,cell[3]),r3,2,eps)),
                    L2 );
        F2 := plots:-surfdata (L2,_rest);
    end if;
    
    return plots:-display([F1,F2]);
end proc;

plotVerticalFaces := proc(cell::listlist,
                          ranges::seq(name=range),
                          n::integer    := 10,
                          eps::constant := 0.001)
    local x1,x2,x3,r1,r2,r3,L0,L1,F1,L2,F2,filter;

    x1,x2,x3 := op(cell[..,3]);
    r1,r2,r3 := op(subs (ranges, [x1,x2,x3]));
    if select (type, [r1,r2,r3], name) <> [] then
        error ("Range missing");
    end if;
    filter := (x,r)->evalb (x>=lhs(r) and x<=rhs(r));

    # extract the abscices of the first coordinate
    L0 := listPoints (cell[1],r1,n,eps);
    if select(filter,L0,r1)=[] then return emptyDisplay3d(); end if;

    #Computes first face
    if cell[2,2]=0 then
        F1 := emptyDisplay3d();
    else
        L1 := map (u->[u,getBound (subs(x1=u,cell[2]),1,eps)], L0);
        if select (c->filter(c[1],r1) and filter (c[2],r2), L1)=[] then
            F1 := emptyDisplay3d();
        else
            L1 := map (c->map (w-> [c[1],c[2],w],
                               listPoints(subs(x1=c[1],x2=c[2], cell[3]),
                                          r3,2,eps)),
                       L1);
            F1 := plots:-surfdata (L1,_rest);
        end if;
    end if;

    #Computes second face
    if cell[2,5]=0 then
        F2 := emptyDisplay3d();
    else
        L2 := map (u->[u,getBound (subs(x1=u,cell[2]),2,eps)], L0);
        if select (c->filter(c[1],r1) and filter (c[2],r2), L2)=[] then
            F2 := emptyDisplay3d();
        else
            L2 := map (c->map (w-> [c[1],c[2],w],
                               listPoints(subs(x1=c[1],x2=c[2], cell[3]),
                                          r3,2,eps)),
                       L2);
            F2 := plots:-surfdata (L2,_rest);
        end if;
    end if;
    
    return plots:-display([F1,F2]);
end proc;

plotHorizontalLeaves := proc(cell::listlist,
                             ranges::seq(name=range),
                             n::integer    := 10,
                             eps::constant := 0.001,
			     {angles::list(truefalse) := [false$3]})
    local x1,x2,x3,r1,r2,r3,L0,L1,L2,F1,F2,filter;

#    printf ( " ---> Cells computation\n");
    x1,x2,x3 := op(cell[..,3]);
    r1,r2,r3 := op(subs (ranges, [x1,x2,x3]));
    if select (type, [r1,r2,r3], name) <> [] then
        error ("Range missing");
    end if;
    filter := (x,r)->evalb (x>=lhs(r) and x<=rhs(r));
#    filter := (x,r)->true;

    # extract the abscices of the first and second coordinates
    L0 := listPoints (cell[1],r1,n,eps,tangent=angles[1]);
    if select(filter,L0,r1)=[] then return emptyDisplay3d(); end if;

    L0 := map (u-> map( v->[u,v],
                        listPoints (subs (x1=u, cell[2]),r2,n,eps,
			                  tangent=angles[2])),
               L0);
    if map2 (select, c->filter(c[1],r1) and filter (c[2],r2), L0)=[[]$nops(L0)]
    then
        return emptyDisplay3d();
    end if;
#    if map2 (select, c->c[1]>-0.02 and c[1]<0 and c[2]>2, L0)<>[[]$nops(L0)]
#    then
#        printf(" --> we're close\n");
#    end if;


    #Computes first face
    if cell[3,2]=0 then
        F1 := emptyDisplay3d();
    else
        L1 := map2 (map,
                    c->[c[1],
                        c[2],
                        getBound(subs(x1=c[1],x2=c[2],cell[3]),1,eps)],
                    L0);
        F1 := plots:-surfdata (L1,_rest);
    end if;

    #Computes second face
    if cell[3,5]=0 then
        F2 := emptyDisplay3d();
    else
        L2 := map2 (map,
                    c->[c[1],
                        c[2],
                        getBound(subs(x1=c[1],x2=c[2],cell[3]),2,eps)],
                    L0);
        F2 := plots:-surfdata (L2,_rest);
    end if;

    return plots:-display([F1,F2]);
end proc;

# Function: PlotCell3D
#   Plot the cells returned *CellDecomposition* or *CellDecompositionPlus*
#
# Parameters:
#   cells - an object returned by *CellDecomposition* or *CellDecompositionPlus*
#   i           (optional) - an integer or a list of integer:
#                            the indices of the cells to plot in *cells*.
#                            default value: all the cells
#   ranges      (optional) - a sequence of the form *v=n1..n2* where
#                            *v* is a variable and *n1,n2* are numerical values;
#                            when not specified for a variable,
#                            the default range is *-5..5*.
#   grid = i    (optional) - *i* is an integer leading to a grid size *i* x *i*;
#                            default value: 10.
#   border = e  (optional) - *e* is a numeric value: defines the precision
#                            on the border;
#                            default value: 0.0000001.
#   nowall = b  (optional) - *b* is a boolean; when *b* is true the vertical
#                                              faces are not displayed
#   color = col (optional) - equation of the shape color=*col*, where col is
#                            a color or a list of colors; when the number of
#                            specified color is not enough, deterministic
#                            colors are chosen;
#                            default value: empty list.
#   noverbose = b         (optional) - *b* is a boolean: if true, no verbose
#                                      is displayed;
#                                      default value: false
#   verboserate = i       (optional) - *i* is an integer: if positive (resp.
#                                      negative) the number (resp. the
#                                      pourcentage) of cells
#                                      computed before displaying a new verbose
#                                      line;
#                                      default value: -10
# Returns:
#   A 3D plot of the cells indiced by *i* in *cells*.
#
# Example:
# > > cells := CellDecompositionPlus ([T],[25-(x^2+y^2+z^2),x+y],[T],[x,y,z]):
# > > PlotCell3D (cells,[11,15],color=blue);
#
# (see cell.gif)
#
PlotCell3D := proc (cells::record,
                    i::{list(integer),integer} := [ $ 1 .. 
                                                    nops(cells:-SamplePoints) ],
                    ranges::seq(name=range) := NULL,
                    {nowall::truefalse := false,
                     grid::integer := 10,
                     border::constant := 0.0000001,
                     color := [],
                     noverbose::truefalse := false,
                     verboserate::integer := -10})
    local lcell, lind, k, disp, lranges, drange, r, c, l, j, opts,
          names, trans, cellangles, oranges, verb, vr;
    
    verb := `if`(noverbose,x->(),printf);
    vr := `if`(verboserate<0,iquo(nops(i)*(-verboserate),100)+1,verboserate);
    opts := axes=boxed;

    drange := -5..5;
    lind := `if`(type([i],list(list)),i,[i]);
    
    # color handling
    randomize (125362);
    r := rand (128..255);
    if type(color,list) then c := color else c := [color $ nops(lind)]; end if;


    l := table ( zip ((k,co)->k=(':-color'=co), lind,
                                            [ op(c), 
                                              seq (RGB(r()/255,r()/255,r()/255),
                                                   j=1..nops(lind)) ]) );

    if nops(lind)=0 then return emptyDisplay3d();
    elif nops(lind)>1 then
        verb (" -> Plotting %a cells ...\n", nops(lind));
        disp:=plots:-display( [seq(op([`if`(k mod vr=0, verb (" ---> %d%%\n",
                                                   iquo(100*k,nops(lind))),
                                           verb ("")),
                                        PlotCell3D(cells,lind[k],
                                                   ranges, l[lind[k]],
                                               ':-nowall'=nowall,
                                               ':-border'=border,
                                               ':-grid'=grid,_rest)]),
                                    k=1..nops(lind))]);
        if (nops(lind) mod vr)<>0 then verb (" ---> 100%%\n") end if;;
        return disp;
    end if;
    
    k := lind[1];
#    printf (" -> Cell number %a ...\n", k);
    lcell := CellDescriptionPlus(cells,k);
    names := AlgebraicTools:-AlgebraicVariables (lcell[..,3],
                                                 cells:-TableVariables);
    
    lranges := subs ([ranges],names);
    lranges := map(r-> if type(r,name) then drange else r end if, lranges);
    oranges := lranges;
    if member (LoopAngles, [exports(cells)]) then
        lranges := zip((f,x)->f(lhs(x),border)..f(rhs(x),border),
                       cells:-AngleInvTransform, lranges);
	cellangles := cells:-Angles;
    else
        cellangles := [ false$3 ];
    end if;
    lranges := zip ((x,r)->x=r, lcell[..,3], lranges);
    lranges := op(lranges);

    if nowall then
        disp := plotHorizontalLeaves (lcell, lranges, grid, border,
	                              angles=cellangles, _rest);
    else
        disp := plotVerticalPlanes (lcell, lranges, grid, border, _rest),
                plotVerticalFaces (lcell, lranges, grid, border, _rest),
                plotHorizontalLeaves (lcell, lranges, grid, border,
		                      angles=cellangles, _rest);
    end if;
    if member (LoopAngles, [exports(cells)]) then
        trans := cells:-AngleTransform;
    else
        trans := unapply (names, names);
    end if;

    disp := plots:-display ([disp]);

    return plots:-display (plottools:-transform (trans) (disp), l[k],
                                                       labels=names,
                                                       view=oranges,
                                                       opts);
end proc;

# Function: PlotCell2D
#   Plot the cells returned *CellDecomposition* or *CellDecompositionPlus*
#
# Parameters:
#   cells - an object returned by *CellDecomposition* or *CellDecompositionPlus*
#   i           (optional) - an integer, a list of integer or 
#                            a list of list of integer:
#                            the indices of the cells to plot in *cells*.
#                            default value: all the cells
#   ranges      (optional) - a sequence of the form *v=n1..n2* where
#                            *v* is a variable and *n1,n2* are numerical values;
#                            when not specified for a variable,
#                            the default range is *-5..5*.
#   grid = i    (optional) - *i* is an integer leading to a grid size *i* x *i*;
#                            default value: 10.
#   border = e  (optional) - *e* is a numeric value: defines the precision
#                            on the border;
#                            default value: 0.0000001.
#   nowall = b  (optional) - *b* is a boolean: when *b* is true the vertical
#                                              faces are not displayed;
#                            default value: false.
#   color = col (optional) - equation of the shape color=*col*, where col is
#                            a color or a list of colors; when the number of
#                            specified color is not enough, deterministic
#                            different colors are chosen;
#                            default value: empty list.
#   samplepoints = b     (optional) - *b* is a boolean; if true, the labels of 
#                                     the cells are displayed;
#                                     default value: false.
#   downupcolors = ducol (optional) - list stating the colors of the lower and
#                                     upper borders of each cell in the
#                                     follwing form:
#                                     [[downcol_cell1, upcol_cell1],
#                                      [downcol_cell2, upcol_cell2],...]
#                                     default value: empty list.
#   points = pts         (optional) - list of coordinates of points to be plot
#                                     of the form [[x1,y1], [x2,y2], ...]
#                                     default value: empty list.
#   colorpoints = colpts (optional) - list of colors for the points specified 
#                                     in "points=pts" of the form
#                                     [col_pt1, col_pt2, ...]
#                                     default value: empty list.
#   noverbose = b         (optional) - *b* is a boolean: if true, no verbose
#                                      is displayed;
#                                      default value: false
#   verboserate = i       (optional) - *i* is an integer: if positive (resp.
#                                      negative) the number (resp. the
#                                      pourcentage) of cells
#                                      computed before displaying a new verbose
#                                      line;
#                                      default value: -10
#                              
# Returns:
#   A 2D plot of the cells indiced by *i* in *cells*.
#
# Example:
# > > cells := CellDecompositionPlus ([T],[25-(x^2+y^2),x+y],[T],[x,y]):
# > > PlotCell2D (cells,samplepoints);
#
# (see cell2d.gif)
#
PlotCell2D := proc (cells::record,
                    i::{list(list(integer)),list(integer),integer} :=
                                           [ $ 1 ..  nops(cells:-SamplePoints) ],
                    ranges::seq(name=range) := NULL,
                    {samplepoints::truefalse := false,
                     nowall::truefalse := false,
                     grid::integer := 10,
                     border::constant := 0.0000001,
                     color := [],
                     downupcolors::list :=[],
                     points::list :=[],
                     colorpoints::list :=[],
                     noverbose::truefalse := false,
                     verboserate::integer := -10})
    local lcell, lind, k, disp, lranges, drange, r, c, l, j, opts, vars,
          nranges, angles, tablevars, trans, invtrans, left, right, duc, ldu,
          pts, colpts, disppt, d, pa, samplept, samples, x, e, verb, vr;
    
    verb := `if`(noverbose,x->(),printf);
    vr := `if`(verboserate<0,iquo(nops(i)*(-verboserate),100)+1,verboserate);
    opts := axes=boxed;

    drange := -5..5;
    lind := `if`(type(i,list),i,[i]);
    
    # color handling
    randomize (125362);
    r := rand (128..255);
    if type(color,list) then c := color else c := [color $ nops(lind)]; end if;

    if not type(downupcolors,list(list)) then
        duc:=[downupcolors $ nops(lind)];
    elif nops(downupcolors)=nops(lind) then
        duc:=downupcolors
    else
        duc:=[[black,black] $ nops(lind)];
    end if;

    pts:=points; 
    if nops(points)=nops(colorpoints) then
        colpts:=colorpoints; 
    else 
        colpts:=[black $ nops(pts)]; 
    end if;

    l := table ( zip ((k,co)->k=(':-color'=co), lind,
                                            [ op(c), 
                                              seq (RGB(r()/255,r()/255,r()/255),
                                                   j=1..nops(lind)) ]) );
    ldu := table ( zip ((k,du)->k=du, lind, [ op(duc), 
                                              seq (RGB(r()/255,r()/255,r()/255),
                                                   j=1..nops(lind)) ]) );


    if nops(lind)=0 then return emptyDisplay2d();
    elif type(lind,list(list)) then
        verb (" -> Plotting %a regions (%a cells) ...\n",nops(lind),
                                                           add(nops(k),k=lind));
        d := plots:-display(
                [seq(op([`if`(k mod vr=0, verb (" ---> %d%%\n",
                                                   iquo(100*k,nops(lind))),
                                           verb ("")),
                          PlotCell2D(cells,lind[k],ranges,l[lind[k]],
                                    ':-nowall'=true,
                                    ':-samplepoints'=samplepoints,
                                    ':-border'=border,
                                    ':-grid'=grid,
                                    ':-downupcolors'=ldu[lind[k]],
                                    ':-points'=pts,
                                    ':-colorpoints'=colpts,
                                    ':-noverbose'=noverbose,
                                    _rest)
                        ]),
                     k in [$1..nops(lind)])]);
        if (nops(lind) mod vr)<>0 then verb (" ---> 100%%\n") end if;;
        return d;

    elif nops(lind)>1 then
        verb (" -> Plotting %a cells ...\n", nops(lind));
        d := plots:-display(
                [seq(op([`if`(k mod vr=0, verb (" ---> %d%%\n",
                                                   iquo(100*k,nops(lind))),
                                           verb ("")),
                          PlotCell2D(cells,lind[k],ranges,l[lind[k]],
                                    ':-samplepoints'=samplepoints,
                                    ':-nowall'=nowall,
                                    ':-border'=border,
                                    ':-grid'=grid,
                                    ':-downupcolors'=[ldu[lind[k]]],
                                    ':-points'=pts,
                                    ':-colorpoints'=colpts,
                                    ':-noverbose'=noverbose,
                                    _rest)
                        ]),
                     k in [$1..nops(lind)])]);
        if (nops(lind) mod vr)<>0 then verb (" ---> 100%%\n") end if;;
        return d;
    end if;
    
    k := lind[1];
#    if type(i,integer) then
#        printf (" -> Cell number %a ...\n", k);
#    end if;
    lcell := CellDescriptionPlus(cells,k);
    pa := ListTools:-Reverse (cells:-Parameters);
    
    if member (':-TableVariables',[exports(cells)]) then
        tablevars := cells:-TableVariables;
        invtrans :=  cells:-AngleInvTransform;
        trans :=  cells:-AngleTransform;
    else
        tablevars := table();
        invtrans := [seq ((x,e)->x,v in pa)] ;
        trans := unapply(pa, pa);
    end if;

    vars := AlgebraicTools:-AlgebraicVariables (lcell[..,3], tablevars);
    lranges := subs ([ranges],vars);
    lranges := zip ((x,r)->if r=x then x=drange else x=r end if,
                    vars, lranges);
    nranges := zip (`=`,lcell[..,3],map(rhs,lranges));
    nranges := zip ((f,x)->lhs(x)=f(lhs(rhs(x)),border)..f(rhs(rhs(x)),border),
                    invtrans, nranges);
    nranges := op(nranges);

    angles := map (x->assigned (tablevars[x]), vars);
    left := false; right := false;
    if   member ([lcell[1,1]], cells:-DiscriminantVariety) then left := true
    elif member ([lcell[1,5]], cells:-DiscriminantVariety) then right := true
    end if;
    disp := plotHorizontalLines (lcell, nranges, grid, border,
                                  ':-nowall'=nowall, ':-left'=left,
                                  ':-right'=right, ':-downupcolors'=ldu[k],
                                  angles, _rest);
    if samplepoints then
        samplept := subs(cells:-SamplePoints[k],pa);
        samples := plots:-display (plots:-pointplot(samplept,symbol=cross,
                                                           ':-color'=':-black'),
                                   plots:-textplot([op(samplept),k],
                                                   align={':-above',':-right'},
                                                   ':-color'=':-black'));
    else
        samples := emptyDisplay2d();
    end if;
    disp := plottools:-transform ( trans )(disp);
    samples := plottools:-transform ( trans )(samples);
    if nops(points)>0 then
        disppt := plots:-pointplot(pts, ':-color'= colpts, symbol = solidcircle,
                                                           symbolsize = 12);
    else
        disppt := emptyDisplay2d();
    end if;

    return plots:-display ([disp,disppt,samples], l[k], labels=vars,
                                                  view=map(rhs,lranges),
                                                  opts);
end proc;


# Function: SetCellColors
#   Set colors to the numbers of solutions obtained by *NumberOfSolutionsPlus*
#
# Parameters:
#   Lc - list returned by *NumberOfSolutionsPlus*
#   Lp - list returned by *DVNumberOfSolutionsPlus*
#                             
# Returns:
#   a list of lists *C* ready to be used by *PlotCell2D*: 
#            C[1] -  contains the numbers of the cells to be plotted
#            C[2] -  contains the inner colors for the cells
#            C[3] -  contains the colors for the lower and upper borders of the cells
#            C[4] -  contains the coordinates of the points to be plotted
#            C[5] -  contains the colors for the points to be plotted.
#
SetCellColors := proc (Lc, Lp)
  local cuspcolors, listcell, incolor, downupcol,u, dcolor, ucolor, listpoint, pointcol; 
  
  cuspcolors := table([0 = red, 1 = cyan, 2 = blue, 3 = black, 4 = yellow, 5 = khaki, 6 = green, 7=gray, 8=orange, 9=brown, 10=violet]);
  listcell := []; 
  incolor := []; 
  downupcol := []; 
  for u in Lc do 
      listcell := [op(listcell), u[1]]; 
      incolor := [op(incolor), cuspcolors[u[2]]]; 
      dcolor := cuspcolors[u[3]]; 
      ucolor := cuspcolors[u[4]]; 
      downupcol := [op(downupcol), [dcolor, ucolor]]; 
  end do;
  
  listpoint := [];
  pointcol := [];
  if nops(Lp)>0 then
     for u in Lp do
        listpoint := [op(listpoint), u[1]];
        pointcol := [op(pointcol), cuspcolors[u[2]]]; 
     end do;
  end if;
  return [listcell, incolor, downupcol, listpoint, pointcol];
end proc;

#Function: Path
#   Interpolate points linearly.
#
# Parameters:
#   L     - list of points.
#   steps - integer: the number of interpolated points.
#                             
# Returns:
#   A list of list of *steps* points; the linear interpolation
#   between the input points of *L*.
Path := proc (L::list,
              steps::integer := 10)
    return [seq ([seq (L[i] + j/steps*(L[i+1]-L[i]), j=0..steps)],
                 i=1..nops(L)-1)];
end proc;

# Function: Trajectory
#   Display a given trajectory.
#
# Parameters:
#   L     - list or list of list of points in the plane or 3d space.
#   steps - integer: the number of interpolated points.
#   color (optional) - a color name or a list of color names;
#                      default value: [].
#                             
# Returns:
#   A plot of the given trajectory:
#       - if the input is a list of list of points, then the i-th list is a path
#         colorized with the i-th element of *color* (or with *color* directly
#         if it is a color name). If there is no i-th color, a random color is
#         chosen.
#       - if the input is a list of points, then a list of list of points is
#         created by computing *steps* points between 2 consecutive points.
#         Then this list of list is displayed.
#
# Example:
# (begin code)
# > Trajectory ( [[0,0],[1,1],[-1,1]],     # the list of vertices
#                color=[blue,red] );       # colors of the edges
# (end code)
# (see Trajectory.gif)
Trajectory := proc (L::list,
                    steps::integer := 20,
                    { color := [] })

    local ll,lc,c,r,pplot;
    if type(L, list(list(constant))) then
        ll := Path (L,steps);
    else
        ll := L;
    end if;

    ll := remove (x->nops(x)=0,ll);

    randomize (125362);
    r := rand (128..255);
    if type(color,list) then c := color else c := [color $ nops(ll)]; end if;
    lc := [ op(c), seq (RGB(r()/255,r()/255,r()/255), j=1..nops(ll)-nops(c)) ];

    if nops(ll)=0 then return emptyDisplay2d();
    elif nops(ll[1,1])=2 then pplot := plots:-pointplot;
    elif nops(ll[1,1])=3 then pplot := plots:-pointplot3d;
    else error ("The first argument must contains points of dimension 2 or 3");
    end if;

    return plots:-display ( [ seq (pplot (ll[i], ':-color'=lc[i]),
                                   i = 1..nops(ll)) ], _rest );
end proc;


# Function: ImageTrajectory
#   Display a given trajectory.
#
# Parameters:
#   robot - the manipulator under study
#   L     - list or list of list of points; a point is a list of numerical 
#           values, coordinates of the variables *pars*;
#   pars  - list of name; default value: robot[ArticularVariables].
#   vars  - list of name: the image space, a list of 2 or 3 variables;
#                         default value: robot[PoseVariables].
#   steps - integer: the number of interpolated points.
#   color (optional) - a color name or a list of color names;
#                      default value: [].
#   loop  (optional) - a boolean: if true and *L* is a list of vertices,
#                      then the last point is connected to the first one.
#                             
# Returns:
#   A plot of the given trajectory:
#       - if the input is a list of list of points, then the i-th list is a path
#         colorized with the i-th element of *color* (or with *color* directly
#         if it is a color name). If there is no i-th color, a random color is
#         chosen.
#       - if the input is a list of points, then a list of list of points is
#         created by computing *steps* points between 2 consecutive points.
#         Then this list of list is displayed.
#
# Example:
# (begin code)
# > robot := ParallelRPR2PRR();
# > ImageTrajectory (robot,                       # the manipulator
#                    [[0,0,0],[1,1,1],[-1,1,1]],  # the list of vertices
#                    [S1,alpha2,alpha3],          # the input variables
#                    [phi,x, y],                  # the output variables
#                    50,                          # number of inbetween points
#                    color=[blue,red],            # colors of the edges
#                    symbol=soliddiamond);        # additional plot options
# (end code)
# (see ImageTrajectory.gif)
ImageTrajectory := proc (robot::Manipulator,
                         L::list,
                         pars::list(name) := robot:-ArticularVariables,
                         vars::list(name) := robot:-PoseVariables,
                         steps::integer := 20,
                         { color := [],
                           loop::truefalse := false })

    local LL, ll,lc,c,r, pplot, ConfNoError, lo, tplot, eplot,
          startpoints, finalpoints, middlepoints, Lstart, Lfinal, Lmiddle;

    if type(L, list(list(constant))) then
        if loop then LL := [op(L),L[1]]; else LL := L; end if;
        ll := Path (LL,steps);
    else
        ll := L;
    end if;

    ConfNoError := proc() try Configurations (args, ordering=vars[1])
                          catch: []
                          end try;
                   end proc;
    
    lo := [seq ([seq (op (map (subs,
                               ConfNoError (robot, op(zip(`=`,pars,ll[i,j]))) ,
                               vars)),
                  j=1..nops(ll[i]))],
            i=1..nops(ll))];

    randomize (125362);
    r := rand (128..255);
    if type(color,list) then c := color else c := [color $ nops(ll)]; end if;
    lc := [ op(c), seq (RGB(r()/255,r()/255,r()/255), j=1..nops(ll)-nops(c)) ];

    if nops(vars)=2   then eplot := emptyDisplay2d;
                           tplot := proc() if [args][1]=[] then emptyDisplay2d()
                                           else plots:-textplot(args) end if;
                                    end proc;
                           pplot := proc() if [args][1]=[] then emptyDisplay2d()
                                           else plots:-pointplot(args) end if;
                                    end proc;
    elif nops(vars)=3 then eplot := emptyDisplay3d;
                           tplot := proc() if [args][1]=[] then emptyDisplay3d()
                                           else plots:-textplot3d(args) end if;
                                    end proc;
                           pplot := proc() if [args][1]=[] then emptyDisplay3d()
                                           else plots:-pointplot3d(args) end if;
                                    end proc;
    else error ("The number of image variables must be 2 or 3");
    end if;

    Lstart := ConfNoError (robot, op(zip(`=`,pars,ll[1,1])));
    startpoints := tplot ([seq ([op(subs(Lstart[i],vars)), cat("Si")],
                                 i = 1..nops(Lstart)) ],
                           align={below,left},
                           font=[TIMES,BOLDITALIC,15],
                           ':-color'='black'),
                   pplot ([seq (subs(Lstart[i],vars),
                                i = 1..nops(Lstart)) ],
                           ':-color'='black',
                           symbol=solidbox,
                           symbolsize=20);

    middlepoints := NULL;
    if type(ll, list(list(constant))) and nops(ll)>=2 then
        Lmiddle := map (x->ConfNoError (robot, op(zip(`=`,pars,x))),
                        ll[2..,1]);
        middlepoints := seq ( op([tplot ([seq ([op(subs(Lmiddle[i,j],vars)),
                                                cat("V[",i,"]")],
                                               j = 1..nops(Lmiddle[i])) ],
                                         align={below,left},
                                         font=[TIMES,BOLDITALIC,15],
                                         ':-color'='black'),
                                  pplot ([seq (subs(Lmiddle[i,j],vars),
                                              j = 1..nops(Lmiddle[i])) ],
                                         ':-color'='black',
                                         symbol=solidbox,
                                         symbolsize=20)]),
                              i=1..nops(Lmiddle));
    end if;

    if loop or
       convert(ll[1,1],rational,exact)=convert(ll[-1,-1],rational,exact) then
       finalpoints := eplot();
    else
        Lfinal := ConfNoError (robot, op(zip(`=`,pars,ll[-1,-1])));
        finalpoints := tplot ([seq ([op(subs(Lfinal[i],vars)),cat("Sf")],
                               i = 1..nops(Lfinal)) ],
                               align={below,left},
                               font=[TIMES,BOLDITALIC,15],
                               ':-color'='black',
                               symbol=solidcircle,
                               symbolsize=20),
                       pplot ([seq (subs(Lfinal[i],vars),
                                    i = 1..nops(Lfinal)) ],
                               ':-color'='black',
                               symbol=solidcircle,
                               symbolsize=20);
    end if;

    return plots:-display ( [ seq (pplot (lo[i], ':-color'=lc[i]),
                                   i = 1..nops(lo)),
                              startpoints,
                              middlepoints,
                              finalpoints ],
                            'labels'=vars, _rest );
end proc;


